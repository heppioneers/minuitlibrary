/* mnread.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;


/* $Id: mnread.F,v 1.1.1.1 1996/03/07 14:31:31 mclareni Exp $ */

/* $Log: mnread.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnread_(fcn, iflgin, iflgut, futil)
/* Subroutine */ int (*fcn) ();
integer *iflgin, *iflgut;
/* Subroutine */ int (*futil) ();
{
    /* Initialized data */

    static char cpromt[40*3+1] = " ENTER MINUIT TITLE, or \"SET INPUT n\" : \
 ENTER MINUIT PARAMETER DEFINITION:      ENTER MINUIT COMMAND:              \
    ";
    static char clower[26+1] = "abcdefghijklmnopqrstuvwxyz";
    static char cupper[26+1] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /* Format strings */
    static char fmt_21[] = "(\002 **********\002/\002 **\002,i5,\002 **\002,\
a/\002 **********\002)";
    static char fmt_405[] = "(\002 \002,10(\002*\002)/\002 **\002,i5,\002 *\
*\002,a)";
    static char fmt_420[] = "(bn,7e11.4,3x)";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();
    /* Subroutine */ int s_copy();
    integer s_rsfe(), e_rsfe(), i_indx();

    /* Local variables */
    static logical leof;
    static integer ierr, npar2, i, ic;
    static char crdbuf[80], cupbuf[10];
    static integer iflgdo, incomp;
    extern /* Subroutine */ int mnstin_(), mnseti_(), mnpars_();
    static integer icondp;
    extern /* Subroutine */ int mncomd_();
    static integer icondn;
    extern /* Subroutine */ int mnmatu_(), mnprin_();

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 0, 0, "(A)", 0 };
    static cilist io___9 = { 1, 0, 1, "(A)", 0 };
    static cilist io___13 = { 0, 0, 0, "(A,I3)", 0 };
    static cilist io___14 = { 0, 0, 0, fmt_21, 0 };
    static cilist io___15 = { 0, 0, 0, "(A,I3)", 0 };
    static cilist io___17 = { 0, 0, 0, "(A,A/)", 0 };
    static cilist io___18 = { 0, 0, 0, "(1X,A50)", 0 };
    static cilist io___19 = { 0, 0, 0, "(1X,78(1H*))", 0 };
    static cilist io___21 = { 0, 0, 0, "(A)", 0 };
    static cilist io___22 = { 0, 0, 0, "(A)", 0 };
    static cilist io___23 = { 0, 0, 0, "(4X,75(1H*))", 0 };
    static cilist io___25 = { 0, 0, 0, fmt_405, 0 };
    static cilist io___26 = { 0, 0, 0, "(1H ,10(1H*))", 0 };
    static cilist io___28 = { 1, 0, 1, fmt_420, 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Called from MINUIT.  Reads all user input to MINUIT. */
/* C     This routine is highly unstructured and defies normal logic. */
/* C */
/* C     IFLGIN indicates the function originally requested: */
/* C           = 1: read one-line title */
/* C             2: read parameter definitions */
/* C             3: read MINUIT commands */
/* C */
/* C     IFLGUT= 1: reading terminated normally */
/* C             2: end-of-data on input */
/* C             3: unrecoverable read error */
/* C             4: unable to process parameter requests */
/* C             5: more than 100 incomprehensible commands */
/* C internally, */
/* C     IFLGDO indicates the subfunction to be performed on the next */
/* C         input record: 1: read a one-line title */
/* C                       2: read a parameter definition */
/* C                       3: read a command */
/* C                       4: read in covariance matrix */
/* C     for example, when IFLGIN=3, but IFLGDO=1, then it should read */
/* C       a title, but this was requested by a command, not by MINUIT. */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */




    *iflgut = 1;
    iflgdo = *iflgin;
    leof = FALSE_;
    incomp = 0;
/*                                           . . . . read next record */
L10:
    if (mn7flg_1.isw[5] == 1) {
	io___7.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___7);
	do_fio(&c__1, cpromt + (iflgdo - 1) * 40, 40L);
	e_wsfe();
	if (iflgdo == 2) {
	    mn7log_1.lphead = FALSE_;
	}
    }
    s_copy(crdbuf, "   ", 80L, 3L);
    io___9.ciunit = mn7iou_1.isysrd;
    i__1 = s_rsfe(&io___9);
    if (i__1 != 0) {
	goto L100001;
    }
    i__1 = do_fio(&c__1, crdbuf, 80L);
    if (i__1 != 0) {
	goto L100001;
    }
    i__1 = e_rsfe();
L100001:
    if (i__1 < 0) {
	goto L45;
    }
    if (i__1 > 0) {
	goto L500;
    }

/*                 CUPBUF is the first few characters in upper case */
    s_copy(cupbuf, crdbuf, 10L, 10L);
    for (i = 1; i <= 10; ++i) {
	if (*(unsigned char *)&crdbuf[i - 1] == '\'') {
	    goto L13;
	}
	for (ic = 1; ic <= 26; ++ic) {
	    if (*(unsigned char *)&crdbuf[i - 1] == *(unsigned char *)&clower[
		    ic - 1]) {
		*(unsigned char *)&cupbuf[i - 1] = *(unsigned char *)&cupper[
			ic - 1];
	    }
/* L11: */
	}
/* L12: */
    }
L13:
/*                                           . .   preemptive commands */
    leof = FALSE_;
    if (i_indx(cupbuf, "*EOF", 10L, 4L) == 1) {
	io___13.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___13);
	do_fio(&c__1, " *EOF ENCOUNTERED ON UNIT NO.", 29L);
	do_fio(&c__1, (char *)&mn7iou_1.isysrd, (ftnlen)sizeof(integer));
	e_wsfe();
	mn7log_1.lphead = TRUE_;
	goto L50;
    }
    if (i_indx(cupbuf, "SET INP", 10L, 7L) == 1) {
	++mn7flg_1.icomnd;
	io___14.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___14);
	do_fio(&c__1, (char *)&mn7flg_1.icomnd, (ftnlen)sizeof(integer));
	do_fio(&c__1, crdbuf, 50L);
	e_wsfe();
	mn7log_1.lphead = TRUE_;
	goto L50;
    }
    goto L80;
/*                                    . . hardware EOF on current ISYSRD 
*/
L45:
    s_copy(crdbuf, "*EOF ", 80L, 5L);
    io___15.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___15);
    do_fio(&c__1, " END OF DATA ON UNIT NO.", 24L);
    do_fio(&c__1, (char *)&mn7iou_1.isysrd, (ftnlen)sizeof(integer));
    e_wsfe();
/*                                     or SET INPUT command */
L50:
    mnstin_(crdbuf, &ierr, 80L);
    if (ierr == 0) {
	goto L10;
    }
    if (ierr == 2) {
	if (! leof) {
	    io___17.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___17);
	    do_fio(&c__1, " TWO CONSECUTIVE EOFs ON ", 25L);
	    do_fio(&c__1, "PRIMARY INPUT FILE WILL TERMINATE EXECUTION.", 44L)
		    ;
	    e_wsfe();
	    leof = TRUE_;
	    goto L10;
	}
    }
    *iflgut = ierr;
    goto L900;
L80:
    if (iflgdo > 1) {
	goto L100;
    }
/*                            read title        . . . . .   IFLGDO = 1 */
/*              if title is 'SET TITLE', skip and read again */
    if (i_indx(cupbuf, "SET TIT", 10L, 7L) == 1) {
	goto L10;
    }
    mnseti_(crdbuf, 50L);
    io___18.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___18);
    do_fio(&c__1, mn7tit_1.ctitl, 50L);
    e_wsfe();
    io___19.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___19);
    e_wsfe();
    mn7log_1.lphead = TRUE_;
    if (*iflgin == iflgdo) {
	goto L900;
    }
    iflgdo = *iflgin;
    goto L10;
/*                            data record is not a title. */
L100:
    if (iflgdo > 2) {
	goto L300;
    }
/*                          expect parameter definitions.   IFLGDO = 2 */
/*              if parameter def is 'PARAMETER', skip and read again */
    if (i_indx(cupbuf, "PAR", 10L, 3L) == 1) {
	goto L10;
    }
/*              if line starts with SET TITLE, read a title first */
    if (i_indx(cupbuf, "SET TIT", 10L, 7L) == 1) {
	iflgdo = 1;
	goto L10;
    }
/*                      we really have parameter definitions now */
    mnpars_(crdbuf, &icondp, 80L);
    if (icondp == 0) {
	goto L10;
    }
/*          format error */
    if (icondp == 1) {
	if (mn7flg_1.isw[5] == 1) {
	    io___21.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___21);
	    do_fio(&c__1, " FORMAT ERROR.  IGNORED.  ENTER AGAIN.", 38L);
	    e_wsfe();
	    goto L10;
	} else {
	    io___22.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___22);
	    do_fio(&c__1, " ERROR IN PARAMETER DEFINITION", 30L);
	    e_wsfe();
	    *iflgut = 4;
	    goto L900;
	}
    }
/*                     ICONDP = 2            . . . end parameter requests 
*/
    if (mn7flg_1.isw[4] >= 0 && mn7flg_1.isw[5] < 1) {
	io___23.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___23);
	e_wsfe();
    }
    mn7log_1.lphead = TRUE_;
    if (*iflgin == iflgdo) {
	goto L900;
    }
    iflgdo = *iflgin;
    goto L10;
/*                                              . . . . .   IFLGDO = 3 */
/*                                           read commands */
L300:
    mncomd_(fcn, crdbuf, &icondn, futil, 80L);
/* C     ICONDN = 0: command executed normally */
/* C              1: command is blank, ignored */
/* C              2: command line unreadable, ignored */
/* C              3: unknown command, ignored */
/* C              4: abnormal termination (e.g., MIGRAD not converged) */
/* C              5: command is a request to read PARAMETER definitions */
/* C              6: 'SET INPUT' command */
/* C              7: 'SET TITLE' command */
/* C              8: 'SET COVAR' command */
/* C              9: reserved */
/* C             10: END command */
/* C             11: EXIT or STOP command */
/* C             12: RETURN command */
    if (icondn == 2 || icondn == 3) {
	++incomp;
	if (incomp > 100) {
	    *iflgut = 5;
	    goto L900;
	}
    }
/*                         parameter */
    if (icondn == 5) {
	iflgdo = 2;
    }
/*                         SET INPUT */
    if (icondn == 6) {
	goto L50;
    }
/*                         SET TITLE */
    if (icondn == 7) {
	iflgdo = 1;
    }
/*                                        . . . . . . . . . . set covar */
    if (icondn == 8) {
	++mn7flg_1.icomnd;
	io___25.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___25);
	do_fio(&c__1, (char *)&mn7flg_1.icomnd, (ftnlen)sizeof(integer));
	do_fio(&c__1, crdbuf, 50L);
	e_wsfe();
	io___26.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___26);
	e_wsfe();
	npar2 = mn7npr_1.npar * (mn7npr_1.npar + 1) / 2;
	io___28.ciunit = mn7iou_1.isysrd;
	i__1 = s_rsfe(&io___28);
	if (i__1 != 0) {
	    goto L100002;
	}
	i__2 = npar2;
	for (i = 1; i <= i__2; ++i) {
	    i__1 = do_fio(&c__1, (char *)&mn7var_1.vhmat[i - 1], (ftnlen)
		    sizeof(doublereal));
	    if (i__1 != 0) {
		goto L100002;
	    }
	}
	i__1 = e_rsfe();
L100002:
	if (i__1 < 0) {
	    goto L45;
	}
	if (i__1 > 0) {
	    goto L500;
	}
	mn7flg_1.isw[1] = 3;
	mn7min_1.dcovar = (float)0.;
	if (mn7flg_1.isw[4] >= 0) {
	    mnmatu_(&c__1);
	}
	if (mn7flg_1.isw[4] >= 1) {
	    mnprin_(&c__2, &mn7min_1.amin);
	}
	goto L10;
    }
    if (icondn < 10) {
	goto L10;
    }
    goto L900;
/*                                              . . . . error conditions 
*/
L500:
    *iflgut = 3;
L900:
    return 0;
} /* mnread_ */

