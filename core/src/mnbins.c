/* mnbins.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static real c_b5 = (float)10.;


/* $Id: mnbins.F,v 1.1.1.1 1996/03/07 14:31:28 mclareni Exp $ */

/* $Log: mnbins.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:28  mclareni */
/* Minuit */


/* Subroutine */ int mnbins_(a1, a2, naa, bl, bh, nb, bwid)
doublereal *a1, *a2;
integer *naa;
doublereal *bl, *bh;
integer *nb;
doublereal *bwid;
{
    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    double d_lg10(), pow_ri();

    /* Local variables */
    static doublereal awid;
    static integer kwid, lwid;
    static doublereal ah, al;
    static integer na;
    static doublereal sigfig, sigrnd, alb;
    static integer log__;


/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/*         SUBROUTINE TO DETERMINE REASONABLE HISTOGRAM INTERVALS */
/*         GIVEN ABSOLUTE UPPER AND LOWER BOUNDS  A1 AND A2 */
/*         AND DESIRED MAXIMUM NUMBER OF BINS NAA */
/*         PROGRAM MAKES REASONABLE BINNING FROM BL TO BH OF WIDTH BWID */
/*         F. JAMES,   AUGUST, 1974 , stolen for Minuit, 1988 */
    al = min(*a1,*a2);
    ah = max(*a1,*a2);
    if (al == ah) {
	ah = al + (float)1.;
    }
/*         IF NAA .EQ. -1 , PROGRAM USES BWID INPUT FROM CALLING ROUTINE 
*/
    if (*naa == -1) {
	goto L150;
    }
L10:
    na = *naa - 1;
    if (na < 1) {
	na = 1;
    }
/*          GET NOMINAL BIN WIDTH IN EXPON FORM */
L20:
    awid = (ah - al) / (real) na;
    d__1 = awid;
    log__ = (integer) d_lg10(&d__1);
    if (awid <= 1.) {
	--log__;
    }
    i__1 = -log__;
    sigfig = awid * pow_ri(&c_b5, &i__1);
/*         ROUND MANTISSA UP TO 2, 2.5, 5, OR 10 */
    if (sigfig > (float)2.) {
	goto L40;
    }
    sigrnd = (float)2.;
    goto L100;
L40:
    if (sigfig > (float)2.5) {
	goto L50;
    }
    sigrnd = (float)2.5;
    goto L100;
L50:
    if (sigfig > (float)5.) {
	goto L60;
    }
    sigrnd = (float)5.;
    goto L100;
L60:
    sigrnd = (float)1.;
    ++log__;
L100:
    *bwid = sigrnd * pow_ri(&c_b5, &log__);
    goto L200;
/*         GET NEW BOUNDS FROM NEW WIDTH BWID */
L150:
    if (*bwid <= 0.) {
	goto L10;
    }
L200:
    alb = al / *bwid;
    lwid = (integer) alb;
    if (alb < 0.) {
	--lwid;
    }
    *bl = *bwid * (real) lwid;
    alb = ah / *bwid + (float)1.;
    kwid = (integer) alb;
    if (alb < 0.) {
	--kwid;
    }
    *bh = *bwid * (real) kwid;
    *nb = kwid - lwid;
    if (*naa > 5) {
	goto L240;
    }
    if (*naa == -1) {
	return 0;
    }
/*          REQUEST FOR ONE BIN IS DIFFICULT CASE */
    if (*naa > 1 || *nb == 1) {
	return 0;
    }
    *bwid *= (float)2.;
    *nb = 1;
    return 0;
L240:
    if (*nb << 1 != *naa) {
	return 0;
    }
    ++na;
    goto L20;
} /* mnbins_ */

