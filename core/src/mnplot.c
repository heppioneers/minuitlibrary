/* mnplot.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__1 = 1;


/* $Id: mnplot.F,v 1.1.1.1 1996/03/07 14:31:31 mclareni Exp $ */

/* $Log: mnplot.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnplot_(xpt, ypt, chpt, nxypt, nunit, npagwd, npagln, 
	chpt_len)
doublereal *xpt, *ypt;
char *chpt;
integer *nxypt, *nunit, *npagwd, *npagln;
ftnlen chpt_len;
{
    /* Initialized data */

    static char cdot[1+1] = ".";
    static char cslash[1+1] = "/";
    static char cblank[1+1] = " ";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();
    /* Subroutine */ int s_copy();

    /* Local variables */
    static integer iten;
    static doublereal xmin, ymin, xmax, ymax, savx, savy, yprt;
    static integer i, j, k;
    static char cline[100], chsav[1];
    static doublereal bwidx, bwidy, xbest, ybest;
    static integer maxnx, maxny, iquit, ni;
    static doublereal ax, ay, bx, by;
    static integer ks, ix, nx, ny;
    static char chbest[1];
    static integer linodd;
    static char chmess[30];
    extern /* Subroutine */ int mnbins_();
    static integer nxbest, nybest, km1;
    static logical overpr;
    static doublereal xvalus[12];
    static integer ibk;
    static doublereal any, dxx, dyy;
    static integer isp1;

    /* Fortran I/O blocks */
    static cilist io___43 = { 0, 0, 0, "(18X,A)", 0 };
    static cilist io___44 = { 0, 0, 0, "(1X,G14.7,A,A)", 0 };
    static cilist io___45 = { 0, 0, 0, "(18X,A)", 0 };
    static cilist io___48 = { 0, 0, 0, "(12X,12G10.4)", 0 };
    static cilist io___50 = { 0, 0, 0, "(25X,A,G13.7,A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        plots points in array xypt onto one page with labelled axes */
/* C        NXYPT is the number of points to be plotted */
/* C        XPT(I) = x-coord. of ith point */
/* C        YPT(I) = y-coord. of ith point */
/* C        CHPT(I) = character to be plotted at this position */
/* C        the input point arrays XPT, YPT, CHPT are destroyed. */
/* C */
    /* Parameter adjustments */
    --chpt;
    --ypt;
    --xpt;

    /* Function Body */
/* Computing MIN */
    i__1 = *npagwd - 20;
    maxnx = min(i__1,100);
    if (maxnx < 10) {
	maxnx = 10;
    }
    maxny = *npagln;
    if (maxny < 10) {
	maxny = 10;
    }
    if (*nxypt <= 1) {
	return 0;
    }
    xbest = xpt[1];
    ybest = ypt[1];
    *(unsigned char *)chbest = *(unsigned char *)&chpt[1];
/*         order the points by decreasing y */
    km1 = *nxypt - 1;
    i__1 = km1;
    for (i = 1; i <= i__1; ++i) {
	iquit = 0;
	ni = *nxypt - i;
	i__2 = ni;
	for (j = 1; j <= i__2; ++j) {
	    if (ypt[j] > ypt[j + 1]) {
		goto L140;
	    }
	    savx = xpt[j];
	    xpt[j] = xpt[j + 1];
	    xpt[j + 1] = savx;
	    savy = ypt[j];
	    ypt[j] = ypt[j + 1];
	    ypt[j + 1] = savy;
	    *(unsigned char *)chsav = *(unsigned char *)&chpt[j];
	    *(unsigned char *)&chpt[j] = *(unsigned char *)&chpt[j + 1];
	    *(unsigned char *)&chpt[j + 1] = *(unsigned char *)chsav;
	    iquit = 1;
L140:
	    ;
	}
	if (iquit == 0) {
	    goto L160;
	}
/* L150: */
    }
L160:
/*         find extreme values */
    xmax = xpt[1];
    xmin = xmax;
    i__1 = *nxypt;
    for (i = 1; i <= i__1; ++i) {
	if (xpt[i] > xmax) {
	    xmax = xpt[i];
	}
	if (xpt[i] < xmin) {
	    xmin = xpt[i];
	}
/* L200: */
    }
    dxx = (xmax - xmin) * (float).001;
    xmax += dxx;
    xmin -= dxx;
    mnbins_(&xmin, &xmax, &maxnx, &xmin, &xmax, &nx, &bwidx);
    ymax = ypt[1];
    ymin = ypt[*nxypt];
    if (ymax == ymin) {
	ymax = ymin + (float)1.;
    }
    dyy = (ymax - ymin) * (float).001;
    ymax += dyy;
    ymin -= dyy;
    mnbins_(&ymin, &ymax, &maxny, &ymin, &ymax, &ny, &bwidy);
    any = (doublereal) ny;
/*         if first point is blank, it is an 'origin' */
    if (*(unsigned char *)chbest == *(unsigned char *)&cblank[0]) {
	goto L50;
    }
    xbest = (xmax + xmin) * (float).5;
    ybest = (ymax + ymin) * (float).5;
L50:
/*         find scale constants */
    ax = (float)1. / bwidx;
    ay = (float)1. / bwidy;
    bx = -ax * xmin + (float)2.;
    by = -ay * ymin - (float)2.;
/*         convert points to grid positions */
    i__1 = *nxypt;
    for (i = 1; i <= i__1; ++i) {
	xpt[i] = ax * xpt[i] + bx;
/* L300: */
	ypt[i] = any - ay * ypt[i] - by;
    }
    nxbest = (integer) (ax * xbest + bx);
    nybest = (integer) (any - ay * ybest - by);
/*         print the points */
    ny += 2;
    nx += 2;
    isp1 = 1;
    linodd = 1;
    overpr = FALSE_;
    i__1 = ny;
    for (i = 1; i <= i__1; ++i) {
	i__2 = nx;
	for (ibk = 1; ibk <= i__2; ++ibk) {
/* L310: */
	    *(unsigned char *)&cline[ibk - 1] = *(unsigned char *)&cblank[0];
	}
	*(unsigned char *)cline = *(unsigned char *)&cdot[0];
	*(unsigned char *)&cline[nx - 1] = *(unsigned char *)&cdot[0];
	*(unsigned char *)&cline[nxbest - 1] = *(unsigned char *)&cdot[0];
	if (i != 1 && i != nybest && i != ny) {
	    goto L320;
	}
	i__2 = nx;
	for (j = 1; j <= i__2; ++j) {
/* L315: */
	    *(unsigned char *)&cline[j - 1] = *(unsigned char *)&cdot[0];
	}
L320:
	yprt = ymax - (real) (i - 1) * bwidy;
	if (isp1 > *nxypt) {
	    goto L350;
	}
/*         find the points to be plotted on this line */
	i__2 = *nxypt;
	for (k = isp1; k <= i__2; ++k) {
	    ks = (integer) ypt[k];
	    if (ks > i) {
		goto L345;
	    }
	    ix = (integer) xpt[k];
	    if (*(unsigned char *)&cline[ix - 1] == *(unsigned char *)&cdot[0]
		    ) {
		goto L340;
	    }
	    if (*(unsigned char *)&cline[ix - 1] == *(unsigned char *)&cblank[
		    0]) {
		goto L340;
	    }
	    if (*(unsigned char *)&cline[ix - 1] == *(unsigned char *)&chpt[k]
		    ) {
		goto L341;
	    }
	    overpr = TRUE_;
/*         OVERPR is true if one or more positions contains more t
han */
/*            one point */
	    *(unsigned char *)&cline[ix - 1] = '&';
	    goto L341;
L340:
	    *(unsigned char *)&cline[ix - 1] = *(unsigned char *)&chpt[k];
L341:
	    ;
	}
	isp1 = *nxypt + 1;
	goto L350;
L345:
	isp1 = k;
L350:
	if (linodd == 1 || i == ny) {
	    goto L380;
	}
	linodd = 1;
	io___43.ciunit = *nunit;
	s_wsfe(&io___43);
	do_fio(&c__1, cline, nx);
	e_wsfe();
	goto L400;
L380:
	io___44.ciunit = *nunit;
	s_wsfe(&io___44);
	do_fio(&c__1, (char *)&yprt, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, " ..", 3L);
	do_fio(&c__1, cline, nx);
	e_wsfe();
	linodd = 0;
L400:
	;
    }
/*         print labels on x-axis every ten columns */
    i__1 = nx;
    for (ibk = 1; ibk <= i__1; ++ibk) {
	*(unsigned char *)&cline[ibk - 1] = *(unsigned char *)&cblank[0];
	if (ibk % 10 == 1) {
	    *(unsigned char *)&cline[ibk - 1] = *(unsigned char *)&cslash[0];
	}
/* L410: */
    }
    io___45.ciunit = *nunit;
    s_wsfe(&io___45);
    do_fio(&c__1, cline, nx);
    e_wsfe();

    for (ibk = 1; ibk <= 12; ++ibk) {
/* L430: */
	xvalus[ibk - 1] = xmin + (real) (ibk - 1) * (float)10. * bwidx;
    }
    iten = (nx + 9) / 10;
    io___48.ciunit = *nunit;
    s_wsfe(&io___48);
    i__1 = iten;
    for (ibk = 1; ibk <= i__1; ++ibk) {
	do_fio(&c__1, (char *)&xvalus[ibk - 1], (ftnlen)sizeof(doublereal));
    }
    e_wsfe();
    s_copy(chmess, " ", 30L, 1L);
    if (overpr) {
	s_copy(chmess, "   Overprint character is &", 30L, 27L);
    }
    io___50.ciunit = *nunit;
    s_wsfe(&io___50);
    do_fio(&c__1, "ONE COLUMN=", 11L);
    do_fio(&c__1, (char *)&bwidx, (ftnlen)sizeof(doublereal));
    do_fio(&c__1, chmess, 30L);
    e_wsfe();
/* L500: */
    return 0;
} /* mnplot_ */

