/* mnexcm.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__3 = 3;
static integer c__30 = 30;
static integer c__0 = 0;
static integer c__5 = 5;
static integer c__2 = 2;


/* $Id: mnexcm.F,v 1.2 1996/03/15 18:02:45 james Exp $ */

/* $Log: mnexcm.F,v $ */
/* Revision 1.2  1996/03/15 18:02:45  james */
/*     Modified Files: */
/* mnderi.F eliminate possible division by zero */
/* mnexcm.F suppress print on STOP when print flag=-1 */
/*          set FVAL3 to flag if FCN already called with IFLAG=3 */
/* mninit.F set version 96.03 */
/* mnlims.F remove arguments, not needed */
/* mnmigr.F VLEN -> LENV in debug print statement */
/* mnparm.F move call to MNRSET to after NPAR redefined, to zero all */
/* mnpsdf.F eliminate possible division by zero */
/* mnscan.F suppress printout when print flag =-1 */
/* mnset.F  remove arguments in call to MNLIMS */
/* mnsimp.F fix CSTATU so status is PROGRESS only if new minimum */
/* mnvert.F eliminate possible division by zero */

/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mnexcm_(fcn, comand, plist, llist, ierflg, futil, 
	comand_len)
/* Subroutine */ int (*fcn) ();
char *comand;
doublereal *plist;
integer *llist, *ierflg;
/* Subroutine */ int (*futil) ();
ftnlen comand_len;
{
    /* Initialized data */

    static char clower[26+1] = "abcdefghijklmnopqrstuvwxyz";
    static char cupper[26+1] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static char cname[10*40+1] = "MINImize  SEEk      SIMplex   MIGrad    MI\
NOs     SET xxx   SHOw xxx  TOP of pagFIX       REStore   RELease   SCAn    \
  CONtour   HESse     SAVe      IMProve   CALl fcn  STAndard  END       EXIt\
      RETurn    CLEar     HELP      MNContour STOp      JUMp                \
                                                            COVARIANCEPRINTO\
UT  GRADIENT  MATOUT    ERROR DEF LIMITS    PUNCH     ";
    static integer nntot = 40;

    /* Format strings */
    static char fmt_25[] = "(\002 \002,10(\002*\002)/\002 **\002,i5,\002 *\
*\002,a,4g12.4)";
    static char fmt_3101[] = "(\002 OBSOLETE COMMAND:\002,1x,a10,5x,\002PLEA\
SE USE:\002,1x,a10)";

    /* System generated locals */
    address a__1[3];
    integer i__1, i__2[3];

    /* Builtin functions */
    integer i_len();
    /* Subroutine */ int s_copy();
    integer s_cmp(), s_wsfe(), do_fio(), e_wsfe(), s_wsfi(), e_wsfi();
    /* Subroutine */ int s_cat();

    /* Local variables */
    static char comd[4];
    static integer icol, kcol, ierr, iint, iext;
    static doublereal step;
    static integer lnow, nptu;
    static doublereal xptu[101], yptu[101], f;
    static integer i, iflag, ierrf;
    extern /* Subroutine */ int stand_();
    static char chwhy[18];
    extern /* Subroutine */ int mnset_();
    static integer ilist, nparx, izero;
    extern /* Subroutine */ int mnrn15_();
    static char c26[30];
    static integer nf, lk, it, iw;
    static logical lfreed;
    static char cvblnk[2], cneway[10];
    static logical ltofix, lfixed;
    static integer inonde;
    extern /* Subroutine */ int mnseek_(), mnsimp_(), mnmigr_(), mnwerr_();
    static integer nsuper;
    extern /* Subroutine */ int mncuve_(), mnmnos_(), mnrset_(), mnfixp_(), 
	    mnfree_(), mnprin_();
    static integer it2;
    extern /* Subroutine */ int mnscan_();
    static integer ke1, ke2;
    extern /* Subroutine */ int mncntr_(), mnhess_(), mnmatu_(), mnsave_(), 
	    mnimpr_();
    static integer nowprt;
    extern /* Subroutine */ int mncler_(), mnhelp_(), mncont_(), mninex_(), 
	    mnamin_();
    static integer kll, let, krl;
    static doublereal rno;

    /* Fortran I/O blocks */
    static cilist io___10 = { 0, 0, 0, fmt_25, 0 };
    static icilist io___15 = { 0, cvblnk, 0, "(I2)", 2, 1 };
    static cilist io___17 = { 0, 0, 0, c26, 0 };
    static cilist io___18 = { 0, 0, 0, "(1H ,10(1H*))", 0 };
    static cilist io___19 = { 0, 0, 0, "(1H ,10(1H*),A,I3,A)", 0 };
    static cilist io___20 = { 0, 0, 0, "(11X,'UNKNOWN COMMAND IGNORED:',A)", 
	    0 };
    static cilist io___23 = { 0, 0, 0, "(/' TOO MANY FUNCTION CALLS. MINOS G\
IVES UP'/)", 0 };
    static cilist io___24 = { 0, 0, 0, "(1H1)", 0 };
    static cilist io___28 = { 0, 0, 0, "(A,A)", 0 };
    static cilist io___35 = { 0, 0, 0, "(A,I4,A,A)", 0 };
    static cilist io___37 = { 0, 0, 0, "(A,I4)", 0 };
    static cilist io___39 = { 0, 0, 0, "(A,I4,A)", 0 };
    static cilist io___42 = { 0, 0, 0, "(A,A)", 0 };
    static cilist io___48 = { 0, 0, 0, "(/A/)", 0 };
    static cilist io___49 = { 0, 0, 0, "(A)", 0 };
    static cilist io___58 = { 0, 0, 0, "(10X,A)", 0 };
    static cilist io___59 = { 0, 0, 0, "(A)", 0 };
    static cilist io___61 = { 0, 0, 0, fmt_3101, 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Interprets a command and takes appropriate action, */
/* C        either directly by skipping to the corresponding code in */
/* C        MNEXCM, or by setting up a call to a subroutine */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


/*   Cannot say DIMENSION PLIST(LLIST) since LLIST can be =0. */
/*  alphabetical order of command names! */

    /* Parameter adjustments */
    --plist;

    /* Function Body */

/*  recognized MINUIT commands: */
/*  obsolete commands: */
/*      IERFLG is now (94.5) defined the same as ICONDN in MNCOMD */
/* C            = 0: command executed normally */
/* C              1: command is blank, ignored */
/* C              2: command line unreadable, ignored */
/* C              3: unknown command, ignored */
/* C              4: abnormal termination (e.g., MIGRAD not converged) */
/* C              9: reserved */
/* C             10: END command */
/* C             11: EXIT or STOP command */
/* C             12: RETURN command */
    lk = i_len(comand, comand_len);
    if (lk > 20) {
	lk = 20;
    }
    s_copy(mn7tit_1.cword, comand, 20L, lk);
/*              get upper case */
    i__1 = lk;
    for (icol = 1; icol <= i__1; ++icol) {
	for (let = 1; let <= 26; ++let) {
	    if (*(unsigned char *)&mn7tit_1.cword[icol - 1] == *(unsigned 
		    char *)&clower[let - 1]) {
		*(unsigned char *)&mn7tit_1.cword[icol - 1] = *(unsigned char 
			*)&cupper[let - 1];
	    }
/* L15: */
	}
/* L16: */
    }
/*           Copy the first MAXP arguments into COMMON (WORD7), making */
/*           sure that WORD7(1)=0. if LLIST=0 */
    for (iw = 1; iw <= 30; ++iw) {
	mn7arg_1.word7[iw - 1] = 0.;
	if (iw <= *llist) {
	    mn7arg_1.word7[iw - 1] = plist[iw];
	}
/* L20: */
    }
    ++mn7flg_1.icomnd;
    mn7cnv_1.nfcnlc = mn7cnv_1.nfcn;
    if (s_cmp(mn7tit_1.cword, "SET PRI", 7L, 7L) != 0 || mn7arg_1.word7[0] >= 
	    (float)0.) {
	if (mn7flg_1.isw[4] >= 0) {
	    lnow = *llist;
	    if (lnow > 4) {
		lnow = 4;
	    }
	    io___10.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___10);
	    do_fio(&c__1, (char *)&mn7flg_1.icomnd, (ftnlen)sizeof(integer));
	    do_fio(&c__1, mn7tit_1.cword, lk);
	    i__1 = lnow;
	    for (i = 1; i <= i__1; ++i) {
		do_fio(&c__1, (char *)&plist[i], (ftnlen)sizeof(doublereal));
	    }
	    e_wsfe();
	    inonde = 0;
	    if (*llist > lnow) {
		kll = *llist;
		if (*llist > 30) {
		    inonde = 1;
		    kll = 30;
		}
		s_wsfi(&io___15);
		do_fio(&c__1, (char *)&lk, (ftnlen)sizeof(integer));
		e_wsfi();
/* Writing concatenation */
		i__2[0] = 16, a__1[0] = "(11H **********,";
		i__2[1] = 2, a__1[1] = cvblnk;
		i__2[2] = 9, a__1[2] = "X,4G12.4)";
		s_cat(c26, a__1, i__2, &c__3, 30L);
		io___17.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___17);
		i__1 = kll;
		for (i = lnow + 1; i <= i__1; ++i) {
		    do_fio(&c__1, (char *)&plist[i], (ftnlen)sizeof(
			    doublereal));
		}
		e_wsfe();
	    }
	    io___18.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___18);
	    e_wsfe();
	    if (inonde > 0) {
		io___19.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___19);
		do_fio(&c__1, "  ERROR: ABOVE CALL TO MNEXCM TRIED TO PASS M\
ORE THAN ", 54L);
		do_fio(&c__1, (char *)&c__30, (ftnlen)sizeof(integer));
		do_fio(&c__1, " PARAMETERS.", 12L);
		e_wsfe();
	    }
	}
    }
    mn7cnv_1.nfcnmx = (integer) mn7arg_1.word7[0];
    if (mn7cnv_1.nfcnmx <= 0) {
/* Computing 2nd power */
	i__1 = mn7npr_1.npar;
	mn7cnv_1.nfcnmx = mn7npr_1.npar * 100 + 200 + i__1 * i__1 * 5;
    }
    mn7min_1.epsi = mn7arg_1.word7[1];
    if (mn7min_1.epsi <= 0.) {
	mn7min_1.epsi = mn7min_1.up * (float).1;
    }
    mn7log_1.lnewmn = FALSE_;
    mn7log_1.lphead = TRUE_;
    mn7flg_1.isw[0] = 0;
    *ierflg = 0;
/*                look for command in list CNAME . . . . . . . . . . */
    i__1 = nntot;
    for (i = 1; i <= i__1; ++i) {
	if (s_cmp(mn7tit_1.cword, cname + (i - 1) * 10, 3L, 3L) == 0) {
	    goto L90;
	}
/* L80: */
    }
    io___20.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___20);
    do_fio(&c__1, comand, comand_len);
    e_wsfe();
    *ierflg = 3;
    goto L5000;
/*                normal case: recognized MINUIT command . . . . . . . */
L90:
    if (s_cmp(mn7tit_1.cword, "MINO", 4L, 4L) == 0) {
	i = 5;
    }
    if (i != 6 && i != 7 && i != 8 && i != 23) {
	s_copy(mn7tit_1.cfrom, cname + (i - 1) * 10, 8L, 10L);
	mn7cnv_1.nfcnfr = mn7cnv_1.nfcn;
    }
/*              1    2    3    4    5    6    7    8    9   10 */
    switch ((int)i) {
	case 1:  goto L400;
	case 2:  goto L200;
	case 3:  goto L300;
	case 4:  goto L400;
	case 5:  goto L500;
	case 6:  goto L700;
	case 7:  goto L700;
	case 8:  goto L800;
	case 9:  goto L900;
	case 10:  goto L1000;
	case 11:  goto L1100;
	case 12:  goto L1200;
	case 13:  goto L1300;
	case 14:  goto L1400;
	case 15:  goto L1500;
	case 16:  goto L1600;
	case 17:  goto L1700;
	case 18:  goto L1800;
	case 19:  goto L1900;
	case 20:  goto L1900;
	case 21:  goto L1900;
	case 22:  goto L2200;
	case 23:  goto L2300;
	case 24:  goto L2400;
	case 25:  goto L1900;
	case 26:  goto L2600;
	case 27:  goto L3300;
	case 28:  goto L3300;
	case 29:  goto L3300;
	case 30:  goto L3300;
	case 31:  goto L3300;
	case 32:  goto L3300;
	case 33:  goto L3300;
	case 34:  goto L3400;
	case 35:  goto L3500;
	case 36:  goto L3600;
	case 37:  goto L3700;
	case 38:  goto L3800;
	case 39:  goto L3900;
	case 40:  goto L4000;
    }
/*                                        . . . . . . . . . . seek */
L200:
    mnseek_(fcn, futil);
    goto L5000;
/*                                        . . . . . . . . . . simplex */
L300:
    mnsimp_(fcn, futil);
    if (mn7flg_1.isw[3] < 1) {
	*ierflg = 4;
    }
    goto L5000;
/*                                        . . . . . . migrad, minimize */
L400:
    nf = mn7cnv_1.nfcn;
    mn7min_1.apsi = mn7min_1.epsi;
    mnmigr_(fcn, futil);
    mnwerr_();
    if (mn7flg_1.isw[3] >= 1) {
	goto L5000;
    }
    *ierflg = 4;
    if (mn7flg_1.isw[0] == 1) {
	goto L5000;
    }
    if (s_cmp(mn7tit_1.cword, "MIG", 3L, 3L) == 0) {
	goto L5000;
    }
    mn7cnv_1.nfcnmx = mn7cnv_1.nfcnmx + nf - mn7cnv_1.nfcn;
    nf = mn7cnv_1.nfcn;
    mnsimp_(fcn, futil);
    if (mn7flg_1.isw[0] == 1) {
	goto L5000;
    }
    mn7cnv_1.nfcnmx = mn7cnv_1.nfcnmx + nf - mn7cnv_1.nfcn;
    mnmigr_(fcn, futil);
    if (mn7flg_1.isw[3] >= 1) {
	*ierflg = 0;
    }
    mnwerr_();
    goto L5000;
/*                                        . . . . . . . . . . minos */
L500:
    nsuper = mn7cnv_1.nfcn + (mn7npr_1.npar + 1 << 1) * mn7cnv_1.nfcnmx;
/*          possible loop over new minima */
    mn7min_1.epsi = mn7min_1.up * (float).1;
L510:
    mncuve_(fcn, futil);
    mnmnos_(fcn, futil);
    if (! mn7log_1.lnewmn) {
	goto L5000;
    }
    mnrset_(&c__0);
    mnmigr_(fcn, futil);
    mnwerr_();
    if (mn7cnv_1.nfcn < nsuper) {
	goto L510;
    }
    io___23.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___23);
    e_wsfe();
    *ierflg = 4;
    goto L5000;
/*                                        . . . . . . . . . .set, show */
L700:
    mnset_(fcn, futil);
    goto L5000;
/*                                        . . . . . . . . . . top of page 
*/
L800:
    io___24.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___24);
    e_wsfe();
    goto L5000;
/*                                        . . . . . . . . . . fix */
L900:
    ltofix = TRUE_;
/*                                        . . (also release) .... */
L901:
    lfreed = FALSE_;
    lfixed = FALSE_;
    if (*llist == 0) {
	io___28.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___28);
	do_fio(&c__1, mn7tit_1.cword, 20L);
	do_fio(&c__1, ":  NO PARAMETERS REQUESTED ", 27L);
	e_wsfe();
	goto L5000;
    }
    i__1 = *llist;
    for (ilist = 1; ilist <= i__1; ++ilist) {
	iext = (integer) plist[ilist];
	s_copy(chwhy, " IS UNDEFINED.", 18L, 14L);
	if (iext <= 0) {
	    goto L930;
	}
	if (iext > mn7npr_1.nu) {
	    goto L930;
	}
	if (mn7inx_1.nvarl[iext - 1] < 0) {
	    goto L930;
	}
	s_copy(chwhy, " IS CONSTANT.  ", 18L, 15L);
	if (mn7inx_1.nvarl[iext - 1] == 0) {
	    goto L930;
	}
	iint = mn7inx_1.niofex[iext - 1];
	if (ltofix) {
	    s_copy(chwhy, " ALREADY FIXED.", 18L, 15L);
	    if (iint == 0) {
		goto L930;
	    }
	    mnfixp_(&iint, &ierr);
	    if (ierr == 0) {
		lfixed = TRUE_;
	    } else {
		*ierflg = 4;
	    }
	} else {
	    s_copy(chwhy, " ALREADY VARIABLE.", 18L, 18L);
	    if (iint > 0) {
		goto L930;
	    }
	    krl = -abs(iext);
	    mnfree_(&krl);
	    lfreed = TRUE_;
	}
	goto L950;
L930:
	io___35.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___35);
	do_fio(&c__1, " PARAMETER", 10L);
	do_fio(&c__1, (char *)&iext, (ftnlen)sizeof(integer));
	do_fio(&c__1, chwhy, 18L);
	do_fio(&c__1, " IGNORED.", 9L);
	e_wsfe();
L950:
	;
    }
    if (lfreed || lfixed) {
	mnrset_(&c__0);
    }
    if (lfreed) {
	mn7flg_1.isw[1] = 0;
	mn7min_1.dcovar = (float)1.;
	mn7min_1.edm = mn7cns_1.bigedm;
	mn7flg_1.isw[3] = 0;
    }
    mnwerr_();
    if (mn7flg_1.isw[4] > 1) {
	mnprin_(&c__5, &mn7min_1.amin);
    }
    goto L5000;
/*                                        . . . . . . . . . . restore */
L1000:
    it = (integer) mn7arg_1.word7[0];
    if (it > 1 || it < 0) {
	goto L1005;
    }
    lfreed = mn7fx1_1.npfix > 0;
    mnfree_(&it);
    if (lfreed) {
	mnrset_(&c__0);
	mn7flg_1.isw[1] = 0;
	mn7min_1.dcovar = (float)1.;
	mn7min_1.edm = mn7cns_1.bigedm;
    }
    goto L5000;
L1005:
    io___37.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___37);
    do_fio(&c__1, " IGNORED.  UNKNOWN ARGUMENT:", 28L);
    do_fio(&c__1, (char *)&it, (ftnlen)sizeof(integer));
    e_wsfe();
    *ierflg = 3;
    goto L5000;
/*                                        . . . . . . . . . . release */
L1100:
    ltofix = FALSE_;
    goto L901;
/*                                       . . . . . . . . . . scan . . . */
L1200:
    iext = (integer) mn7arg_1.word7[0];
    if (iext <= 0) {
	goto L1210;
    }
    it2 = 0;
    if (iext <= mn7npr_1.nu) {
	it2 = mn7inx_1.niofex[iext - 1];
    }
    if (it2 <= 0) {
	goto L1250;
    }
L1210:
    mnscan_(fcn, futil);
    goto L5000;
L1250:
    io___39.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___39);
    do_fio(&c__1, " PARAMETER", 10L);
    do_fio(&c__1, (char *)&iext, (ftnlen)sizeof(integer));
    do_fio(&c__1, " NOT VARIABLE.", 14L);
    e_wsfe();
    *ierflg = 3;
    goto L5000;
/*                                        . . . . . . . . . . contour */
L1300:
    ke1 = (integer) mn7arg_1.word7[0];
    ke2 = (integer) mn7arg_1.word7[1];
    if (ke1 == 0) {
	if (mn7npr_1.npar == 2) {
	    ke1 = mn7inx_1.nexofi[0];
	    ke2 = mn7inx_1.nexofi[1];
	} else {
	    io___42.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___42);
	    do_fio(&c__1, mn7tit_1.cword, 20L);
	    do_fio(&c__1, ":  NO PARAMETERS REQUESTED ", 27L);
	    e_wsfe();
	    *ierflg = 3;
	    goto L5000;
	}
    }
    mn7cnv_1.nfcnmx = 1000;
    mncntr_(fcn, &ke1, &ke2, &ierrf, futil);
    if (ierrf > 0) {
	*ierflg = 3;
    }
    goto L5000;
/*                                        . . . . . . . . . . hesse */
L1400:
    mnhess_(fcn, futil);
    mnwerr_();
    if (mn7flg_1.isw[4] >= 0) {
	mnprin_(&c__2, &mn7min_1.amin);
    }
    if (mn7flg_1.isw[4] >= 1) {
	mnmatu_(&c__1);
    }
    goto L5000;
/*                                        . . . . . . . . . . save */
L1500:
    mnsave_();
    goto L5000;
/*                                        . . . . . . . . . . improve */
L1600:
    mncuve_(fcn, futil);
    mnimpr_(fcn, futil);
    if (mn7log_1.lnewmn) {
	goto L400;
    }
    *ierflg = 4;
    goto L5000;
/*                                        . . . . . . . . . . call fcn */
L1700:
    iflag = (integer) mn7arg_1.word7[0];
    nparx = mn7npr_1.npar;
    f = mn7cns_1.undefi;
    (*fcn)(&nparx, mn7der_1.gin, &f, mn7ext_1.u, &iflag, futil);
    ++mn7cnv_1.nfcn;
    nowprt = 0;
    if (f != mn7cns_1.undefi) {
	if (mn7min_1.amin == mn7cns_1.undefi) {
	    mn7min_1.amin = f;
	    nowprt = 1;
	} else if (f < mn7min_1.amin) {
	    mn7min_1.amin = f;
	    nowprt = 1;
	}
	if (mn7flg_1.isw[4] >= 0 && iflag <= 5 && nowprt == 1) {
	    mnprin_(&c__5, &mn7min_1.amin);
	}
	if (iflag == 3) {
	    mn7min_1.fval3 = f;
	}
    }
    if (iflag > 5) {
	mnrset_(&c__1);
    }
    goto L5000;
/*                                        . . . . . . . . . . standard */
L1800:
    stand_();
    goto L5000;
/*                                       . . . return, stop, end, exit */
L1900:
    it = (integer) mn7arg_1.word7[0];
    if (mn7min_1.fval3 != mn7min_1.amin && it == 0) {
	iflag = 3;
	if (mn7flg_1.isw[4] >= 0) {
	    io___48.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___48);
	    do_fio(&c__1, " CALL TO USER FUNCTION WITH IFLAG = 3", 37L);
	    e_wsfe();
	}
	nparx = mn7npr_1.npar;
	(*fcn)(&nparx, mn7der_1.gin, &f, mn7ext_1.u, &iflag, futil);
	++mn7cnv_1.nfcn;
	mn7min_1.fval3 = f;
    }
    *ierflg = 11;
    if (s_cmp(mn7tit_1.cword, "END", 3L, 3L) == 0) {
	*ierflg = 10;
    }
    if (s_cmp(mn7tit_1.cword, "RET", 3L, 3L) == 0) {
	*ierflg = 12;
    }
    goto L5000;
/*                                        . . . . . . . . . . clear */
L2200:
    mncler_();
    if (mn7flg_1.isw[4] >= 1) {
	io___49.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___49);
	do_fio(&c__1, " MINUIT MEMORY CLEARED. NO PARAMETERS NOW DEFINED.", 
		50L);
	e_wsfe();
    }
    goto L5000;
/*                                        . . . . . . . . . . help */
L2300:
/* CCC      IF (INDEX(CWORD,'SHO') .GT. 0)  GO TO 700 */
/* CCC      IF (INDEX(CWORD,'SET') .GT. 0)  GO TO 700 */
    kcol = 0;
    i__1 = lk;
    for (icol = 5; icol <= i__1; ++icol) {
	if (*(unsigned char *)&mn7tit_1.cword[icol - 1] == ' ') {
	    goto L2310;
	}
	kcol = icol;
	goto L2320;
L2310:
	;
    }
L2320:
    if (kcol == 0) {
	s_copy(comd, "*   ", 4L, 4L);
    } else {
	s_copy(comd, mn7tit_1.cword + (kcol - 1), 4L, lk - (kcol - 1));
    }
    mnhelp_(comd, &mn7iou_1.isyswr, 4L);
    goto L5000;
/*                                       . . . . . . . . . . MNContour */
L2400:
    mn7min_1.epsi = mn7min_1.up * (float).05;
    ke1 = (integer) mn7arg_1.word7[0];
    ke2 = (integer) mn7arg_1.word7[1];
    if (ke1 == 0 && mn7npr_1.npar == 2) {
	ke1 = mn7inx_1.nexofi[0];
	ke2 = mn7inx_1.nexofi[1];
    }
    nptu = (integer) mn7arg_1.word7[2];
    if (nptu <= 0) {
	nptu = 20;
    }
    if (nptu > 101) {
	nptu = 101;
    }
    mn7cnv_1.nfcnmx = (nptu + 5) * 100 * (mn7npr_1.npar + 1);
    mncont_(fcn, &ke1, &ke2, &nptu, xptu, yptu, &ierrf, futil);
    if (ierrf < nptu) {
	*ierflg = 4;
    }
    if (ierrf == -1) {
	*ierflg = 3;
    }
    goto L5000;
/*                                      . . . . . . . . . . jump */
L2600:
    step = mn7arg_1.word7[0];
    if (step <= 0.) {
	step = (float)2.;
    }
    rno = (float)0.;
    izero = 0;
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	mnrn15_(&rno, &izero);
	rno = rno * (float)2. - (float)1.;
/* L2620: */
	mn7int_1.x[i - 1] += rno * step * mn7err_1.werr[i - 1];
    }
    mninex_(mn7int_1.x);
    mnamin_(fcn, futil);
    mnrset_(&c__0);
    goto L5000;
/*                                      . . . . . . . . . . blank line */
L3300:
    io___58.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___58);
    do_fio(&c__1, " BLANK COMMAND IGNORED.", 23L);
    e_wsfe();
    *ierflg = 1;
    goto L5000;
/*  . . . . . . . . obsolete commands     . . . . . . . . . . . . . . */
/*                                      . . . . . . . . . . covariance */
L3400:
    io___59.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___59);
    do_fio(&c__1, " THE \"COVARIANCE\" COMMAND IS OSBSOLETE.", 39L);
    do_fio(&c__1, " THE COVARIANCE MATRIX IS NOW SAVED IN A DIFFERENT FORMAT",
	     57L);
    do_fio(&c__1, " WITH THE \"SAVE\" COMMAND AND READ IN WITH:\"SET COVARIA\
NCE\"", 58L);
    e_wsfe();
    *ierflg = 3;
    goto L5000;
/*                                        . . . . . . . . . . printout */
L3500:
    s_copy(cneway, "SET PRInt ", 10L, 10L);
    goto L3100;
/*                                        . . . . . . . . . . gradient */
L3600:
    s_copy(cneway, "SET GRAd  ", 10L, 10L);
    goto L3100;
/*                                        . . . . . . . . . . matout */
L3700:
    s_copy(cneway, "SHOW COVar", 10L, 10L);
    goto L3100;
/*                                        . . . . . . . . . error def */
L3800:
    s_copy(cneway, "SET ERRdef", 10L, 10L);
    goto L3100;
/*                                        . . . . . . . . . . limits */
L3900:
    s_copy(cneway, "SET LIMits", 10L, 10L);
    goto L3100;
/*                                        . . . . . . . . . . punch */
L4000:
    s_copy(cneway, "SAVE      ", 10L, 10L);
/*                                ....... come from obsolete commands */
L3100:
    io___61.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___61);
    do_fio(&c__1, mn7tit_1.cword, 20L);
    do_fio(&c__1, cneway, 10L);
    e_wsfe();
    s_copy(mn7tit_1.cword, cneway, 20L, 10L);
    if (s_cmp(mn7tit_1.cword, "SAVE      ", 20L, 10L) == 0) {
	goto L1500;
    }
    goto L700;
/*                                 . . . . . . . . . . . . . . . . . . */
L5000:
    return 0;
} /* mnexcm_ */

