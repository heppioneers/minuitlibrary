/* mnhess.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__4 = 4;
static integer c__2 = 2;


/* $Id: mnhess.F,v 1.1.1.1 1996/03/07 14:31:30 mclareni Exp $ */

/* $Log: mnhess.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mnhess_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    /* Format strings */
    static char fmt_31[] = "(i4,i2,6g12.5)";

    /* System generated locals */
    address a__1[2];
    integer i__1[2], i__2, i__3;
    doublereal d__1, d__2;
    char ch__1[48], ch__2[41], ch__3[40], ch__4[45];

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();
    /* Subroutine */ int s_copy();
    integer s_wsfi(), e_wsfi();
    /* Subroutine */ int s_cat();
    double sqrt(), d_sign();

    /* Local variables */
    static doublereal dmin__, dxdi;
    static integer icyc;
    static doublereal elem;
    static integer ncyc, ndex, idrv, iext;
    static doublereal wint;
    static integer npar2;
    static doublereal tlrg2, d;
    static integer i, j, ifail, npard;
    static doublereal dlast;
    static integer nparx;
    static doublereal ztemp, g2bfor;
    extern /* Subroutine */ int mnhes1_();
    static doublereal df;
    static integer id;
    static doublereal aimsag;
    static logical ldebug;
    static doublereal yy[50];
    extern /* Subroutine */ int mnamin_(), mninex_();
    static doublereal fs1, tlrstp;
    extern /* Subroutine */ int mnwarn_(), mndxdi_();
    static integer multpy;
    static doublereal fs2, stpinm;
    extern /* Subroutine */ int mnpsdf_(), mnvert_();
    static doublereal g2i, sag, xtf, xti, xtj;
    static char cbf1[22];

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 0, 0, "(A)", 0 };
    static icilist io___11 = { 0, cbf1, 0, "(G12.3)", 12, 1 };
    static cilist io___12 = { 0, 0, 0, "(A,A)", 0 };
    static icilist io___19 = { 0, cbf1, 0, "(I4)", 4, 1 };
    static icilist io___29 = { 0, cbf1, 0, "(I4)", 4, 1 };
    static cilist io___31 = { 0, 0, 0, fmt_31, 0 };
    static icilist io___35 = { 0, cbf1, 0, "(I2,2E10.2)", 22, 1 };
    static cilist io___43 = { 0, 0, 0, "(A)", 0 };
    static cilist io___44 = { 0, 0, 0, "(A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Calculates the full second-derivative matrix of FCN */
/* C        by taking finite differences. When calculating diagonal */
/* C        elements, it may iterate so that step size is nearly that */
/* C        which gives function change= UP/10. The first derivatives */
/* C        of course come as a free side effect, but with a smaller */
/* C        step size in order to obtain a known accuracy. */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */



    ldebug = mn7flg_1.idbg[3] >= 1;
    if (mn7min_1.amin == mn7cns_1.undefi) {
	mnamin_(fcn, futil);
    }
    if (mn7cnv_1.istrat <= 0) {
	ncyc = 3;
	tlrstp = (float).5;
	tlrg2 = (float).1;
    } else if (mn7cnv_1.istrat == 1) {
	ncyc = 5;
	tlrstp = (float).3;
	tlrg2 = (float).05;
    } else {
	ncyc = 7;
	tlrstp = (float).1;
	tlrg2 = (float).02;
    }
    if (mn7flg_1.isw[4] >= 2 || ldebug) {
	io___5.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___5);
	do_fio(&c__1, "   START COVARIANCE MATRIX CALCULATION.", 39L);
	e_wsfe();
    }
    s_copy(mn7tit_1.cfrom, "HESSE   ", 8L, 8L);
    mn7cnv_1.nfcnfr = mn7cnv_1.nfcn;
    s_copy(mn7tit_1.cstatu, "OK        ", 10L, 10L);
    npard = mn7npr_1.npar;
/*                 make sure starting at the right place */
    mninex_(mn7int_1.x);
    nparx = mn7npr_1.npar;
    (*fcn)(&nparx, mn7der_1.gin, &fs1, mn7ext_1.u, &c__4, futil);
    ++mn7cnv_1.nfcn;
    if (fs1 != mn7min_1.amin) {
	df = mn7min_1.amin - fs1;
	s_wsfi(&io___11);
	do_fio(&c__1, (char *)&df, (ftnlen)sizeof(doublereal));
	e_wsfi();
/* Writing concatenation */
	i__1[0] = 36, a__1[0] = "function value differs from AMIN by ";
	i__1[1] = 12, a__1[1] = cbf1;
	s_cat(ch__1, a__1, i__1, &c__2, 48L);
	mnwarn_("D", "MNHESS", ch__1, 1L, 6L, 48L);
    }
    mn7min_1.amin = fs1;
    if (ldebug) {
	io___12.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___12);
	do_fio(&c__1, " PAR D   GSTEP          ", 24L);
	do_fio(&c__1, " D          G2         GRD         SAG    ", 42L);
	e_wsfe();
    }
/*                                        . . . . . . diagonal elements . 
*/
/*         ISW(2) = 1 if approx, 2 if not posdef, 3 if ok */
/*         AIMSAG is the sagitta we are aiming for in second deriv calc. 
*/
    aimsag = sqrt(mn7cns_1.epsma2) * (abs(mn7min_1.amin) + mn7min_1.up);
/*         Zero the second derivative matrix */
    npar2 = mn7npr_1.npar * (mn7npr_1.npar + 1) / 2;
    i__2 = npar2;
    for (i = 1; i <= i__2; ++i) {
/* L10: */
	mn7var_1.vhmat[i - 1] = (float)0.;
    }

/*         Loop over variable parameters for second derivatives */
    idrv = 2;
    i__2 = npard;
    for (id = 1; id <= i__2; ++id) {
	i = id + mn7npr_1.npar - npard;
	iext = mn7inx_1.nexofi[i - 1];
	if (mn7der_1.g2[i - 1] == 0.) {
	    s_wsfi(&io___19);
	    do_fio(&c__1, (char *)&iext, (ftnlen)sizeof(integer));
	    e_wsfi();
/* Writing concatenation */
	    i__1[0] = 37, a__1[0] = "Second derivative enters zero, param ";
	    i__1[1] = 4, a__1[1] = cbf1;
	    s_cat(ch__2, a__1, i__1, &c__2, 41L);
	    mnwarn_("W", "HESSE", ch__2, 1L, 5L, 41L);
	    wint = mn7err_1.werr[i - 1];
	    if (mn7inx_1.nvarl[iext - 1] > 1) {
		mndxdi_(&mn7int_1.x[i - 1], &i, &dxdi);
		if (abs(dxdi) < (float).001) {
		    wint = (float).01;
		} else {
		    wint /= abs(dxdi);
		}
	    }
/* Computing 2nd power */
	    d__1 = wint;
	    mn7der_1.g2[i - 1] = mn7min_1.up / (d__1 * d__1);
	}
	xtf = mn7int_1.x[i - 1];
	dmin__ = mn7cns_1.epsma2 * (float)8. * abs(xtf);

/*                               find step which gives sagitta = AIMSA
G */
	d = (d__1 = mn7der_1.gstep[i - 1], abs(d__1));
	i__3 = ncyc;
	for (icyc = 1; icyc <= i__3; ++icyc) {
/*                               loop here only if SAG=0. */
	    for (multpy = 1; multpy <= 5; ++multpy) {
/*           take two steps */
		mn7int_1.x[i - 1] = xtf + d;
		mninex_(mn7int_1.x);
		nparx = mn7npr_1.npar;
		(*fcn)(&nparx, mn7der_1.gin, &fs1, mn7ext_1.u, &c__4, futil);
		++mn7cnv_1.nfcn;
		mn7int_1.x[i - 1] = xtf - d;
		mninex_(mn7int_1.x);
		(*fcn)(&nparx, mn7der_1.gin, &fs2, mn7ext_1.u, &c__4, futil);
		++mn7cnv_1.nfcn;
		mn7int_1.x[i - 1] = xtf;
		sag = (fs1 + fs2 - mn7min_1.amin * (float)2.) * (float).5;
		if (sag != 0.) {
		    goto L30;
		}
		if (mn7der_1.gstep[i - 1] < 0.) {
		    if (d >= (float).5) {
			goto L26;
		    }
		    d *= (float)10.;
		    if (d > (float).5) {
			d = (float).51;
		    }
		    goto L25;
		}
		d *= (float)10.;
L25:
		;
	    }
L26:
	    s_wsfi(&io___29);
	    do_fio(&c__1, (char *)&iext, (ftnlen)sizeof(integer));
	    e_wsfi();
/* Writing concatenation */
	    i__1[0] = 36, a__1[0] = "Second derivative zero for parameter";
	    i__1[1] = 4, a__1[1] = cbf1;
	    s_cat(ch__3, a__1, i__1, &c__2, 40L);
	    mnwarn_("W", "HESSE", ch__3, 1L, 5L, 40L);
	    goto L390;
/*                             SAG is not zero */
L30:
	    g2bfor = mn7der_1.g2[i - 1];
/* Computing 2nd power */
	    d__1 = d;
	    mn7der_1.g2[i - 1] = sag * (float)2. / (d__1 * d__1);
	    mn7der_1.grd[i - 1] = (fs1 - fs2) / (d * (float)2.);
	    if (ldebug) {
		io___31.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___31);
		do_fio(&c__1, (char *)&i, (ftnlen)sizeof(integer));
		do_fio(&c__1, (char *)&idrv, (ftnlen)sizeof(integer));
		do_fio(&c__1, (char *)&mn7der_1.gstep[i - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&d, (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&mn7der_1.g2[i - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&mn7der_1.grd[i - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&sag, (ftnlen)sizeof(doublereal));
		e_wsfe();
	    }
	    mn7der_1.gstep[i - 1] = d_sign(&d, &mn7der_1.gstep[i - 1]);
	    mn7int_1.dirin[i - 1] = d;
	    yy[i - 1] = fs1;
	    dlast = d;
	    d = sqrt(aimsag * (float)2. / (d__1 = mn7der_1.g2[i - 1], abs(
		    d__1)));
/*         if parameter has limits, max int step size = 0.5 */
	    stpinm = (float).5;
	    if (mn7der_1.gstep[i - 1] < 0.) {
		d = min(d,stpinm);
	    }
	    if (d < dmin__) {
		d = dmin__;
	    }
/*           see if converged */
	    if ((d__1 = (d - dlast) / d, abs(d__1)) < tlrstp) {
		goto L50;
	    }
	    if ((d__1 = (mn7der_1.g2[i - 1] - g2bfor) / mn7der_1.g2[i - 1], 
		    abs(d__1)) < tlrg2) {
		goto L50;
	    }
/* Computing MIN */
	    d__1 = d, d__2 = dlast * (float)10.;
	    d = min(d__1,d__2);
/* Computing MAX */
	    d__1 = d, d__2 = dlast * (float).1;
	    d = max(d__1,d__2);
/* L40: */
	}
/*                       end of step size loop */
	s_wsfi(&io___35);
	do_fio(&c__1, (char *)&iext, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&sag, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&aimsag, (ftnlen)sizeof(doublereal));
	e_wsfi();
/* Writing concatenation */
	i__1[0] = 23, a__1[0] = "Second Deriv. SAG,AIM= ";
	i__1[1] = 22, a__1[1] = cbf1;
	s_cat(ch__4, a__1, i__1, &c__2, 45L);
	mnwarn_("D", "MNHESS", ch__4, 1L, 6L, 45L);

L50:
	ndex = i * (i + 1) / 2;
	mn7var_1.vhmat[ndex - 1] = mn7der_1.g2[i - 1];
/* L100: */
    }
/*                              end of diagonal second derivative loop */
    mninex_(mn7int_1.x);
/*                                     refine the first derivatives */
    if (mn7cnv_1.istrat > 0) {
	mnhes1_(fcn, futil);
    }
    mn7flg_1.isw[1] = 3;
    mn7min_1.dcovar = (float)0.;
/*                                        . . . .  off-diagonal elements 
*/
    if (mn7npr_1.npar == 1) {
	goto L214;
    }
    i__2 = mn7npr_1.npar;
    for (i = 1; i <= i__2; ++i) {
	i__3 = i - 1;
	for (j = 1; j <= i__3; ++j) {
	    xti = mn7int_1.x[i - 1];
	    xtj = mn7int_1.x[j - 1];
	    mn7int_1.x[i - 1] = xti + mn7int_1.dirin[i - 1];
	    mn7int_1.x[j - 1] = xtj + mn7int_1.dirin[j - 1];
	    mninex_(mn7int_1.x);
	    (*fcn)(&nparx, mn7der_1.gin, &fs1, mn7ext_1.u, &c__4, futil);
	    ++mn7cnv_1.nfcn;
	    mn7int_1.x[i - 1] = xti;
	    mn7int_1.x[j - 1] = xtj;
	    elem = (fs1 + mn7min_1.amin - yy[i - 1] - yy[j - 1]) / (
		    mn7int_1.dirin[i - 1] * mn7int_1.dirin[j - 1]);
	    ndex = i * (i - 1) / 2 + j;
	    mn7var_1.vhmat[ndex - 1] = elem;
/* L180: */
	}
/* L200: */
    }
L214:
    mninex_(mn7int_1.x);
/*                  verify matrix positive-definite */
    mnpsdf_();
    i__2 = mn7npr_1.npar;
    for (i = 1; i <= i__2; ++i) {
	i__3 = i;
	for (j = 1; j <= i__3; ++j) {
	    ndex = i * (i - 1) / 2 + j;
	    mn7sim_1.p[i + j * 50 - 51] = mn7var_1.vhmat[ndex - 1];
/* L219: */
	    mn7sim_1.p[j + i * 50 - 51] = mn7sim_1.p[i + j * 50 - 51];
	}
/* L220: */
    }
    mnvert_(mn7sim_1.p, &mn7npr_1.maxint, &mn7npr_1.maxint, &mn7npr_1.npar, &
	    ifail);
    if (ifail > 0) {
	mnwarn_("W", "HESSE", "Matrix inversion fails.", 1L, 5L, 23L);
	goto L390;
    }
/*                                        . . . . . . .  calculate  e d m 
*/
    mn7min_1.edm = (float)0.;
    i__2 = mn7npr_1.npar;
    for (i = 1; i <= i__2; ++i) {
/*                              off-diagonal elements */
	ndex = i * (i - 1) / 2;
	i__3 = i - 1;
	for (j = 1; j <= i__3; ++j) {
	    ++ndex;
	    ztemp = mn7sim_1.p[i + j * 50 - 51] * (float)2.;
	    mn7min_1.edm += mn7der_1.grd[i - 1] * ztemp * mn7der_1.grd[j - 1];
/* L225: */
	    mn7var_1.vhmat[ndex - 1] = ztemp;
	}
/*                              diagonal elements */
	++ndex;
	mn7var_1.vhmat[ndex - 1] = mn7sim_1.p[i + i * 50 - 51] * (float)2.;
/* Computing 2nd power */
	d__1 = mn7der_1.grd[i - 1];
	mn7min_1.edm += mn7sim_1.p[i + i * 50 - 51] * (d__1 * d__1);
/* L230: */
    }
    if (mn7flg_1.isw[4] >= 1 && mn7flg_1.isw[1] == 3 && mn7cnv_1.itaur == 0) {
	io___43.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___43);
	do_fio(&c__1, " COVARIANCE MATRIX CALCULATED SUCCESSFULLY", 42L);
	e_wsfe();
    }
    goto L900;
/*                              failure to invert 2nd deriv matrix */
L390:
    mn7flg_1.isw[1] = 1;
    mn7min_1.dcovar = (float)1.;
    s_copy(mn7tit_1.cstatu, "FAILED    ", 10L, 10L);
    if (mn7flg_1.isw[4] >= 0) {
	io___44.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___44);
	do_fio(&c__1, "  MNHESS FAILS AND WILL RETURN DIAGONAL MATRIX. ", 48L)
		;
	e_wsfe();
    }
    i__2 = mn7npr_1.npar;
    for (i = 1; i <= i__2; ++i) {
	ndex = i * (i - 1) / 2;
	i__3 = i - 1;
	for (j = 1; j <= i__3; ++j) {
	    ++ndex;
/* L394: */
	    mn7var_1.vhmat[ndex - 1] = (float)0.;
	}
	++ndex;
	g2i = mn7der_1.g2[i - 1];
	if (g2i <= 0.) {
	    g2i = (float)1.;
	}
/* L395: */
	mn7var_1.vhmat[ndex - 1] = (float)2. / g2i;
    }
L900:
    return 0;
} /* mnhess_ */

