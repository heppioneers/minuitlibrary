/* mnrn15.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"


/* $Id: mnrn15.F,v 1.1.1.1 1996/03/07 14:31:31 mclareni Exp $ */

/* $Log: mnrn15.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnrn15_(val, inseed)
doublereal *val;
integer *inseed;
{
    /* Initialized data */

    static integer iseed = 12345;

    static integer k;


/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/*         This is a super-portable random number generator. */
/*         It should not overflow on any 32-bit machine. */
/*         The cycle is only ~10**9, so use with care! */
/*         Note especially that VAL must not be undefined on input. */
/*                    Set Default Starting Seed */
    if (*val == 3.) {
	goto L100;
    }

    *inseed = iseed;
    k = iseed / 53668;
    iseed = (iseed - k * 53668) * 40014 - k * 12211;
    if (iseed < 0) {
	iseed += 2147483563;
    }
    *val = (real) iseed * (float)4.656613e-10;
    return 0;
/*               "entry" to set seed, flag is VAL=3. */
L100:
    iseed = *inseed;
    return 0;
} /* mnrn15_ */

