/* mncomd.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__20 = 20;
static integer c__30 = 30;


/* $Id: mncomd.F,v 1.1.1.1 1996/03/07 14:31:29 mclareni Exp $ */

/* $Log: mncomd.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mncomd_(fcn, crdbin, icondn, futil, crdbin_len)
/* Subroutine */ int (*fcn) ();
char *crdbin;
integer *icondn;
/* Subroutine */ int (*futil) ();
ftnlen crdbin_len;
{
    /* Initialized data */

    static char clower[26+1] = "abcdefghijklmnopqrstuvwxyz";
    static char cupper[26+1] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer i_len();
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe(), s_cmp();

    /* Local variables */
    static integer ierr, ipos, i, llist;
    static doublereal plist[30];
    static integer ic;
    static logical leader;
    static char comand[20], crdbuf[100];
    static integer lenbuf;
    extern /* Subroutine */ int mncrck_(), mnexcm_();
    static integer lnc;

    /* Fortran I/O blocks */
    static cilist io___9 = { 0, 0, 0, "(A)", 0 };
    static cilist io___15 = { 0, 0, 0, "(A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Called by user.  'Reads' a command string and executes. */
/* C     Equivalent to MNEXCM except that the command is given as a */
/* C          character string. */
/* C */
/* C     ICONDN = 0: command executed normally */
/* C              1: command is blank, ignored */
/* C              2: command line unreadable, ignored */
/* C              3: unknown command, ignored */
/* C              4: abnormal termination (e.g., MIGRAD not converged) */
/* C              5: command is a request to read PARAMETER definitions */
/* C              6: 'SET INPUT' command */
/* C              7: 'SET TITLE' command */
/* C              8: 'SET COVAR' command */
/* C              9: reserved */
/* C             10: END command */
/* C             11: EXIT or STOP command */
/* C             12: RETURN command */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */




    lenbuf = i_len(crdbin, crdbin_len);
    s_copy(crdbuf, crdbin, 100L, crdbin_len);
    *icondn = 0;
/*     record not case-sensitive, get upper case, strip leading blanks */
    leader = TRUE_;
    ipos = 1;
    i__1 = min(20,lenbuf);
    for (i = 1; i <= i__1; ++i) {
	if (*(unsigned char *)&crdbuf[i - 1] == '\'') {
	    goto L111;
	}
	if (*(unsigned char *)&crdbuf[i - 1] == ' ') {
	    if (leader) {
		++ipos;
	    }
	    goto L110;
	}
	leader = FALSE_;
	for (ic = 1; ic <= 26; ++ic) {
	    if (*(unsigned char *)&crdbuf[i - 1] == *(unsigned char *)&clower[
		    ic - 1]) {
		*(unsigned char *)&crdbuf[i - 1] = *(unsigned char *)&cupper[
			ic - 1];
	    }
/* L108: */
	}
L110:
	;
    }
L111:
/*                     blank or null command */
    if (ipos > lenbuf) {
	io___9.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___9);
	do_fio(&c__1, " BLANK COMMAND IGNORED.", 23L);
	e_wsfe();
	*icondn = 1;
	goto L900;
    }
/*                                           . .   preemptive commands */
/*               if command is 'PARAMETER' */
    if (s_cmp(crdbuf + (ipos - 1), "PAR", 3L, 3L) == 0) {
	*icondn = 5;
	mn7log_1.lphead = TRUE_;
	goto L900;
    }
/*               if command is 'SET INPUT' */
    if (s_cmp(crdbuf + (ipos - 1), "SET INP", 7L, 7L) == 0) {
	*icondn = 6;
	mn7log_1.lphead = TRUE_;
	goto L900;
    }
/*              if command is 'SET TITLE' */
    if (s_cmp(crdbuf + (ipos - 1), "SET TIT", 7L, 7L) == 0) {
	*icondn = 7;
	mn7log_1.lphead = TRUE_;
	goto L900;
    }
/*               if command is 'SET COVARIANCE' */
    if (s_cmp(crdbuf + (ipos - 1), "SET COV", 7L, 7L) == 0) {
	*icondn = 8;
	mn7log_1.lphead = TRUE_;
	goto L900;
    }
/*               crack the command . . . . . . . . . . . . . . . . */
    mncrck_(crdbuf + (ipos - 1), &c__20, comand, &lnc, &c__30, plist, &llist, 
	    &ierr, &mn7iou_1.isyswr, lenbuf - (ipos - 1), 20L);
    if (ierr > 0) {
	io___15.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___15);
	do_fio(&c__1, " COMMAND CANNOT BE INTERPRETED", 30L);
	e_wsfe();
	*icondn = 2;
	goto L900;
    }

    mnexcm_(fcn, comand, plist, &llist, &ierr, futil, lnc);
    *icondn = ierr;
L900:
    return 0;
} /* mncomd_ */

