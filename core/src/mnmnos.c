/* mnmnos.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__4 = 4;
static integer c__0 = 0;


/* $Id: mnmnos.F,v 1.1.1.1 1996/03/07 14:31:30 mclareni Exp $ */

/* $Log: mnmnos.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mnmnos_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    /* Format strings */
    static char fmt_564[] = "(\002 PARAMETER NUMBER \002,i5,\002 NOT VARIABL\
E. IGNORED.\002)";
    static char fmt_675[] = "(/\002 NEW MINIMUM FOUND.  GO BACK TO MINIMIZAT\
ION STEP.\002/\002 \002,60(\002=\002)/60x,\002V\002/60x,\002V\002/60x,\002\
V\002/57x,\002VVVVVVV\002/58x,\002VVVVV\002/59x,\002VVV\002/60x,\002V\002//)";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();
    /* Subroutine */ int s_copy();

    /* Local variables */
    static integer nbad, ilax, ilax2, ngood;
    static doublereal val2mi, val2pl;
    static integer nfcnmi;
    extern /* Subroutine */ int mnmnot_(), mnprin_(), mnmatu_();
    static integer iin, knt;

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 0, 0, fmt_564, 0 };
    static cilist io___11 = { 0, 0, 0, fmt_675, 0 };
    static cilist io___12 = { 0, 0, 0, "(A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Performs a MINOS error analysis on those parameters for */
/* C        which it is requested on the MINOS command by calling */
/* C        MNMNOT for each parameter requested. */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    if (mn7npr_1.npar <= 0) {
	goto L700;
    }
    ngood = 0;
    nbad = 0;
    nfcnmi = mn7cnv_1.nfcn;
/*                                      . loop over parameters requested 
*/
    i__1 = mn7npr_1.npar;
    for (knt = 1; knt <= i__1; ++knt) {
	if ((integer) mn7arg_1.word7[1] == 0) {
	    ilax = mn7inx_1.nexofi[knt - 1];
	} else {
	    if (knt >= 7) {
		goto L580;
	    }
	    ilax = (integer) mn7arg_1.word7[knt];
	    if (ilax == 0) {
		goto L580;
	    }
	    if (ilax > 0 && ilax <= mn7npr_1.nu) {
		if (mn7inx_1.niofex[ilax - 1] > 0) {
		    goto L565;
		}
	    }
	    io___6.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___6);
	    do_fio(&c__1, (char *)&ilax, (ftnlen)sizeof(integer));
	    e_wsfe();
	    goto L570;
	}
L565:
/*                                         calculate one pair of M E's
 */
	ilax2 = 0;
	mnmnot_(fcn, &ilax, &ilax2, &val2pl, &val2mi, futil);
	if (mn7log_1.lnewmn) {
	    goto L650;
	}
/*                                          update NGOOD and NBAD */
	iin = mn7inx_1.niofex[ilax - 1];
	if (mn7err_1.erp[iin - 1] > 0.) {
	    ++ngood;
	} else {
	    ++nbad;
	}
	if (mn7err_1.ern[iin - 1] < 0.) {
	    ++ngood;
	} else {
	    ++nbad;
	}
L570:
	;
    }
/*                                           end of loop . . . . . . . */
L580:
/*                                        . . . . printout final values . 
*/
    s_copy(mn7tit_1.cfrom, "MINOS   ", 8L, 8L);
    mn7cnv_1.nfcnfr = nfcnmi;
    s_copy(mn7tit_1.cstatu, "UNCHANGED ", 10L, 10L);
    if (ngood == 0 && nbad == 0) {
	goto L700;
    }
    if (ngood > 0 && nbad == 0) {
	s_copy(mn7tit_1.cstatu, "SUCCESSFUL", 10L, 10L);
    }
    if (ngood == 0 && nbad > 0) {
	s_copy(mn7tit_1.cstatu, "FAILURE   ", 10L, 10L);
    }
    if (ngood > 0 && nbad > 0) {
	s_copy(mn7tit_1.cstatu, "PROBLEMS  ", 10L, 10L);
    }
    if (mn7flg_1.isw[4] >= 0) {
	mnprin_(&c__4, &mn7min_1.amin);
    }
    if (mn7flg_1.isw[4] >= 2) {
	mnmatu_(&c__0);
    }
    goto L900;
/*                                        . . . new minimum found . . . . 
*/
L650:
    s_copy(mn7tit_1.cfrom, "MINOS   ", 8L, 8L);
    mn7cnv_1.nfcnfr = nfcnmi;
    s_copy(mn7tit_1.cstatu, "NEW MINIMU", 10L, 10L);
    if (mn7flg_1.isw[4] >= 0) {
	mnprin_(&c__4, &mn7min_1.amin);
    }
    io___11.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___11);
    e_wsfe();
    goto L900;
L700:
    io___12.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___12);
    do_fio(&c__1, " THERE ARE NO MINOS ERRORS TO CALCULATE.", 40L);
    e_wsfe();
L900:
    return 0;
} /* mnmnos_ */

