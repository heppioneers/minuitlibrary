/* mnparm.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__3 = 3;


/* $Id: mnparm.F,v 1.2 1996/03/15 18:02:50 james Exp $ */

/* $Log: mnparm.F,v $ */
/* Revision 1.2  1996/03/15 18:02:50  james */
/*     Modified Files: */
/* mnderi.F eliminate possible division by zero */
/* mnexcm.F suppress print on STOP when print flag=-1 */
/*          set FVAL3 to flag if FCN already called with IFLAG=3 */
/* mninit.F set version 96.03 */
/* mnlims.F remove arguments, not needed */
/* mnmigr.F VLEN -> LENV in debug print statement */
/* mnparm.F move call to MNRSET to after NPAR redefined, to zero all */
/* mnpsdf.F eliminate possible division by zero */
/* mnscan.F suppress printout when print flag =-1 */
/* mnset.F  remove arguments in call to MNLIMS */
/* mnsimp.F fix CSTATU so status is PROGRESS only if new minimum */
/* mnvert.F eliminate possible division by zero */

/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnparm_(k, cnamj, uk, wk, a, b, ierflg, cnamj_len)
integer *k;
char *cnamj;
doublereal *uk, *wk, *a, *b;
integer *ierflg;
ftnlen cnamj_len;
{
    /* Format strings */
    static char fmt_9[] = "(/\002 MINUIT USER ERROR.  PARAMETER NUMBER IS\
\002,i11/\002,  ALLOWED RANGE IS ONE TO\002,i4/)";
    static char fmt_61[] = "(/\002 PARAMETER DEFINITIONS:\002/\002    NO.   \
NAME         VALUE      STEP SIZE      LIMITS\002)";
    static char fmt_82[] = "(1x,i5,1x,\002'\002,a10,\002'\002,1x,g13.5,\002 \
 constant\002)";
    static char fmt_127[] = "(1x,i5,1x,\002'\002,a10,\002'\002,1x,2g13.5,\
\002     no limits\002)";
    static char fmt_132[] = "(1x,i5,1x,\002'\002,a10,\002'\002,1x,2g13.5,2x,\
2g13.5)";
    static char fmt_135[] = "(/\002 MINUIT USER ERROR.   TOO MANY VARIABLE P\
ARAMETERS.\002/\002 THIS VERSION OF MINUIT DIMENSIONED FOR\002,i4//)";

    /* System generated locals */
    address a__1[3];
    integer i__1, i__2[3];
    doublereal d__1, d__2;
    char ch__1[34];

    /* Builtin functions */
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe(), s_wsfi(), e_wsfi();
    /* Subroutine */ int s_cat();
    double sqrt();

    /* Local variables */
    static integer ierr, kint;
    static doublereal vplu;
    static char cnamk[10];
    static doublereal small, gsmin, pinti, vminu;
    static integer in;
    static char chbufi[4];
    static integer ix, ktofix;
    extern /* Subroutine */ int mnwarn_(), mnfree_();
    static doublereal danger;
    static integer lastin;
    extern /* Subroutine */ int mnrset_(), mnpint_();
    static integer kinfix;
    extern /* Subroutine */ int mnfixp_();
    static doublereal sav;
    static integer nvl;
    static doublereal sav2;

    /* Fortran I/O blocks */
    static cilist io___3 = { 0, 0, 0, fmt_9, 0 };
    static cilist io___6 = { 0, 0, 0, "(A)", 0 };
    static cilist io___7 = { 0, 0, 0, fmt_61, 0 };
    static cilist io___8 = { 0, 0, 0, fmt_82, 0 };
    static cilist io___10 = { 0, 0, 0, fmt_127, 0 };
    static cilist io___11 = { 0, 0, 0, fmt_132, 0 };
    static cilist io___12 = { 0, 0, 0, fmt_135, 0 };
    static cilist io___13 = { 0, 0, 0, "(/A,A/A/)", 0 };
    static icilist io___16 = { 0, chbufi, 0, "(I4)", 4, 1 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Called from MNPARS and user-callable */
/* C    Implements one parameter definition, that is: */
/* C          K     (external) parameter number */
/* C          CNAMK parameter name */
/* C          UK    starting value */
/* C          WK    starting step size or uncertainty */
/* C          A, B  lower and upper physical parameter limits */
/* C    and sets up (updates) the parameter lists. */
/* C    Output: IERFLG=0 if no problems */
/* C                  >0 if MNPARM unable to implement definition */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */



    s_copy(cnamk, cnamj, 10L, cnamj_len);
    kint = mn7npr_1.npar;
    if (*k < 1 || *k > mn7npr_1.maxext) {
/*                     parameter number exceeds allowed maximum value 
*/
	io___3.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___3);
	do_fio(&c__1, (char *)&(*k), (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&mn7npr_1.maxext, (ftnlen)sizeof(integer));
	e_wsfe();
	goto L800;
    }
/*                     normal parameter request */
    ktofix = 0;
    if (mn7inx_1.nvarl[*k - 1] < 0) {
	goto L50;
    }
/*         previously defined parameter is being redefined */
/*                                     find if parameter was fixed */
    i__1 = mn7fx1_1.npfix;
    for (ix = 1; ix <= i__1; ++ix) {
	if (mn7fx1_1.ipfix[ix - 1] == *k) {
	    ktofix = *k;
	}
/* L40: */
    }
    if (ktofix > 0) {
	mnwarn_("W", "PARAM DEF", "REDEFINING A FIXED PARAMETER.", 1L, 9L, 
		29L);
	if (kint >= mn7npr_1.maxint) {
	    io___6.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___6);
	    do_fio(&c__1, " CANNOT RELEASE. MAX NPAR EXCEEDED.", 35L);
	    e_wsfe();
	    goto L800;
	}
	i__1 = -(*k);
	mnfree_(&i__1);
    }
/*                       if redefining previously variable parameter */
    if (mn7inx_1.niofex[*k - 1] > 0) {
	kint = mn7npr_1.npar - 1;
    }
L50:

/*                                      . . .print heading */
    if (mn7log_1.lphead && mn7flg_1.isw[4] >= 0) {
	io___7.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___7);
	e_wsfe();
	mn7log_1.lphead = FALSE_;
    }
    if (*wk > 0.) {
	goto L122;
    }
/*                                        . . .constant parameter . . . . 
*/
    if (mn7flg_1.isw[4] >= 0) {
	io___8.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___8);
	do_fio(&c__1, (char *)&(*k), (ftnlen)sizeof(integer));
	do_fio(&c__1, cnamk, 10L);
	do_fio(&c__1, (char *)&(*uk), (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    nvl = 0;
    goto L200;
L122:
    if (*a == 0. && *b == 0.) {
/*                                      variable parameter without lim
its */
	nvl = 1;
	if (mn7flg_1.isw[4] >= 0) {
	    io___10.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___10);
	    do_fio(&c__1, (char *)&(*k), (ftnlen)sizeof(integer));
	    do_fio(&c__1, cnamk, 10L);
	    do_fio(&c__1, (char *)&(*uk), (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*wk), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	}
    } else {
/*                                         variable parameter with lim
its */
	nvl = 4;
	mn7log_1.lnolim = FALSE_;
	if (mn7flg_1.isw[4] >= 0) {
	    io___11.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___11);
	    do_fio(&c__1, (char *)&(*k), (ftnlen)sizeof(integer));
	    do_fio(&c__1, cnamk, 10L);
	    do_fio(&c__1, (char *)&(*uk), (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*wk), (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*a), (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*b), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	}
    }
/*                             . . request for another variable parameter 
*/
    ++kint;
    if (kint > mn7npr_1.maxint) {
	io___12.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___12);
	do_fio(&c__1, (char *)&mn7npr_1.maxint, (ftnlen)sizeof(integer));
	e_wsfe();
	goto L800;
    }
    if (nvl == 1) {
	goto L200;
    }
    if (*a == *b) {
	io___13.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___13);
	do_fio(&c__1, " USER ERROR IN MINUIT PARAMETER", 31L);
	do_fio(&c__1, " DEFINITION", 11L);
	do_fio(&c__1, " UPPER AND LOWER LIMITS EQUAL.", 30L);
	e_wsfe();
	goto L800;
    }
    if (*b < *a) {
	sav = *b;
	*b = *a;
	*a = sav;
	mnwarn_("W", "PARAM DEF", "PARAMETER LIMITS WERE REVERSED.", 1L, 9L, 
		31L);
	if (mn7log_1.lwarn) {
	    mn7log_1.lphead = TRUE_;
	}
    }
    if (*b - *a > (float)1e7) {
	s_wsfi(&io___16);
	do_fio(&c__1, (char *)&(*k), (ftnlen)sizeof(integer));
	e_wsfi();
/* Writing concatenation */
	i__2[0] = 15, a__1[0] = "LIMITS ON PARAM";
	i__2[1] = 4, a__1[1] = chbufi;
	i__2[2] = 15, a__1[2] = " TOO FAR APART.";
	s_cat(ch__1, a__1, i__2, &c__3, 34L);
	mnwarn_("W", "PARAM DEF", ch__1, 1L, 9L, 34L);
	if (mn7log_1.lwarn) {
	    mn7log_1.lphead = TRUE_;
	}
    }
    danger = (*b - *uk) * (*uk - *a);
    if (danger < (float)0.) {
	mnwarn_("W", "PARAM DEF", "STARTING VALUE OUTSIDE LIMITS.", 1L, 9L, 
		30L);
    }
    if (danger == (float)0.) {
	mnwarn_("W", "PARAM DEF", "STARTING VALUE IS AT LIMIT.", 1L, 9L, 27L);
    }
L200:
/*                           . . . input OK, set values, arrange lists, */
/*                                    calculate step sizes GSTEP, DIRIN */
    s_copy(mn7tit_1.cfrom, "PARAMETR", 8L, 8L);
    mn7cnv_1.nfcnfr = mn7cnv_1.nfcn;
    s_copy(mn7tit_1.cstatu, "NEW VALUES", 10L, 10L);
    mn7npr_1.nu = max(mn7npr_1.nu,*k);
    s_copy(mn7nam_1.cpnam + (*k - 1) * 10, cnamk, 10L, 10L);
    mn7ext_1.u[*k - 1] = *uk;
    mn7ext_1.alim[*k - 1] = *a;
    mn7ext_1.blim[*k - 1] = *b;
    mn7inx_1.nvarl[*k - 1] = nvl;
/*                             K is external number of new parameter */
/*           LASTIN is the number of var. params with ext. param. no.< K 
*/
    lastin = 0;
    i__1 = *k - 1;
    for (ix = 1; ix <= i__1; ++ix) {
	if (mn7inx_1.niofex[ix - 1] > 0) {
	    ++lastin;
	}
/* L240: */
    }
/*                 KINT is new number of variable params, NPAR is old */
    if (kint == mn7npr_1.npar) {
	goto L280;
    }
    if (kint > mn7npr_1.npar) {
/*                          insert new variable parameter in list */
	i__1 = lastin + 1;
	for (in = mn7npr_1.npar; in >= i__1; --in) {
	    ix = mn7inx_1.nexofi[in - 1];
	    mn7inx_1.niofex[ix - 1] = in + 1;
	    mn7inx_1.nexofi[in] = ix;
	    mn7int_1.x[in] = mn7int_1.x[in - 1];
	    mn7int_1.xt[in] = mn7int_1.xt[in - 1];
	    mn7int_1.dirin[in] = mn7int_1.dirin[in - 1];
	    mn7der_1.g2[in] = mn7der_1.g2[in - 1];
	    mn7der_1.gstep[in] = mn7der_1.gstep[in - 1];
/* L260: */
	}
    } else {
/*                          remove variable parameter from list */
	i__1 = kint;
	for (in = lastin + 1; in <= i__1; ++in) {
	    ix = mn7inx_1.nexofi[in];
	    mn7inx_1.niofex[ix - 1] = in;
	    mn7inx_1.nexofi[in - 1] = ix;
	    mn7int_1.x[in - 1] = mn7int_1.x[in];
	    mn7int_1.xt[in - 1] = mn7int_1.xt[in];
	    mn7int_1.dirin[in - 1] = mn7int_1.dirin[in];
	    mn7der_1.g2[in - 1] = mn7der_1.g2[in];
	    mn7der_1.gstep[in - 1] = mn7der_1.gstep[in];
/* L270: */
	}
    }
L280:
    ix = *k;
    mn7inx_1.niofex[ix - 1] = 0;
    mn7npr_1.npar = kint;
    mnrset_(&c__1);
/*                                       lists are now arranged . . . . */
    if (nvl > 0) {
	in = lastin + 1;
	mn7inx_1.nexofi[in - 1] = ix;
	mn7inx_1.niofex[ix - 1] = in;
	sav = mn7ext_1.u[ix - 1];
	mnpint_(&sav, &ix, &pinti);
	mn7int_1.x[in - 1] = pinti;
	mn7int_1.xt[in - 1] = mn7int_1.x[in - 1];
	mn7err_1.werr[in - 1] = *wk;
	sav2 = sav + *wk;
	mnpint_(&sav2, &ix, &pinti);
	vplu = pinti - mn7int_1.x[in - 1];
	sav2 = sav - *wk;
	mnpint_(&sav2, &ix, &pinti);
	vminu = pinti - mn7int_1.x[in - 1];
	mn7int_1.dirin[in - 1] = (abs(vplu) + abs(vminu)) * (float).5;
/* Computing 2nd power */
	d__1 = mn7int_1.dirin[in - 1];
	mn7der_1.g2[in - 1] = mn7min_1.up * (float)2. / (d__1 * d__1);
	gsmin = mn7cns_1.epsma2 * (float)8. * (d__1 = mn7int_1.x[in - 1], abs(
		d__1));
/* Computing MAX */
	d__1 = gsmin, d__2 = mn7int_1.dirin[in - 1] * (float).1;
	mn7der_1.gstep[in - 1] = max(d__1,d__2);
	if (mn7min_1.amin != mn7cns_1.undefi) {
	    small = sqrt(mn7cns_1.epsma2 * (mn7min_1.amin + mn7min_1.up) / 
		    mn7min_1.up);
/* Computing MAX */
	    d__1 = gsmin, d__2 = small * mn7int_1.dirin[in - 1];
	    mn7der_1.gstep[in - 1] = max(d__1,d__2);
	}
	mn7der_1.grd[in - 1] = mn7der_1.g2[in - 1] * mn7int_1.dirin[in - 1];
/*                   if parameter has limits */
	if (mn7inx_1.nvarl[*k - 1] > 1) {
	    if (mn7der_1.gstep[in - 1] > (float).5) {
		mn7der_1.gstep[in - 1] = (float).5;
	    }
	    mn7der_1.gstep[in - 1] = -mn7der_1.gstep[in - 1];
	}
    }
    if (ktofix > 0) {
	kinfix = mn7inx_1.niofex[ktofix - 1];
	if (kinfix > 0) {
	    mnfixp_(&kinfix, &ierr);
	}
	if (ierr > 0) {
	    goto L800;
	}
    }
    *ierflg = 0;
    return 0;
/*                   error on input, unable to implement request  . . . . 
*/
L800:
    *ierflg = 1;
    return 0;
} /* mnparm_ */

