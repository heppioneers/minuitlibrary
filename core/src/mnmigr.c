/* mnmigr.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;
static doublereal c_b16 = .05;
static integer c__3 = 3;
static integer c__0 = 0;


/* $Id: mnmigr.F,v 1.2 1996/03/15 18:02:49 james Exp $ */

/* $Log: mnmigr.F,v $ */
/* Revision 1.2  1996/03/15 18:02:49  james */
/*     Modified Files: */
/* mnderi.F eliminate possible division by zero */
/* mnexcm.F suppress print on STOP when print flag=-1 */
/*          set FVAL3 to flag if FCN already called with IFLAG=3 */
/* mninit.F set version 96.03 */
/* mnlims.F remove arguments, not needed */
/* mnmigr.F VLEN -> LENV in debug print statement */
/* mnparm.F move call to MNRSET to after NPAR redefined, to zero all */
/* mnpsdf.F eliminate possible division by zero */
/* mnscan.F suppress printout when print flag =-1 */
/* mnset.F  remove arguments in call to MNLIMS */
/* mnsimp.F fix CSTATU so status is PROGRESS only if new minimum */
/* mnvert.F eliminate possible division by zero */

/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mnmigr_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    /* Format strings */
    static char fmt_470[] = "(\002 START MIGRAD MINIMIZATION.  STRATEGY\002,\
i2,\002.  CONVERGENCE WHEN EDM .LT.\002,e9.2)";

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe();
    double d_sign();

    /* Local variables */
    static doublereal gdel, gami;
    static integer npfn;
    static doublereal flnu[50];
    static integer lenv, ndex, iext, iter;
    static doublereal step[50], dsum, gssq, vsum, d;
    static integer i, j, m, n, npsdf, nparx;
    static doublereal fzero;
    static integer iswtr, lined2, kk;
    static doublereal gs[50], fs, ri, vg[50], delgam;
    static logical ldebug;
    extern /* Subroutine */ int mnamin_();
    static integer nfcnmg;
    static doublereal rhotol;
    extern /* Subroutine */ int mnhess_(), mnwerr_(), mninex_(), mnderi_();
    static integer nrstrt;
    extern /* Subroutine */ int mnline_(), mnwarn_(), mnprin_(), mnmatu_(), 
	    mnpsdf_();
    static doublereal gdgssq, gvg, vgi, xxs[50];

    /* Fortran I/O blocks */
    static cilist io___11 = { 0, 0, 0, fmt_470, 0 };
    static cilist io___19 = { 0, 0, 0, "(A,I3,2G13.3)", 0 };
    static cilist io___22 = { 0, 0, 0, "(A,A/(1X,10G10.2))", 0 };
    static cilist io___36 = { 0, 0, 0, "(A,(1X,10G10.3))", 0 };
    static cilist io___40 = { 0, 0, 0, "(A,F5.1,A)", 0 };
    static cilist io___41 = { 0, 0, 0, "(A,(1X,10G10.3))", 0 };
    static cilist io___43 = { 0, 0, 0, "(A)", 0 };
    static cilist io___44 = { 0, 0, 0, "(A)", 0 };
    static cilist io___45 = { 0, 0, 0, "(A)", 0 };
    static cilist io___46 = { 0, 0, 0, "(A)", 0 };
    static cilist io___47 = { 0, 0, 0, "(A)", 0 };
    static cilist io___48 = { 0, 0, 0, "(/A)", 0 };
    static cilist io___49 = { 0, 0, 0, "(/A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Performs a local function minimization using basically the */
/* C        method of Davidon-Fletcher-Powell as modified by Fletcher */
/* C        ref. -- Fletcher, Comp.J. 13,317 (1970)   "switching method" 
*/
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    if (mn7npr_1.npar <= 0) {
	return 0;
    }
    if (mn7min_1.amin == mn7cns_1.undefi) {
	mnamin_(fcn, futil);
    }
    ldebug = mn7flg_1.idbg[4] >= 1;
    s_copy(mn7tit_1.cfrom, "MIGRAD  ", 8L, 8L);
    mn7cnv_1.nfcnfr = mn7cnv_1.nfcn;
    nfcnmg = mn7cnv_1.nfcn;
    s_copy(mn7tit_1.cstatu, "INITIATE  ", 10L, 10L);
    iswtr = mn7flg_1.isw[4] - (mn7cnv_1.itaur << 1);
    npfn = mn7cnv_1.nfcn;
    nparx = mn7npr_1.npar;
    lenv = mn7npr_1.npar * (mn7npr_1.npar + 1) / 2;
    nrstrt = 0;
    npsdf = 0;
    lined2 = 0;
    mn7flg_1.isw[3] = -1;
    rhotol = mn7min_1.apsi * (float).001;
    if (iswtr >= 1) {
	io___11.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___11);
	do_fio(&c__1, (char *)&mn7cnv_1.istrat, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&rhotol, (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
/*                                           initialization strategy */
    if (mn7cnv_1.istrat < 2 || mn7flg_1.isw[1] >= 3) {
	goto L2;
    }
/*                                come (back) here to restart completely 
*/
L1:
    if (nrstrt > mn7cnv_1.istrat) {
	s_copy(mn7tit_1.cstatu, "FAILED    ", 10L, 10L);
	mn7flg_1.isw[3] = -1;
	goto L230;
    }
/*                                      . get full covariance and gradient
 */
    mnhess_(fcn, futil);
    mnwerr_();
    npsdf = 0;
    if (mn7flg_1.isw[1] >= 1) {
	goto L10;
    }
/*                                        . get gradient at start point */
L2:
    mninex_(mn7int_1.x);
    if (mn7flg_1.isw[2] == 1) {
	(*fcn)(&nparx, mn7der_1.gin, &fzero, mn7ext_1.u, &c__2, futil);
	++mn7cnv_1.nfcn;
    }
    mnderi_(fcn, futil);
    if (mn7flg_1.isw[1] >= 1) {
	goto L10;
    }
/*                                   sometimes start with diagonal matrix 
*/
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	xxs[i - 1] = mn7int_1.x[i - 1];
	step[i - 1] = 0.;
/* L3: */
    }
/*                           do line search if second derivative negative 
*/
    ++lined2;
    if (lined2 < (mn7cnv_1.istrat + 1) * mn7npr_1.npar) {
	i__1 = mn7npr_1.npar;
	for (i = 1; i <= i__1; ++i) {
	    if (mn7der_1.g2[i - 1] > 0.) {
		goto L5;
	    }
	    step[i - 1] = -d_sign(&mn7der_1.gstep[i - 1], &mn7der_1.grd[i - 1]
		    );
	    gdel = step[i - 1] * mn7der_1.grd[i - 1];
	    fs = mn7min_1.amin;
	    mnline_(fcn, xxs, &fs, step, &gdel, &c_b16, futil);
	    mnwarn_("D", "MNMIGR", "Negative G2 line search", 1L, 6L, 23L);
	    iext = mn7inx_1.nexofi[i - 1];
	    if (ldebug) {
		io___19.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___19);
		do_fio(&c__1, " Negative G2 line search, param ", 32L);
		do_fio(&c__1, (char *)&iext, (ftnlen)sizeof(integer));
		do_fio(&c__1, (char *)&fs, (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&mn7min_1.amin, (ftnlen)sizeof(
			doublereal));
		e_wsfe();
	    }
	    goto L2;
L5:
	    ;
	}
    }
/*                           make diagonal error matrix */
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	ndex = i * (i - 1) / 2;
	i__2 = i - 1;
	for (j = 1; j <= i__2; ++j) {
	    ++ndex;
/* L7: */
	    mn7var_1.vhmat[ndex - 1] = (float)0.;
	}
	++ndex;
	if (mn7der_1.g2[i - 1] <= 0.) {
	    mn7der_1.g2[i - 1] = (float)1.;
	}
	mn7var_1.vhmat[ndex - 1] = (float)2. / mn7der_1.g2[i - 1];
/* L8: */
    }
    mn7min_1.dcovar = (float)1.;
    if (ldebug) {
	io___22.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___22);
	do_fio(&c__1, " DEBUG MNMIGR,", 14L);
	do_fio(&c__1, " STARTING MATRIX DIAGONAL,  VHMAT=", 34L);
	i__1 = lenv;
	for (kk = 1; kk <= i__1; ++kk) {
	    do_fio(&c__1, (char *)&mn7var_1.vhmat[kk - 1], (ftnlen)sizeof(
		    doublereal));
	}
	e_wsfe();
    }
/*                                         ready to start first iteration 
*/
L10:
    ++nrstrt;
    if (nrstrt > mn7cnv_1.istrat + 1) {
	s_copy(mn7tit_1.cstatu, "FAILED    ", 10L, 10L);
	goto L230;
    }
    fs = mn7min_1.amin;
/*                                        . . . get EDM and set up loop */
    mn7min_1.edm = (float)0.;
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	gs[i - 1] = mn7der_1.grd[i - 1];
	xxs[i - 1] = mn7int_1.x[i - 1];
	ndex = i * (i - 1) / 2;
	i__2 = i - 1;
	for (j = 1; j <= i__2; ++j) {
	    ++ndex;
/* L17: */
	    mn7min_1.edm += gs[i - 1] * mn7var_1.vhmat[ndex - 1] * gs[j - 1];
	}
	++ndex;
/* L18: */
/* Computing 2nd power */
	d__1 = gs[i - 1];
	mn7min_1.edm += d__1 * d__1 * (float).5 * mn7var_1.vhmat[ndex - 1];
    }
    mn7min_1.edm = mn7min_1.edm * (float).5 * (mn7min_1.dcovar * (float)3. + (
	    float)1.);
    if (mn7min_1.edm < 0.) {
	mnwarn_("W", "MIGRAD", "STARTING MATRIX NOT POS-DEFINITE.", 1L, 6L, 
		33L);
	mn7flg_1.isw[1] = 0;
	mn7min_1.dcovar = (float)1.;
	goto L2;
    }
    if (mn7flg_1.isw[1] == 0) {
	mn7min_1.edm = mn7cns_1.bigedm;
    }
    iter = 0;
    mninex_(mn7int_1.x);
    mnwerr_();
    if (iswtr >= 1) {
	mnprin_(&c__3, &mn7min_1.amin);
    }
    if (iswtr >= 2) {
	mnmatu_(&c__0);
    }
/*                                        . . . . .  start main loop */
L24:
    if (mn7cnv_1.nfcn - npfn >= mn7cnv_1.nfcnmx) {
	goto L190;
    }
    gdel = (float)0.;
    gssq = (float)0.;
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	ri = (float)0.;
/* Computing 2nd power */
	d__1 = gs[i - 1];
	gssq += d__1 * d__1;
	i__2 = mn7npr_1.npar;
	for (j = 1; j <= i__2; ++j) {
	    m = max(i,j);
	    n = min(i,j);
	    ndex = m * (m - 1) / 2 + n;
/* L25: */
	    ri += mn7var_1.vhmat[ndex - 1] * gs[j - 1];
	}
	step[i - 1] = ri * (float)-.5;
/* L30: */
	gdel += step[i - 1] * gs[i - 1];
    }
    if (gssq == 0.) {
	mnwarn_("D", "MIGRAD", " FIRST DERIVATIVES OF FCN ARE ALL ZERO", 1L, 
		6L, 38L);
	goto L300;
    }
/*                 if gdel positive, V not posdef */
    if (gdel >= 0.) {
	mnwarn_("D", "MIGRAD", " NEWTON STEP NOT DESCENT.", 1L, 6L, 25L);
	if (npsdf == 1) {
	    goto L1;
	}
	mnpsdf_();
	npsdf = 1;
	goto L24;
    }
/*                                        . . . . do line search */
    mnline_(fcn, xxs, &fs, step, &gdel, &c_b16, futil);
    if (mn7min_1.amin == fs) {
	goto L200;
    }
    s_copy(mn7tit_1.cfrom, "MIGRAD  ", 8L, 8L);
    mn7cnv_1.nfcnfr = nfcnmg;
    s_copy(mn7tit_1.cstatu, "PROGRESS  ", 10L, 10L);
/*                                        . get gradient at new point */
    mninex_(mn7int_1.x);
    if (mn7flg_1.isw[2] == 1) {
	(*fcn)(&nparx, mn7der_1.gin, &fzero, mn7ext_1.u, &c__2, futil);
	++mn7cnv_1.nfcn;
    }
    mnderi_(fcn, futil);
/*                                         . calculate new EDM */
    npsdf = 0;
L81:
    mn7min_1.edm = (float)0.;
    gvg = (float)0.;
    delgam = (float)0.;
    gdgssq = (float)0.;
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	ri = (float)0.;
	vgi = (float)0.;
	i__2 = mn7npr_1.npar;
	for (j = 1; j <= i__2; ++j) {
	    m = max(i,j);
	    n = min(i,j);
	    ndex = m * (m - 1) / 2 + n;
	    vgi += mn7var_1.vhmat[ndex - 1] * (mn7der_1.grd[j - 1] - gs[j - 1]
		    );
/* L90: */
	    ri += mn7var_1.vhmat[ndex - 1] * mn7der_1.grd[j - 1];
	}
	vg[i - 1] = vgi * (float).5;
	gami = mn7der_1.grd[i - 1] - gs[i - 1];
/* Computing 2nd power */
	d__1 = gami;
	gdgssq += d__1 * d__1;
	gvg += gami * vg[i - 1];
	delgam += mn7int_1.dirin[i - 1] * gami;
/* L100: */
	mn7min_1.edm += mn7der_1.grd[i - 1] * ri * (float).5;
    }
    mn7min_1.edm = mn7min_1.edm * (float).5 * (mn7min_1.dcovar * (float)3. + (
	    float)1.);
/*                          . if EDM negative,  not positive-definite */
    if (mn7min_1.edm < 0. || gvg <= 0.) {
	mnwarn_("D", "MIGRAD", "NOT POS-DEF. EDM OR GVG NEGATIVE.", 1L, 6L, 
		33L);
	s_copy(mn7tit_1.cstatu, "NOT POSDEF", 10L, 10L);
	if (npsdf == 1) {
	    goto L230;
	}
	mnpsdf_();
	npsdf = 1;
	goto L81;
    }
/*                            print information about this iteration */
    ++iter;
    if (iswtr >= 3 || iswtr == 2 && iter % 10 == 1) {
	mnwerr_();
	mnprin_(&c__3, &mn7min_1.amin);
    }
    if (gdgssq == 0.) {
	mnwarn_("D", "MIGRAD", "NO CHANGE IN FIRST DERIVATIVES OVER LAST STEP"
		, 1L, 6L, 45L);
    }
    if (delgam < 0.) {
	mnwarn_("D", "MIGRAD", "FIRST DERIVATIVES INCREASING ALONG SEARCH LI\
NE", 1L, 6L, 46L);
    }
/*                                        .  update covariance matrix */
    s_copy(mn7tit_1.cstatu, "IMPROVEMNT", 10L, 10L);
    if (ldebug) {
	io___36.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___36);
	do_fio(&c__1, " VHMAT 1 =", 10L);
	for (kk = 1; kk <= 10; ++kk) {
	    do_fio(&c__1, (char *)&mn7var_1.vhmat[kk - 1], (ftnlen)sizeof(
		    doublereal));
	}
	e_wsfe();
    }
    dsum = (float)0.;
    vsum = (float)0.;
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	i__2 = i;
	for (j = 1; j <= i__2; ++j) {
	    d = mn7int_1.dirin[i - 1] * mn7int_1.dirin[j - 1] / delgam - vg[i 
		    - 1] * vg[j - 1] / gvg;
	    dsum += abs(d);
	    ndex = i * (i - 1) / 2 + j;
	    mn7var_1.vhmat[ndex - 1] += d * (float)2.;
	    vsum += (d__1 = mn7var_1.vhmat[ndex - 1], abs(d__1));
/* L120: */
	}
    }
/*                smooth local fluctuations by averaging DCOVAR */
    mn7min_1.dcovar = (mn7min_1.dcovar + dsum / vsum) * (float).5;
    if (iswtr >= 3 || ldebug) {
	io___40.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___40);
	do_fio(&c__1, " RELATIVE CHANGE IN COV. MATRIX=", 32L);
	d__1 = mn7min_1.dcovar * (float)100.;
	do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, "%", 1L);
	e_wsfe();
    }
    if (ldebug) {
	io___41.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___41);
	do_fio(&c__1, " VHMAT 2 =", 10L);
	for (kk = 1; kk <= 10; ++kk) {
	    do_fio(&c__1, (char *)&mn7var_1.vhmat[kk - 1], (ftnlen)sizeof(
		    doublereal));
	}
	e_wsfe();
    }
    if (delgam <= gvg) {
	goto L135;
    }
    i__2 = mn7npr_1.npar;
    for (i = 1; i <= i__2; ++i) {
/* L125: */
	flnu[i - 1] = mn7int_1.dirin[i - 1] / delgam - vg[i - 1] / gvg;
    }
    i__2 = mn7npr_1.npar;
    for (i = 1; i <= i__2; ++i) {
	i__1 = i;
	for (j = 1; j <= i__1; ++j) {
	    ndex = i * (i - 1) / 2 + j;
/* L130: */
	    mn7var_1.vhmat[ndex - 1] += gvg * (float)2. * flnu[i - 1] * flnu[
		    j - 1];
	}
    }
L135:
/*                                              and see if converged */
    if (mn7min_1.edm < rhotol * (float).1) {
	goto L300;
    }
/*                                    if not, prepare next iteration */
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	xxs[i - 1] = mn7int_1.x[i - 1];
	gs[i - 1] = mn7der_1.grd[i - 1];
/* L140: */
    }
    fs = mn7min_1.amin;
    if (mn7flg_1.isw[1] == 0 && mn7min_1.dcovar < (float).5) {
	mn7flg_1.isw[1] = 1;
    }
    if (mn7flg_1.isw[1] == 3 && mn7min_1.dcovar > (float).1) {
	mn7flg_1.isw[1] = 1;
    }
    if (mn7flg_1.isw[1] == 1 && mn7min_1.dcovar < (float).05) {
	mn7flg_1.isw[1] = 3;
    }
    goto L24;
/*                                        . . . . .  end main loop */
/*                                         . . call limit in MNMIGR */
L190:
    mn7flg_1.isw[0] = 1;
    if (mn7flg_1.isw[4] >= 0) {
	io___43.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___43);
	do_fio(&c__1, " CALL LIMIT EXCEEDED IN MIGRAD.", 31L);
	e_wsfe();
    }
    s_copy(mn7tit_1.cstatu, "CALL LIMIT", 10L, 10L);
    goto L230;
/*                                         . . fails to improve . . */
L200:
    if (iswtr >= 1) {
	io___44.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___44);
	do_fio(&c__1, " MIGRAD FAILS TO FIND IMPROVEMENT", 33L);
	e_wsfe();
    }
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
/* L210: */
	mn7int_1.x[i - 1] = xxs[i - 1];
    }
    if (mn7min_1.edm < rhotol) {
	goto L300;
    }
    if (mn7min_1.edm < (d__1 = mn7cns_1.epsma2 * mn7min_1.amin, abs(d__1))) {
	if (iswtr >= 0) {
	    io___45.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___45);
	    do_fio(&c__1, " MACHINE ACCURACY LIMITS FURTHER IMPROVEMENT.", 
		    45L);
	    e_wsfe();
	}
	goto L300;
    }
    if (mn7cnv_1.istrat < 1) {
	if (mn7flg_1.isw[4] >= 0) {
	    io___46.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___46);
	    do_fio(&c__1, " MIGRAD FAILS WITH STRATEGY=0.   WILL TRY WITH ST\
RATEGY=1.", 58L);
	    e_wsfe();
	}
	mn7cnv_1.istrat = 1;
    }
    goto L1;
/*                                         . . fails to converge */
L230:
    if (iswtr >= 0) {
	io___47.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___47);
	do_fio(&c__1, " MIGRAD TERMINATED WITHOUT CONVERGENCE.", 39L);
	e_wsfe();
    }
    if (mn7flg_1.isw[1] == 3) {
	mn7flg_1.isw[1] = 1;
    }
    mn7flg_1.isw[3] = -1;
    goto L400;
/*                                         . . apparent convergence */
L300:
    if (iswtr >= 0) {
	io___48.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___48);
	do_fio(&c__1, " MIGRAD MINIMIZATION HAS CONVERGED.", 35L);
	e_wsfe();
    }
    if (mn7cnv_1.itaur == 0) {
	if (mn7cnv_1.istrat >= 2 || mn7cnv_1.istrat == 1 && mn7flg_1.isw[1] < 
		3) {
	    if (mn7flg_1.isw[4] >= 0) {
		io___49.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___49);
		do_fio(&c__1, " MIGRAD WILL VERIFY CONVERGENCE AND ERROR MAT\
RIX.", 49L);
		e_wsfe();
	    }
	    mnhess_(fcn, futil);
	    mnwerr_();
	    npsdf = 0;
	    if (mn7min_1.edm > rhotol) {
		goto L10;
	    }
	}
    }
    s_copy(mn7tit_1.cstatu, "CONVERGED ", 10L, 10L);
    mn7flg_1.isw[3] = 1;
/*                                           come here in any case */
L400:
    s_copy(mn7tit_1.cfrom, "MIGRAD  ", 8L, 8L);
    mn7cnv_1.nfcnfr = nfcnmg;
    mninex_(mn7int_1.x);
    mnwerr_();
    if (iswtr >= 0) {
	mnprin_(&c__3, &mn7min_1.amin);
    }
    if (iswtr >= 1) {
	mnmatu_(&c__1);
    }
    return 0;
} /* mnmigr_ */

