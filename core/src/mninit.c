/* mninit.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;


/* $Id: mninit.F,v 1.4 1997/09/02 15:16:08 mclareni Exp $ */

/* $Log: mninit.F,v $ */
/* Revision 1.4  1997/09/02 15:16:08  mclareni */
/* WINNT corrections */

/* Revision 1.3  1997/03/14 17:18:00  mclareni */
/* WNT mods */

/* Revision 1.2.2.1  1997/01/21 11:33:28  mclareni */
/* All mods for Winnt 96a on winnt branch */

/* Revision 1.2  1996/03/15 18:02:47  james */
/*     Modified Files: */
/* mnderi.F eliminate possible division by zero */
/* mnexcm.F suppress print on STOP when print flag=-1 */
/*          set FVAL3 to flag if FCN already called with IFLAG=3 */
/* mninit.F set version 96.03 */
/* mnlims.F remove arguments, not needed */
/* mnmigr.F VLEN -> LENV in debug print statement */
/* mnparm.F move call to MNRSET to after NPAR redefined, to zero all */
/* mnpsdf.F eliminate possible division by zero */
/* mnscan.F suppress printout when print flag =-1 */
/* mnset.F  remove arguments in call to MNLIMS */
/* mnsimp.F fix CSTATU so status is PROGRESS only if new minimum */
/* mnvert.F eliminate possible division by zero */

/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mninit_(i1, i2, i3)
integer *i1, *i2, *i3;
{
    /* Builtin functions */
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe();
    double sqrt(), atan();

    /* Local variables */
    static doublereal piby2, epsp1;
    static integer i;
    static doublereal dummy, epsbak;
    extern logical intrac_();
    extern /* Subroutine */ int mncler_();
    static doublereal epstry;
    extern /* Subroutine */ int mntiny_();
    static doublereal distnn;
    static integer idb;

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 0, 0, "(A,A,E10.2)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        This is the main initialization subroutine for MINUIT */
/* C     It initializes some constants in common */
/* C                (including the logical I/O unit nos.), */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */



/*            I/O unit numbers */
    mn7iou_1.isysrd = *i1;
    mn7iou_1.isyswr = *i2;
    mn7io2_1.istkwr[0] = mn7iou_1.isyswr;
    mn7io2_1.nstkwr = 1;
    mn7iou_1.isyssa = *i3;
    mn7io2_1.nstkrd = 0;
/*               version identifier */
    s_copy(mn7tit_1.cvrsn, "96.03 ", 6L, 6L);
/*               some CONSTANT constants in COMMON */
    mn7npr_1.maxint = 50;
    mn7npr_1.maxext = 100;
    mn7cns_1.undefi = (float)-54321.;
    mn7cns_1.bigedm = (float)123456.;
    s_copy(mn7tit_1.cundef, ")UNDEFINED", 10L, 10L);
    s_copy(mn7tit_1.covmes, "NO ERROR MATRIX       ", 22L, 22L);
    s_copy(mn7tit_1.covmes + 22, "ERR MATRIX APPROXIMATE", 22L, 22L);
    s_copy(mn7tit_1.covmes + 44, "ERR MATRIX NOT POS-DEF", 22L, 22L);
    s_copy(mn7tit_1.covmes + 66, "ERROR MATRIX ACCURATE ", 22L, 22L);
/*                some starting values in COMMON */
    mn7flg_1.nblock = 0;
    mn7flg_1.icomnd = 0;
    s_copy(mn7tit_1.ctitl, mn7tit_1.cundef, 50L, 10L);
    s_copy(mn7tit_1.cfrom, "INPUT   ", 8L, 8L);
    mn7cnv_1.nfcnfr = mn7cnv_1.nfcn;
    s_copy(mn7tit_1.cstatu, "INITIALIZE", 10L, 10L);
    mn7flg_1.isw[2] = 0;
    mn7flg_1.isw[3] = 0;
    mn7flg_1.isw[4] = 1;
/*         ISW(6)=0 for batch jobs,  =1 for interactive jobs */
/*                      =-1 for originally interactive temporarily batch 
*/
    mn7flg_1.isw[5] = 0;
    if (intrac_(&dummy)) {
	mn7flg_1.isw[5] = 1;
    }
/*        DEBUG options set to default values */
    for (idb = 0; idb <= 10; ++idb) {
/* L10: */
	mn7flg_1.idbg[idb] = 0;
    }
    mn7log_1.lrepor = FALSE_;
    mn7log_1.lwarn = TRUE_;
    mn7log_1.limset = FALSE_;
    mn7log_1.lnewmn = FALSE_;
    mn7cnv_1.istrat = 1;
    mn7cnv_1.itaur = 0;
/*        default page dimensions and 'new page' carriage control integer 
*/
    mn7iou_1.npagwd = 120;
    mn7iou_1.npagln = 56;
    mn7iou_1.newpag = 1;
    if (mn7flg_1.isw[5] > 0) {
	mn7iou_1.npagwd = 80;
	mn7iou_1.npagln = 30;
	mn7iou_1.newpag = 0;
    }
    mn7min_1.up = (float)1.;
    mn7cns_1.updflt = mn7min_1.up;
/*                   determine machine accuracy epsmac */
    epstry = (float).5;
    for (i = 1; i <= 100; ++i) {
	epstry *= (float).5;
	epsp1 = epstry + 1.;
	mntiny_(&epsp1, &epsbak);
	if (epsbak < epstry) {
	    goto L35;
	}
/* L33: */
    }
    epstry = (float)1e-7;
    mn7cns_1.epsmac = epstry * (float)4.;
    io___7.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___7);
    do_fio(&c__1, " MNINIT UNABLE TO DETERMINE", 27L);
    do_fio(&c__1, " ARITHMETIC PRECISION. WILL ASSUME:", 35L);
    do_fio(&c__1, (char *)&mn7cns_1.epsmac, (ftnlen)sizeof(doublereal));
    e_wsfe();
L35:
    mn7cns_1.epsmac = epstry * (float)8.;
    mn7cns_1.epsma2 = sqrt(mn7cns_1.epsmac) * (float)2.;
/*                 the vlims are a non-negligible distance from pi/2 */
/*         used by MNPINT to set variables "near" the physical limits */
    piby2 = atan((float)1.) * (float)2.;
    distnn = sqrt(mn7cns_1.epsma2) * (float)8.;
    mn7cns_1.vlimhi = piby2 - distnn;
    mn7cns_1.vlimlo = -piby2 + distnn;
    mncler_();
/*      WRITE (ISYSWR,'(3A,I3,A,I3,A,E10.2)')  '  MINUIT RELEASE ',CVRSN, 
*/
/*     +' INITIALIZED.   DIMENSIONS ',MNE,'/',MNI,'  EPSMAC=',EPSMAC */
    return 0;
} /* mninit_ */

