/* mnhelp.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"


/* $Id: mnhelp.F,v 1.1.1.1 1996/03/07 14:31:30 mclareni Exp $ */

/* $Log: mnhelp.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mnhelp_(comd, lout, comd_len)
char *comd;
integer *lout;
ftnlen comd_len;
{
    /* Format strings */
    static char fmt_10000[] = "(\002   ==>List of MINUIT Interactive command\
s:\002,/,\002 CLEar     Reset all parameter names and values undefined\002,/,\
\002 CONtour   Make contour map of the user function\002,/,\002 EXIT      Ex\
it from Interactive Minuit\002,/,\002 FIX       Cause parameter(s) to remain\
 constant\002,/,\002 HESse     Calculate the Hessian or error matrix.\002,/\
,\002 IMPROVE   Search for a new minimum around current minimum\002,/,\002 M\
IGrad    Minimize by the method of Migrad\002,/,\002 MINImize  MIGRAD + SIMP\
LEX method if Migrad fails\002,/,\002 MINOs     Exact (non-linear) parameter\
 error analysis\002)";
    static char fmt_10001[] = "(\002 MNContour Calculate one MINOS function \
contour\002,/,\002 PARameter Define or redefine new parameters and values\
\002,/,\002 RELease   Make previously FIXed parameters variable again\002,/\
,\002 REStore   Release last parameter fixed\002,/,\002 SAVe      Save curre\
nt parameter values on a file\002,/,\002 SCAn      Scan the user function by\
 varying parameters\002,/,\002 SEEk      Minimize by the method of Monte Car\
lo\002,/,\002 SET       Set various MINUIT constants or conditions\002,/,\
\002 SHOw      Show values of current constants or conditions\002,/,\002 SIM\
plex   Minimize by the method of Simplex\002)";
    static char fmt_10100[] = "(\002 ***>CLEAR\002,/,\002 Resets all paramet\
er names and values to undefined.\002,/,\002 Must normally be followed by a \
PARameters command or \002,/,\002 equivalent, in order to define parameter v\
alues.\002)";
    static char fmt_10200[] = "(\002 ***>CONTOUR <par1>  <par2>  [devs]  [ng\
rid]\002,/,\002 Instructs Minuit to trace contour lines of the user functio\
n\002,/,\002 with respect to the two parameters whose external numbers\002,/,\
\002 are <par1> and <par2>.\002,/,\002 Other variable parameters of the func\
tion, if any, will have\002,/,\002 their values fixed at the current values \
during the contour\002,/,\002 tracing. The optional parameter [devs] (defaul\
t value 2.)\002,/,\002 gives the number of standard deviations in each param\
eter\002,/,\002 which should lie entirely within the plotting area.\002,/\
,\002 Optional parameter [ngrid] (default value 25 unless page\002,/,\002 si\
ze is too small) determines the resolution of the plot,\002,/,\002 i.e. the \
number of rows and columns of the grid at which the\002,/,\002 function will\
 be evaluated. [See also MNContour.]\002)";
    static char fmt_10300[] = "(\002 ***>END\002,/,\002 Signals the end of a\
 data block (i.e., the end of a fit),\002,/,\002 and implies that execution \
should continue, because another\002,/,\002 Data Block follows. A Data Block\
 is a set of Minuit data\002,/,\002 consisting of\002,/,\002     (1) A Title,\
\002,/,\002     (2) One or more Parameter Definitions,\002,/,\002     (3) A \
blank line, and\002,/,\002     (4) A set of Minuit Commands.\002,/,\002 The \
END command is used when more than one Data Block is to\002,/,\002 be used w\
ith the same FCN function. It first causes Minuit\002,/,\002 to issue a CALL\
 FCN with IFLAG=3, in order to allow FCN to\002,/,\002 perform any calculati\
ons associated with the final fitted\002,/,\002 parameter values, unless a C\
ALL FCN 3 command has already\002,/,\002 been executed at the current FCN va\
lue.\002)";
    static char fmt_10400[] = "(\002 ***>EXIT\002,/,\002 Signals the end of \
execution.\002,/,\002 The EXIT command first causes Minuit to issue a CALL F\
CN\002,/,\002 with IFLAG=3, to allow FCN to perform any calculations\002,/\
,\002 associated with the final fitted parameter values, unless a\002,/,\002\
 CALL FCN 3 command has already been executed.\002)";
    static char fmt_10500[] = "(\002 ***>FIX} <parno> [parno] ... [parno]\
\002,/,\002 Causes parameter(s) <parno> to be removed from the list of\002,/,\
\002 variable parameters, and their value(s) will remain constant\002,/,\002\
 during subsequent minimizations, etc., until another command\002,/,\002 cha\
nges their value(s) or status.\002)";
    static char fmt_10600[] = "(\002 ***>HESse  [maxcalls]\002,/,\002 Calcul\
ate, by finite differences, the Hessian or error matrix.\002,/,\002  That is\
, it calculates the full matrix of second derivatives\002,/,\002 of the func\
tion with respect to the currently variable\002,/,\002 parameters, and inver\
ts it, printing out the resulting error\002,/,\002 matrix. The optional argu\
ment [maxcalls] specifies the\002,/,\002 (approximate) maximum number of fun\
ction calls after which\002,/,\002 the calculation will be stopped.\002)";
    static char fmt_10700[] = "(\002 ***>IMPROVE  [maxcalls]\002,/,\002 If a\
 previous minimization has converged, and the current\002,/,\002 values of t\
he parameters therefore correspond to a local\002,/,\002 minimum of the func\
tion, this command requests a search for\002,/,\002 additional distinct loca\
l minima.\002,/,\002 The optional argument [maxcalls] specifies the (approxi\
mate)\002,/,\002 maximum number of function calls after which the calculation\
\002,/,\002 will be stopped.\002)";
    static char fmt_10800[] = "(\002 ***>MIGrad  [maxcalls]  [tolerance]\002\
,/,\002 Causes minimization of the function by the method of Migrad,\002,/\
,\002 the most efficient and complete single method, recommended\002,/,\002 \
for general functions (see also MINImize).\002,/,\002 The minimization produ\
ces as a by-product the error matrix\002,/,\002 of the parameters, which is \
usually reliable unless warning\002,/,\002 messages are produced.\002,/,\002\
 The optional argument [maxcalls] specifies the (approximate)\002,/,\002 max\
imum number of function calls after which the calculation\002,/,\002 will be\
 stopped even if it has not yet converged.\002,/,\002 The optional argument \
[tolerance] specifies required tolerance\002,/,\002 on the function value at\
 the minimum.\002,/,\002 The default tolerance is 0.1, and the minimization \
will stop\002,/,\002 when the estimated vertical distance to the minimum (ED\
M) is\002,/,\002 less than 0.001*[tolerance]*UP (see [SET ERRordef]).\002)";
    static char fmt_10900[] = "(\002 ***>MINImize  [maxcalls] [tolerance]\
\002,/,\002 Causes minimization of the function by the method of Migrad,\002\
,/,\002 as does the MIGrad command, but switches to the SIMplex method\002,/,\
\002 if Migrad fails to converge. Arguments are as for MIGrad.\002,/,\002 No\
te that command requires four characters to be unambiguous.\002)";
    static char fmt_11000[] = "(\002 ***>MINOs  [maxcalls]  [parno] [parno] \
...\002,/,\002 Causes a Minos error analysis to be performed on the paramete\
rs\002,/,\002 whose numbers [parno] are specified. If none are specified,\
\002,/,\002 Minos errors are calculated for all variable parameters.\002,/\
,\002 Minos errors may be expensive to calculate, but are very\002,/,\002 re\
liable since they take account of non-linearities in the\002,/,\002 problem \
as well as parameter correlations, and are in general\002,/\002 asymmetric\
.\002,/,\002 The optional argument [maxcalls] specifies the (approximate)\
\002,/,\002 maximum number of function calls per parameter requested,\002,/\
,\002 after which the calculation will stop for that parameter.\002)";
    static char fmt_11100[] = "(\002 ***>MNContour  <par1> <par2> [npts]\002\
,/,\002 Calculates one function contour of FCN with respect to\002,/,\002 pa\
rameters par1 and par2, with FCN minimized always with\002,/,\002 respect to\
 all other NPAR-2 variable parameters (if any).\002,/,\002 Minuit will try t\
o find npts points on the contour (default 20)\002,/,\002 If only two parame\
ters are variable at the time, it is not\002,/,\002 necessary to specify the\
ir numbers. To calculate more than\002,/,\002 one contour, it is necessary t\
o SET ERRordef to the appropriate\002,/,\002 value and issue the MNContour c\
ommand for each contour.\002)";
    static char fmt_11150[] = "(\002 ***>PARameters\002,/,\002 followed by o\
ne or more parameter definitions.\002,/,\002 Parameter definitions are of th\
e form:\002,/,\002   <number>  'name'  <value>  <step>  [lolim] [uplim] \002\
,/,\002 for example:\002,/,\002  3  'K width'  1.2   0.1\002,/,\002 the last\
 definition is followed by a blank line or a zero.\002)";
    static char fmt_11200[] = "(\002 ***>RELease  <parno> [parno] ... [par\
no]\002,/,\002 If <parno> is the number of a previously variable paramete\
r\002,/,\002 which has been fixed by a command: FIX <parno>, then that\002,/,\
\002 parameter will return to variable status.  Otherwise a warning\002,/\
,\002 message is printed and the command is ignored.\002,/,\002 Note that th\
is command operates only on parameters which were\002,/\002 at one time vari\
able and have been FIXed. It cannot make\002,/,\002 constant parameters vari\
able; that must be done by redefining\002,/\002 the parameter with a PARamet\
ers command.\002)";
    static char fmt_11300[] = "(\002 ***>REStore  [code]\002,/,\002 If no [c\
ode] is specified, this command restores all previously\002,/,\002 FIXed par\
ameters to variable status. If [code]=1, then only\002,/,\002 the last param\
eter FIXed is restored to variable status.\002,/,\002 If code is neither zer\
o nor one, the command is ignored.\002)";
    static char fmt_11400[] = "(\002 ***>RETURN\002,/,\002 Signals the end o\
f a data block, and instructs Minuit to return\002,/,\002 to the program whi\
ch called it. The RETurn command first\002,/,\002 causes Minuit to CALL FCN \
with IFLAG=3, in order to allow FCN\002,/,\002 to perform any calculations a\
ssociated with the final fitted\002,/,\002 parameter values, unless a CALL F\
CN 3 command has already been\002,/,\002 executed at the current FCN value\
.\002)";
    static char fmt_11500[] = "(\002 ***>SAVe\002,/,\002 Causes the current \
parameter values to be saved on a file in\002,/,\002 such a format that they\
 can be read in again as Minuit\002,/,\002 parameter definitions. If the cov\
ariance matrix exists, it is\002,/,\002 also output in such a format. The un\
it number is by default 7,\002,/,\002 or that specified by the user in his c\
all to MINTIO or\002,/,\002 MNINIT. The user is responsible for opening the \
file previous\002,/,\002 to issuing the [SAVe] command (except where this ca\
n be done\002,/,\002 interactively).\002)";
    static char fmt_11600[] = "(\002 ***>SCAn  [parno]  [numpts] [from]  [\
to]\002,/,\002 Scans the value of the user function by varying parameter\002\
,/,\002 number [parno], leaving all other parameters fixed at the\002,/,\002\
 current value. If [parno] is not specified, all variable\002,/,\002 paramet\
ers are scanned in sequence.\002,/,\002 The number of points [numpts] in the\
 scan is 40 by default,\002,/,\002 and cannot exceed 100. The range of the s\
can is by default\002,/,\002 2 standard deviations on each side of the curre\
nt best value,\002,/,\002 but can be specified as from [from] to [to].\002,/,\
\002 After each scan, if a new minimum is found, the best parameter\002,/\
,\002 values are retained as start values for future scans or\002,/,\002 min\
imizations. The curve resulting from each scan is plotted\002,/,\002 on the \
output unit in order to show the approximate behaviour\002,/,\002 of the fun\
ction.\002,/,\002 This command is not intended for minimization, but is some\
times\002,/,\002 useful for debugging the user function or finding a\002,/\
,\002 reasonable starting point.\002)";
    static char fmt_11700[] = "(\002 ***>SEEk  [maxcalls]  [devs]\002,/,\002\
 Causes a Monte Carlo minimization of the function, by choosing\002,/,\002 r\
andom values of the variable parameters, chosen uniformly\002,/,\002 over a \
hypercube centered at the current best value.\002,/,\002 The region size is \
by default 3 standard deviations on each\002,/,\002 side, but can be changed\
 by specifying the value of [devs].\002)";
    static char fmt_11800[] = "(\002 ***>SET <option_name>\002,/,/,\002  SET\
 BATch\002,/,\002    Informs Minuit that it is running in batch mode.\002,//,\
\002  SET EPSmachine  <accuracy>\002,/,\002    Informs Minuit that the relat\
ive floating point arithmetic\002,/\002    precision is <accuracy>. Minuit d\
etermines the nominal\002,/,\002    precision itself, but the SET EPSmachine\
 command can be\002,/,\002    used to override Minuit own determination, whe\
n the user\002,/,\002    knows that the FCN function value is not calculated\
 to\002,/,\002    the nominal machine accuracy. Typical values of <accuracy\
>\002,/\002    are between 10**-5 and 10**-14.\002)";
    static char fmt_11801[] = "(/,\002  SET ERRordef  <up>\002,/,\002    Set\
s the value of UP (default value= 1.), defining parameter\002,/,\002    erro\
rs. Minuit defines parameter errors as the change\002,/,\002    in parameter\
 value required to change the function value\002,/,\002    by UP. Normally, \
for chisquared fits UP=1, and for negative\002,/,\002    log likelihood, UP=\
0.5.\002)";
    static char fmt_11802[] = "(/,\002   SET GRAdient  [force]\002,/,\002   \
 Informs Minuit that the user function is prepared to\002,/,\002    calculat\
e its own first derivatives and return their values\002,/,\002    in the arr\
ay GRAD when IFLAG=2 (see specs of FCN).\002,/,\002    If [force] is not spe\
cified, Minuit will calculate\002,/,\002    the FCN derivatives by finite di\
fferences at the current\002,/,\002    point and compare with the user calcu\
lation at that point,\002,/,\002    accepting the user values only if they a\
gree.\002,/,\002    If [force]=1, Minuit does not do its own derivative\002,\
/,\002    calculation, and uses the derivatives calculated in FCN.\002)";
    static char fmt_11803[] = "(/,\002   SET INPut  [unitno]  [filename]\002\
,/,\002    Causes Minuit, in data-driven mode only, to read subsequent\002,/,\
\002    commands (or parameter definitions) from a different input\002,/,\
\002    file. If no [unitno] is specified, reading reverts to the\002,/,\002\
    previous input file, assuming that there was one.\002,/,\002    If [unit\
no] is specified, and that unit has not been opened,\002,/,\002    then Minu\
it attempts to open the file [filename]} if a\002,/,\002    name is specifie\
d. If running in interactive mode and\002,/,\002    [filename] is not specif\
ied and [unitno] is not opened,\002,/,\002    Minuit prompts the user to ent\
er a file name.\002,/,\002    If the word REWIND is added to the command (no\
te:no blanks\002,/\002    between INPUT and REWIND), the file is rewound bef\
ore\002,/,\002    reading. Note that this command is implemented in standar\
d\002,/\002    Fortran 77 and the results may depend on the  system;\002,/\
,\002    for example, if a filename is given under VM/CMS, it must\002,/,\
\002    be preceeded by a slash.\002)";
    static char fmt_11804[] = "(/,\002   SET INTeractive\002,/,\002    Infor\
ms Minuit that it is running interactively.\002)";
    static char fmt_11805[] = "(/,\002   SET LIMits  [parno]  [lolim]  [upli\
m]\002,/,\002    Allows the user to change the limits on one or all\002,/\
,\002    parameters. If no arguments are specified, all limits are\002,/,\
\002    removed from all parameters. If [parno] alone is specified,\002,/\
,\002    limits are removed from parameter [parno].\002,/,\002    If all arg\
uments are specified, then parameter [parno] will\002,/,\002    be bounded b\
etween [lolim] and [uplim].\002,/,\002    Limits can be specified in either \
order, Minuit will take\002,/,\002    the smaller as [lolim] and the larger \
as [uplim].\002,/,\002    However, if [lolim] is equal to [uplim], an error \
condition\002,/,\002    results.\002)";
    static char fmt_11806[] = "(/,\002   SET LINesperpage\002,/,\002     Set\
s the number of lines for one page of output.\002,/,\002     Default value i\
s 24 for interactive mode\002)";
    static char fmt_11807[] = "(/,\002   SET NOGradient\002,/,\002    The in\
verse of Cind{SET GRAdient}, instructs Minuit not to\002,/,\002    use the f\
irst derivatives calculated by the user in FCN.\002)";
    static char fmt_11808[] = "(/,\002   SET NOWarnings\002,/,\002    Supres\
ses Minuit warning messages.\002)";
    static char fmt_11809[] = "(/,\002   SET OUTputfile  <unitno>\002,/,\002\
    Instructs Minuit to write further output to unit <unitno>.\002)";
    static char fmt_11810[] = "(/,\002   SET PAGethrow  <integer>\002,/,\002\
    Sets the carriage control character for ``new page' to\002,/,\002    <in\
teger>. Thus the value 1 produces a new page, and 0\002,/,\002    produces a\
 blank line, on some devices (see TOPofpage)\002)";
    static char fmt_11811[] = "(/,\002   SET PARameter  <parno>  <value>\002\
,/,\002    Sets the value of parameter <parno> to <value>.\002,/,\002    The\
 parameter in question may be variable, fixed, or\002,/,\002    constant, bu\
t must be defined.\002)";
    static char fmt_11812[] = "(/,\002   SET PRIntout  <level>\002,/,\002   \
 Sets the print level, determining how much output will be\002,/,\002    pro\
duced. Allowed values and their meanings are displayed\002,/,\002    after a\
 SHOw PRInt command, and are currently <level>=:\002,/,\002      [-1]  no ou\
tput except from SHOW commands\002,/,\002       [0]  minimum output\002,/\
,\002       [1]  default value, normal output\002,/,\002       [2]  addition\
al output giving intermediate results.\002,/,\002       [3]  maximum output,\
 showing progress of minimizations.\002,/\002    Note: See also the SET WARn\
ings command.\002)";
    static char fmt_11813[] = "(/,\002   SET RANdomgenerator  <seed>\002,/\
,\002    Sets the seed of the random number generator used in SEEk.\002,/\
\002    This can be any integer between 10000 and 900000000, for\002,/,\002 \
   example one which was output from a SHOw RANdom command of\002,/\002    a\
 previous run.\002)";
    static char fmt_11814[] = "(/,\002   SET STRategy  <level>\002,/,\002   \
 Sets the strategy to be used in calculating first and second\002,/,\002    \
derivatives and in certain minimization methods.\002,/,\002    In general, l\
ow values of <level> mean fewer function calls\002,/,\002    and high values\
 mean more reliable minimization.\002,/,\002    Currently allowed values are\
 0, 1 (default), and 2.\002)";
    static char fmt_11815[] = "(/,\002   SET TITle\002,/,\002    Informs Min\
uit that the next input line is to be considered\002,/,\002    the (new) tit\
le for this task or sub-task.  This is for\002,/,\002    the convenience of \
the user in reading his output.\002)";
    static char fmt_11816[] = "(/,\002   SET WARnings\002,/,\002    Instruct\
s Minuit to output warning messages when suspicious\002,/,\002    conditions\
 arise which may indicate unreliable results.\002,/\002    This is the defau\
lt.\002)";
    static char fmt_11817[] = "(/,\002    SET WIDthpage\002,/,\002    Inform\
s Minuit of the output page width.\002,/,\002    Default values are 80 for i\
nteractive jobs\002)";
    static char fmt_11900[] = "(\002 ***>SHOw  <option_name>\002,/,\002  All\
 SET XXXX commands have a corresponding SHOw XXXX command.\002,/,\002  In ad\
dition, the SHOw commands listed starting here have no\002,/,\002  correspon\
ding SET command for obvious reasons.\002)";
    static char fmt_11901[] = "(/,\002   SHOw CORrelations\002,/,\002    Cal\
culates and prints the parameter correlations from the\002,/,\002    error m\
atrix.\002)";
    static char fmt_11902[] = "(/,\002   SHOw COVariance\002,/,\002    Print\
s the (external) covariance (error) matrix.\002)";
    static char fmt_11903[] = "(/,\002   SHOw EIGenvalues\002,/,\002    Calc\
ulates and prints the eigenvalues of the covariance\002,/,\002    matrix.\
\002)";
    static char fmt_11904[] = "(/,\002   SHOw FCNvalue\002,/,\002    Prints \
the current value of FCN.\002)";
    static char fmt_12000[] = "(\002 ***>SIMplex  [maxcalls]  [tolerance]\
\002,/,\002 Performs a function minimization using the simplex method of\002\
,/\002 Nelder and Mead. Minimization terminates either when the\002,/,\002 f\
unction has been called (approximately) [maxcalls] times,\002,/,\002 or when\
 the estimated vertical distance to minimum (EDM) is\002,/,\002 less than [t\
olerance].\002,/,\002 The default value of [tolerance] is 0.1*UP(see SET ERR\
ordef).\002)";
    static char fmt_12100[] = "(\002 ***>STAndard\002,/,\002 Causes Minuit t\
o execute the Fortran instruction CALL STAND\002,/,\002 where STAND is a sub\
routine supplied by the user.\002)";
    static char fmt_12200[] = "(\002 ***>STOP\002,/,\002 Same as EXIT.\002)";
    static char fmt_12300[] = "(\002 ***>TOPofpage\002,/,\002 Causes Minuit \
to write the character specified in a\002,/,\002 SET PAGethrow command (defa\
ult = 1) to column 1 of the output\002,/,\002 file, which may or may not pos\
ition your output medium to\002,/,\002 the top of a page depending on the de\
vice and system.\002)";
    static char fmt_13000[] = "(\002 Unknown MINUIT command. Type HELP for l\
ist of commands.\002)";

    /* Builtin functions */
    integer s_wsfe(), e_wsfe();
    /* Subroutine */ int s_copy();
    integer s_cmp();

    /* Local variables */
    static char cmd3[3];

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 0, 0, fmt_10000, 0 };
    static cilist io___2 = { 0, 0, 0, fmt_10001, 0 };
    static cilist io___4 = { 0, 0, 0, fmt_10100, 0 };
    static cilist io___5 = { 0, 0, 0, fmt_10200, 0 };
    static cilist io___6 = { 0, 0, 0, fmt_10300, 0 };
    static cilist io___7 = { 0, 0, 0, fmt_10400, 0 };
    static cilist io___8 = { 0, 0, 0, fmt_10500, 0 };
    static cilist io___9 = { 0, 0, 0, fmt_10600, 0 };
    static cilist io___10 = { 0, 0, 0, fmt_10700, 0 };
    static cilist io___11 = { 0, 0, 0, fmt_10800, 0 };
    static cilist io___12 = { 0, 0, 0, fmt_10900, 0 };
    static cilist io___13 = { 0, 0, 0, fmt_11000, 0 };
    static cilist io___14 = { 0, 0, 0, fmt_11100, 0 };
    static cilist io___15 = { 0, 0, 0, fmt_11150, 0 };
    static cilist io___16 = { 0, 0, 0, fmt_11200, 0 };
    static cilist io___17 = { 0, 0, 0, fmt_11300, 0 };
    static cilist io___18 = { 0, 0, 0, fmt_11400, 0 };
    static cilist io___19 = { 0, 0, 0, fmt_11500, 0 };
    static cilist io___20 = { 0, 0, 0, fmt_11600, 0 };
    static cilist io___21 = { 0, 0, 0, fmt_11700, 0 };
    static cilist io___22 = { 0, 0, 0, fmt_11800, 0 };
    static cilist io___23 = { 0, 0, 0, fmt_11801, 0 };
    static cilist io___24 = { 0, 0, 0, fmt_11802, 0 };
    static cilist io___25 = { 0, 0, 0, fmt_11803, 0 };
    static cilist io___26 = { 0, 0, 0, fmt_11804, 0 };
    static cilist io___27 = { 0, 0, 0, fmt_11805, 0 };
    static cilist io___28 = { 0, 0, 0, fmt_11806, 0 };
    static cilist io___29 = { 0, 0, 0, fmt_11807, 0 };
    static cilist io___30 = { 0, 0, 0, fmt_11808, 0 };
    static cilist io___31 = { 0, 0, 0, fmt_11809, 0 };
    static cilist io___32 = { 0, 0, 0, fmt_11810, 0 };
    static cilist io___33 = { 0, 0, 0, fmt_11811, 0 };
    static cilist io___34 = { 0, 0, 0, fmt_11812, 0 };
    static cilist io___35 = { 0, 0, 0, fmt_11813, 0 };
    static cilist io___36 = { 0, 0, 0, fmt_11814, 0 };
    static cilist io___37 = { 0, 0, 0, fmt_11815, 0 };
    static cilist io___38 = { 0, 0, 0, fmt_11816, 0 };
    static cilist io___39 = { 0, 0, 0, fmt_11817, 0 };
    static cilist io___40 = { 0, 0, 0, fmt_11900, 0 };
    static cilist io___41 = { 0, 0, 0, fmt_11901, 0 };
    static cilist io___42 = { 0, 0, 0, fmt_11902, 0 };
    static cilist io___43 = { 0, 0, 0, fmt_11903, 0 };
    static cilist io___44 = { 0, 0, 0, fmt_11904, 0 };
    static cilist io___45 = { 0, 0, 0, fmt_12000, 0 };
    static cilist io___46 = { 0, 0, 0, fmt_12100, 0 };
    static cilist io___47 = { 0, 0, 0, fmt_12200, 0 };
    static cilist io___48 = { 0, 0, 0, fmt_12300, 0 };
    static cilist io___49 = { 0, 0, 0, fmt_13000, 0 };


/* . */
/* .         HELP routine for MINUIT interactive commands. */
/* . */
/* .      COMD ='*   '  prints a global help for all commands */
/* .      COMD =Command_name: print detailed help for one command. */
/*.          Note that at least 3 characters must be given for the command
 name.*/
/* . */
/* .     Author: Rene Brun */
/*             comments extracted from the MINUIT documentation file. */
/* . */
/* . */
/* -- command name ASSUMED to be in upper case */
/* __________________________________________________________________ */
/* -- */
/* --  Global HELP: Summary of all commands */
/* --  ==================================== */
/* -- */
    if (*(unsigned char *)comd == '*') {
	io___1.ciunit = *lout;
	s_wsfe(&io___1);
	e_wsfe();
	io___2.ciunit = *lout;
	s_wsfe(&io___2);
	e_wsfe();
	goto L99;
    }

    s_copy(cmd3, comd, 3L, 3L);
/* __________________________________________________________________ */
/* -- */
/* --  Command CLEAR */
/* --  ============= */
/* . */
    if (s_cmp(cmd3, "CLE", 3L, 3L) == 0) {
	io___4.ciunit = *lout;
	s_wsfe(&io___4);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command CONTOUR */
/* --  =============== */
/* . */
    if (s_cmp(cmd3, "CON", 3L, 3L) == 0) {
	io___5.ciunit = *lout;
	s_wsfe(&io___5);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command END */
/* --  =========== */
/* . */
    if (s_cmp(cmd3, "END", 3L, 3L) == 0) {
	io___6.ciunit = *lout;
	s_wsfe(&io___6);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* . */
/* -- */
/* --  Command EXIT */
/* --  ============ */
    if (s_cmp(cmd3, "EXI", 3L, 3L) == 0) {
	io___7.ciunit = *lout;
	s_wsfe(&io___7);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command FIX */
/* --  =========== */
/* . */
    if (s_cmp(cmd3, "FIX", 3L, 3L) == 0) {
	io___8.ciunit = *lout;
	s_wsfe(&io___8);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command HESSE */
/* --  ============= */
/* . */
    if (s_cmp(cmd3, "HES", 3L, 3L) == 0) {
	io___9.ciunit = *lout;
	s_wsfe(&io___9);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command IMPROVE */
/* --  =============== */
/* . */
    if (s_cmp(cmd3, "IMP", 3L, 3L) == 0) {
	io___10.ciunit = *lout;
	s_wsfe(&io___10);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command MIGRAD */
/* --  ============== */
/* . */
    if (s_cmp(cmd3, "MIG", 3L, 3L) == 0) {
	io___11.ciunit = *lout;
	s_wsfe(&io___11);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command MINIMIZE */
/* --  ================ */
/* . */
    if (s_cmp(comd, "MINI", 4L, 4L) == 0) {
	io___12.ciunit = *lout;
	s_wsfe(&io___12);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command MINOS */
/* --  ============= */
/* . */
    if (s_cmp(comd, "MINO", 4L, 4L) == 0) {
	io___13.ciunit = *lout;
	s_wsfe(&io___13);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command MNCONTOUR */
/* --  ================= */
/* . */
    if (s_cmp(cmd3, "MNC", 3L, 3L) == 0) {
	io___14.ciunit = *lout;
	s_wsfe(&io___14);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command PARAMETER */
/* --  ================= */
/* . */
    if (s_cmp(cmd3, "PAR", 3L, 3L) == 0) {
	io___15.ciunit = *lout;
	s_wsfe(&io___15);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command RELEASE */
/* --  =============== */
/* . */
    if (s_cmp(cmd3, "REL", 3L, 3L) == 0) {
	io___16.ciunit = *lout;
	s_wsfe(&io___16);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command RESTORE */
/* --  =============== */
/* . */
    if (s_cmp(cmd3, "RES", 3L, 3L) == 0) {
	io___17.ciunit = *lout;
	s_wsfe(&io___17);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command RETURN */
/* --  ============== */
/* . */
    if (s_cmp(cmd3, "RET", 3L, 3L) == 0) {
	io___18.ciunit = *lout;
	s_wsfe(&io___18);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command SAVE */
/* --  ============ */
/* . */
    if (s_cmp(cmd3, "SAV", 3L, 3L) == 0) {
	io___19.ciunit = *lout;
	s_wsfe(&io___19);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command SCAN */
/* --  ============ */
/* . */
    if (s_cmp(cmd3, "SCA", 3L, 3L) == 0) {
	io___20.ciunit = *lout;
	s_wsfe(&io___20);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command SEEK */
/* --  ============ */
/* . */
    if (s_cmp(cmd3, "SEE", 3L, 3L) == 0) {
	io___21.ciunit = *lout;
	s_wsfe(&io___21);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command SET */
/* --  =========== */
/* . */
    if (s_cmp(cmd3, "SET", 3L, 3L) == 0) {
	io___22.ciunit = *lout;
	s_wsfe(&io___22);
	e_wsfe();
	io___23.ciunit = *lout;
	s_wsfe(&io___23);
	e_wsfe();
	io___24.ciunit = *lout;
	s_wsfe(&io___24);
	e_wsfe();
	io___25.ciunit = *lout;
	s_wsfe(&io___25);
	e_wsfe();
	io___26.ciunit = *lout;
	s_wsfe(&io___26);
	e_wsfe();
	io___27.ciunit = *lout;
	s_wsfe(&io___27);
	e_wsfe();
	io___28.ciunit = *lout;
	s_wsfe(&io___28);
	e_wsfe();
	io___29.ciunit = *lout;
	s_wsfe(&io___29);
	e_wsfe();
	io___30.ciunit = *lout;
	s_wsfe(&io___30);
	e_wsfe();
	io___31.ciunit = *lout;
	s_wsfe(&io___31);
	e_wsfe();
	io___32.ciunit = *lout;
	s_wsfe(&io___32);
	e_wsfe();
	io___33.ciunit = *lout;
	s_wsfe(&io___33);
	e_wsfe();
	io___34.ciunit = *lout;
	s_wsfe(&io___34);
	e_wsfe();
	io___35.ciunit = *lout;
	s_wsfe(&io___35);
	e_wsfe();
	io___36.ciunit = *lout;
	s_wsfe(&io___36);
	e_wsfe();
	io___37.ciunit = *lout;
	s_wsfe(&io___37);
	e_wsfe();
	io___38.ciunit = *lout;
	s_wsfe(&io___38);
	e_wsfe();
	io___39.ciunit = *lout;
	s_wsfe(&io___39);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command SHOW */
/* --  ============ */
/* . */
    if (s_cmp(cmd3, "SHO", 3L, 3L) == 0) {
	io___40.ciunit = *lout;
	s_wsfe(&io___40);
	e_wsfe();
	io___41.ciunit = *lout;
	s_wsfe(&io___41);
	e_wsfe();
	io___42.ciunit = *lout;
	s_wsfe(&io___42);
	e_wsfe();
	io___43.ciunit = *lout;
	s_wsfe(&io___43);
	e_wsfe();
	io___44.ciunit = *lout;
	s_wsfe(&io___44);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command SIMPLEX */
/* --  =============== */
/* . */
    if (s_cmp(cmd3, "SIM", 3L, 3L) == 0) {
	io___45.ciunit = *lout;
	s_wsfe(&io___45);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command STANDARD */
/* --  ================ */
/* . */
    if (s_cmp(cmd3, "STA", 3L, 3L) == 0) {
	io___46.ciunit = *lout;
	s_wsfe(&io___46);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command STOP */
/* --  ============ */
/* . */
    if (s_cmp(cmd3, "STO", 3L, 3L) == 0) {
	io___47.ciunit = *lout;
	s_wsfe(&io___47);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */
/* -- */
/* --  Command TOPOFPAGE */
/* --  ================= */
/* . */
    if (s_cmp(cmd3, "TOP", 3L, 3L) == 0) {
	io___48.ciunit = *lout;
	s_wsfe(&io___48);
	e_wsfe();
	goto L99;
    }
/* __________________________________________________________________ */

    io___49.ciunit = *lout;
    s_wsfe(&io___49);
    e_wsfe();

L99:
    return 0;
} /* mnhelp_ */

