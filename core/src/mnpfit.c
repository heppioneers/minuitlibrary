/* mnpfit.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"


/* $Id: mnpfit.F,v 1.1.1.1 1996/03/07 14:31:31 mclareni Exp $ */

/* $Log: mnpfit.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnpfit_(parx2p, pary2p, npar2p, coef2p, sdev2p)
doublereal *parx2p, *pary2p;
integer *npar2p;
doublereal *coef2p, *sdev2p;
{
    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Local variables */
    static doublereal a, f;
    static integer i;
    static doublereal s, t, y, s2, x2, x3, x4, y2, cz[3], xm, xy, x2y;


/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */

/*     to fit a parabola to npar2p points */

/*   npar2p   no. of points */
/*   parx2p(i)   x value of point i */
/*   pary2p(i)   y value of point i */

/*   coef2p(1...3)  coefficients of the fitted parabola */
/*   y=coef2p(1) + coef2p(2)*x + coef2p(3)*x**2 */
/*   sdev2p= variance */
/*   method : chi**2 = min equation solved explicitly */

    /* Parameter adjustments */
    --coef2p;
    --pary2p;
    --parx2p;

    /* Function Body */
    for (i = 1; i <= 3; ++i) {
/* L3: */
	cz[i - 1] = (float)0.;
    }
    *sdev2p = (float)0.;
    if (*npar2p < 3) {
	goto L10;
    }
    f = (doublereal) (*npar2p);
/* --- center x values for reasons of machine precision */
    xm = (float)0.;
    i__1 = *npar2p;
    for (i = 1; i <= i__1; ++i) {
/* L2: */
	xm += parx2p[i];
    }
    xm /= f;
    x2 = (float)0.;
    x3 = (float)0.;
    x4 = (float)0.;
    y = (float)0.;
    y2 = (float)0.;
    xy = (float)0.;
    x2y = (float)0.;
    i__1 = *npar2p;
    for (i = 1; i <= i__1; ++i) {
	s = parx2p[i] - xm;
	t = pary2p[i];
	s2 = s * s;
	x2 += s2;
	x3 += s * s2;
	x4 += s2 * s2;
	y += t;
	y2 += t * t;
	xy += s * t;
	x2y += s2 * t;
/* L1: */
    }
/* Computing 2nd power */
    d__1 = x2;
/* Computing 2nd power */
    d__2 = x3;
    a = (f * x4 - d__1 * d__1) * x2 - f * (d__2 * d__2);
    if (a == (float)0.) {
	goto L10;
    }
    cz[2] = (x2 * (f * x2y - x2 * y) - f * x3 * xy) / a;
    cz[1] = (xy - x3 * cz[2]) / x2;
    cz[0] = (y - x2 * cz[2]) / f;
    if (*npar2p == 3) {
	goto L6;
    }
    *sdev2p = y2 - (cz[0] * y + cz[1] * xy + cz[2] * x2y);
    if (*sdev2p < (float)0.) {
	*sdev2p = (float)0.;
    }
    *sdev2p /= f - (float)3.;
L6:
    cz[0] += xm * (xm * cz[2] - cz[1]);
    cz[1] -= xm * (float)2. * cz[2];
L10:
    for (i = 1; i <= 3; ++i) {
/* L11: */
	coef2p[i] = cz[i - 1];
    }
    return 0;
} /* mnpfit_ */

