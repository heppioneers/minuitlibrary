/* mntiny.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"


/* $Id: mntiny.F,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: mntiny.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */


/* Subroutine */ int mntiny_(epsp1, epsbak)
doublereal *epsp1, *epsbak;
{

/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Compares its argument with the value 1.0, and returns */
/* C        the value .TRUE. if they are equal.  To find EPSMAC */
/* C        safely by foiling the Fortran optimizer */
/* C */
    *epsbak = *epsp1 - 1.;
    return 0;
} /* mntiny_ */

