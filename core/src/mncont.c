/* mncont.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;


/* $Id: mncont.F,v 1.1.1.1 1996/03/07 14:31:29 mclareni Exp $ */

/* $Log: mncont.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mncont_(fcn, ke1, ke2, nptu, xptu, yptu, ierrf, futil)
/* Subroutine */ int (*fcn) ();
integer *ke1, *ke2, *nptu;
doublereal *xptu, *yptu;
integer *ierrf;
/* Subroutine */ int (*futil) ();
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1, d__2, d__3, d__4;

    /* Builtin functions */
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe();

    /* Local variables */
    static integer nall, iold, line, mpar, ierr, inew;
    static doublereal dist, xdir, ydir, aopt;
    static integer move, next;
    static doublereal u1min, u2min;
    static integer i, j;
    static doublereal w[50], abest;
    static integer nfcol, iercr;
    static doublereal scalx, scaly;
    static integer idist, npcol, kints, i2, i1;
    static doublereal a1, a2, val2mi, val2pl, dc, sclfac;
    static integer lr;
    static doublereal bigdis;
    static logical ldebug;
    static integer nfcnco;
    extern /* Subroutine */ int mnfree_();
    static integer ki1, ki2;
    extern /* Subroutine */ int mncuve_();
    static integer ki3, ke3;
    extern /* Subroutine */ int mnmnot_(), mnwarn_();
    static integer nowpts;
    extern /* Subroutine */ int mnplot_();
    static doublereal sigsav;
    static integer istrav, nfmxin;
    extern /* Subroutine */ int mnfixp_(), mncros_(), mninex_();
    static doublereal gcc[50];
    static integer isw2, isw4;

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 0, 0, "(1X,A,I4,A)", 0 };
    static cilist io___10 = { 0, 0, 0, "(1X,A,I3,2X,A)", 0 };
    static cilist io___11 = { 0, 0, 0, "(1X,A,I3,A)", 0 };
    static cilist io___18 = { 0, 0, 0, "(A)", 0 };
    static cilist io___48 = { 0, 0, 0, "(A,A,I3,A)", 0 };
    static cilist io___50 = { 0, 0, 0, "(A,I3,2X,A)", 0 };
    static cilist io___51 = { 0, 0, 0, "(25X,A,I3,2X,A)", 0 };
    static cilist io___54 = { 0, 0, 0, "(/I5,A,G13.5,A,G11.3)", 0 };
    static cilist io___55 = { 0, 0, 0, "(9X,A,3X,A,18X,A,3X,A)", 0 };
    static cilist io___58 = { 0, 0, 0, "(1X,I5,2G13.5,10X,I5,2G13.5)", 0 };
    static cilist io___59 = { 0, 0, 0, "(1X,I5,2G13.5)", 0 };
    static cilist io___60 = { 0, 0, 0, "(A)", 0 };
    static cilist io___61 = { 0, 0, 0, "(A)", 0 };
    static cilist io___62 = { 0, 0, 0, "(A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C       Find NPTU points along a contour where the function */
/* C             FMIN (X(KE1),X(KE2)) =  AMIN+UP */
/* C       where FMIN is the minimum of FCN with respect to all */
/* C       the other NPAR-2 variable parameters (if any). */
/* C   IERRF on return will be equal to the number of points found: */
/* C     NPTU if normal termination with NPTU points found */
/* C     -1   if errors in the calling sequence (KE1, KE2 not variable) */
/* C      0   if less than four points can be found (using MNMNOT) */
/* C     n>3  if only n points can be found (n < NPTU) */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


/*                 input arguments: parx, pary, devs, ngrid */
    /* Parameter adjustments */
    --yptu;
    --xptu;

    /* Function Body */
    ldebug = mn7flg_1.idbg[6] >= 1;
    if (*ke1 <= 0 || *ke2 <= 0) {
	goto L1350;
    }
    if (*ke1 > mn7npr_1.nu || *ke2 > mn7npr_1.nu) {
	goto L1350;
    }
    ki1 = mn7inx_1.niofex[*ke1 - 1];
    ki2 = mn7inx_1.niofex[*ke2 - 1];
    if (ki1 <= 0 || ki2 <= 0) {
	goto L1350;
    }
    if (ki1 == ki2) {
	goto L1350;
    }
    if (*nptu < 4) {
	goto L1400;
    }

    nfcnco = mn7cnv_1.nfcn;
    mn7cnv_1.nfcnmx = (*nptu + 5) * 100 * (mn7npr_1.npar + 1);
/*           The minimum */
    mncuve_(fcn, futil);
    u1min = mn7ext_1.u[*ke1 - 1];
    u2min = mn7ext_1.u[*ke2 - 1];
    *ierrf = 0;
    s_copy(mn7tit_1.cfrom, "MNContour ", 8L, 10L);
    mn7cnv_1.nfcnfr = nfcnco;
    if (mn7flg_1.isw[4] >= 0) {
	io___7.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___7);
	do_fio(&c__1, "START MNCONTOUR CALCULATION OF", 30L);
	do_fio(&c__1, (char *)&(*nptu), (ftnlen)sizeof(integer));
	do_fio(&c__1, " POINTS ON CONTOUR.", 19L);
	e_wsfe();
	if (mn7npr_1.npar > 2) {
	    if (mn7npr_1.npar == 3) {
		ki3 = 6 - ki1 - ki2;
		ke3 = mn7inx_1.nexofi[ki3 - 1];
		io___10.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___10);
		do_fio(&c__1, "EACH POINT IS A MINIMUM WITH RESPECT TO PARAM\
ETER ", 50L);
		do_fio(&c__1, (char *)&ke3, (ftnlen)sizeof(integer));
		do_fio(&c__1, mn7nam_1.cpnam + (ke3 - 1) * 10, 10L);
		e_wsfe();
	    } else {
		io___11.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___11);
		do_fio(&c__1, "EACH POINT IS A MINIMUM WITH RESPECT TO THE O\
THER", 49L);
		i__1 = mn7npr_1.npar - 2;
		do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
		do_fio(&c__1, " VARIABLE PARAMETERS.", 21L);
		e_wsfe();
	    }
	}
    }

/*           Find the first four points using MNMNOT */
/*              ........................ first two points */
    mnmnot_(fcn, ke1, ke2, &val2pl, &val2mi, futil);
    if (mn7err_1.ern[ki1 - 1] == mn7cns_1.undefi) {
	xptu[1] = mn7ext_1.alim[*ke1 - 1];
	mnwarn_("W", "MNContour ", "Contour squeezed by parameter limits.", 
		1L, 10L, 37L);
    } else {
	if (mn7err_1.ern[ki1 - 1] >= 0.) {
	    goto L1500;
	}
	xptu[1] = u1min + mn7err_1.ern[ki1 - 1];
    }
    yptu[1] = val2mi;

    if (mn7err_1.erp[ki1 - 1] == mn7cns_1.undefi) {
	xptu[3] = mn7ext_1.blim[*ke1 - 1];
	mnwarn_("W", "MNContour ", "Contour squeezed by parameter limits.", 
		1L, 10L, 37L);
    } else {
	if (mn7err_1.erp[ki1 - 1] <= 0.) {
	    goto L1500;
	}
	xptu[3] = u1min + mn7err_1.erp[ki1 - 1];
    }
    yptu[3] = val2pl;
    scalx = (float)1. / (xptu[3] - xptu[1]);
/*              ........................... next two points */
    mnmnot_(fcn, ke2, ke1, &val2pl, &val2mi, futil);
    if (mn7err_1.ern[ki2 - 1] == mn7cns_1.undefi) {
	yptu[2] = mn7ext_1.alim[*ke2 - 1];
	mnwarn_("W", "MNContour ", "Contour squeezed by parameter limits.", 
		1L, 10L, 37L);
    } else {
	if (mn7err_1.ern[ki2 - 1] >= 0.) {
	    goto L1500;
	}
	yptu[2] = u2min + mn7err_1.ern[ki2 - 1];
    }
    xptu[2] = val2mi;
    if (mn7err_1.erp[ki2 - 1] == mn7cns_1.undefi) {
	yptu[4] = mn7ext_1.blim[*ke2 - 1];
	mnwarn_("W", "MNContour ", "Contour squeezed by parameter limits.", 
		1L, 10L, 37L);
    } else {
	if (mn7err_1.erp[ki2 - 1] <= 0.) {
	    goto L1500;
	}
	yptu[4] = u2min + mn7err_1.erp[ki2 - 1];
    }
    xptu[4] = val2pl;
    scaly = (float)1. / (yptu[4] - yptu[2]);
    nowpts = 4;
    next = 5;
    if (ldebug) {
	io___18.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___18);
	do_fio(&c__1, " Plot of four points found by MINOS", 35L);
	e_wsfe();
	mn7rpt_1.xpt[0] = u1min;
	mn7rpt_1.ypt[0] = u2min;
	*(unsigned char *)&mn7cpt_1.chpt[0] = ' ';
/* Computing MIN */
	i__1 = nowpts + 1;
	nall = min(i__1,101);
	i__1 = nall;
	for (i = 2; i <= i__1; ++i) {
	    mn7rpt_1.xpt[i - 1] = xptu[i - 1];
	    mn7rpt_1.ypt[i - 1] = yptu[i - 1];
/* L85: */
	}
	*(unsigned char *)&mn7cpt_1.chpt[1] = 'A';
	*(unsigned char *)&mn7cpt_1.chpt[2] = 'B';
	*(unsigned char *)&mn7cpt_1.chpt[3] = 'C';
	*(unsigned char *)&mn7cpt_1.chpt[4] = 'D';
	mnplot_(mn7rpt_1.xpt, mn7rpt_1.ypt, mn7cpt_1.chpt, &nall, &
		mn7iou_1.isyswr, &mn7iou_1.npagwd, &mn7iou_1.npagln, 1L);
    }

/*               ..................... save some values before fixing */
    isw2 = mn7flg_1.isw[1];
    isw4 = mn7flg_1.isw[3];
    sigsav = mn7min_1.edm;
    istrav = mn7cnv_1.istrat;
    dc = mn7min_1.dcovar;
    mn7min_1.apsi = mn7min_1.epsi * (float).5;
    abest = mn7min_1.amin;
    mpar = mn7npr_1.npar;
    nfmxin = mn7cnv_1.nfcnmx;
    i__1 = mpar;
    for (i = 1; i <= i__1; ++i) {
/* L125: */
	mn7int_1.xt[i - 1] = mn7int_1.x[i - 1];
    }
    i__1 = mpar * (mpar + 1) / 2;
    for (j = 1; j <= i__1; ++j) {
/* L130: */
	mn7vat_1.vthmat[j - 1] = mn7var_1.vhmat[j - 1];
    }
    i__1 = mpar;
    for (i = 1; i <= i__1; ++i) {
	gcc[i - 1] = mn7err_1.globcc[i - 1];
/* L135: */
	w[i - 1] = mn7err_1.werr[i - 1];
    }
/*                           fix the two parameters in question */
    kints = mn7inx_1.niofex[*ke1 - 1];
    mnfixp_(&kints, &ierr);
    kints = mn7inx_1.niofex[*ke2 - 1];
    mnfixp_(&kints, &ierr);
/*               ......................Fill in the rest of the points */
    i__1 = *nptu;
    for (inew = next; inew <= i__1; ++inew) {
/*            find the two neighbouring points with largest separation
 */
	bigdis = (float)0.;
	i__2 = inew - 1;
	for (iold = 1; iold <= i__2; ++iold) {
	    i2 = iold + 1;
	    if (i2 == inew) {
		i2 = 1;
	    }
/* Computing 2nd power */
	    d__1 = scalx * (xptu[iold] - xptu[i2]);
/* Computing 2nd power */
	    d__2 = scaly * (yptu[iold] - yptu[i2]);
	    dist = d__1 * d__1 + d__2 * d__2;
	    if (dist > bigdis) {
		bigdis = dist;
		idist = iold;
	    }
/* L200: */
	}
	i1 = idist;
	i2 = i1 + 1;
	if (i2 == inew) {
	    i2 = 1;
	}
/*                   next point goes between I1 and I2 */
	a1 = .5;
	a2 = .5;
L300:
	mn7xcr_1.xmidcr = a1 * xptu[i1] + a2 * xptu[i2];
	mn7xcr_1.ymidcr = a1 * yptu[i1] + a2 * yptu[i2];
	xdir = yptu[i2] - yptu[i1];
	ydir = xptu[i1] - xptu[i2];
/* Computing MAX */
	d__3 = (d__1 = xdir * scalx, abs(d__1)), d__4 = (d__2 = ydir * scaly, 
		abs(d__2));
	sclfac = max(d__3,d__4);
	mn7xcr_1.xdircr = xdir / sclfac;
	mn7xcr_1.ydircr = ydir / sclfac;
	mn7xcr_1.ke1cr = *ke1;
	mn7xcr_1.ke2cr = *ke2;
/*                Find the contour crossing point along DIR */
	mn7min_1.amin = abest;
	mncros_(fcn, &aopt, &iercr, futil);
	if (iercr > 1) {
/*              If cannot find mid-point, try closer to point 1 */
	    if (a1 > .5) {
		if (mn7flg_1.isw[4] >= 0) {
		    io___48.ciunit = mn7iou_1.isyswr;
		    s_wsfe(&io___48);
		    do_fio(&c__1, " MNCONT CANNOT FIND NEXT", 24L);
		    do_fio(&c__1, " POINT ON CONTOUR.  ONLY ", 25L);
		    do_fio(&c__1, (char *)&nowpts, (ftnlen)sizeof(integer));
		    do_fio(&c__1, " POINTS FOUND.", 14L);
		    e_wsfe();
		}
		goto L950;
	    }
	    mnwarn_("W", "MNContour ", "Cannot find midpoint, try closer.", 
		    1L, 10L, 33L);
	    a1 = (float).75;
	    a2 = (float).25;
	    goto L300;
	}
/*                Contour has been located, insert new point in list 
*/
	i__2 = i1 + 1;
	for (move = nowpts; move >= i__2; --move) {
	    xptu[move + 1] = xptu[move];
	    yptu[move + 1] = yptu[move];
/* L830: */
	}
	++nowpts;
	xptu[i1 + 1] = mn7xcr_1.xmidcr + mn7xcr_1.xdircr * aopt;
	yptu[i1 + 1] = mn7xcr_1.ymidcr + mn7xcr_1.ydircr * aopt;
/* L900: */
    }
L950:

    *ierrf = nowpts;
    s_copy(mn7tit_1.cstatu, "SUCCESSFUL", 10L, 10L);
    if (nowpts < *nptu) {
	s_copy(mn7tit_1.cstatu, "INCOMPLETE", 10L, 10L);
    }
/*                make a lineprinter plot of the contour */
    if (mn7flg_1.isw[4] >= 0) {
	mn7rpt_1.xpt[0] = u1min;
	mn7rpt_1.ypt[0] = u2min;
	*(unsigned char *)&mn7cpt_1.chpt[0] = ' ';
/* Computing MIN */
	i__1 = nowpts + 1;
	nall = min(i__1,101);
	i__1 = nall;
	for (i = 2; i <= i__1; ++i) {
	    mn7rpt_1.xpt[i - 1] = xptu[i - 1];
	    mn7rpt_1.ypt[i - 1] = yptu[i - 1];
	    *(unsigned char *)&mn7cpt_1.chpt[i - 1] = 'X';
/* L1000: */
	}
	io___50.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___50);
	do_fio(&c__1, " Y-AXIS: PARAMETER ", 19L);
	do_fio(&c__1, (char *)&(*ke2), (ftnlen)sizeof(integer));
	do_fio(&c__1, mn7nam_1.cpnam + (*ke2 - 1) * 10, 10L);
	e_wsfe();
	mnplot_(mn7rpt_1.xpt, mn7rpt_1.ypt, mn7cpt_1.chpt, &nall, &
		mn7iou_1.isyswr, &mn7iou_1.npagwd, &mn7iou_1.npagln, 1L);
	io___51.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___51);
	do_fio(&c__1, "X-AXIS: PARAMETER ", 18L);
	do_fio(&c__1, (char *)&(*ke1), (ftnlen)sizeof(integer));
	do_fio(&c__1, mn7nam_1.cpnam + (*ke1 - 1) * 10, 10L);
	e_wsfe();
    }
/*                 print out the coordinates around the contour */
    if (mn7flg_1.isw[4] >= 1) {
	npcol = (nowpts + 1) / 2;
	nfcol = nowpts / 2;
	io___54.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___54);
	do_fio(&c__1, (char *)&nowpts, (ftnlen)sizeof(integer));
	do_fio(&c__1, " POINTS ON CONTOUR.   FMIN=", 27L);
	do_fio(&c__1, (char *)&abest, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, "   ERRDEF=", 10L);
	do_fio(&c__1, (char *)&mn7min_1.up, (ftnlen)sizeof(doublereal));
	e_wsfe();
	io___55.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___55);
	do_fio(&c__1, mn7nam_1.cpnam + (*ke1 - 1) * 10, 10L);
	do_fio(&c__1, mn7nam_1.cpnam + (*ke2 - 1) * 10, 10L);
	do_fio(&c__1, mn7nam_1.cpnam + (*ke1 - 1) * 10, 10L);
	do_fio(&c__1, mn7nam_1.cpnam + (*ke2 - 1) * 10, 10L);
	e_wsfe();
	i__1 = nfcol;
	for (line = 1; line <= i__1; ++line) {
	    lr = line + npcol;
	    io___58.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___58);
	    do_fio(&c__1, (char *)&line, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&xptu[line], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&yptu[line], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&lr, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&xptu[lr], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&yptu[lr], (ftnlen)sizeof(doublereal));
	    e_wsfe();
/* L1050: */
	}
	if (nfcol < npcol) {
	    io___59.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___59);
	    do_fio(&c__1, (char *)&npcol, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&xptu[npcol], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&yptu[npcol], (ftnlen)sizeof(doublereal));
	    e_wsfe();
	}
    }
/*                                    . . contour finished. reset v */
    mn7cnv_1.itaur = 1;
    mnfree_(&c__1);
    mnfree_(&c__1);
    i__1 = mpar * (mpar + 1) / 2;
    for (j = 1; j <= i__1; ++j) {
/* L1100: */
	mn7var_1.vhmat[j - 1] = mn7vat_1.vthmat[j - 1];
    }
    i__1 = mpar;
    for (i = 1; i <= i__1; ++i) {
	mn7err_1.globcc[i - 1] = gcc[i - 1];
	mn7err_1.werr[i - 1] = w[i - 1];
/* L1120: */
	mn7int_1.x[i - 1] = mn7int_1.xt[i - 1];
    }
    mninex_(mn7int_1.x);
    mn7min_1.edm = sigsav;
    mn7min_1.amin = abest;
    mn7flg_1.isw[1] = isw2;
    mn7flg_1.isw[3] = isw4;
    mn7min_1.dcovar = dc;
    mn7cnv_1.itaur = 0;
    mn7cnv_1.nfcnmx = nfmxin;
    mn7cnv_1.istrat = istrav;
    mn7ext_1.u[*ke1 - 1] = u1min;
    mn7ext_1.u[*ke2 - 1] = u2min;
    goto L2000;
/*                                     Error returns */
L1350:
    io___60.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___60);
    do_fio(&c__1, " INVALID PARAMETER NUMBERS.", 27L);
    e_wsfe();
    goto L1450;
L1400:
    io___61.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___61);
    do_fio(&c__1, " LESS THAN FOUR POINTS REQUESTED.", 33L);
    e_wsfe();
L1450:
    *ierrf = -1;
    s_copy(mn7tit_1.cstatu, "USER ERROR", 10L, 10L);
    goto L2000;
L1500:
    io___62.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___62);
    do_fio(&c__1, " MNCONT UNABLE TO FIND FOUR POINTS.", 35L);
    e_wsfe();
    mn7ext_1.u[*ke1 - 1] = u1min;
    mn7ext_1.u[*ke2 - 1] = u2min;
    *ierrf = 0;
    s_copy(mn7tit_1.cstatu, "FAILED", 10L, 6L);
L2000:
    s_copy(mn7tit_1.cfrom, "MNContour ", 8L, 10L);
    mn7cnv_1.nfcnfr = nfcnco;
    return 0;
} /* mncont_ */

