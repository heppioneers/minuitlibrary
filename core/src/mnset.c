/* mnset.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__0 = 0;
static integer c__5 = 5;
static integer c__3 = 3;
static integer c__4 = 4;


/* $Id: mnset.F,v 1.2 1996/03/15 18:02:52 james Exp $ */

/* $Log: mnset.F,v $ */
/* Revision 1.2  1996/03/15 18:02:52  james */
/*     Modified Files: */
/* mnderi.F eliminate possible division by zero */
/* mnexcm.F suppress print on STOP when print flag=-1 */
/*          set FVAL3 to flag if FCN already called with IFLAG=3 */
/* mninit.F set version 96.03 */
/* mnlims.F remove arguments, not needed */
/* mnmigr.F VLEN -> LENV in debug print statement */
/* mnparm.F move call to MNRSET to after NPAR redefined, to zero all */
/* mnpsdf.F eliminate possible division by zero */
/* mnscan.F suppress printout when print flag =-1 */
/* mnset.F  remove arguments in call to MNLIMS */
/* mnsimp.F fix CSTATU so status is PROGRESS only if new minimum */
/* mnvert.F eliminate possible division by zero */

/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mnset_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    /* Initialized data */

    static char cname[10*30+1] = "FCN value PARametersLIMits    COVarianceCO\
RrelatioPRInt levlNOGradientGRAdient  ERRor def INPut fileWIDth pageLINes pa\
geNOWarningsWARnings  RANdom genTITle     STRategy  EIGenvaluePAGe throwMINo\
s errsEPSmachineOUTputfileBATch     INTeractivVERsion   reserve   NODebug   \
DEBug     SHOw      SET       ";
    static integer nname = 25;
    static integer nntot = 30;
    static char cprlev[34*5+1] = "-1: NO OUTPUT EXCEPT FROM \"SHOW\"   0: RE\
DUCED OUTPUT                 1: NORMAL OUTPUT                  2: EXTRA OUTP\
UT FOR PROBLEM CASES 3: MAXIMUM OUTPUT                ";
    static char cstrat[44*3+1] = " 0: MINIMIZE THE NUMBER OF CALLS TO FUNCTI\
ON 1: TRY TO BALANCE SPEED AGAINST RELIABILITY 2: MAKE SURE MINIMUM TRUE, ER\
RORS CORRECT  ";
    static char cdbopt[40*7+1] = "REPORT ALL EXCEPTIONAL CONDITIONS       MN\
LINE: LINE SEARCH MINIMIZATION        MNDERI: FIRST DERIVATIVE CALCULATIONS \
  MNHESS: SECOND DERIVATIVE CALCULATIONS  MNMIGR: COVARIANCE MATRIX UPDATES \
      MNHES1: FIRST DERIVATIVE UNCERTAINTIES  MNCONT: MNCONTOUR PLOT (MNCROS\
 SEARCH)  ";

    /* Format strings */
    static char fmt_151[] = "(\002 MINUIT RANDOM NUMBER SEED SET TO \002,i10)"
	    ;
    static char fmt_289[] = "(\002 UNKNOWN DEBUG OPTION\002,i6,\002 REQUESTE\
D. IGNORED\002)";
    static char fmt_1061[] = "(/\002 CURRENT PRINTOUT LEVEL IS \002,a)";
    static char fmt_1081[] = "(\002 NOGRAD IS SET.  DERIVATIVES NOT COMPUTED\
 IN FCN.\002)";
    static char fmt_1082[] = "(\002   GRAD IS SET.  USER COMPUTES DERIVATIVE\
S IN FCN.\002)";
    static char fmt_1091[] = "(\002 ERRORS CORRESPOND TO FUNCTION CHANGE O\
F\002,g13.5)";
    static char fmt_1002[] = "(\002 INPUT NOW BEING READ IN \002,a,\002 FROM\
 UNIT NO.\002,i3/\002 FILENAME: \002,a)";
    static char fmt_1111[] = "(10x,\002PAGE WIDTH IS SET TO\002,i4,\002 COLU\
MNS\002)";
    static char fmt_1121[] = "(10x,\002PAGE LENGTH IS SET TO\002,i4,\002 LIN\
ES\002)";
    static char fmt_1141[] = "(\002 MINUIT WARNING MESSAGES ARE \002,a)";
    static char fmt_1151[] = "(\002 MINUIT RNDM SEED IS CURRENTLY=\002,i10/)";
    static char fmt_1175[] = "(/\002 NOW USING STRATEGY \002,a/)";
    static char fmt_1286[] = "(10x,\002DEBUG OPTION\002,i3,\002 IS \002,a3\
,\002 :\002,a)";
    static char fmt_1901[] = "(\002 THE COMMAND:\002,a10,\002 IS UNKNOWN.\
\002/)";
    static char fmt_2101[] = "(\002 THE FORMAT OF THE \002,a4,\002 COMMAND I\
S:\002//1x,a4,\002 xxx    [numerical arguments if any]\002//\002 WHERE xxx M\
AY BE ONE OF THE FOLLOWING:\002/(7x,6a12))";

    /* System generated locals */
    integer i__1;
    inlist ioin__1;

    /* Builtin functions */
    integer i_indx();
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe();
    double sqrt();
    integer f_inqu();

    /* Local variables */
    static integer iset;
    static char copt[3];
    static integer iprm, i;
    static char cmode[16], ckind[4];
    static integer jseed, kname;
    static logical lname;
    static integer iseed;
    static char cwarn[10];
    extern /* Subroutine */ int mnrn15_();
    static integer iunit, id, ii, kk;
    static char cfname[64];
    static integer ikseed;
    extern /* Subroutine */ int mnexin_(), mnrset_(), mnlims_(), mngrad_(), 
	    mnwerr_(), mnwarn_();
    static integer idbopt;
    extern /* Subroutine */ int mnamin_(), mnprin_(), mnmatu_();
    static integer igrain, iswsav;
    extern /* Subroutine */ int mnpsdf_();
    static doublereal val;
    static integer isw2;

    /* Fortran I/O blocks */
    static cilist io___12 = { 0, 0, 0, "(A/)", 0 };
    static cilist io___15 = { 0, 0, 0, fmt_151, 0 };
    static cilist io___20 = { 0, 0, 0, fmt_289, 0 };
    static cilist io___21 = { 0, 0, 0, "(A)", 0 };
    static cilist io___22 = { 0, 0, 0, "(27X,A)", 0 };
    static cilist io___23 = { 0, 0, 0, fmt_1061, 0 };
    static cilist io___24 = { 0, 0, 0, fmt_1081, 0 };
    static cilist io___25 = { 0, 0, 0, fmt_1082, 0 };
    static cilist io___26 = { 0, 0, 0, fmt_1091, 0 };
    static cilist io___30 = { 0, 0, 0, fmt_1002, 0 };
    static cilist io___31 = { 0, 0, 0, fmt_1111, 0 };
    static cilist io___32 = { 0, 0, 0, fmt_1121, 0 };
    static cilist io___34 = { 0, 0, 0, fmt_1141, 0 };
    static cilist io___37 = { 0, 0, 0, fmt_1151, 0 };
    static cilist io___39 = { 0, 0, 0, "(A,A)", 0 };
    static cilist io___40 = { 0, 0, 0, "(A)", 0 };
    static cilist io___41 = { 0, 0, 0, "(20X,A)", 0 };
    static cilist io___42 = { 0, 0, 0, fmt_1175, 0 };
    static cilist io___44 = { 0, 0, 0, "(1X,A)", 0 };
    static cilist io___45 = { 0, 0, 0, "(A,I3)", 0 };
    static cilist io___46 = { 0, 0, 0, "(A)", 0 };
    static cilist io___48 = { 0, 0, 0, "(A)", 0 };
    static cilist io___49 = { 0, 0, 0, "(A,E12.3)", 0 };
    static cilist io___50 = { 0, 0, 0, "(A,I4)", 0 };
    static cilist io___51 = { 0, 0, 0, "(A,A)", 0 };
    static cilist io___53 = { 0, 0, 0, fmt_1286, 0 };
    static cilist io___54 = { 0, 0, 0, fmt_1901, 0 };
    static cilist io___55 = { 0, 0, 0, fmt_2101, 0 };
    static cilist io___57 = { 0, 0, 0, "(' ABOVE COMMAND IS ILLEGAL.   IGNOR\
ED')", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Called from MNEXCM */
/* C        Interprets the commands that start with SET and SHOW */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */



/*        file characteristics for SET INPUT */
/*       'SET ' or 'SHOW',  'ON ' or 'OFF', 'SUPPRESSED' or 'REPORTED  ' 
*/
/*        explanation of print level numbers -1:3  and strategies 0:2 */
/*        identification of debug options */
/*        things that can be set or shown */
/*        options not intended for normal users */





    i__1 = nntot;
    for (i = 1; i <= i__1; ++i) {
	if (i_indx(mn7tit_1.cword + 3, cname + (i - 1) * 10, 7L, 3L) > 0) {
	    goto L5;
	}
/* L2: */
    }
    i = 0;
L5:
    kname = i;

/*           Command could be SET xxx, SHOW xxx,  HELP SET or HELP SHOW */
    if (i_indx(mn7tit_1.cword, "HEL", 4L, 3L) > 0) {
	goto L2000;
    }
    if (i_indx(mn7tit_1.cword, "SHO", 4L, 3L) > 0) {
	goto L1000;
    }
    if (i_indx(mn7tit_1.cword, "SET", 4L, 3L) == 0) {
	goto L1900;
    }
/*                           --- */
    s_copy(ckind, "SET ", 4L, 4L);
/*                                        . . . . . . . . . . set unknown 
*/
    if (kname <= 0) {
	goto L1900;
    }
/*                                        . . . . . . . . . . set known */
    switch ((int)kname) {
	case 1:  goto L3000;
	case 2:  goto L20;
	case 3:  goto L30;
	case 4:  goto L40;
	case 5:  goto L3000;
	case 6:  goto L60;
	case 7:  goto L70;
	case 8:  goto L80;
	case 9:  goto L90;
	case 10:  goto L100;
	case 11:  goto L110;
	case 12:  goto L120;
	case 13:  goto L130;
	case 14:  goto L140;
	case 15:  goto L150;
	case 16:  goto L160;
	case 17:  goto L170;
	case 18:  goto L3000;
	case 19:  goto L190;
	case 20:  goto L3000;
	case 21:  goto L210;
	case 22:  goto L220;
	case 23:  goto L230;
	case 24:  goto L240;
	case 25:  goto L3000;
	case 26:  goto L1900;
	case 27:  goto L270;
	case 28:  goto L280;
	case 29:  goto L290;
	case 30:  goto L300;
    }

/*                                        . . . . . . . . . . set param */
L20:
    iprm = (integer) mn7arg_1.word7[0];
    if (iprm > mn7npr_1.nu) {
	goto L25;
    }
    if (iprm <= 0) {
	goto L25;
    }
    if (mn7inx_1.nvarl[iprm - 1] < 0) {
	goto L25;
    }
    mn7ext_1.u[iprm - 1] = mn7arg_1.word7[1];
    mnexin_(mn7int_1.x);
    isw2 = mn7flg_1.isw[1];
    mnrset_(&c__1);
/*        Keep approximate covariance matrix, even if new param value */
    mn7flg_1.isw[1] = min(isw2,1);
    s_copy(mn7tit_1.cfrom, "SET PARM", 8L, 8L);
    mn7cnv_1.nfcnfr = mn7cnv_1.nfcn;
    s_copy(mn7tit_1.cstatu, "NEW VALUES", 10L, 10L);
    goto L4000;
L25:
    io___12.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___12);
    do_fio(&c__1, " UNDEFINED PARAMETER NUMBER.  IGNORED.", 38L);
    e_wsfe();
    goto L4000;
/*                                        . . . . . . . . . . set limits 
*/
L30:
    mnlims_();
    goto L4000;
/*                                        . . . . . . . . . . set covar */
L40:
/*   this command must be handled by MNREAD, and is not Fortran-callable 
*/
    goto L3000;
/*                                        . . . . . . . . . . set print */
L60:
    mn7flg_1.isw[4] = (integer) mn7arg_1.word7[0];
    goto L4000;
/*                                        . . . . . . . . . . set nograd 
*/
L70:
    mn7flg_1.isw[2] = 0;
    goto L4000;
/*                                        . . . . . . . . . . set grad */
L80:
    mngrad_(fcn, futil);
    goto L4000;
/*                                        . . . . . . . . . . set errdef 
*/
L90:
    if (mn7arg_1.word7[0] == mn7min_1.up) {
	goto L4000;
    }
    if (mn7arg_1.word7[0] <= 0.) {
	if (mn7min_1.up == mn7cns_1.updflt) {
	    goto L4000;
	}
	mn7min_1.up = mn7cns_1.updflt;
    } else {
	mn7min_1.up = mn7arg_1.word7[0];
    }
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	mn7err_1.ern[i - 1] = (float)0.;
/* L95: */
	mn7err_1.erp[i - 1] = (float)0.;
    }
    mnwerr_();
    goto L4000;
/*                                        . . . . . . . . . . set input */
/* This command must be handled by MNREAD. If it gets this far, */
/*         it is illegal. */
L100:
    goto L3000;
/*                                        . . . . . . . . . . set width */
L110:
    mn7iou_1.npagwd = (integer) mn7arg_1.word7[0];
    mn7iou_1.npagwd = max(mn7iou_1.npagwd,50);
    goto L4000;
/*                                        . . . . . . . . . . set lines */
L120:
    mn7iou_1.npagln = (integer) mn7arg_1.word7[0];
    goto L4000;
/*                                        . . . . . . . . . . set nowarn 
*/
L130:
    mn7log_1.lwarn = FALSE_;
    goto L4000;
/*                                        . . . . . . . . . . set warn */
L140:
    mn7log_1.lwarn = TRUE_;
    mnwarn_("W", "SHO", "SHO", 1L, 3L, 3L);
    goto L4000;
/*                                        . . . . . . . . . . set random 
*/
L150:
    jseed = (integer) mn7arg_1.word7[0];
    val = (float)3.;
    mnrn15_(&val, &jseed);
    if (mn7flg_1.isw[4] > 0) {
	io___15.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___15);
	do_fio(&c__1, (char *)&jseed, (ftnlen)sizeof(integer));
	e_wsfe();
    }
    goto L4000;
/*                                        . . . . . . . . . . set title */
L160:
/*   this command must be handled by MNREAD, and is not Fortran-callable 
*/
    goto L3000;
/*                                        . . . . . . . . . set strategy 
*/
L170:
    mn7cnv_1.istrat = (integer) mn7arg_1.word7[0];
    mn7cnv_1.istrat = max(mn7cnv_1.istrat,0);
    mn7cnv_1.istrat = min(mn7cnv_1.istrat,2);
    if (mn7flg_1.isw[4] > 0) {
	goto L1172;
    }
    goto L4000;
/*                                       . . . . . . . . . set page throw 
*/
L190:
    mn7iou_1.newpag = (integer) mn7arg_1.word7[0];
    goto L1190;
/*                                        . . . . . . . . . . set epsmac 
*/
L210:
    if (mn7arg_1.word7[0] > 0. && mn7arg_1.word7[0] < (float).1) {
	mn7cns_1.epsmac = mn7arg_1.word7[0];
    }
    mn7cns_1.epsma2 = sqrt(mn7cns_1.epsmac);
    goto L1210;
/*                                       . . . . . . . . . . set outputfil
e*/
L220:
    iunit = (integer) mn7arg_1.word7[0];
    mn7iou_1.isyswr = iunit;
    mn7io2_1.istkwr[0] = iunit;
    if (mn7flg_1.isw[4] >= 0) {
	goto L1220;
    }
    goto L4000;
/*                                        . . . . . . . . . . set batch */
L230:
    mn7flg_1.isw[5] = 0;
    if (mn7flg_1.isw[4] >= 0) {
	goto L1100;
    }
    goto L4000;
/*                                       . . . . . . . . . . set interacti
ve*/
L240:
    mn7flg_1.isw[5] = 1;
    if (mn7flg_1.isw[4] >= 0) {
	goto L1100;
    }
    goto L4000;
/*                                        . . . . . . . . . . set nodebug 
*/
L270:
    iset = 0;
    goto L281;
/*                                        . . . . . . . . . . set debug */
L280:
    iset = 1;
L281:
    idbopt = (integer) mn7arg_1.word7[0];
    if (idbopt > 6) {
	goto L288;
    }
    if (idbopt >= 0) {
	mn7flg_1.idbg[idbopt] = iset;
	if (iset == 1) {
	    mn7flg_1.idbg[0] = 1;
	}
    } else {
/*             SET DEBUG -1  sets all debug options */
	for (id = 0; id <= 6; ++id) {
/* L285: */
	    mn7flg_1.idbg[id] = iset;
	}
    }
    mn7log_1.lrepor = mn7flg_1.idbg[0] >= 1;
    mnwarn_("D", "SHO", "SHO", 1L, 3L, 3L);
    goto L4000;
L288:
    io___20.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___20);
    do_fio(&c__1, (char *)&idbopt, (ftnlen)sizeof(integer));
    e_wsfe();
    goto L4000;
/*                                        . . . . . . . . . . set show */
L290:
/*                                        . . . . . . . . . . set set */
L300:
    goto L3000;
/*                ----------------------------------------------------- */
L1000:
/*               at this point, CWORD must be 'SHOW' */
    s_copy(ckind, "SHOW", 4L, 4L);
    if (kname <= 0) {
	goto L1900;
    }
    switch ((int)kname) {
	case 1:  goto L1010;
	case 2:  goto L1020;
	case 3:  goto L1030;
	case 4:  goto L1040;
	case 5:  goto L1050;
	case 6:  goto L1060;
	case 7:  goto L1070;
	case 8:  goto L1070;
	case 9:  goto L1090;
	case 10:  goto L1100;
	case 11:  goto L1110;
	case 12:  goto L1120;
	case 13:  goto L1130;
	case 14:  goto L1130;
	case 15:  goto L1150;
	case 16:  goto L1160;
	case 17:  goto L1170;
	case 18:  goto L1180;
	case 19:  goto L1190;
	case 20:  goto L1200;
	case 21:  goto L1210;
	case 22:  goto L1220;
	case 23:  goto L1100;
	case 24:  goto L1100;
	case 25:  goto L1250;
	case 26:  goto L1900;
	case 27:  goto L1270;
	case 28:  goto L1270;
	case 29:  goto L1290;
	case 30:  goto L1300;
    }

/*                                        . . . . . . . . . . show fcn */
L1010:
    if (mn7min_1.amin == mn7cns_1.undefi) {
	mnamin_(fcn, futil);
    }
    mnprin_(&c__0, &mn7min_1.amin);
    goto L4000;
/*                                        . . . . . . . . . . show param 
*/
L1020:
    if (mn7min_1.amin == mn7cns_1.undefi) {
	mnamin_(fcn, futil);
    }
    mnprin_(&c__5, &mn7min_1.amin);
    goto L4000;
/*                                        . . . . . . . . . . show limits 
*/
L1030:
    if (mn7min_1.amin == mn7cns_1.undefi) {
	mnamin_(fcn, futil);
    }
    mnprin_(&c__1, &mn7min_1.amin);
    goto L4000;
/*                                        . . . . . . . . . . show covar 
*/
L1040:
    mnmatu_(&c__1);
    goto L4000;
/*                                        . . . . . . . . . . show corre 
*/
L1050:
    mnmatu_(&c__0);
    goto L4000;
/*                                        . . . . . . . . . . show print 
*/
L1060:
    if (mn7flg_1.isw[4] < -1) {
	mn7flg_1.isw[4] = -1;
    }
    if (mn7flg_1.isw[4] > 3) {
	mn7flg_1.isw[4] = 3;
    }
    io___21.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___21);
    do_fio(&c__1, " ALLOWED PRINT LEVELS ARE:", 26L);
    e_wsfe();
    io___22.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___22);
    do_fio(&c__5, cprlev, 34L);
    e_wsfe();
    io___23.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___23);
    do_fio(&c__1, cprlev + (mn7flg_1.isw[4] + 1) * 34, 34L);
    e_wsfe();
    goto L4000;
/*                                        . . . . . . . show nograd, grad 
*/
L1070:
    if (mn7flg_1.isw[2] <= 0) {
	io___24.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___24);
	e_wsfe();
    } else {
	io___25.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___25);
	e_wsfe();
    }
    goto L4000;
/*                                       . . . . . . . . . . show errdef 
*/
L1090:
    io___26.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___26);
    do_fio(&c__1, (char *)&mn7min_1.up, (ftnlen)sizeof(doublereal));
    e_wsfe();
    goto L4000;
/*                                       . . . . . . . . . . show input, 
*/
/*                                                batch, or interactive */
L1100:
    ioin__1.inerr = 0;
    ioin__1.inunit = mn7iou_1.isysrd;
    ioin__1.infile = 0;
    ioin__1.inex = 0;
    ioin__1.inopen = 0;
    ioin__1.innum = 0;
    ioin__1.innamed = &lname;
    ioin__1.innamlen = 64;
    ioin__1.inname = cfname;
    ioin__1.inacc = 0;
    ioin__1.inseq = 0;
    ioin__1.indir = 0;
    ioin__1.infmt = 0;
    ioin__1.inform = 0;
    ioin__1.inunf = 0;
    ioin__1.inrecl = 0;
    ioin__1.innrec = 0;
    ioin__1.inblank = 0;
    f_inqu(&ioin__1);
    s_copy(cmode, "BATCH MODE      ", 16L, 16L);
    if (mn7flg_1.isw[5] == 1) {
	s_copy(cmode, "INTERACTIVE MODE", 16L, 16L);
    }
    if (! lname) {
	s_copy(cfname, "unknown", 64L, 7L);
    }
    io___30.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___30);
    do_fio(&c__1, cmode, 16L);
    do_fio(&c__1, (char *)&mn7iou_1.isysrd, (ftnlen)sizeof(integer));
    do_fio(&c__1, cfname, 64L);
    e_wsfe();
    goto L4000;
/*                                       . . . . . . . . . . show width */
L1110:
    io___31.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___31);
    do_fio(&c__1, (char *)&mn7iou_1.npagwd, (ftnlen)sizeof(integer));
    e_wsfe();
    goto L4000;
/*                                       . . . . . . . . . . show lines */
L1120:
    io___32.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___32);
    do_fio(&c__1, (char *)&mn7iou_1.npagln, (ftnlen)sizeof(integer));
    e_wsfe();
    goto L4000;
/*                                       . . . . . . .show nowarn, warn */
L1130:
    s_copy(cwarn, "SUPPRESSED", 10L, 10L);
    if (mn7log_1.lwarn) {
	s_copy(cwarn, "REPORTED  ", 10L, 10L);
    }
    io___34.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___34);
    do_fio(&c__1, cwarn, 10L);
    e_wsfe();
    if (! mn7log_1.lwarn) {
	mnwarn_("W", "SHO", "SHO", 1L, 3L, 3L);
    }
    goto L4000;
/*                                      . . . . . . . . . . show random */
L1150:
    val = (float)0.;
    mnrn15_(&val, &igrain);
    ikseed = igrain;
    io___37.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___37);
    do_fio(&c__1, (char *)&ikseed, (ftnlen)sizeof(integer));
    e_wsfe();
    val = (float)3.;
    iseed = ikseed;
    mnrn15_(&val, &iseed);
    goto L4000;
/*                                        . . . . . . . . . show title */
L1160:
    io___39.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___39);
    do_fio(&c__1, " TITLE OF CURRENT TASK IS:", 26L);
    do_fio(&c__1, mn7tit_1.ctitl, 50L);
    e_wsfe();
    goto L4000;
/*                                        . . . . . . . show strategy */
L1170:
    io___40.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___40);
    do_fio(&c__1, " ALLOWED STRATEGIES ARE:", 24L);
    e_wsfe();
    io___41.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___41);
    do_fio(&c__3, cstrat, 44L);
    e_wsfe();
L1172:
    io___42.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___42);
    do_fio(&c__1, cstrat + mn7cnv_1.istrat * 44, 44L);
    e_wsfe();
    goto L4000;
/*                                          . . . . . show eigenvalues */
L1180:
    iswsav = mn7flg_1.isw[4];
    mn7flg_1.isw[4] = 3;
    if (mn7flg_1.isw[1] < 1) {
	io___44.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___44);
	do_fio(&c__1, mn7tit_1.covmes, 22L);
	e_wsfe();
    } else {
	mnpsdf_();
    }
    mn7flg_1.isw[4] = iswsav;
    goto L4000;
/*                                            . . . . . show page throw */
L1190:
    io___45.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___45);
    do_fio(&c__1, " PAGE THROW CARRIAGE CONTROL =", 30L);
    do_fio(&c__1, (char *)&mn7iou_1.newpag, (ftnlen)sizeof(integer));
    e_wsfe();
    if (mn7iou_1.newpag == 0) {
	io___46.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___46);
	do_fio(&c__1, " NO PAGE THROWS IN MINUIT OUTPUT", 32L);
	e_wsfe();
    }
    goto L4000;
/*                                        . . . . . . show minos errors */
L1200:
    i__1 = mn7npr_1.npar;
    for (ii = 1; ii <= i__1; ++ii) {
	if (mn7err_1.erp[ii - 1] > 0. || mn7err_1.ern[ii - 1] < 0.) {
	    goto L1204;
	}
/* L1202: */
    }
    io___48.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___48);
    do_fio(&c__1, "       THERE ARE NO MINOS ERRORS CURRENTLY VALID.", 49L);
    e_wsfe();
    goto L4000;
L1204:
    mnprin_(&c__4, &mn7min_1.amin);
    goto L4000;
/*                                        . . . . . . . . . show epsmac */
L1210:
    io___49.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___49);
    do_fio(&c__1, " FLOATING-POINT NUMBERS ASSUMED ACCURATE TO", 43L);
    do_fio(&c__1, (char *)&mn7cns_1.epsmac, (ftnlen)sizeof(doublereal));
    e_wsfe();
    goto L4000;
/*                                        . . . . . . show outputfiles */
L1220:
    io___50.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___50);
    do_fio(&c__1, "  MINUIT PRIMARY OUTPUT TO UNIT", 31L);
    do_fio(&c__1, (char *)&mn7iou_1.isyswr, (ftnlen)sizeof(integer));
    e_wsfe();
    goto L4000;
/*                                        . . . . . . show version */
L1250:
    io___51.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___51);
    do_fio(&c__1, " THIS IS MINUIT VERSION:", 24L);
    do_fio(&c__1, mn7tit_1.cvrsn, 6L);
    e_wsfe();
    goto L4000;
/*                                        . . . . . . show nodebug, debug 
*/
L1270:
    for (id = 0; id <= 6; ++id) {
	s_copy(copt, "OFF", 3L, 3L);
	if (mn7flg_1.idbg[id] >= 1) {
	    s_copy(copt, "ON ", 3L, 3L);
	}
/* L1285: */
	io___53.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___53);
	do_fio(&c__1, (char *)&id, (ftnlen)sizeof(integer));
	do_fio(&c__1, copt, 3L);
	do_fio(&c__1, cdbopt + id * 40, 40L);
	e_wsfe();
    }
    if (! mn7log_1.lrepor) {
	mnwarn_("D", "SHO", "SHO", 1L, 3L, 3L);
    }
    goto L4000;
/*                                        . . . . . . . . . . show show */
L1290:
    s_copy(ckind, "SHOW", 4L, 4L);
    goto L2100;
/*                                        . . . . . . . . . . show set */
L1300:
    s_copy(ckind, "SET ", 4L, 4L);
    goto L2100;
/*                ----------------------------------------------------- */
/*                              UNKNOWN COMMAND */
L1900:
    io___54.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___54);
    do_fio(&c__1, mn7tit_1.cword, 20L);
    e_wsfe();
    goto L2100;
/*                ----------------------------------------------------- */
/*                    HELP SHOW,  HELP SET,  SHOW SET, or SHOW SHOW */
L2000:
    s_copy(ckind, "SET ", 4L, 4L);
    if (i_indx(mn7tit_1.cword + 3, "SHO", 7L, 3L) > 0) {
	s_copy(ckind, "SHOW", 4L, 4L);
    }
L2100:
    io___55.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___55);
    do_fio(&c__1, ckind, 4L);
    do_fio(&c__1, ckind, 4L);
    i__1 = nname;
    for (kk = 1; kk <= i__1; ++kk) {
	do_fio(&c__1, cname + (kk - 1) * 10, 10L);
    }
    e_wsfe();
    goto L4000;
/*                ----------------------------------------------------- */
/*                               ILLEGAL COMMAND */
L3000:
    io___57.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___57);
    e_wsfe();
L4000:
    return 0;
} /* mnset_ */

