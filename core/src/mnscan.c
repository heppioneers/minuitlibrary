/* mnscan.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__4 = 4;
static integer c__1 = 1;
static integer c__5 = 5;


/* $Id: mnscan.F,v 1.2 1996/03/15 18:02:51 james Exp $ */

/* $Log: mnscan.F,v $ */
/* Revision 1.2  1996/03/15 18:02:51  james */
/*     Modified Files: */
/* mnderi.F eliminate possible division by zero */
/* mnexcm.F suppress print on STOP when print flag=-1 */
/*          set FVAL3 to flag if FCN already called with IFLAG=3 */
/* mninit.F set version 96.03 */
/* mnlims.F remove arguments, not needed */
/* mnmigr.F VLEN -> LENV in debug print statement */
/* mnparm.F move call to MNRSET to after NPAR redefined, to zero all */
/* mnpsdf.F eliminate possible division by zero */
/* mnscan.F suppress printout when print flag =-1 */
/* mnset.F  remove arguments in call to MNLIMS */
/* mnsimp.F fix CSTATU so status is PROGRESS only if new minimum */
/* mnvert.F eliminate possible division by zero */

/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnscan_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    /* Format strings */
    static char fmt_1001[] = "(i1,\002SCAN OF PARAMETER NO.\002,i3,\002, \
 \002,a10)";
    static char fmt_1000[] = "(\002 REQUESTED RANGE OUTSIDE LIMITS FOR PARAM\
ETER \002,i3/)";

    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe();

    /* Local variables */
    static integer ipar, iint;
    static doublereal step;
    static integer icall, ncall;
    static doublereal uhigh;
    static integer nbins;
    static doublereal xhreq, xlreq, ubest;
    static integer nparx;
    static doublereal fnext;
    static integer nunit;
    static doublereal unext;
    static integer nxypt, nccall;
    static doublereal xh, xl;
    extern /* Subroutine */ int mnamin_();
    static integer iparwd;
    extern /* Subroutine */ int mnbins_(), mnexin_(), mnplot_(), mnprin_();

    /* Fortran I/O blocks */
    static cilist io___19 = { 0, 0, 0, fmt_1001, 0 };
    static cilist io___21 = { 0, 0, 0, fmt_1000, 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Scans the values of FCN as a function of one parameter */
/* C        and plots the resulting values as a curve using MNPLOT. */
/* C        It may be called to scan one parameter or all parameters. */
/* C        retains the best function and parameter values found. */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    xlreq = min(mn7arg_1.word7[2],mn7arg_1.word7[3]);
    xhreq = max(mn7arg_1.word7[2],mn7arg_1.word7[3]);
    ncall = (integer) (mn7arg_1.word7[1] + (float).01);
    if (ncall <= 1) {
	ncall = 41;
    }
    if (ncall > 101) {
	ncall = 101;
    }
    nccall = ncall;
    if (mn7min_1.amin == mn7cns_1.undefi) {
	mnamin_(fcn, futil);
    }
    iparwd = (integer) (mn7arg_1.word7[0] + (float).1);
    ipar = max(iparwd,0);
    iint = mn7inx_1.niofex[ipar - 1];
    s_copy(mn7tit_1.cstatu, "NO CHANGE", 10L, 9L);
    if (iparwd > 0) {
	goto L200;
    }

/*         equivalent to a loop over parameters requested */
L100:
    ++ipar;
    if (ipar > mn7npr_1.nu) {
	goto L900;
    }
    iint = mn7inx_1.niofex[ipar - 1];
    if (iint <= 0) {
	goto L100;
    }
/*         set up range for parameter IPAR */
L200:
    ubest = mn7ext_1.u[ipar - 1];
    mn7rpt_1.xpt[0] = ubest;
    mn7rpt_1.ypt[0] = mn7min_1.amin;
    *(unsigned char *)&mn7cpt_1.chpt[0] = ' ';
    mn7rpt_1.xpt[1] = ubest;
    mn7rpt_1.ypt[1] = mn7min_1.amin;
    *(unsigned char *)&mn7cpt_1.chpt[1] = 'X';
    nxypt = 2;
    if (mn7inx_1.nvarl[ipar - 1] > 1) {
	goto L300;
    }
/*         no limits on parameter */
    if (xlreq == xhreq) {
	goto L250;
    }
    unext = xlreq;
    step = (xhreq - xlreq) / (real) (ncall - 1);
    goto L500;
L250:
    xl = ubest - mn7err_1.werr[iint - 1];
    xh = ubest + mn7err_1.werr[iint - 1];
    mnbins_(&xl, &xh, &ncall, &unext, &uhigh, &nbins, &step);
    nccall = nbins + 1;
    goto L500;
/*         limits on parameter */
L300:
    if (xlreq == xhreq) {
	goto L350;
    }
/* Computing MAX */
    d__1 = xlreq, d__2 = mn7ext_1.alim[ipar - 1];
    xl = max(d__1,d__2);
/* Computing MIN */
    d__1 = xhreq, d__2 = mn7ext_1.blim[ipar - 1];
    xh = min(d__1,d__2);
    if (xl >= xh) {
	goto L700;
    }
    unext = xl;
    step = (xh - xl) / (real) (ncall - 1);
    goto L500;
L350:
    unext = mn7ext_1.alim[ipar - 1];
    step = (mn7ext_1.blim[ipar - 1] - mn7ext_1.alim[ipar - 1]) / (real) (
	    ncall - 1);
/*         main scanning loop over parameter IPAR */
L500:
    i__1 = nccall;
    for (icall = 1; icall <= i__1; ++icall) {
	mn7ext_1.u[ipar - 1] = unext;
	nparx = mn7npr_1.npar;
	(*fcn)(&nparx, mn7der_1.gin, &fnext, mn7ext_1.u, &c__4, futil);
	++mn7cnv_1.nfcn;
	++nxypt;
	mn7rpt_1.xpt[nxypt - 1] = unext;
	mn7rpt_1.ypt[nxypt - 1] = fnext;
	*(unsigned char *)&mn7cpt_1.chpt[nxypt - 1] = '*';
	if (fnext < mn7min_1.amin) {
	    mn7min_1.amin = fnext;
	    ubest = unext;
	    s_copy(mn7tit_1.cstatu, "IMPROVED  ", 10L, 10L);
	}
/* L530: */
	unext += step;
/* L600: */
    }
/*         finished with scan of parameter IPAR */
    mn7ext_1.u[ipar - 1] = ubest;
    mnexin_(mn7int_1.x);
    if (mn7flg_1.isw[4] >= 1) {
	io___19.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___19);
	do_fio(&c__1, (char *)&mn7iou_1.newpag, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&ipar, (ftnlen)sizeof(integer));
	do_fio(&c__1, mn7nam_1.cpnam + (ipar - 1) * 10, 10L);
	e_wsfe();
	nunit = mn7iou_1.isyswr;
	mnplot_(mn7rpt_1.xpt, mn7rpt_1.ypt, mn7cpt_1.chpt, &nxypt, &nunit, &
		mn7iou_1.npagwd, &mn7iou_1.npagln, 1L);
    }
    goto L800;
L700:
    io___21.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___21);
    do_fio(&c__1, (char *)&ipar, (ftnlen)sizeof(integer));
    e_wsfe();
L800:
    if (iparwd <= 0) {
	goto L100;
    }
/*         finished with all parameters */
L900:
    if (mn7flg_1.isw[4] >= 0) {
	mnprin_(&c__5, &mn7min_1.amin);
    }
    return 0;
} /* mnscan_ */

