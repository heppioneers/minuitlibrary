/* mnmnot.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;


/* $Id: mnmnot.F,v 1.1.1.1 1996/03/07 14:31:30 mclareni Exp $ */

/* $Log: mnmnot.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mnmnot_(fcn, ilax, ilax2, val2pl, val2mi, futil)
/* Subroutine */ int (*fcn) ();
integer *ilax, *ilax2;
doublereal *val2pl, *val2mi;
/* Subroutine */ int (*futil) ();
{
    /* Format strings */
    static char fmt_806[] = "(/\002 DETERMINATION OF \002,a4,\002TIVE MINOS \
ERROR FOR PARAMETER\002,i3,2x,a)";
    static char fmt_801[] = "(/\002 PARAMETER\002,i4,\002 SET TO\002,e11.3\
,\002 + \002,e10.3,\002 = \002,e12.3)";
    static char fmt_808[] = "(/9x,\002THE \002,a4,\002TIVE MINOS ERROR OF PA\
RAMETER\002,i3,\002 ,\002,a10,\002, IS\002,e12.4)";
    static char fmt_807[] = "(5x,\002THE \002,a4,\002TIVE MINOS ERROR OF PAR\
AMETER\002,i3,\002, \002,a,\002, EXCEEDS ITS LIMIT.\002/)";
    static char fmt_802[] = "(9x,\002THE \002,a,\002TIVE MINOS ERROR\002,i4\
,\002 REQUIRES MORE THAN\002,i5,\002 FUNCTION CALLS.\002/)";
    static char fmt_805[] = "(25x,a,\002TIVE MINOS ERROR NOT CALCULATED FOR \
PARAMETER\002,i4/)";

    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    double sqrt();
    integer s_wsfe(), do_fio(), e_wsfe();
    /* Subroutine */ int s_copy();

    /* Local variables */
    static char csig[4];
    static integer marc;
    static doublereal delu;
    static integer isig, mpar, ndex;
    static doublereal xdev[50];
    static integer imax, indx, ierr;
    static doublereal aopt, eros;
    static integer i, j;
    static doublereal w[50], abest;
    static integer iercr;
    static doublereal xunit, dc;
    static integer it;
    static doublereal ut;
    extern /* Subroutine */ int mnfree_();
    static doublereal sigsav;
    static integer istrav, nfmxin;
    extern /* Subroutine */ int mninex_(), mnfixp_(), mnwarn_();
    static integer nlimit;
    static doublereal du1;
    extern /* Subroutine */ int mncros_(), mnexin_();
    static doublereal fac, gcc[50], sig, sav;
    static integer isw2, isw4;

    /* Fortran I/O blocks */
    static cilist io___22 = { 0, 0, 0, "(A,I5,A,I5)", 0 };
    static cilist io___26 = { 0, 0, 0, fmt_806, 0 };
    static cilist io___31 = { 0, 0, 0, fmt_801, 0 };
    static cilist io___35 = { 0, 0, 0, fmt_808, 0 };
    static cilist io___36 = { 0, 0, 0, fmt_807, 0 };
    static cilist io___37 = { 0, 0, 0, fmt_802, 0 };
    static cilist io___38 = { 0, 0, 0, fmt_805, 0 };
    static cilist io___39 = { 0, 0, 0, "(5X, 74(1H*))", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Performs a MINOS error analysis on one parameter. */
/* C        The parameter ILAX is varied, and the minimum of the */
/* C        function with respect to the other parameters is followed */
/* C        until it crosses the value FMIN+UP. */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


/*                                        . . save and prepare start vals 
*/
    isw2 = mn7flg_1.isw[1];
    isw4 = mn7flg_1.isw[3];
    sigsav = mn7min_1.edm;
    istrav = mn7cnv_1.istrat;
    dc = mn7min_1.dcovar;
    mn7log_1.lnewmn = FALSE_;
    mn7min_1.apsi = mn7min_1.epsi * (float).5;
    abest = mn7min_1.amin;
    mpar = mn7npr_1.npar;
    nfmxin = mn7cnv_1.nfcnmx;
    i__1 = mpar;
    for (i = 1; i <= i__1; ++i) {
/* L125: */
	mn7int_1.xt[i - 1] = mn7int_1.x[i - 1];
    }
    i__1 = mpar * (mpar + 1) / 2;
    for (j = 1; j <= i__1; ++j) {
/* L130: */
	mn7vat_1.vthmat[j - 1] = mn7var_1.vhmat[j - 1];
    }
    i__1 = mpar;
    for (i = 1; i <= i__1; ++i) {
	gcc[i - 1] = mn7err_1.globcc[i - 1];
/* L135: */
	w[i - 1] = mn7err_1.werr[i - 1];
    }
    it = mn7inx_1.niofex[*ilax - 1];
    mn7err_1.erp[it - 1] = (float)0.;
    mn7err_1.ern[it - 1] = (float)0.;
    mninex_(mn7int_1.xt);
    ut = mn7ext_1.u[*ilax - 1];
    if (mn7inx_1.nvarl[*ilax - 1] == 1) {
	mn7ext_1.alim[*ilax - 1] = ut - w[it - 1] * (float)100.;
	mn7ext_1.blim[*ilax - 1] = ut + w[it - 1] * (float)100.;
    }
    ndex = it * (it + 1) / 2;
    xunit = sqrt(mn7min_1.up / mn7vat_1.vthmat[ndex - 1]);
    marc = 0;
    i__1 = mpar;
    for (i = 1; i <= i__1; ++i) {
	if (i == it) {
	    goto L162;
	}
	++marc;
	imax = max(it,i);
	indx = imax * (imax - 1) / 2 + min(it,i);
	xdev[marc - 1] = xunit * mn7vat_1.vthmat[indx - 1];
L162:
	;
    }
/*                           fix the parameter in question */
    mnfixp_(&it, &ierr);
    if (ierr > 0) {
	io___22.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___22);
	do_fio(&c__1, " MINUIT ERROR. CANNOT FIX PARAMETER", 35L);
	do_fio(&c__1, (char *)&(*ilax), (ftnlen)sizeof(integer));
	do_fio(&c__1, "    INTERNAL", 12L);
	do_fio(&c__1, (char *)&it, (ftnlen)sizeof(integer));
	e_wsfe();
	goto L700;
    }
/*                       . . . . . Nota Bene: from here on, NPAR=MPAR-1 */
/*      Remember: MNFIXP squeezes IT out of X, XT, WERR, and VHMAT, */
/*                                                    not W, VTHMAT */
    for (isig = 1; isig <= 2; ++isig) {
	if (isig == 1) {
	    sig = (float)1.;
	    s_copy(csig, "POSI", 4L, 4L);
	} else {
	    sig = (float)-1.;
	    s_copy(csig, "NEGA", 4L, 4L);
	}
/*                                        . sig=sign of error being ca
lcd */
	if (mn7flg_1.isw[4] > 1) {
	    io___26.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___26);
	    do_fio(&c__1, csig, 4L);
	    do_fio(&c__1, (char *)&(*ilax), (ftnlen)sizeof(integer));
	    do_fio(&c__1, mn7nam_1.cpnam + (*ilax - 1) * 10, 10L);
	    e_wsfe();
	}
	if (mn7flg_1.isw[1] <= 0) {
	    mnwarn_("D", "MINOS", "NO COVARIANCE MATRIX.", 1L, 5L, 21L);
	}
	nlimit = mn7cnv_1.nfcn + nfmxin;
/* Computing MAX */
	i__1 = istrav - 1;
	mn7cnv_1.istrat = max(i__1,0);
	du1 = w[it - 1];
	mn7ext_1.u[*ilax - 1] = ut + sig * du1;
/* Computing MIN */
	d__1 = mn7ext_1.u[*ilax - 1], d__2 = mn7ext_1.blim[*ilax - 1];
	mn7ext_1.u[*ilax - 1] = min(d__1,d__2);
/* Computing MAX */
	d__1 = mn7ext_1.u[*ilax - 1], d__2 = mn7ext_1.alim[*ilax - 1];
	mn7ext_1.u[*ilax - 1] = max(d__1,d__2);
	delu = mn7ext_1.u[*ilax - 1] - ut;
/*         stop if already at limit with negligible step size */
	if (abs(delu) / (abs(ut) + (d__1 = mn7ext_1.u[*ilax - 1], abs(d__1))) 
		< mn7cns_1.epsmac) {
	    goto L440;
	}
	fac = delu / w[it - 1];
	i__1 = mn7npr_1.npar;
	for (i = 1; i <= i__1; ++i) {
/* L185: */
	    mn7int_1.x[i - 1] = mn7int_1.xt[i - 1] + fac * xdev[i - 1];
	}
	if (mn7flg_1.isw[4] > 1) {
	    io___31.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___31);
	    do_fio(&c__1, (char *)&(*ilax), (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&ut, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&delu, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&mn7ext_1.u[*ilax - 1], (ftnlen)sizeof(
		    doublereal));
	    e_wsfe();
	}
/*                                        loop to hit AMIN+UP */
	mn7xcr_1.ke1cr = *ilax;
	mn7xcr_1.ke2cr = 0;
	mn7xcr_1.xmidcr = mn7ext_1.u[*ilax - 1];
	mn7xcr_1.xdircr = delu;

	mn7min_1.amin = abest;
	mn7cnv_1.nfcnmx = nlimit - mn7cnv_1.nfcn;
	mncros_(fcn, &aopt, &iercr, futil);
	if (abest - mn7min_1.amin > mn7min_1.up * (float).01) {
	    goto L650;
	}
	if (iercr == 1) {
	    goto L440;
	}
	if (iercr == 2) {
	    goto L450;
	}
	if (iercr == 3) {
	    goto L460;
	}
/*                                        . error successfully calcula
ted */
	eros = mn7xcr_1.xmidcr - ut + aopt * mn7xcr_1.xdircr;
	if (mn7flg_1.isw[4] > 1) {
	    io___35.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___35);
	    do_fio(&c__1, csig, 4L);
	    do_fio(&c__1, (char *)&(*ilax), (ftnlen)sizeof(integer));
	    do_fio(&c__1, mn7nam_1.cpnam + (*ilax - 1) * 10, 10L);
	    do_fio(&c__1, (char *)&eros, (ftnlen)sizeof(doublereal));
	    e_wsfe();
	}
	goto L480;
/*                                        . . . . . . . . failure retu
rns */
L440:
	if (mn7flg_1.isw[4] >= 1) {
	    io___36.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___36);
	    do_fio(&c__1, csig, 4L);
	    do_fio(&c__1, (char *)&(*ilax), (ftnlen)sizeof(integer));
	    do_fio(&c__1, mn7nam_1.cpnam + (*ilax - 1) * 10, 10L);
	    e_wsfe();
	}
	eros = mn7cns_1.undefi;
	goto L480;
L450:
	if (mn7flg_1.isw[4] >= 1) {
	    io___37.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___37);
	    do_fio(&c__1, csig, 4L);
	    do_fio(&c__1, (char *)&(*ilax), (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&nfmxin, (ftnlen)sizeof(integer));
	    e_wsfe();
	}
	eros = (float)0.;
	goto L480;
L460:
	if (mn7flg_1.isw[4] >= 1) {
	    io___38.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___38);
	    do_fio(&c__1, csig, 4L);
	    do_fio(&c__1, (char *)&(*ilax), (ftnlen)sizeof(integer));
	    e_wsfe();
	}
	eros = (float)0.;

L480:
	if (mn7flg_1.isw[4] > 1) {
	    io___39.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___39);
	    e_wsfe();
	}
	if (sig < 0.) {
	    mn7err_1.ern[it - 1] = eros;
	    if (*ilax2 > 0 && *ilax2 <= mn7npr_1.nu) {
		*val2mi = mn7ext_1.u[*ilax2 - 1];
	    }
	} else {
	    mn7err_1.erp[it - 1] = eros;
	    if (*ilax2 > 0 && *ilax2 <= mn7npr_1.nu) {
		*val2pl = mn7ext_1.u[*ilax2 - 1];
	    }
	}
/* L500: */
    }
/*                                        . . parameter finished. reset v 
*/
/*                       normal termination */
    mn7cnv_1.itaur = 1;
    mnfree_(&c__1);
    i__1 = mpar * (mpar + 1) / 2;
    for (j = 1; j <= i__1; ++j) {
/* L550: */
	mn7var_1.vhmat[j - 1] = mn7vat_1.vthmat[j - 1];
    }
    i__1 = mpar;
    for (i = 1; i <= i__1; ++i) {
	mn7err_1.werr[i - 1] = w[i - 1];
	mn7err_1.globcc[i - 1] = gcc[i - 1];
/* L595: */
	mn7int_1.x[i - 1] = mn7int_1.xt[i - 1];
    }
    mninex_(mn7int_1.x);
    mn7min_1.edm = sigsav;
    mn7min_1.amin = abest;
    mn7flg_1.isw[1] = isw2;
    mn7flg_1.isw[3] = isw4;
    mn7min_1.dcovar = dc;
    goto L700;
/*                       new minimum */
L650:
    mn7log_1.lnewmn = TRUE_;
    mn7flg_1.isw[1] = 0;
    mn7min_1.dcovar = (float)1.;
    mn7flg_1.isw[3] = 0;
    sav = mn7ext_1.u[*ilax - 1];
    mn7cnv_1.itaur = 1;
    mnfree_(&c__1);
    mn7ext_1.u[*ilax - 1] = sav;
    mnexin_(mn7int_1.x);
    mn7min_1.edm = mn7cns_1.bigedm;
/*                       in any case */
L700:
    mn7cnv_1.itaur = 0;
    mn7cnv_1.nfcnmx = nfmxin;
    mn7cnv_1.istrat = istrav;
    return 0;
} /* mnmnot_ */

