/* mnfree.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;


/* $Id: mnfree.F,v 1.1.1.1 1996/03/07 14:31:29 mclareni Exp $ */

/* $Log: mnfree.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mnfree_(k)
integer *k;
{
    /* Format strings */
    static char fmt_510[] = "(\002 CALL TO MNFREE IGNORED.  ARGUMENT GREATER\
 THAN ONE\002/)";
    static char fmt_500[] = "(\002 CALL TO MNFREE IGNORED.  THERE ARE NO FIX\
ED PA\002,\002RAMETERS\002/)";
    static char fmt_540[] = "(\002 IGNORED.  PARAMETER SPECIFIED IS ALREADY \
VARIABLE.\002)";
    static char fmt_530[] = "(\002 PARAMETER\002,i4,\002 NOT FIXED.  CANNOT \
BE RELEASED.\002)";
    static char fmt_520[] = "(20x,\002PARAMETER\002,i4,\002, \002,a10,\002 R\
ESTORED TO VARIABLE.\002)";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(), e_wsfe(), do_fio();

    /* Local variables */
    static doublereal grdv;
    static integer i, ipsav, ka, lc, ik, iq, ir, is;
    static doublereal xv, dirinv, g2v, gstepv;
    extern /* Subroutine */ int mnexin_();
    static doublereal xtv;

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 0, 0, fmt_510, 0 };
    static cilist io___2 = { 0, 0, 0, fmt_500, 0 };
    static cilist io___4 = { 0, 0, 0, fmt_540, 0 };
    static cilist io___6 = { 0, 0, 0, fmt_530, 0 };
    static cilist io___19 = { 0, 0, 0, fmt_520, 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Restores one or more fixed parameter(s) to variable status */
/* C        by inserting it into the internal parameter list at the */
/* C        appropriate place. */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


/* --       K = 0 means restore all parameters */
/* --       K = 1 means restore the last parameter fixed */
/* --       K = -I means restore external parameter I (if possible) */
/* --       IQ = fix-location where internal parameters were stored */
/* --       IR = external number of parameter being restored */
/* --       IS = internal number of parameter being restored */
    if (*k > 1) {
	io___1.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___1);
	e_wsfe();
    }
    if (mn7fx1_1.npfix < 1) {
	io___2.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___2);
	e_wsfe();
    }
    if (*k == 1 || *k == 0) {
	goto L40;
    }
/*                   release parameter with specified external number */
    ka = abs(*k);
    if (mn7inx_1.niofex[ka - 1] == 0) {
	goto L15;
    }
    io___4.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___4);
    e_wsfe();
    return 0;
L15:
    if (mn7fx1_1.npfix < 1) {
	goto L21;
    }
    i__1 = mn7fx1_1.npfix;
    for (ik = 1; ik <= i__1; ++ik) {
	if (mn7fx1_1.ipfix[ik - 1] == ka) {
	    goto L24;
	}
/* L20: */
    }
L21:
    io___6.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___6);
    do_fio(&c__1, (char *)&ka, (ftnlen)sizeof(integer));
    e_wsfe();
    return 0;
L24:
    if (ik == mn7fx1_1.npfix) {
	goto L40;
    }
/*                   move specified parameter to end of list */
    ipsav = ka;
    xv = mn7fx2_1.xs[ik - 1];
    xtv = mn7fx2_1.xts[ik - 1];
    dirinv = mn7fx2_1.dirins[ik - 1];
    grdv = mn7fx3_1.grds[ik - 1];
    g2v = mn7fx3_1.g2s[ik - 1];
    gstepv = mn7fx3_1.gsteps[ik - 1];
    i__1 = mn7fx1_1.npfix;
    for (i = ik + 1; i <= i__1; ++i) {
	mn7fx1_1.ipfix[i - 2] = mn7fx1_1.ipfix[i - 1];
	mn7fx2_1.xs[i - 2] = mn7fx2_1.xs[i - 1];
	mn7fx2_1.xts[i - 2] = mn7fx2_1.xts[i - 1];
	mn7fx2_1.dirins[i - 2] = mn7fx2_1.dirins[i - 1];
	mn7fx3_1.grds[i - 2] = mn7fx3_1.grds[i - 1];
	mn7fx3_1.g2s[i - 2] = mn7fx3_1.g2s[i - 1];
	mn7fx3_1.gsteps[i - 2] = mn7fx3_1.gsteps[i - 1];
/* L30: */
    }
    mn7fx1_1.ipfix[mn7fx1_1.npfix - 1] = ipsav;
    mn7fx2_1.xs[mn7fx1_1.npfix - 1] = xv;
    mn7fx2_1.xts[mn7fx1_1.npfix - 1] = xtv;
    mn7fx2_1.dirins[mn7fx1_1.npfix - 1] = dirinv;
    mn7fx3_1.grds[mn7fx1_1.npfix - 1] = grdv;
    mn7fx3_1.g2s[mn7fx1_1.npfix - 1] = g2v;
    mn7fx3_1.gsteps[mn7fx1_1.npfix - 1] = gstepv;
/*                restore last parameter in fixed list  -- IPFIX(NPFIX) */
L40:
    if (mn7fx1_1.npfix < 1) {
	goto L300;
    }
    ir = mn7fx1_1.ipfix[mn7fx1_1.npfix - 1];
    is = 0;
    i__1 = ir;
    for (ik = mn7npr_1.nu; ik >= i__1; --ik) {
	if (mn7inx_1.niofex[ik - 1] > 0) {
	    lc = mn7inx_1.niofex[ik - 1] + 1;
	    is = lc - 1;
	    mn7inx_1.niofex[ik - 1] = lc;
	    mn7inx_1.nexofi[lc - 1] = ik;
	    mn7int_1.x[lc - 1] = mn7int_1.x[lc - 2];
	    mn7int_1.xt[lc - 1] = mn7int_1.xt[lc - 2];
	    mn7int_1.dirin[lc - 1] = mn7int_1.dirin[lc - 2];
	    mn7err_1.werr[lc - 1] = mn7err_1.werr[lc - 2];
	    mn7der_1.grd[lc - 1] = mn7der_1.grd[lc - 2];
	    mn7der_1.g2[lc - 1] = mn7der_1.g2[lc - 2];
	    mn7der_1.gstep[lc - 1] = mn7der_1.gstep[lc - 2];
	}
/* L100: */
    }
    ++mn7npr_1.npar;
    if (is == 0) {
	is = mn7npr_1.npar;
    }
    mn7inx_1.niofex[ir - 1] = is;
    mn7inx_1.nexofi[is - 1] = ir;
    iq = mn7fx1_1.npfix;
    mn7int_1.x[is - 1] = mn7fx2_1.xs[iq - 1];
    mn7int_1.xt[is - 1] = mn7fx2_1.xts[iq - 1];
    mn7int_1.dirin[is - 1] = mn7fx2_1.dirins[iq - 1];
    mn7err_1.werr[is - 1] = mn7fx2_1.dirins[iq - 1];
    mn7der_1.grd[is - 1] = mn7fx3_1.grds[iq - 1];
    mn7der_1.g2[is - 1] = mn7fx3_1.g2s[iq - 1];
    mn7der_1.gstep[is - 1] = mn7fx3_1.gsteps[iq - 1];
    --mn7fx1_1.npfix;
    mn7flg_1.isw[1] = 0;
    mn7min_1.dcovar = (float)1.;
    if (mn7flg_1.isw[4] - mn7cnv_1.itaur >= 1) {
	io___19.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___19);
	do_fio(&c__1, (char *)&ir, (ftnlen)sizeof(integer));
	do_fio(&c__1, mn7nam_1.cpnam + (ir - 1) * 10, 10L);
	e_wsfe();
    }
    if (*k == 0) {
	goto L40;
    }
L300:
/*         if different from internal, external values are taken */
    mnexin_(mn7int_1.x);
/* L400: */
    return 0;
} /* mnfree_ */

