/* mnseek.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;
static integer c__4 = 4;
static integer c__0 = 0;


/* $Id: mnseek.F,v 1.1.1.1 1996/03/07 14:31:31 mclareni Exp $ */

/* $Log: mnseek.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnseek_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    /* Format strings */
    static char fmt_3[] = "(\002 MNSEEK: MONTE CARLO MINIMIZATION USING METR\
OPOLIS\002,\002 ALGORITHM\002/\002 TO STOP AFTER\002,i6,\002 SUCCESSIVE FAIL\
URES, OR\002,i7,\002 STEPS\002/\002 MAXIMUM STEP SIZE IS\002,f9.3,\002 ERROR\
 BARS.\002)";
    static char fmt_601[] = "(\002 MNSEEK:\002,i5,\002 SUCCESSIVE UNSUCCESSF\
UL TRIALS.\002)";

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();
    /* Subroutine */ int s_copy();
    double log();

    /* Local variables */
    static doublereal dxdi;
    static integer ipar;
    static doublereal xmid[50];
    static integer iext;
    static doublereal rnum, ftry, rnum1, rnum2;
    static integer j, ifail;
    static doublereal alpha;
    static integer iseed;
    static doublereal flast, xbest[50];
    static integer nparx, istep;
    extern /* Subroutine */ int mnrn15_();
    static integer ib, mxfail, mxstep;
    extern /* Subroutine */ int mnamin_(), mnprin_(), mndxdi_(), mninex_();
    static doublereal bar;

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 0, 0, fmt_3, 0 };
    static cilist io___22 = { 0, 0, 0, fmt_601, 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C   Performs a rough (but global) minimization by monte carlo search. 
*/
/* C        Each time a new minimum is found, the search area is shifted 
*/
/* C        to be centered at the best value.  Random points are chosen */
/* C        uniformly over a hypercube determined by current step sizes. 
*/
/* C   The Metropolis algorithm accepts a worse point with probability */
/* C      exp(-d/UP), where d is the degradation.  Improved points */
/* C      are of course always accepted.  Actual steps are random */
/* C      multiples of the nominal steps (DIRIN). */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    mxfail = (integer) mn7arg_1.word7[0];
    if (mxfail <= 0) {
	mxfail = mn7npr_1.npar * 20 + 100;
    }
    mxstep = mxfail * 10;
    if (mn7min_1.amin == mn7cns_1.undefi) {
	mnamin_(fcn, futil);
    }
    alpha = mn7arg_1.word7[1];
    if (alpha <= 0.) {
	alpha = (float)3.;
    }
    if (mn7flg_1.isw[4] >= 1) {
	io___4.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___4);
	do_fio(&c__1, (char *)&mxfail, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&mxstep, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&alpha, (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    s_copy(mn7tit_1.cstatu, "INITIAL  ", 10L, 9L);
    if (mn7flg_1.isw[4] >= 2) {
	mnprin_(&c__2, &mn7min_1.amin);
    }
    s_copy(mn7tit_1.cstatu, "UNCHANGED ", 10L, 10L);
    ifail = 0;
    rnum = 0.;
    rnum1 = 0.;
    rnum2 = 0.;
    nparx = mn7npr_1.npar;
    flast = mn7min_1.amin;
/*              set up step sizes, starting values */
    i__1 = mn7npr_1.npar;
    for (ipar = 1; ipar <= i__1; ++ipar) {
	iext = mn7inx_1.nexofi[ipar - 1];
	mn7int_1.dirin[ipar - 1] = alpha * (float)2. * mn7err_1.werr[ipar - 1]
		;
	if (mn7inx_1.nvarl[iext - 1] > 1) {
/*              parameter with limits */
	    mndxdi_(&mn7int_1.x[ipar - 1], &ipar, &dxdi);
	    if (dxdi == 0.) {
		dxdi = (float)1.;
	    }
	    mn7int_1.dirin[ipar - 1] = alpha * (float)2. * mn7err_1.werr[ipar 
		    - 1] / dxdi;
	    if ((d__1 = mn7int_1.dirin[ipar - 1], abs(d__1)) > 
		    6.2831859999999997) {
		mn7int_1.dirin[ipar - 1] = 6.2831859999999997;
	    }
	}
	xmid[ipar - 1] = mn7int_1.x[ipar - 1];
/* L10: */
	xbest[ipar - 1] = mn7int_1.x[ipar - 1];
    }
/*                              search loop */
    i__1 = mxstep;
    for (istep = 1; istep <= i__1; ++istep) {
	if (ifail >= mxfail) {
	    goto L600;
	}
	i__2 = mn7npr_1.npar;
	for (ipar = 1; ipar <= i__2; ++ipar) {
	    mnrn15_(&rnum1, &iseed);
	    mnrn15_(&rnum2, &iseed);
/* L100: */
	    mn7int_1.x[ipar - 1] = xmid[ipar - 1] + (rnum1 + rnum2 - (float)
		    1.) * (float).5 * mn7int_1.dirin[ipar - 1];
	}
	mninex_(mn7int_1.x);
	(*fcn)(&nparx, mn7der_1.gin, &ftry, mn7ext_1.u, &c__4, futil);
	++mn7cnv_1.nfcn;
	if (ftry < flast) {
	    if (ftry < mn7min_1.amin) {
		s_copy(mn7tit_1.cstatu, "IMPROVEMNT", 10L, 10L);
		mn7min_1.amin = ftry;
		i__2 = mn7npr_1.npar;
		for (ib = 1; ib <= i__2; ++ib) {
/* L200: */
		    xbest[ib - 1] = mn7int_1.x[ib - 1];
		}
		ifail = 0;
		if (mn7flg_1.isw[4] >= 2) {
		    mnprin_(&c__2, &mn7min_1.amin);
		}
	    }
	    goto L300;
	} else {
	    ++ifail;
/*                   Metropolis algorithm */
	    bar = (mn7min_1.amin - ftry) / mn7min_1.up;
	    mnrn15_(&rnum, &iseed);
	    if (bar < log(rnum)) {
		goto L500;
	    }
	}
/*                    Accept new point, move there */
L300:
	i__2 = mn7npr_1.npar;
	for (j = 1; j <= i__2; ++j) {
	    xmid[j - 1] = mn7int_1.x[j - 1];
/* L350: */
	}
	flast = ftry;
L500:
	;
    }
/*                               end search loop */
L600:
    if (mn7flg_1.isw[4] > 1) {
	io___22.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___22);
	do_fio(&c__1, (char *)&ifail, (ftnlen)sizeof(integer));
	e_wsfe();
    }
    i__1 = mn7npr_1.npar;
    for (ib = 1; ib <= i__1; ++ib) {
/* L700: */
	mn7int_1.x[ib - 1] = xbest[ib - 1];
    }
    mninex_(mn7int_1.x);
    if (mn7flg_1.isw[4] >= 1) {
	mnprin_(&c__2, &mn7min_1.amin);
    }
    if (mn7flg_1.isw[4] == 0) {
	mnprin_(&c__0, &mn7min_1.amin);
    }
    return 0;
} /* mnseek_ */

