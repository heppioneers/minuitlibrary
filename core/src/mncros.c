/* mncros.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__3 = 3;


/* $Id: mncros.F,v 1.1.1.1 1996/03/07 14:31:29 mclareni Exp $ */

/* $Log: mncros.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mncros_(fcn, aopt, iercr, futil)
/* Subroutine */ int (*fcn) ();
doublereal *aopt;
integer *iercr;
/* Subroutine */ int (*futil) ();
{
    /* Initialized data */

    static char charal[28+1] = " .ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2, d__3, d__4;

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();
    double sqrt();
    /* Subroutine */ int s_copy();

    /* Local variables */
    static doublereal alsb[3], flsb[3], bmin, bmax;
    static integer inew;
    static doublereal zmid, sdev, zdir, zlim;
    static integer iout, i;
    static doublereal coeff[3], aleft, ecart;
    static integer ileft;
    static doublereal aulim, fdist;
    static integer ierev;
    static doublereal adist;
    static integer maxlk, ibest;
    static doublereal anext, fnext, slope, s1, s2, x1, x2;
    static integer ik, it;
    static logical ldebug;
    static char chsign[4];
    extern /* Subroutine */ int mneval_();
    static doublereal aminsv;
    extern /* Subroutine */ int mnwarn_();
    static doublereal ecarmn, ecarmx;
    static integer noless, iworst;
    extern /* Subroutine */ int mnpfit_();
    static doublereal determ, rt;
    static integer iright;
    static doublereal smalla, aright;
    static integer itoohi;
    extern /* Subroutine */ int mnplot_();
    static doublereal aim, tla, tlf;
    static integer kex, ipt;
    static doublereal dfda;

    /* Fortran I/O blocks */
    static cilist io___17 = { 0, 0, 0, "(A,I8,A,F10.5,A,2F10.5)", 0 };
    static cilist io___20 = { 0, 0, 0, "(A,I8,A,F10.5,A,2F10.5)", 0 };
    static cilist io___24 = { 0, 0, 0, "(A,I8,A,F10.5,A,2F10.5)", 0 };
    static cilist io___29 = { 0, 0, 0, "(A,I8,A,F10.5,A,2F10.5)", 0 };
    static cilist io___46 = { 0, 0, 0, "(A)", 0 };
    static cilist io___54 = { 0, 0, 0, "(A,I8,A,F10.5,A,2F10.5)", 0 };
    static cilist io___57 = { 0, 0, 0, "(2X,A,A,I3)", 0 };
    static cilist io___58 = { 0, 0, 0, "(10X,A)", 0 };
    static cilist io___59 = { 0, 0, 0, "(10X,A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C       Find point where MNEVAL=AMIN+UP, along the line through */
/* C       XMIDCR,YMIDCR with direction XDIRCR,YDIRCR,   where X and Y */
/* C       are parameters KE1CR and KE2CR.  If KE2CR=0 (from MINOS), */
/* C       only KE1CR is varied.  From MNCONT, both are varied. */
/* C       Crossing point is at */
/* C        (U(KE1),U(KE2)) = (XMID,YMID) + AOPT*(XDIR,YDIR) */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    ldebug = mn7flg_1.idbg[6] >= 1;
    aminsv = mn7min_1.amin;
/*        convergence when F is within TLF of AIM and next prediction */
/*        of AOPT is within TLA of previous value of AOPT */
    aim = mn7min_1.amin + mn7min_1.up;
    tlf = mn7min_1.up * .01;
    tla = .01;
    mn7rpt_1.xpt[0] = (float)0.;
    mn7rpt_1.ypt[0] = aim;
    *(unsigned char *)&mn7cpt_1.chpt[0] = ' ';
    ipt = 1;
    if (mn7xcr_1.ke2cr == 0) {
	mn7rpt_1.xpt[1] = (float)-1.;
	mn7rpt_1.ypt[1] = mn7min_1.amin;
	*(unsigned char *)&mn7cpt_1.chpt[1] = '.';
	ipt = 2;
    }
/*                    find the largest allowed A */
    aulim = (float)100.;
    for (ik = 1; ik <= 2; ++ik) {
	if (ik == 1) {
	    kex = mn7xcr_1.ke1cr;
	    zmid = mn7xcr_1.xmidcr;
	    zdir = mn7xcr_1.xdircr;
	} else {
	    if (mn7xcr_1.ke2cr == 0) {
		goto L100;
	    }
	    kex = mn7xcr_1.ke2cr;
	    zmid = mn7xcr_1.ymidcr;
	    zdir = mn7xcr_1.ydircr;
	}
	if (mn7inx_1.nvarl[kex - 1] <= 1) {
	    goto L100;
	}
	if (zdir == 0.) {
	    goto L100;
	}
	zlim = mn7ext_1.alim[kex - 1];
	if (zdir > 0.) {
	    zlim = mn7ext_1.blim[kex - 1];
	}
/* Computing MIN */
	d__1 = aulim, d__2 = (zlim - zmid) / zdir;
	aulim = min(d__1,d__2);
L100:
	;
    }
/*                  LSB = Line Search Buffer */
/*          first point */
    anext = (float)0.;
    *aopt = anext;
    mn7log_1.limset = FALSE_;
    if (aulim < *aopt + tla) {
	mn7log_1.limset = TRUE_;
    }
    mneval_(fcn, &anext, &fnext, &ierev, futil);
/* debug printout: */
    if (ldebug) {
	io___17.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___17);
	do_fio(&c__1, " MNCROS: calls=", 15L);
	do_fio(&c__1, (char *)&mn7cnv_1.nfcn, (ftnlen)sizeof(integer));
	do_fio(&c__1, "   AIM=", 7L);
	do_fio(&c__1, (char *)&aim, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, "  F,A=", 6L);
	do_fio(&c__1, (char *)&fnext, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*aopt), (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    if (ierev > 0) {
	goto L900;
    }
    if (mn7log_1.limset && fnext <= aim) {
	goto L930;
    }
    ++ipt;
    mn7rpt_1.xpt[ipt - 1] = anext;
    mn7rpt_1.ypt[ipt - 1] = fnext;
    *(unsigned char *)&mn7cpt_1.chpt[ipt - 1] = *(unsigned char *)&charal[ipt 
	    - 1];
    alsb[0] = anext;
    flsb[0] = fnext;
/* Computing MAX */
    d__1 = fnext, d__2 = aminsv + mn7min_1.up * (float).1;
    fnext = max(d__1,d__2);
    *aopt = sqrt(mn7min_1.up / (fnext - aminsv)) - (float)1.;
    if ((d__1 = fnext - aim, abs(d__1)) < tlf) {
	goto L800;
    }

    if (*aopt < -.5) {
	*aopt = -.5;
    }
    if (*aopt > 1.) {
	*aopt = 1.;
    }
    mn7log_1.limset = FALSE_;
    if (*aopt > aulim) {
	*aopt = aulim;
	mn7log_1.limset = TRUE_;
    }
    mneval_(fcn, aopt, &fnext, &ierev, futil);
/* debug printout: */
    if (ldebug) {
	io___20.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___20);
	do_fio(&c__1, " MNCROS: calls=", 15L);
	do_fio(&c__1, (char *)&mn7cnv_1.nfcn, (ftnlen)sizeof(integer));
	do_fio(&c__1, "   AIM=", 7L);
	do_fio(&c__1, (char *)&aim, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, "  F,A=", 6L);
	do_fio(&c__1, (char *)&fnext, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*aopt), (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    if (ierev > 0) {
	goto L900;
    }
    if (mn7log_1.limset && fnext <= aim) {
	goto L930;
    }
    alsb[1] = *aopt;
    ++ipt;
    mn7rpt_1.xpt[ipt - 1] = alsb[1];
    mn7rpt_1.ypt[ipt - 1] = fnext;
    *(unsigned char *)&mn7cpt_1.chpt[ipt - 1] = *(unsigned char *)&charal[ipt 
	    - 1];
    flsb[1] = fnext;
    dfda = (flsb[1] - flsb[0]) / (alsb[1] - alsb[0]);
/*                   DFDA must be positive on the contour */
    if (dfda > 0.) {
	goto L460;
    }
L300:
    mnwarn_("D", "MNCROS    ", "Looking for slope of the right sign", 1L, 10L,
	     35L);
    maxlk = 15 - ipt;
    i__1 = maxlk;
    for (it = 1; it <= i__1; ++it) {
	alsb[0] = alsb[1];
	flsb[0] = flsb[1];
	*aopt = alsb[0] + (real) it * (float).2;
	mn7log_1.limset = FALSE_;
	if (*aopt > aulim) {
	    *aopt = aulim;
	    mn7log_1.limset = TRUE_;
	}
	mneval_(fcn, aopt, &fnext, &ierev, futil);
/* debug printout: */
	if (ldebug) {
	    io___24.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___24);
	    do_fio(&c__1, " MNCROS: calls=", 15L);
	    do_fio(&c__1, (char *)&mn7cnv_1.nfcn, (ftnlen)sizeof(integer));
	    do_fio(&c__1, "   AIM=", 7L);
	    do_fio(&c__1, (char *)&aim, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, "  F,A=", 6L);
	    do_fio(&c__1, (char *)&fnext, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*aopt), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	}
	if (ierev > 0) {
	    goto L900;
	}
	if (mn7log_1.limset && fnext <= aim) {
	    goto L930;
	}
	alsb[1] = *aopt;
	++ipt;
	mn7rpt_1.xpt[ipt - 1] = alsb[1];
	mn7rpt_1.ypt[ipt - 1] = fnext;
	*(unsigned char *)&mn7cpt_1.chpt[ipt - 1] = *(unsigned char *)&charal[
		ipt - 1];
	flsb[1] = fnext;
	dfda = (flsb[1] - flsb[0]) / (alsb[1] - alsb[0]);
	if (dfda > 0.) {
	    goto L450;
	}
/* L400: */
    }
    mnwarn_("W", "MNCROS    ", "Cannot find slope of the right sign", 1L, 10L,
	     35L);
    goto L950;
L450:
/*                    we have two points with the right slope */
L460:
    *aopt = alsb[1] + (aim - flsb[1]) / dfda;
/* Computing MIN */
    d__3 = (d__1 = aim - flsb[0], abs(d__1)), d__4 = (d__2 = aim - flsb[1], 
	    abs(d__2));
    fdist = min(d__3,d__4);
/* Computing MIN */
    d__3 = (d__1 = *aopt - alsb[0], abs(d__1)), d__4 = (d__2 = *aopt - alsb[1]
	    , abs(d__2));
    adist = min(d__3,d__4);
    tla = .01;
    if (abs(*aopt) > 1.) {
	tla = abs(*aopt) * .01;
    }
    if (adist < tla && fdist < tlf) {
	goto L800;
    }
    if (ipt >= 15) {
	goto L950;
    }
    bmin = min(alsb[0],alsb[1]) - (float)1.;
    if (*aopt < bmin) {
	*aopt = bmin;
    }
    bmax = max(alsb[0],alsb[1]) + (float)1.;
    if (*aopt > bmax) {
	*aopt = bmax;
    }
/*                    Try a third point */
    mn7log_1.limset = FALSE_;
    if (*aopt > aulim) {
	*aopt = aulim;
	mn7log_1.limset = TRUE_;
    }
    mneval_(fcn, aopt, &fnext, &ierev, futil);
/* debug printout: */
    if (ldebug) {
	io___29.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___29);
	do_fio(&c__1, " MNCROS: calls=", 15L);
	do_fio(&c__1, (char *)&mn7cnv_1.nfcn, (ftnlen)sizeof(integer));
	do_fio(&c__1, "   AIM=", 7L);
	do_fio(&c__1, (char *)&aim, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, "  F,A=", 6L);
	do_fio(&c__1, (char *)&fnext, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*aopt), (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    if (ierev > 0) {
	goto L900;
    }
    if (mn7log_1.limset && fnext <= aim) {
	goto L930;
    }
    alsb[2] = *aopt;
    ++ipt;
    mn7rpt_1.xpt[ipt - 1] = alsb[2];
    mn7rpt_1.ypt[ipt - 1] = fnext;
    *(unsigned char *)&mn7cpt_1.chpt[ipt - 1] = *(unsigned char *)&charal[ipt 
	    - 1];
    flsb[2] = fnext;
    inew = 3;
/*                now we have three points, ask how many <AIM */
    ecarmn = (d__1 = fnext - aim, abs(d__1));
    ibest = 3;
    ecarmx = (float)0.;
    noless = 0;
    for (i = 1; i <= 3; ++i) {
	ecart = (d__1 = flsb[i - 1] - aim, abs(d__1));
	if (ecart > ecarmx) {
	    ecarmx = ecart;
	    iworst = i;
	}
	if (ecart < ecarmn) {
	    ecarmn = ecart;
	    ibest = i;
	}
	if (flsb[i - 1] < aim) {
	    ++noless;
	}
/* L480: */
    }
    inew = ibest;
/*           if at least one on each side of AIM, fit a parabola */
    if (noless == 1 || noless == 2) {
	goto L500;
    }
/*           if all three are above AIM, third must be closest to AIM */
    if (noless == 0 && ibest != 3) {
	goto L950;
    }
/*           if all three below, and third is not best, then slope */
/*             has again gone negative, look for positive slope. */
    if (noless == 3 && ibest != 3) {
	alsb[1] = alsb[2];
	flsb[1] = flsb[2];
	goto L300;
    }
/*           in other cases, new straight line thru last two points */
    alsb[iworst - 1] = alsb[2];
    flsb[iworst - 1] = flsb[2];
    dfda = (flsb[1] - flsb[0]) / (alsb[1] - alsb[0]);
    goto L460;
/*                parabola fit */
L500:
    mnpfit_(alsb, flsb, &c__3, coeff, &sdev);
    if (coeff[2] <= 0.) {
	mnwarn_("D", "MNCROS    ", "Curvature is negative near contour line.",
		 1L, 10L, 40L);
    }
/* Computing 2nd power */
    d__1 = coeff[1];
    determ = d__1 * d__1 - coeff[2] * (float)4. * (coeff[0] - aim);
    if (determ <= 0.) {
	mnwarn_("D", "MNCROS    ", "Problem 2, impossible determinant", 1L, 
		10L, 33L);
	goto L950;
    }
/*                Find which root is the right one */
    rt = sqrt(determ);
    x1 = (-coeff[1] + rt) / (coeff[2] * (float)2.);
    x2 = (-coeff[1] - rt) / (coeff[2] * (float)2.);
    s1 = coeff[1] + x1 * (float)2. * coeff[2];
    s2 = coeff[1] + x2 * (float)2. * coeff[2];
    if (s1 * s2 > 0.) {
	io___46.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___46);
	do_fio(&c__1, " MNCONTour problem 1", 20L);
	e_wsfe();
    }
    *aopt = x1;
    slope = s1;
    if (s2 > 0.) {
	*aopt = x2;
	slope = s2;
    }
/*         ask if converged */
    tla = .01;
    if (abs(*aopt) > 1.) {
	tla = abs(*aopt) * .01;
    }
    if ((d__1 = *aopt - alsb[ibest - 1], abs(d__1)) < tla && (d__2 = flsb[
	    ibest - 1] - aim, abs(d__2)) < tlf) {
	goto L800;
    }
    if (ipt >= 15) {
	goto L950;
    }
/*         see if proposed point is in acceptable zone between L and R */
/*         first find ILEFT, IRIGHT, IOUT and IBEST */
    ileft = 0;
    iright = 0;
    ibest = 1;
    ecarmx = (float)0.;
    ecarmn = (d__1 = aim - flsb[0], abs(d__1));
    for (i = 1; i <= 3; ++i) {
	ecart = (d__1 = flsb[i - 1] - aim, abs(d__1));
	if (ecart < ecarmn) {
	    ecarmn = ecart;
	    ibest = i;
	}
	if (ecart > ecarmx) {
	    ecarmx = ecart;
	}
	if (flsb[i - 1] > aim) {
	    if (iright == 0) {
		iright = i;
	    } else if (flsb[i - 1] > flsb[iright - 1]) {
		iout = i;
	    } else {
		iout = iright;
		iright = i;
	    }
	} else if (ileft == 0) {
	    ileft = i;
	} else if (flsb[i - 1] < flsb[ileft - 1]) {
	    iout = i;
	} else {
	    iout = ileft;
	    ileft = i;
	}
/* L550: */
    }
/*       avoid keeping a very bad point next time around */
    if (ecarmx > (d__1 = flsb[iout - 1] - aim, abs(d__1)) * (float)10.) {
	*aopt = *aopt * .5 + (alsb[iright - 1] + alsb[ileft - 1]) * .25;
    }
/*         knowing ILEFT and IRIGHT, get acceptable window */
    smalla = tla * (float).1;
    if (slope * smalla > tlf) {
	smalla = tlf / slope;
    }
    aleft = alsb[ileft - 1] + smalla;
    aright = alsb[iright - 1] - smalla;
/*         move proposed point AOPT into window if necessary */
    if (*aopt < aleft) {
	*aopt = aleft;
    }
    if (*aopt > aright) {
	*aopt = aright;
    }
    if (aleft > aright) {
	*aopt = (aleft + aright) * .5;
    }
/*         see if proposed point outside limits (should be impossible!) */
    mn7log_1.limset = FALSE_;
    if (*aopt > aulim) {
	*aopt = aulim;
	mn7log_1.limset = TRUE_;
    }
/*                  Evaluate function at new point AOPT */
    mneval_(fcn, aopt, &fnext, &ierev, futil);
/* debug printout: */
    if (ldebug) {
	io___54.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___54);
	do_fio(&c__1, " MNCROS: calls=", 15L);
	do_fio(&c__1, (char *)&mn7cnv_1.nfcn, (ftnlen)sizeof(integer));
	do_fio(&c__1, "   AIM=", 7L);
	do_fio(&c__1, (char *)&aim, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, "  F,A=", 6L);
	do_fio(&c__1, (char *)&fnext, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*aopt), (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    if (ierev > 0) {
	goto L900;
    }
    if (mn7log_1.limset && fnext <= aim) {
	goto L930;
    }
    ++ipt;
    mn7rpt_1.xpt[ipt - 1] = *aopt;
    mn7rpt_1.ypt[ipt - 1] = fnext;
    *(unsigned char *)&mn7cpt_1.chpt[ipt - 1] = *(unsigned char *)&charal[ipt 
	    - 1];
/*                Replace odd point by new one */
    alsb[iout - 1] = *aopt;
    flsb[iout - 1] = fnext;
/*          the new point may not be the best, but it is the only one */
/*          which could be good enough to pass convergence criteria */
    ibest = iout;
    goto L500;

/*       Contour has been located, return point to MNCONT OR MINOS */
L800:
    *iercr = 0;
    goto L1000;
/*                error in the minimization */
L900:
    if (ierev == 1) {
	goto L940;
    }
    goto L950;
/*                parameter up against limit */
L930:
    *iercr = 1;
    goto L1000;
/*                too many calls to FCN */
L940:
    *iercr = 2;
    goto L1000;
/*                cannot find next point */
L950:
    *iercr = 3;
/*                in any case */
L1000:
    if (ldebug) {
	itoohi = 0;
	i__1 = ipt;
	for (i = 1; i <= i__1; ++i) {
	    if (mn7rpt_1.ypt[i - 1] > aim + mn7min_1.up) {
		mn7rpt_1.ypt[i - 1] = aim + mn7min_1.up;
		*(unsigned char *)&mn7cpt_1.chpt[i - 1] = '+';
		itoohi = 1;
	    }
/* L1100: */
	}
	s_copy(chsign, "POSI", 4L, 4L);
	if (mn7xcr_1.xdircr < 0.) {
	    s_copy(chsign, "NEGA", 4L, 4L);
	}
	if (mn7xcr_1.ke2cr == 0) {
	    io___57.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___57);
	    do_fio(&c__1, chsign, 4L);
	    do_fio(&c__1, "TIVE MINOS ERROR, PARAMETER ", 28L);
	    do_fio(&c__1, (char *)&mn7xcr_1.ke1cr, (ftnlen)sizeof(integer));
	    e_wsfe();
	}
	if (itoohi == 1) {
	    io___58.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___58);
	    do_fio(&c__1, "POINTS LABELLED \"+\" WERE TOO HIGH TO PLOT.", 42L)
		    ;
	    e_wsfe();
	}
	if (*iercr == 1) {
	    io___59.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___59);
	    do_fio(&c__1, "RIGHTMOST POINT IS UP AGAINST LIMIT.", 36L);
	    e_wsfe();
	}
	mnplot_(mn7rpt_1.xpt, mn7rpt_1.ypt, mn7cpt_1.chpt, &ipt, &
		mn7iou_1.isyswr, &mn7iou_1.npagwd, &mn7iou_1.npagln, 1L);
    }
    return 0;
} /* mncros_ */

