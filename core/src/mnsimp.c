/* mnsimp.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__4 = 4;
static integer c__5 = 5;


/* $Id: mnsimp.F,v 1.2 1996/03/15 18:02:54 james Exp $ */

/* $Log: mnsimp.F,v $ */
/* Revision 1.2  1996/03/15 18:02:54  james */
/*     Modified Files: */
/* mnderi.F eliminate possible division by zero */
/* mnexcm.F suppress print on STOP when print flag=-1 */
/*          set FVAL3 to flag if FCN already called with IFLAG=3 */
/* mninit.F set version 96.03 */
/* mnlims.F remove arguments, not needed */
/* mnmigr.F VLEN -> LENV in debug print statement */
/* mnparm.F move call to MNRSET to after NPAR redefined, to zero all */
/* mnpsdf.F eliminate possible division by zero */
/* mnscan.F suppress printout when print flag =-1 */
/* mnset.F  remove arguments in call to MNLIMS */
/* mnsimp.F fix CSTATU so status is PROGRESS only if new minimum */
/* mnvert.F eliminate possible division by zero */

/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnsimp_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    /* Initialized data */

    static doublereal alpha = 1.;
    static doublereal beta = .5;
    static doublereal gamma = 2.;
    static doublereal rhomin = 4.;
    static doublereal rhomax = 8.;

    /* Format strings */
    static char fmt_100[] = "(\002 START SIMPLEX MINIMIZATION.    CONVERGENC\
E WHEN EDM .LT.\002,e10.2)";

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe();

    /* Local variables */
    static doublereal dmin__, dxdi;
    static integer npfn;
    static doublereal yrho, f, ynpp1;
    static integer i, j, k;
    static doublereal y[51], aming;
    static integer jhold, ncycl;
    static doublereal ypbar;
    static integer nparx;
    static doublereal bestx, ystar, y1, y2, ystst;
    static integer nparp1, kg, jh, nf;
    static doublereal pb;
    static integer jl;
    static doublereal wg;
    static integer ns;
    extern /* Subroutine */ int mnamin_(), mndxdi_();
    static doublereal absmin;
    extern /* Subroutine */ int mninex_(), mnrazz_(), mnprin_();
    static doublereal rho, sig2, rho1, rho2;

    /* Fortran I/O blocks */
    static cilist io___12 = { 0, 0, 0, fmt_100, 0 };
    static cilist io___39 = { 0, 0, 0, "(A)", 0 };
    static cilist io___40 = { 0, 0, 0, "(A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Performs a minimization using the simplex method of Nelder */
/* C        and Mead (ref. -- Comp. J. 7,308 (1965)). */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    if (mn7npr_1.npar <= 0) {
	return 0;
    }
    if (mn7min_1.amin == mn7cns_1.undefi) {
	mnamin_(fcn, futil);
    }
    s_copy(mn7tit_1.cfrom, "SIMPLEX ", 8L, 8L);
    mn7cnv_1.nfcnfr = mn7cnv_1.nfcn;
    s_copy(mn7tit_1.cstatu, "UNCHANGED ", 10L, 10L);
    npfn = mn7cnv_1.nfcn;
    nparp1 = mn7npr_1.npar + 1;
    nparx = mn7npr_1.npar;
    rho1 = alpha + (float)1.;
    rho2 = rho1 + alpha * gamma;
    wg = (float)1. / (real) mn7npr_1.npar;
    if (mn7flg_1.isw[4] >= 0) {
	io___12.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___12);
	do_fio(&c__1, (char *)&mn7min_1.epsi, (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	mn7int_1.dirin[i - 1] = mn7err_1.werr[i - 1];
	mndxdi_(&mn7int_1.x[i - 1], &i, &dxdi);
	if (dxdi != 0.) {
	    mn7int_1.dirin[i - 1] = mn7err_1.werr[i - 1] / dxdi;
	}
	dmin__ = mn7cns_1.epsma2 * (d__1 = mn7int_1.x[i - 1], abs(d__1));
	if (mn7int_1.dirin[i - 1] < dmin__) {
	    mn7int_1.dirin[i - 1] = dmin__;
	}
/* L2: */
    }
/* **       choose the initial simplex using single-parameter searches */
L1:
    ynpp1 = mn7min_1.amin;
    jl = nparp1;
    y[nparp1 - 1] = mn7min_1.amin;
    absmin = mn7min_1.amin;
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	aming = mn7min_1.amin;
	mn7sim_1.pbar[i - 1] = mn7int_1.x[i - 1];
	bestx = mn7int_1.x[i - 1];
	kg = 0;
	ns = 0;
	nf = 0;
L4:
	mn7int_1.x[i - 1] = bestx + mn7int_1.dirin[i - 1];
	mninex_(mn7int_1.x);
	(*fcn)(&nparx, mn7der_1.gin, &f, mn7ext_1.u, &c__4, futil);
	++mn7cnv_1.nfcn;
	if (f < aming) {
	    goto L6;
	}
/*         failure */
	if (kg == 1) {
	    goto L8;
	}
	kg = -1;
	++nf;
	mn7int_1.dirin[i - 1] *= (float)-.4;
	if (nf < 3) {
	    goto L4;
	}
/*         stop after three failures */
	bestx = mn7int_1.x[i - 1];
	mn7int_1.dirin[i - 1] *= (float)3.;
	aming = f;
	goto L8;

/*         success */
L6:
	bestx = mn7int_1.x[i - 1];
	mn7int_1.dirin[i - 1] *= (float)3.;
	aming = f;
	s_copy(mn7tit_1.cstatu, "PROGRESS  ", 10L, 10L);
	kg = 1;
	++ns;
	if (ns < 6) {
	    goto L4;
	}

/*         3 failures or 6 successes or */
/*         local minimum found in ith direction */
L8:
	y[i - 1] = aming;
	if (aming < absmin) {
	    jl = i;
	}
	if (aming < absmin) {
	    absmin = aming;
	}
	mn7int_1.x[i - 1] = bestx;
	i__2 = mn7npr_1.npar;
	for (k = 1; k <= i__2; ++k) {
/* L9: */
	    mn7sim_1.p[k + i * 50 - 51] = mn7int_1.x[k - 1];
	}
/* L10: */
    }
    jh = nparp1;
    mn7min_1.amin = y[jl - 1];
    mnrazz_(&ynpp1, mn7sim_1.pbar, y, &jh, &jl);
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
/* L20: */
	mn7int_1.x[i - 1] = mn7sim_1.p[i + jl * 50 - 51];
    }
    mninex_(mn7int_1.x);
    if (mn7flg_1.isw[4] >= 1) {
	mnprin_(&c__5, &mn7min_1.amin);
    }
    mn7min_1.edm = mn7cns_1.bigedm;
    sig2 = mn7min_1.edm;
    ncycl = 0;
/*                                        . . . . .  start main loop */
L50:
    if (sig2 < mn7min_1.epsi && mn7min_1.edm < mn7min_1.epsi) {
	goto L76;
    }
    sig2 = mn7min_1.edm;
    if (mn7cnv_1.nfcn - npfn > mn7cnv_1.nfcnmx) {
	goto L78;
    }
/*         calculate new point * by reflection */
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	pb = (float)0.;
	i__2 = nparp1;
	for (j = 1; j <= i__2; ++j) {
/* L59: */
	    pb += wg * mn7sim_1.p[i + j * 50 - 51];
	}
	mn7sim_1.pbar[i - 1] = pb - wg * mn7sim_1.p[i + jh * 50 - 51];
/* L60: */
	mn7sim_1.pstar[i - 1] = (alpha + (float)1.) * mn7sim_1.pbar[i - 1] - 
		alpha * mn7sim_1.p[i + jh * 50 - 51];
    }
    mninex_(mn7sim_1.pstar);
    (*fcn)(&nparx, mn7der_1.gin, &ystar, mn7ext_1.u, &c__4, futil);
    ++mn7cnv_1.nfcn;
    if (ystar >= mn7min_1.amin) {
	goto L70;
    }
/*         point * better than jl, calculate new point ** */
    s_copy(mn7tit_1.cstatu, "PROGRESS  ", 10L, 10L);
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
/* L61: */
	mn7sim_1.pstst[i - 1] = gamma * mn7sim_1.pstar[i - 1] + ((float)1. - 
		gamma) * mn7sim_1.pbar[i - 1];
    }
    mninex_(mn7sim_1.pstst);
    (*fcn)(&nparx, mn7der_1.gin, &ystst, mn7ext_1.u, &c__4, futil);
    ++mn7cnv_1.nfcn;
/*         try a parabola through ph, pstar, pstst.  min = prho */
    y1 = (ystar - y[jh - 1]) * rho2;
    y2 = (ystst - y[jh - 1]) * rho1;
    rho = (rho2 * y1 - rho1 * y2) * (float).5 / (y1 - y2);
    if (rho < rhomin) {
	goto L66;
    }
    if (rho > rhomax) {
	rho = rhomax;
    }
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
/* L64: */
	mn7sim_1.prho[i - 1] = rho * mn7sim_1.pbar[i - 1] + ((float)1. - rho) 
		* mn7sim_1.p[i + jh * 50 - 51];
    }
    mninex_(mn7sim_1.prho);
    (*fcn)(&nparx, mn7der_1.gin, &yrho, mn7ext_1.u, &c__4, futil);
    ++mn7cnv_1.nfcn;
    if (yrho < mn7min_1.amin) {
	s_copy(mn7tit_1.cstatu, "PROGRESS  ", 10L, 10L);
    }
    if (yrho < y[jl - 1] && yrho < ystst) {
	goto L65;
    }
    if (ystst < y[jl - 1]) {
	goto L67;
    }
    if (yrho > y[jl - 1]) {
	goto L66;
    }
/*         accept minimum point of parabola, PRHO */
L65:
    mnrazz_(&yrho, mn7sim_1.prho, y, &jh, &jl);
    goto L68;
L66:
    if (ystst < y[jl - 1]) {
	goto L67;
    }
    mnrazz_(&ystar, mn7sim_1.pstar, y, &jh, &jl);
    goto L68;
L67:
    mnrazz_(&ystst, mn7sim_1.pstst, y, &jh, &jl);
L68:
    ++ncycl;
    if (mn7flg_1.isw[4] < 2) {
	goto L50;
    }
    if (mn7flg_1.isw[4] >= 3 || ncycl % 10 == 0) {
	mnprin_(&c__5, &mn7min_1.amin);
    }
    goto L50;
/*         point * is not as good as jl */
L70:
    if (ystar >= y[jh - 1]) {
	goto L73;
    }
    jhold = jh;
    mnrazz_(&ystar, mn7sim_1.pstar, y, &jh, &jl);
    if (jhold != jh) {
	goto L50;
    }
/*         calculate new point ** */
L73:
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
/* L74: */
	mn7sim_1.pstst[i - 1] = beta * mn7sim_1.p[i + jh * 50 - 51] + ((float)
		1. - beta) * mn7sim_1.pbar[i - 1];
    }
    mninex_(mn7sim_1.pstst);
    (*fcn)(&nparx, mn7der_1.gin, &ystst, mn7ext_1.u, &c__4, futil);
    ++mn7cnv_1.nfcn;
    if (ystst > y[jh - 1]) {
	goto L1;
    }
/*     point ** is better than jh */
    if (ystst < mn7min_1.amin) {
	s_copy(mn7tit_1.cstatu, "PROGRESS  ", 10L, 10L);
    }
    if (ystst < mn7min_1.amin) {
	goto L67;
    }
    mnrazz_(&ystst, mn7sim_1.pstst, y, &jh, &jl);
    goto L50;
/*                                        . . . . . .  end main loop */
L76:
    if (mn7flg_1.isw[4] >= 0) {
	io___39.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___39);
	do_fio(&c__1, " SIMPLEX MINIMIZATION HAS CONVERGED.", 36L);
	e_wsfe();
    }
    mn7flg_1.isw[3] = 1;
    goto L80;
L78:
    if (mn7flg_1.isw[4] >= 0) {
	io___40.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___40);
	do_fio(&c__1, " SIMPLEX TERMINATES WITHOUT CONVERGENCE.", 40L);
	e_wsfe();
    }
    s_copy(mn7tit_1.cstatu, "CALL LIMIT", 10L, 10L);
    mn7flg_1.isw[3] = -1;
    mn7flg_1.isw[0] = 1;
L80:
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	pb = (float)0.;
	i__2 = nparp1;
	for (j = 1; j <= i__2; ++j) {
/* L81: */
	    pb += wg * mn7sim_1.p[i + j * 50 - 51];
	}
/* L82: */
	mn7sim_1.pbar[i - 1] = pb - wg * mn7sim_1.p[i + jh * 50 - 51];
    }
    mninex_(mn7sim_1.pbar);
    (*fcn)(&nparx, mn7der_1.gin, &ypbar, mn7ext_1.u, &c__4, futil);
    ++mn7cnv_1.nfcn;
    if (ypbar < mn7min_1.amin) {
	mnrazz_(&ypbar, mn7sim_1.pbar, y, &jh, &jl);
    }
    mninex_(mn7int_1.x);
    if (mn7cnv_1.nfcnmx + npfn - mn7cnv_1.nfcn < mn7npr_1.npar * 3) {
	goto L90;
    }
    if (mn7min_1.edm > mn7min_1.epsi * (float)2.) {
	goto L1;
    }
L90:
    if (mn7flg_1.isw[4] >= 0) {
	mnprin_(&c__5, &mn7min_1.amin);
    }
    return 0;
} /* mnsimp_ */

