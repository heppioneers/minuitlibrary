/* mnstin.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;


/* $Id: mnstin.F,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: mnstin.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */


/* Subroutine */ int mnstin_(crdbuf, ierr, crdbuf_len)
char *crdbuf;
integer *ierr;
ftnlen crdbuf_len;
{
    /* Format strings */
    static char fmt_132[] = "(\002 UNIT\002,i3,\002 ALREADY OPENED WITH NA\
ME:\002,a/\002                 NEW NAME IGNORED:\002,a)";
    static char fmt_135[] = "(\002 UNIT\002,i3,\002 IS NOT OPENED.\002)";
    static char fmt_137[] = "(\002 SHOULD UNIT\002,i3,\002 BE REWOUND?\002)";
    static char fmt_290[] = "(\002 INPUT WILL NOW BE READ IN \002,a,\002 FRO\
M UNIT NO.\002,i3/\002 FILENAME: \002,a)";
    static char fmt_601[] = "(\002 SYSTEM IS UNABLE TO OPEN FILE:\002,a)";

    /* System generated locals */
    integer i__1;
    olist o__1;
    alist al__1;
    inlist ioin__1;

    /* Builtin functions */
    integer i_indx(), i_len();
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe(), s_rsfi(), e_rsfi(), f_inqu(), 
	    s_rsfe(), e_rsfe(), f_open(), f_rew();

    /* Local variables */
    static integer lend, icol;
    static char cmode[16];
    static logical lname, lopen;
    static char cunit[10];
    static doublereal funit;
    static integer iunit, ic;
    static char cfname[64], cgname[64];
    static logical noname;
    static char canswr[1];
    static logical lrewin;
    extern logical mnunpt_();
    static integer ic1, ic2;

    /* Fortran I/O blocks */
    static cilist io___9 = { 0, 0, 0, "(A,A)", 0 };
    static icilist io___10 = { 1, cunit, 0, "(BN,F10.0)", 10, 1 };
    static cilist io___14 = { 0, 0, 0, "(A,A)", 0 };
    static cilist io___18 = { 0, 0, 0, fmt_132, 0 };
    static cilist io___19 = { 0, 0, 0, fmt_135, 0 };
    static cilist io___20 = { 0, 0, 0, "(A)", 0 };
    static cilist io___21 = { 0, 0, 0, "(A)", 0 };
    static cilist io___22 = { 0, 0, 0, "(A)", 0 };
    static cilist io___23 = { 0, 0, 0, "(A)", 0 };
    static cilist io___24 = { 0, 0, 0, fmt_137, 0 };
    static cilist io___25 = { 0, 0, 0, "(A)", 0 };
    static cilist io___27 = { 0, 0, 0, "(A,A)", 0 };
    static cilist io___28 = { 0, 0, 0, "(A)", 0 };
    static cilist io___30 = { 0, 0, 0, fmt_290, 0 };
    static cilist io___31 = { 0, 0, 0, "(A)", 0 };
    static cilist io___32 = { 0, 0, 0, "(A,A)", 0 };
    static cilist io___33 = { 0, 0, 0, fmt_601, 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C Called from MNREAD. */
/* C Implements the SET INPUT command to change input units. */
/* C If command is: 'SET INPUT'   'SET INPUT 0'   or  '*EOF', */
/* C                 or 'SET INPUT , ,  ', */
/* C                reverts to previous input unit number,if any. */
/* C */
/* C      If it is: 'SET INPUT n'  or  'SET INPUT n filename', */
/* C                changes to new input file, added to stack */
/* C */
/* C      IERR = 0: reading terminated normally */
/* C             2: end-of-data on primary input file */
/* C             3: unrecoverable read error */
/* C             4: unable to process request */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    noname = TRUE_;
    *ierr = 0;
    if (i_indx(crdbuf, "*EOF", crdbuf_len, 4L) == 1) {
	goto L190;
    }
    if (i_indx(crdbuf, "*eof", crdbuf_len, 4L) == 1) {
	goto L190;
    }
    lend = i_len(crdbuf, crdbuf_len);
/*                               look for end of SET INPUT command */
    i__1 = lend;
    for (ic = 8; ic <= i__1; ++ic) {
	if (*(unsigned char *)&crdbuf[ic - 1] == ' ') {
	    goto L25;
	}
	if (*(unsigned char *)&crdbuf[ic - 1] == ',') {
	    goto L53;
	}
/* L20: */
    }
    goto L200;
L25:
/*         look for end of separator between command and first argument */
    icol = ic + 1;
    i__1 = lend;
    for (ic = icol; ic <= i__1; ++ic) {
	if (*(unsigned char *)&crdbuf[ic - 1] == ' ') {
	    goto L50;
	}
	if (*(unsigned char *)&crdbuf[ic - 1] == ',') {
	    goto L53;
	}
	goto L55;
L50:
	;
    }
    goto L200;
L53:
    ++ic;
L55:
    ic1 = ic;
/*                      see if "REWIND" was requested in command */
    lrewin = FALSE_;
    if (i_indx(crdbuf, "REW", ic1, 3L) > 5) {
	lrewin = TRUE_;
    }
    if (i_indx(crdbuf, "rew", ic1, 3L) > 5) {
	lrewin = TRUE_;
    }
/*                      first argument begins in or after col IC1 */
    i__1 = lend;
    for (ic = ic1; ic <= i__1; ++ic) {
	if (*(unsigned char *)&crdbuf[ic - 1] == ' ') {
	    goto L75;
	}
	if (*(unsigned char *)&crdbuf[ic - 1] == ',') {
	    goto L200;
	}
	goto L80;
L75:
	;
    }
    goto L200;
L80:
    ic1 = ic;
/*                        first argument really begins in col IC1 */
    i__1 = lend;
    for (ic = ic1 + 1; ic <= i__1; ++ic) {
	if (*(unsigned char *)&crdbuf[ic - 1] == ' ') {
	    goto L108;
	}
	if (*(unsigned char *)&crdbuf[ic - 1] == ',') {
	    goto L108;
	}
/* L100: */
    }
    ic = lend + 1;
L108:
    ic2 = ic - 1;
/*                            end of first argument is in col IC2 */
/* L110: */
    s_copy(cunit, crdbuf + (ic1 - 1), 10L, ic2 - (ic1 - 1));
    io___9.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___9);
    do_fio(&c__1, " UNIT NO. :", 11L);
    do_fio(&c__1, cunit, 10L);
    e_wsfe();
    i__1 = s_rsfi(&io___10);
    if (i__1 != 0) {
	goto L500;
    }
    i__1 = do_fio(&c__1, (char *)&funit, (ftnlen)sizeof(doublereal));
    if (i__1 != 0) {
	goto L500;
    }
    i__1 = e_rsfi();
    if (i__1 != 0) {
	goto L500;
    }
    iunit = (integer) funit;
    if (iunit == 0) {
	goto L200;
    }
/*                             skip blanks and commas, find file name */
    i__1 = lend;
    for (ic = ic2 + 1; ic <= i__1; ++ic) {
	if (*(unsigned char *)&crdbuf[ic - 1] == ' ') {
	    goto L120;
	}
	if (*(unsigned char *)&crdbuf[ic - 1] == ',') {
	    goto L120;
	}
	goto L130;
L120:
	;
    }
    goto L131;
L130:
    s_copy(cfname, crdbuf + (ic - 1), 64L, lend - (ic - 1));
    noname = FALSE_;
    io___14.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___14);
    do_fio(&c__1, " FILE NAME IS:", 14L);
    do_fio(&c__1, cfname, 64L);
    e_wsfe();
/*              ask if file exists, if not ask for name and open it */
L131:
    ioin__1.inerr = 0;
    ioin__1.inunit = iunit;
    ioin__1.infile = 0;
    ioin__1.inex = 0;
    ioin__1.inopen = &lopen;
    ioin__1.innum = 0;
    ioin__1.innamed = &lname;
    ioin__1.innamlen = 64;
    ioin__1.inname = cgname;
    ioin__1.inacc = 0;
    ioin__1.inseq = 0;
    ioin__1.indir = 0;
    ioin__1.infmt = 0;
    ioin__1.inform = 0;
    ioin__1.inunf = 0;
    ioin__1.inrecl = 0;
    ioin__1.innrec = 0;
    ioin__1.inblank = 0;
    f_inqu(&ioin__1);
    if (lopen) {
	if (noname) {
	    goto L136;
	} else {
	    if (! lname) {
		s_copy(cgname, "unknown", 64L, 7L);
	    }
	    io___18.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___18);
	    do_fio(&c__1, (char *)&iunit, (ftnlen)sizeof(integer));
	    do_fio(&c__1, cgname, 64L);
	    do_fio(&c__1, cfname, 64L);
	    e_wsfe();
	}
    } else {
/*                new file, open it */
	io___19.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___19);
	do_fio(&c__1, (char *)&iunit, (ftnlen)sizeof(integer));
	e_wsfe();
	if (noname) {
	    io___20.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___20);
	    do_fio(&c__1, " NO FILE NAME GIVEN IN COMMAND.", 31L);
	    e_wsfe();
	    if (mn7flg_1.isw[5] < 1) {
		goto L800;
	    }
	    io___21.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___21);
	    do_fio(&c__1, " PLEASE GIVE FILE NAME:", 23L);
	    e_wsfe();
	    io___22.ciunit = mn7iou_1.isysrd;
	    s_rsfe(&io___22);
	    do_fio(&c__1, cfname, 64L);
	    e_rsfe();
	}
	o__1.oerr = 1;
	o__1.ounit = iunit;
	o__1.ofnmlen = 64;
	o__1.ofnm = cfname;
	o__1.orl = 0;
	o__1.osta = "OLD";
	o__1.oacc = 0;
	o__1.ofm = 0;
	o__1.oblnk = 0;
	i__1 = f_open(&o__1);
	if (i__1 != 0) {
	    goto L600;
	}
	io___23.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___23);
	do_fio(&c__1, " FILE OPENED SUCCESSFULLY.", 26L);
	e_wsfe();
    }
/*                                     . .   file is correctly opened */
L136:
    if (lrewin) {
	goto L150;
    }
    if (mn7flg_1.isw[5] < 1) {
	goto L300;
    }
    io___24.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___24);
    do_fio(&c__1, (char *)&iunit, (ftnlen)sizeof(integer));
    e_wsfe();
    io___25.ciunit = mn7iou_1.isysrd;
    s_rsfe(&io___25);
    do_fio(&c__1, canswr, 1L);
    e_rsfe();
    if (*(unsigned char *)canswr != 'Y' && *(unsigned char *)canswr != 'y') {
	goto L300;
    }
L150:
    al__1.aerr = 0;
    al__1.aunit = iunit;
    f_rew(&al__1);
    goto L300;
/*                      *EOF */
L190:
    if (mn7io2_1.nstkrd == 0) {
	*ierr = 2;
	goto L900;
    }
/*                      revert to previous input file */
L200:
    if (mn7io2_1.nstkrd == 0) {
	io___27.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___27);
	do_fio(&c__1, " COMMAND IGNORED:", 17L);
	do_fio(&c__1, crdbuf, crdbuf_len);
	e_wsfe();
	io___28.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___28);
	do_fio(&c__1, " ALREADY READING FROM PRIMARY INPUT", 35L);
	e_wsfe();
    } else {
	mn7iou_1.isysrd = mn7io2_1.istkrd[mn7io2_1.nstkrd - 1];
	--mn7io2_1.nstkrd;
	if (mn7io2_1.nstkrd == 0) {
	    mn7flg_1.isw[5] = abs(mn7flg_1.isw[5]);
	}
	if (mn7flg_1.isw[4] >= 0) {
	    ioin__1.inerr = 0;
	    ioin__1.inunit = mn7iou_1.isysrd;
	    ioin__1.infile = 0;
	    ioin__1.inex = 0;
	    ioin__1.inopen = 0;
	    ioin__1.innum = 0;
	    ioin__1.innamed = &lname;
	    ioin__1.innamlen = 64;
	    ioin__1.inname = cfname;
	    ioin__1.inacc = 0;
	    ioin__1.inseq = 0;
	    ioin__1.indir = 0;
	    ioin__1.infmt = 0;
	    ioin__1.inform = 0;
	    ioin__1.inunf = 0;
	    ioin__1.inrecl = 0;
	    ioin__1.innrec = 0;
	    ioin__1.inblank = 0;
	    f_inqu(&ioin__1);
	    s_copy(cmode, "BATCH MODE      ", 16L, 16L);
	    if (mn7flg_1.isw[5] == 1) {
		s_copy(cmode, "INTERACTIVE MODE", 16L, 16L);
	    }
	    if (! lname) {
		s_copy(cfname, "unknown", 64L, 7L);
	    }
	    if (mnunpt_(cfname, 64L)) {
		s_copy(cfname, "unprintable", 64L, 11L);
	    }
	    io___30.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___30);
	    do_fio(&c__1, cmode, 16L);
	    do_fio(&c__1, (char *)&mn7iou_1.isysrd, (ftnlen)sizeof(integer));
	    do_fio(&c__1, cfname, 64L);
	    e_wsfe();
	}
    }
    goto L900;
/*                      switch to new input file, add to stack */
L300:
    if (mn7io2_1.nstkrd >= 10) {
	io___31.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___31);
	do_fio(&c__1, " INPUT FILE STACK SIZE EXCEEDED.", 32L);
	e_wsfe();
	goto L800;
    }
    ++mn7io2_1.nstkrd;
    mn7io2_1.istkrd[mn7io2_1.nstkrd - 1] = mn7iou_1.isysrd;
    mn7iou_1.isysrd = iunit;
/*                   ISW(6) = 0 for batch, =1 for interactive, and */
/*                      =-1 for originally interactive temporarily batch 
*/
    if (mn7flg_1.isw[5] == 1) {
	mn7flg_1.isw[5] = -1;
    }
    goto L900;
/*                      format error */
L500:
    io___32.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___32);
    do_fio(&c__1, " CANNOT READ FOLLOWING AS INTEGER:", 34L);
    do_fio(&c__1, cunit, 10L);
    e_wsfe();
    goto L800;
L600:
    io___33.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___33);
    do_fio(&c__1, cfname, 64L);
    e_wsfe();
/*                      serious error */
L800:
    *ierr = 3;
L900:
    return 0;
} /* mnstin_ */

