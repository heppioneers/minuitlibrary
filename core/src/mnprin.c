/* mnprin.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;


/* $Id: mnprin.F,v 1.1.1.1 1996/03/07 14:31:31 mclareni Exp $ */

/* $Log: mnprin.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnprin_(inkode, fval)
integer *inkode;
doublereal *fval;
{
    /* Initialized data */

    static char cblank[11+1] = "           ";

    /* Format strings */
    static char fmt_905[] = "(/\002 FCN=\002,a,\002 FROM \002,a8,\002  STATU\
S=\002,a10,i6,\002 CALLS\002,i9,\002 TOTAL\002)";
    static char fmt_907[] = "(21x,\002EDM=\002,a,\002    STRATEGY=\002,i2,6x\
,a)";
    static char fmt_908[] = "(21x,\002EDM=\002,a,\002  STRATEGY=\002,i1,\002\
  ERROR MATRIX\002,\002 UNCERTAINTY=\002,f5.1,\002%\002)";
    static char fmt_910[] = "(/\002  EXT PARAMETER \002,13x,6a14)";
    static char fmt_911[] = "(\002  NO.   NAME    \002,\002    VALUE    \002\
,6a14)";
    static char fmt_952[] = "(i4,1x,a11,2g14.5,2a)";
    static char fmt_1004[] = "(\002 \002,32x,\002WARNING -   - ABOVE PARAMET\
ER IS AT LIMIT.\002)";

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    doublereal d__1;

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();
    /* Subroutine */ int s_copy();
    integer s_cmp(), s_wsfi(), e_wsfi();
    /* Subroutine */ int s_cat();
    double cos();

    /* Local variables */
    static integer nadd, ncol, i, k, l, m;
    static char chedm[10];
    static integer ikode;
    static doublereal dcmax, x1, x2, x3, dc;
    static integer ic, nc;
    static char cnambf[11];
    static integer kk;
    static char colhdl[14*6], colhdu[14*6], cx2[14], cx3[14], cheval[15];
    static integer ntrail, lbl;

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 0, 0, "(A)", 0 };
    static cilist io___7 = { 0, 0, 0, "(/A,A)", 0 };
    static icilist io___9 = { 0, cheval, 0, "(G15.7)", 15, 1 };
    static icilist io___11 = { 0, chedm, 0, "(E10.2)", 10, 1 };
    static cilist io___13 = { 0, 0, 0, fmt_905, 0 };
    static cilist io___15 = { 0, 0, 0, fmt_907, 0 };
    static cilist io___18 = { 0, 0, 0, fmt_908, 0 };
    static cilist io___25 = { 0, 0, 0, fmt_910, 0 };
    static cilist io___27 = { 0, 0, 0, fmt_911, 0 };
    static cilist io___33 = { 0, 0, 0, fmt_952, 0 };
    static icilist io___36 = { 0, cx2, 0, "(G14.5)", 14, 1 };
    static icilist io___37 = { 0, cx3, 0, "(G14.5)", 14, 1 };
    static cilist io___38 = { 0, 0, 0, fmt_952, 0 };
    static cilist io___39 = { 0, 0, 0, fmt_1004, 0 };
    static cilist io___40 = { 0, 0, 0, "(I4,1X,A11,G14.5,A,2G14.5)", 0 };
    static cilist io___41 = { 0, 0, 0, "(I4,1X,A11,G14.5,A)", 0 };
    static cilist io___42 = { 0, 0, 0, "(31X,A,G10.3)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Prints the values of the parameters at the time of the call. 
*/
/* C        also prints other relevant information such as function value,
 */
/* C        estimated distance to minimum, parameter errors, step sizes. 
*/
/* C */
/*         According to the value of IKODE, the printout is: */
/*    IKODE=INKODE= 0    only info about function value */
/*                  1    parameter values, errors, limits */
/*                  2    values, errors, step sizes, internal values */
/*                  3    values, errors, step sizes, first derivs. */
/*                  4    values, parabolic errors, MINOS errors */
/*    when INKODE=5, MNPRIN chooses IKODE=1,2, or 3, according to ISW(2) 
*/


/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */




    if (mn7npr_1.nu == 0) {
	io___2.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___2);
	do_fio(&c__1, " THERE ARE CURRENTLY NO PARAMETERS DEFINED", 42L);
	e_wsfe();
	goto L700;
    }
/*                  get value of IKODE based in INKODE, ISW(2) */
    ikode = *inkode;
    if (*inkode == 5) {
	ikode = mn7flg_1.isw[1] + 1;
	if (ikode > 3) {
	    ikode = 3;
	}
    }
/*                  set 'default' column headings */
    for (k = 1; k <= 6; ++k) {
	s_copy(colhdu + (k - 1) * 14, "UNDEFINED", 14L, 9L);
/* L5: */
	s_copy(colhdl + (k - 1) * 14, "COLUMN HEAD", 14L, 11L);
    }
/*              print title if Minos errors, and title exists. */
    if (ikode == 4 && s_cmp(mn7tit_1.ctitl, mn7tit_1.cundef, 50L, 10L) != 0) {
	io___7.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___7);
	do_fio(&c__1, " MINUIT TASK: ", 14L);
	do_fio(&c__1, mn7tit_1.ctitl, 50L);
	e_wsfe();
    }
/*              report function value and status */
    if (*fval == mn7cns_1.undefi) {
	s_copy(cheval, " unknown       ", 15L, 15L);
    } else {
	s_wsfi(&io___9);
	do_fio(&c__1, (char *)&(*fval), (ftnlen)sizeof(doublereal));
	e_wsfi();
    }
    if (mn7min_1.edm == mn7cns_1.bigedm) {
	s_copy(chedm, " unknown  ", 10L, 10L);
    } else {
	s_wsfi(&io___11);
	do_fio(&c__1, (char *)&mn7min_1.edm, (ftnlen)sizeof(doublereal));
	e_wsfi();
    }
    nc = mn7cnv_1.nfcn - mn7cnv_1.nfcnfr;
    io___13.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___13);
    do_fio(&c__1, cheval, 15L);
    do_fio(&c__1, mn7tit_1.cfrom, 8L);
    do_fio(&c__1, mn7tit_1.cstatu, 10L);
    do_fio(&c__1, (char *)&nc, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&mn7cnv_1.nfcn, (ftnlen)sizeof(integer));
    e_wsfe();
    m = mn7flg_1.isw[1];
    if (m == 0 || m == 2 || mn7min_1.dcovar == 0.) {
	io___15.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___15);
	do_fio(&c__1, chedm, 10L);
	do_fio(&c__1, (char *)&mn7cnv_1.istrat, (ftnlen)sizeof(integer));
	do_fio(&c__1, mn7tit_1.covmes + m * 22, 22L);
	e_wsfe();
    } else {
	dcmax = (float)1.;
	dc = min(mn7min_1.dcovar,dcmax) * (float)100.;
	io___18.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___18);
	do_fio(&c__1, chedm, 10L);
	do_fio(&c__1, (char *)&mn7cnv_1.istrat, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&dc, (ftnlen)sizeof(doublereal));
	e_wsfe();
    }

    if (ikode == 0) {
	goto L700;
    }
/*               find longest name (for Rene!) */
    ntrail = 10;
    i__1 = mn7npr_1.nu;
    for (i = 1; i <= i__1; ++i) {
	if (mn7inx_1.nvarl[i - 1] < 0) {
	    goto L20;
	}
	for (ic = 10; ic >= 1; --ic) {
	    if (*(unsigned char *)&mn7nam_1.cpnam[(i - 1) * 10 + (ic - 1)] != 
		    ' ') {
		goto L16;
	    }
/* L15: */
	}
	ic = 1;
L16:
	lbl = 10 - ic;
	if (lbl < ntrail) {
	    ntrail = lbl;
	}
L20:
	;
    }
    nadd = ntrail / 2 + 1;
    if (ikode == 1) {
	s_copy(colhdu, "              ", 14L, 14L);
	s_copy(colhdl, "      ERROR   ", 14L, 14L);
	s_copy(colhdu + 14, "      PHYSICAL", 14L, 14L);
	s_copy(colhdu + 28, " LIMITS       ", 14L, 14L);
	s_copy(colhdl + 14, "    NEGATIVE  ", 14L, 14L);
	s_copy(colhdl + 28, "    POSITIVE  ", 14L, 14L);
    }
    if (ikode == 2) {
	s_copy(colhdu, "              ", 14L, 14L);
	s_copy(colhdl, "      ERROR   ", 14L, 14L);
	s_copy(colhdu + 14, "    INTERNAL  ", 14L, 14L);
	s_copy(colhdl + 14, "    STEP SIZE ", 14L, 14L);
	s_copy(colhdu + 28, "    INTERNAL  ", 14L, 14L);
	s_copy(colhdl + 28, "      VALUE   ", 14L, 14L);
    }
    if (ikode == 3) {
	s_copy(colhdu, "              ", 14L, 14L);
	s_copy(colhdl, "      ERROR   ", 14L, 14L);
	s_copy(colhdu + 14, "       STEP   ", 14L, 14L);
	s_copy(colhdl + 14, "       SIZE   ", 14L, 14L);
	s_copy(colhdu + 28, "      FIRST   ", 14L, 14L);
	s_copy(colhdl + 28, "   DERIVATIVE ", 14L, 14L);
    }
    if (ikode == 4) {
	s_copy(colhdu, "    PARABOLIC ", 14L, 14L);
	s_copy(colhdl, "      ERROR   ", 14L, 14L);
	s_copy(colhdu + 14, "        MINOS ", 14L, 14L);
	s_copy(colhdu + 28, "ERRORS        ", 14L, 14L);
	s_copy(colhdl + 14, "   NEGATIVE   ", 14L, 14L);
	s_copy(colhdl + 28, "   POSITIVE   ", 14L, 14L);
    }

    if (ikode != 4) {
	if (mn7flg_1.isw[1] < 3) {
	    s_copy(colhdu, "  APPROXIMATE ", 14L, 14L);
	}
	if (mn7flg_1.isw[1] < 1) {
	    s_copy(colhdu, " CURRENT GUESS", 14L, 14L);
	}
    }
    ncol = 3;
    io___25.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___25);
    i__1 = ncol;
    for (kk = 1; kk <= i__1; ++kk) {
	do_fio(&c__1, colhdu + (kk - 1) * 14, 14L);
    }
    e_wsfe();
    io___27.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___27);
    i__1 = ncol;
    for (kk = 1; kk <= i__1; ++kk) {
	do_fio(&c__1, colhdl + (kk - 1) * 14, 14L);
    }
    e_wsfe();

/*                                        . . . loop over parameters . . 
*/
    i__1 = mn7npr_1.nu;
    for (i = 1; i <= i__1; ++i) {
	if (mn7inx_1.nvarl[i - 1] < 0) {
	    goto L200;
	}
	l = mn7inx_1.niofex[i - 1];
/* Writing concatenation */
	i__2[0] = nadd, a__1[0] = cblank;
	i__2[1] = 10, a__1[1] = mn7nam_1.cpnam + (i - 1) * 10;
	s_cat(cnambf, a__1, i__2, &c__2, 11L);
	if (l == 0) {
	    goto L55;
	}
/*              variable parameter. */
	x1 = mn7err_1.werr[l - 1];
	s_copy(cx2, "PLEASE GET X..", 14L, 14L);
	s_copy(cx3, "PLEASE GET X..", 14L, 14L);
	if (ikode == 1) {
	    if (mn7inx_1.nvarl[i - 1] <= 1) {
		io___33.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___33);
		do_fio(&c__1, (char *)&i, (ftnlen)sizeof(integer));
		do_fio(&c__1, cnambf, 11L);
		do_fio(&c__1, (char *)&mn7ext_1.u[i - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&x1, (ftnlen)sizeof(doublereal));
		e_wsfe();
		goto L200;
	    } else {
		x2 = mn7ext_1.alim[i - 1];
		x3 = mn7ext_1.blim[i - 1];
	    }
	}
	if (ikode == 2) {
	    x2 = mn7int_1.dirin[l - 1];
	    x3 = mn7int_1.x[l - 1];
	}
	if (ikode == 3) {
	    x2 = mn7int_1.dirin[l - 1];
	    x3 = mn7der_1.grd[l - 1];
	    if (mn7inx_1.nvarl[i - 1] > 1 && (d__1 = cos(mn7int_1.x[l - 1]), 
		    abs(d__1)) < (float).001) {
		s_copy(cx3, "** at limit **", 14L, 14L);
	    }
	}
	if (ikode == 4) {
	    x2 = mn7err_1.ern[l - 1];
	    if (x2 == 0.) {
		s_copy(cx2, " ", 14L, 1L);
	    }
	    if (x2 == mn7cns_1.undefi) {
		s_copy(cx2, "   at limit   ", 14L, 14L);
	    }
	    x3 = mn7err_1.erp[l - 1];
	    if (x3 == 0.) {
		s_copy(cx3, " ", 14L, 1L);
	    }
	    if (x3 == mn7cns_1.undefi) {
		s_copy(cx3, "   at limit   ", 14L, 14L);
	    }
	}
	if (s_cmp(cx2, "PLEASE GET X..", 14L, 14L) == 0) {
	    s_wsfi(&io___36);
	    do_fio(&c__1, (char *)&x2, (ftnlen)sizeof(doublereal));
	    e_wsfi();
	}
	if (s_cmp(cx3, "PLEASE GET X..", 14L, 14L) == 0) {
	    s_wsfi(&io___37);
	    do_fio(&c__1, (char *)&x3, (ftnlen)sizeof(doublereal));
	    e_wsfi();
	}
	io___38.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___38);
	do_fio(&c__1, (char *)&i, (ftnlen)sizeof(integer));
	do_fio(&c__1, cnambf, 11L);
	do_fio(&c__1, (char *)&mn7ext_1.u[i - 1], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&x1, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, cx2, 14L);
	do_fio(&c__1, cx3, 14L);
	e_wsfe();
/*               check if parameter is at limit */
	if (mn7inx_1.nvarl[i - 1] <= 1 || ikode == 3) {
	    goto L200;
	}
	if ((d__1 = cos(mn7int_1.x[l - 1]), abs(d__1)) < (float).001) {
	    io___39.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___39);
	    e_wsfe();
	}
	goto L200;

/*                                print constant or fixed parameter. 
*/
L55:
	s_copy(colhdu, "   constant   ", 14L, 14L);
	if (mn7inx_1.nvarl[i - 1] > 0) {
	    s_copy(colhdu, "     fixed    ", 14L, 14L);
	}
	if (mn7inx_1.nvarl[i - 1] == 4 && ikode == 1) {
	    io___40.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___40);
	    do_fio(&c__1, (char *)&i, (ftnlen)sizeof(integer));
	    do_fio(&c__1, cnambf, 11L);
	    do_fio(&c__1, (char *)&mn7ext_1.u[i - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, colhdu, 14L);
	    do_fio(&c__1, (char *)&mn7ext_1.alim[i - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, (char *)&mn7ext_1.blim[i - 1], (ftnlen)sizeof(
		    doublereal));
	    e_wsfe();
	} else {
	    io___41.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___41);
	    do_fio(&c__1, (char *)&i, (ftnlen)sizeof(integer));
	    do_fio(&c__1, cnambf, 11L);
	    do_fio(&c__1, (char *)&mn7ext_1.u[i - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, colhdu, 14L);
	    e_wsfe();
	}
L200:
	;
    }

    if (mn7min_1.up != mn7cns_1.updflt) {
	io___42.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___42);
	do_fio(&c__1, "ERR DEF=", 8L);
	do_fio(&c__1, (char *)&mn7min_1.up, (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
L700:
    return 0;
} /* mnprin_ */

