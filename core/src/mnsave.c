/* mnsave.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;


/* $Id: mnsave.F,v 1.1.1.1 1996/03/07 14:31:31 mclareni Exp $ */

/* $Log: mnsave.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnsave_()
{
    /* Format strings */
    static char fmt_32[] = "(\002 CURRENT VALUES WILL BE SAVED ON UNIT\002,i\
3,\002: \002,a/)";
    static char fmt_35[] = "(\002 UNIT\002,i3,\002 IS NOT OPENED.\002)";
    static char fmt_37[] = "(\002 SHOULD UNIT\002,i3,\002 BE REWOUND BEFORE \
WRITING TO IT?\002)";
    static char fmt_1001[] = "(1x,i5,\002'\002,a10,\002'\002,4e13.5)";
    static char fmt_1003[] = "(\002SET COVARIANCE\002,i6)";
    static char fmt_1004[] = "(bn,7e11.4,3x)";
    static char fmt_501[] = "(1x,i5,\002 RECORDS WRITTEN TO UNIT\002,i4\
,\002:\002,a)";
    static char fmt_502[] = "(\002 INCLUDING\002,i5,\002 RECORDS FOR THE COV\
ARIANCE MATRIX.\002/)";

    /* System generated locals */
    integer i__1;
    olist o__1;
    alist al__1;
    inlist ioin__1;

    /* Builtin functions */
    integer f_inqu();
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe(), s_rsfe(), e_rsfe(), f_open(), f_rew(
	    );

    /* Local variables */
    static integer iint, npar2, i;
    static logical lname, lopen;
    static char cgname[64], cfname[64], canswr[1];
    static integer nlines, ncovar;

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 0, 0, fmt_32, 0 };
    static cilist io___5 = { 0, 0, 0, fmt_35, 0 };
    static cilist io___6 = { 0, 0, 0, "(A)", 0 };
    static cilist io___7 = { 0, 0, 0, "(A)", 0 };
    static cilist io___9 = { 0, 0, 0, fmt_37, 0 };
    static cilist io___10 = { 0, 0, 0, "(A)", 0 };
    static cilist io___12 = { 1, 0, 0, "(10HSET TITLE )", 0 };
    static cilist io___13 = { 0, 0, 0, "(A)", 0 };
    static cilist io___14 = { 0, 0, 0, "(10HPARAMETERS)", 0 };
    static cilist io___18 = { 0, 0, 0, fmt_1001, 0 };
    static cilist io___19 = { 0, 0, 0, fmt_1001, 0 };
    static cilist io___20 = { 0, 0, 0, "(A)", 0 };
    static cilist io___21 = { 1, 0, 0, fmt_1003, 0 };
    static cilist io___23 = { 0, 0, 0, fmt_1004, 0 };
    static cilist io___25 = { 0, 0, 0, fmt_501, 0 };
    static cilist io___26 = { 0, 0, 0, fmt_502, 0 };
    static cilist io___27 = { 0, 0, 0, "(A,I4)", 0 };
    static cilist io___28 = { 0, 0, 0, "(A,I4,A)", 0 };
    static cilist io___29 = { 0, 0, 0, "(A,I4)", 0 };
    static cilist io___30 = { 0, 0, 0, "(A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C       Writes current parameter values and step sizes onto file ISYSSA
 */
/* C          in format which can be reread by Minuit for restarting. */
/* C       The covariance matrix is also output if it exists. */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */



    ioin__1.inerr = 0;
    ioin__1.inunit = mn7iou_1.isyssa;
    ioin__1.infile = 0;
    ioin__1.inex = 0;
    ioin__1.inopen = &lopen;
    ioin__1.innum = 0;
    ioin__1.innamed = &lname;
    ioin__1.innamlen = 64;
    ioin__1.inname = cgname;
    ioin__1.inacc = 0;
    ioin__1.inseq = 0;
    ioin__1.indir = 0;
    ioin__1.infmt = 0;
    ioin__1.inform = 0;
    ioin__1.inunf = 0;
    ioin__1.inrecl = 0;
    ioin__1.innrec = 0;
    ioin__1.inblank = 0;
    f_inqu(&ioin__1);
    if (lopen) {
	if (! lname) {
	    s_copy(cgname, "UNNAMED FILE", 64L, 12L);
	}
	io___4.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___4);
	do_fio(&c__1, (char *)&mn7iou_1.isyssa, (ftnlen)sizeof(integer));
	do_fio(&c__1, cgname, 64L);
	e_wsfe();
    } else {
/*                new file, open it */
	io___5.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___5);
	do_fio(&c__1, (char *)&mn7iou_1.isyssa, (ftnlen)sizeof(integer));
	e_wsfe();
	if (mn7flg_1.isw[5] == 1) {
	    io___6.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___6);
	    do_fio(&c__1, " PLEASE GIVE FILE NAME:", 23L);
	    e_wsfe();
	    io___7.ciunit = mn7iou_1.isysrd;
	    s_rsfe(&io___7);
	    do_fio(&c__1, cfname, 64L);
	    e_rsfe();
	    o__1.oerr = 1;
	    o__1.ounit = mn7iou_1.isyssa;
	    o__1.ofnmlen = 64;
	    o__1.ofnm = cfname;
	    o__1.orl = 0;
	    o__1.osta = "NEW";
	    o__1.oacc = 0;
	    o__1.ofm = 0;
	    o__1.oblnk = 0;
	    i__1 = f_open(&o__1);
	    if (i__1 != 0) {
		goto L600;
	    }
	    s_copy(cgname, cfname, 64L, 64L);
	} else {
	    goto L650;
	}
    }
/*                               file is now correctly opened */
    if (mn7flg_1.isw[5] == 1) {
	io___9.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___9);
	do_fio(&c__1, (char *)&mn7iou_1.isyssa, (ftnlen)sizeof(integer));
	e_wsfe();
	io___10.ciunit = mn7iou_1.isysrd;
	s_rsfe(&io___10);
	do_fio(&c__1, canswr, 1L);
	e_rsfe();
	if (*(unsigned char *)canswr == 'Y' || *(unsigned char *)canswr == 
		'y') {
	    al__1.aerr = 0;
	    al__1.aunit = mn7iou_1.isyssa;
	    f_rew(&al__1);
	}
    }
/*                               and rewound if requested */
    io___12.ciunit = mn7iou_1.isyssa;
    i__1 = s_wsfe(&io___12);
    if (i__1 != 0) {
	goto L700;
    }
    i__1 = e_wsfe();
    if (i__1 != 0) {
	goto L700;
    }
    io___13.ciunit = mn7iou_1.isyssa;
    s_wsfe(&io___13);
    do_fio(&c__1, mn7tit_1.ctitl, 50L);
    e_wsfe();
    io___14.ciunit = mn7iou_1.isyssa;
    s_wsfe(&io___14);
    e_wsfe();
    nlines = 3;
/*                                write out parameter values */
    i__1 = mn7npr_1.nu;
    for (i = 1; i <= i__1; ++i) {
	if (mn7inx_1.nvarl[i - 1] < 0) {
	    goto L200;
	}
	++nlines;
	iint = mn7inx_1.niofex[i - 1];
	if (mn7inx_1.nvarl[i - 1] > 1) {
	    goto L100;
	}
/*         parameter without limits */
	io___18.ciunit = mn7iou_1.isyssa;
	s_wsfe(&io___18);
	do_fio(&c__1, (char *)&i, (ftnlen)sizeof(integer));
	do_fio(&c__1, mn7nam_1.cpnam + (i - 1) * 10, 10L);
	do_fio(&c__1, (char *)&mn7ext_1.u[i - 1], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&mn7err_1.werr[iint - 1], (ftnlen)sizeof(
		doublereal));
	e_wsfe();
	goto L200;
/*         parameter with limits */
L100:
	io___19.ciunit = mn7iou_1.isyssa;
	s_wsfe(&io___19);
	do_fio(&c__1, (char *)&i, (ftnlen)sizeof(integer));
	do_fio(&c__1, mn7nam_1.cpnam + (i - 1) * 10, 10L);
	do_fio(&c__1, (char *)&mn7ext_1.u[i - 1], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&mn7err_1.werr[iint - 1], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&mn7ext_1.alim[i - 1], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&mn7ext_1.blim[i - 1], (ftnlen)sizeof(
		doublereal));
	e_wsfe();
L200:
	;
    }
    io___20.ciunit = mn7iou_1.isyssa;
    s_wsfe(&io___20);
    do_fio(&c__1, " ", 1L);
    e_wsfe();
    ++nlines;
/*                                  write out covariance matrix, if any */
    if (mn7flg_1.isw[1] < 1) {
	goto L750;
    }
    io___21.ciunit = mn7iou_1.isyssa;
    i__1 = s_wsfe(&io___21);
    if (i__1 != 0) {
	goto L700;
    }
    i__1 = do_fio(&c__1, (char *)&mn7npr_1.npar, (ftnlen)sizeof(integer));
    if (i__1 != 0) {
	goto L700;
    }
    i__1 = e_wsfe();
    if (i__1 != 0) {
	goto L700;
    }
    npar2 = mn7npr_1.npar * (mn7npr_1.npar + 1) / 2;
    io___23.ciunit = mn7iou_1.isyssa;
    s_wsfe(&io___23);
    i__1 = npar2;
    for (i = 1; i <= i__1; ++i) {
	do_fio(&c__1, (char *)&mn7var_1.vhmat[i - 1], (ftnlen)sizeof(
		doublereal));
    }
    e_wsfe();
    ncovar = npar2 / 7 + 1;
    if (npar2 % 7 > 0) {
	++ncovar;
    }
    nlines += ncovar;
    io___25.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___25);
    do_fio(&c__1, (char *)&nlines, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&mn7iou_1.isyssa, (ftnlen)sizeof(integer));
    do_fio(&c__1, cgname, 45L);
    e_wsfe();
    if (ncovar > 0) {
	io___26.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___26);
	do_fio(&c__1, (char *)&ncovar, (ftnlen)sizeof(integer));
	e_wsfe();
    }
    goto L900;
/*                                           some error conditions */
L600:
    io___27.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___27);
    do_fio(&c__1, " I/O ERROR: UNABLE TO OPEN UNIT", 31L);
    do_fio(&c__1, (char *)&mn7iou_1.isyssa, (ftnlen)sizeof(integer));
    e_wsfe();
    goto L900;
L650:
    io___28.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___28);
    do_fio(&c__1, " UNIT", 5L);
    do_fio(&c__1, (char *)&mn7iou_1.isyssa, (ftnlen)sizeof(integer));
    do_fio(&c__1, " IS NOT OPENED.", 15L);
    e_wsfe();
    goto L900;
L700:
    io___29.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___29);
    do_fio(&c__1, " ERROR: UNABLE TO WRITE TO UNIT", 31L);
    do_fio(&c__1, (char *)&mn7iou_1.isyssa, (ftnlen)sizeof(integer));
    e_wsfe();
    goto L900;
L750:
    io___30.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___30);
    do_fio(&c__1, " THERE IS NO COVARIANCE MATRIX TO SAVE.", 39L);
    e_wsfe();

L900:
    return 0;
} /* mnsave_ */

