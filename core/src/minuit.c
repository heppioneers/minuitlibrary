/* minuit.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;
static integer c__4 = 4;
static integer c__3 = 3;


/* $Id: minuit.F,v 1.1.1.1 1996/03/07 14:31:28 mclareni Exp $ */

/* $Log: minuit.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:28  mclareni */
/* Minuit */


/* Subroutine */ int minuit_0_(n__, fcn, futil, i1, i2, i3)
int n__;
/* Subroutine */ int (*fcn) (), (*futil) ();
integer *i1, *i2, *i3;
{
    /* Initialized data */

    static char cwhyxt[40+1] = "FOR UNKNOWN REASONS                     ";
    static integer jsysrd = 5;
    static integer jsyswr = 6;
    static integer jsyssa = 7;

    /* Format strings */
    static char fmt_280[] = "(/\002 MINUIT WARNING: PROBABLE ERROR IN USER F\
UNCTION.\002/\002 FOR FIXED VALUES OF PARAMETERS, FCN IS TIME-DEPENDENT\002\
/\002 F =\002,e22.14,\002 FOR FIRST CALL\002/\002 F =\002,e22.14,\002 FOR SE\
COND CALL.\002/)";

    /* System generated locals */
    address a__1[2];
    integer i__1[2];

    /* Builtin functions */
    integer s_wsfe(), e_wsfe(), do_fio();
    /* Subroutine */ int s_copy(), s_cat();
    integer i_indx();
    /* Subroutine */ int s_stop();

    /* Local variables */
    static doublereal fnew;
    static integer nparx;
    static doublereal fzero, first;
    extern /* Subroutine */ int mnread_(), mncler_(), mninit_();
    static integer iflgut;
    extern /* Subroutine */ int mninex_(), mnprin_();

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 0, 0, "(1X,75(1H*))", 0 };
    static cilist io___6 = { 0, 0, 0, "(1X,75(1H*))", 0 };
    static cilist io___7 = { 0, 0, 0, "(26X,A,I4)", 0 };
    static cilist io___8 = { 0, 0, 0, "(1X,75(1H*))", 0 };
    static cilist io___10 = { 0, 0, 0, "(/A,A)", 0 };
    static cilist io___14 = { 0, 0, 0, "(/A,A/)", 0 };
    static cilist io___16 = { 0, 0, 0, fmt_280, 0 };
    static cilist io___17 = { 0, 0, 0, "(A,A)", 0 };
    static cilist io___18 = { 0, 0, 0, "(A,A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */



/*  CPNAM   Parameter name (10 characters) */
/*  U       External (visible to user in FCN) value of parameter */
/*  ALIM, BLIM Lower and upper parameter limits. If both zero, no limits. 
*/
/*  ERP,ERN Positive and negative MINOS errors, if calculated. */
/*  WERR    External parameter error (standard deviation, defined by UP) 
*/
/*  GLOBCC  Global Correlation Coefficient */
/*  NVARL   =-1 if parameter undefined,      =0 if constant, */
/*          = 1 if variable without limits,  =4 if variable with limits */
/*   (Note that if parameter has been fixed, NVARL=1 or =4, and NIOFEX=0) 
*/
/*  NIOFEX  Internal parameter number, or zero if not currently variable 
*/
/*  NEXOFI  External parameter number for currently variable parameters */
/*  X, XT   Internal parameter values (X are sometimes saved in XT) */
/*  DIRIN   (Internal) step sizes for current step */
/*  variables with names ending in ..S are saved values for fixed params 
*/
/*  VHMAT   (Internal) error matrix stored as Half MATrix, since */
/*                it is symmetric */
/*  VTHMAT  VHMAT is sometimes saved in VTHMAT, especially in MNMNOT */

/*  ISW definitions: */
/*      ISW(1) =0 normally, =1 means CALL LIMIT EXCEEDED */
/*      ISW(2) =0 means no error matrix */
/*             =1 means only approximate error matrix */
/*             =2 means full error matrix, but forced pos-def. */
/*             =3 means good normal full error matrix exists */
/*      ISW(3) =0 if Minuit is calculating the first derivatives */
/*             =1 if first derivatives calculated inside FCN */
/*      ISW(4) =-1 if most recent minimization did not converge. */
/*             = 0 if problem redefined since most recent minimization. */
/*             =+1 if most recent minimization did converge. */
/*      ISW(5) is the PRInt level.  See SHO PRIntlevel */
/*      ISW(6) = 0 for batch mode, =1 for interactive mode */
/*                      =-1 for originally interactive temporarily batch 
*/

/*  LWARN is true if warning messges are to be put out (default=true) */
/*            SET WARN turns it on, set NOWarn turns it off */
/*  LREPOR is true if exceptional conditions are put out (default=false) 
*/
/*            SET DEBUG turns it on, SET NODebug turns it off */
/*  LIMSET is true if a parameter is up against limits (for MINOS) */
/*  LNOLIM is true if there are no limits on any parameters (not yet used)
 */
/*  LNEWMN is true if the previous process has unexpectedly improved FCN 
*/
/*  LPHEAD is true if a heading should be put out for the next parameter 
*/
/*        definition, false if a parameter has just been defined */

    switch(n__) {
	case 1: goto L_mintio;
	}

/*                                 . . . . . . . . . . initialize minuit 
*/
    io___5.ciunit = jsyswr;
    s_wsfe(&io___5);
    e_wsfe();
    mninit_(&jsysrd, &jsyswr, &jsyssa);
/*                                      . . . . initialize new data block 
*/
L100:
    io___6.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___6);
    e_wsfe();
    ++mn7flg_1.nblock;
    io___7.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___7);
    do_fio(&c__1, "MINUIT DATA BLOCK NO.", 21L);
    do_fio(&c__1, (char *)&mn7flg_1.nblock, (ftnlen)sizeof(integer));
    e_wsfe();
    io___8.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___8);
    e_wsfe();
/*               . . . . . . . . . . .   set parameter lists to undefined 
*/
    mncler_();
/*                                             . . . . . . . . read title 
*/
    mnread_(fcn, &c__1, &iflgut, futil);
    if (iflgut == 2) {
	goto L500;
    }
    if (iflgut == 3) {
	goto L600;
    }
/*                                        . . . . . . . . read parameters 
*/
    mnread_(fcn, &c__2, &iflgut, futil);
    if (iflgut == 2) {
	goto L500;
    }
    if (iflgut == 3) {
	goto L600;
    }
    if (iflgut == 4) {
	goto L700;
    }
/*                              . . . . . . verify FCN not time-dependent 
*/
    io___10.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___10);
    do_fio(&c__1, " MINUIT: FIRST CALL TO USER FUNCTION,", 37L);
    do_fio(&c__1, " WITH IFLAG=1", 13L);
    e_wsfe();
    nparx = mn7npr_1.npar;
    mninex_(mn7int_1.x);
    fzero = mn7cns_1.undefi;
    (*fcn)(&nparx, mn7der_1.gin, &fzero, mn7ext_1.u, &c__1, futil);
    first = mn7cns_1.undefi;
    (*fcn)(&nparx, mn7der_1.gin, &first, mn7ext_1.u, &c__4, futil);
    mn7cnv_1.nfcn = 2;
    if (fzero == mn7cns_1.undefi && first == mn7cns_1.undefi) {
	s_copy(cwhyxt, "BY ERROR IN USER FUNCTION.  ", 40L, 28L);
	io___14.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___14);
	do_fio(&c__1, " USER HAS NOT CALCULATED FUNCTION", 33L);
	do_fio(&c__1, " VALUE WHEN IFLAG=1 OR 4", 24L);
	e_wsfe();
	goto L800;
    }
    mn7min_1.amin = first;
    if (first == mn7cns_1.undefi) {
	mn7min_1.amin = fzero;
    }
    mnprin_(&c__1, &mn7min_1.amin);
    mn7cnv_1.nfcn = 2;
    if (first == fzero) {
	goto L300;
    }
    fnew = (float)0.;
    (*fcn)(&nparx, mn7der_1.gin, &fnew, mn7ext_1.u, &c__4, futil);
    if (fnew != mn7min_1.amin) {
	io___16.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___16);
	do_fio(&c__1, (char *)&mn7min_1.amin, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&fnew, (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    mn7cnv_1.nfcn = 3;
L300:
    mn7min_1.fval3 = mn7min_1.amin * (float)2. + (float)1.;
/*                                   . . . . . . . . . . . read commands 
*/
    mnread_(fcn, &c__3, &iflgut, futil);
    if (iflgut == 2) {
	goto L500;
    }
    if (iflgut == 3) {
	goto L600;
    }
    if (iflgut == 4) {
	goto L700;
    }
/* Writing concatenation */
    i__1[0] = 19, a__1[0] = "BY MINUIT COMMAND: ";
    i__1[1] = 20, a__1[1] = mn7tit_1.cword;
    s_cat(cwhyxt, a__1, i__1, &c__2, 40L);
    if (i_indx(mn7tit_1.cword, "STOP", 20L, 4L) > 0) {
	goto L800;
    }
    if (i_indx(mn7tit_1.cword, "EXI", 20L, 3L) > 0) {
	goto L800;
    }
    if (i_indx(mn7tit_1.cword, "RET", 20L, 3L) == 0) {
	goto L100;
    }
    s_copy(cwhyxt, "AND RETURNS TO USER PROGRAM.    ", 40L, 32L);
    io___17.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___17);
    do_fio(&c__1, " ..........MINUIT TERMINATED ", 29L);
    do_fio(&c__1, cwhyxt, 40L);
    e_wsfe();
    return 0;
/*                                           . . . . . . stop conditions 
*/
L500:
    s_copy(cwhyxt, "BY END-OF-DATA ON PRIMARY INPUT FILE.   ", 40L, 40L);
    goto L800;
L600:
    s_copy(cwhyxt, "BY UNRECOVERABLE READ ERROR ON INPUT.   ", 40L, 40L);
    goto L800;
L700:
    s_copy(cwhyxt, ": FATAL ERROR IN PARAMETER DEFINITIONS. ", 40L, 40L);
L800:
    io___18.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___18);
    do_fio(&c__1, " ..........MINUIT TERMINATED ", 29L);
    do_fio(&c__1, cwhyxt, 40L);
    e_wsfe();
    s_stop("", 0L);

/*  ......................entry to set unit numbers  - - - - - - - - - - 
*/

L_mintio:
    jsysrd = *i1;
    jsyswr = *i2;
    jsyssa = *i3;
    return 0;
} /* minuit_ */

/* Subroutine */ int minuit_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    return minuit_0_(0, fcn, futil, (integer *)0, (integer *)0, (integer *)0);
    }

/* Subroutine */ int mintio_(i1, i2, i3)
integer *i1, *i2, *i3;
{
    return minuit_0_(1, (int (*)())0, (int (*)())0, i1, i2, i3);
    }

