/* mnpars.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;
static integer c__20 = 20;
static integer c__30 = 30;


/* $Id: mnpars.F,v 1.1.1.1 1996/03/07 14:31:31 mclareni Exp $ */

/* $Log: mnpars.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnpars_(crdbuf, icondn, crdbuf_len)
char *crdbuf;
integer *icondn;
ftnlen crdbuf_len;
{
    /* Format strings */
    static char fmt_158[] = "(bn,f10.0,a10,4f10.0)";

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2[2];
    icilist ici__1;

    /* Builtin functions */
    integer i_len(), i_indx();
    /* Subroutine */ int s_copy();
    integer s_rsfi(), do_fio(), e_rsfi();
    /* Subroutine */ int s_cat();

    /* Local variables */
    static integer ierr, kapo1, kapo2;
    static doublereal a, b;
    static integer k;
    static char cnamk[10];
    static integer llist;
    static doublereal plist[30], fk;
    static integer ibegin;
    static doublereal uk;
    static char comand[20], celmnt[20];
    static integer lenbuf, istart;
    static doublereal wk;
    extern /* Subroutine */ int mncrck_();
    static doublereal xk;
    extern /* Subroutine */ int mnparm_();
    static integer lnc, icy;

    /* Fortran I/O blocks */
    static icilist io___6 = { 1, celmnt, 0, "(BN,F20.0)", 20, 1 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Called from MNREAD and user-callable */
/* C    Implements one parameter definition, that is: */
/* C       parses the string CRDBUF and calls MNPARM */

/* output conditions: */
/*        ICONDN = 0    all OK */
/*        ICONDN = 1    error, attempt to define parameter is ignored */
/*        ICONDN = 2    end of parameter definitions */


/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */




    lenbuf = i_len(crdbuf, crdbuf_len);
/*                     find out whether fixed or free-field format */
    kapo1 = i_indx(crdbuf, "'", crdbuf_len, 1L);
    if (kapo1 == 0) {
	goto L150;
    }
    i__1 = kapo1;
    kapo2 = i_indx(crdbuf + i__1, "'", crdbuf_len - i__1, 1L);
    if (kapo2 == 0) {
	goto L150;
    }
/*          new (free-field) format */
    kapo2 += kapo1;
/*                             skip leading blanks if any */
    i__1 = kapo1 - 1;
    for (istart = 1; istart <= i__1; ++istart) {
	if (*(unsigned char *)&crdbuf[istart - 1] != ' ') {
	    goto L120;
	}
/* L115: */
    }
    goto L210;
L120:
/*                               parameter number integer */
    s_copy(celmnt, crdbuf + (istart - 1), 20L, kapo1 - 1 - (istart - 1));
    i__1 = s_rsfi(&io___6);
    if (i__1 != 0) {
	goto L180;
    }
    i__1 = do_fio(&c__1, (char *)&fk, (ftnlen)sizeof(doublereal));
    if (i__1 != 0) {
	goto L180;
    }
    i__1 = e_rsfi();
    if (i__1 != 0) {
	goto L180;
    }
    k = (integer) fk;
    if (k <= 0) {
	goto L210;
    }
/* Writing concatenation */
    i__2[0] = 6, a__1[0] = "PARAM ";
    i__2[1] = 20, a__1[1] = celmnt;
    s_cat(cnamk, a__1, i__2, &c__2, 10L);
    if (kapo2 - kapo1 > 1) {
	i__1 = kapo1;
	s_copy(cnamk, crdbuf + i__1, 10L, kapo2 - 1 - i__1);
    }
/*  special handling if comma or blanks and a comma follow 'name' */
    i__1 = lenbuf;
    for (icy = kapo2 + 1; icy <= i__1; ++icy) {
	if (*(unsigned char *)&crdbuf[icy - 1] == ',') {
	    goto L139;
	}
	if (*(unsigned char *)&crdbuf[icy - 1] != ' ') {
	    goto L140;
	}
/* L135: */
    }
    uk = (float)0.;
    wk = (float)0.;
    a = (float)0.;
    b = (float)0.;
    goto L170;
L139:
    ++icy;
L140:
    ibegin = icy;
    mncrck_(crdbuf + (ibegin - 1), &c__20, comand, &lnc, &c__30, plist, &
	    llist, &ierr, &mn7iou_1.isyswr, crdbuf_len - (ibegin - 1), 20L);
    if (ierr > 0) {
	goto L180;
    }
    uk = plist[0];
    wk = (float)0.;
    if (llist >= 2) {
	wk = plist[1];
    }
    a = (float)0.;
    if (llist >= 3) {
	a = plist[2];
    }
    b = (float)0.;
    if (llist >= 4) {
	b = plist[3];
    }
    goto L170;
/*          old (fixed-field) format */
L150:
    ici__1.icierr = 1;
    ici__1.iciend = 0;
    ici__1.icirnum = 1;
    ici__1.icirlen = crdbuf_len;
    ici__1.iciunit = crdbuf;
    ici__1.icifmt = fmt_158;
    i__1 = s_rsfi(&ici__1);
    if (i__1 != 0) {
	goto L180;
    }
    i__1 = do_fio(&c__1, (char *)&xk, (ftnlen)sizeof(doublereal));
    if (i__1 != 0) {
	goto L180;
    }
    i__1 = do_fio(&c__1, cnamk, 10L);
    if (i__1 != 0) {
	goto L180;
    }
    i__1 = do_fio(&c__1, (char *)&uk, (ftnlen)sizeof(doublereal));
    if (i__1 != 0) {
	goto L180;
    }
    i__1 = do_fio(&c__1, (char *)&wk, (ftnlen)sizeof(doublereal));
    if (i__1 != 0) {
	goto L180;
    }
    i__1 = do_fio(&c__1, (char *)&a, (ftnlen)sizeof(doublereal));
    if (i__1 != 0) {
	goto L180;
    }
    i__1 = do_fio(&c__1, (char *)&b, (ftnlen)sizeof(doublereal));
    if (i__1 != 0) {
	goto L180;
    }
    i__1 = e_rsfi();
    if (i__1 != 0) {
	goto L180;
    }
    k = (integer) xk;
    if (k == 0) {
	goto L210;
    }
/*          parameter format cracked, implement parameter definition */
L170:
    mnparm_(&k, cnamk, &uk, &wk, &a, &b, &ierr, 10L);
    *icondn = ierr;
    return 0;
/*          format or other error */
L180:
    *icondn = 1;
    return 0;
/*        end of data */
L210:
    *icondn = 2;
    return 0;
} /* mnpars_ */

