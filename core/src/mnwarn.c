/* mnwarn.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

struct {
    char origin[200]	/* was [10][2] */, warmes[1200]	/* was [10][2] */;
} mn7wrc_;

#define mn7wrc_1 mn7wrc_

struct {
    integer nfcwar[20]	/* was [10][2] */, icirc[2];
} mn7wri_;

#define mn7wri_1 mn7wri_

/* Table of constant values */

static integer c__1 = 1;
static integer c__10 = 10;


/* $Id: mnwarn.F,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: mnwarn.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */


/* Subroutine */ int mnwarn_(copt, corg, cmes, copt_len, corg_len, cmes_len)
char *copt, *corg, *cmes;
ftnlen copt_len;
ftnlen corg_len;
ftnlen cmes_len;
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_cmp(), s_wsfe(), do_fio(), e_wsfe();
    /* Subroutine */ int s_copy();

    /* Local variables */
    static char ctyp[7];
    static integer ityp, i, ic, nm;
    static char englsh[20];

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 0, 0, "(A,A/A,A)", 0 };
    static cilist io___3 = { 0, 0, 0, "(A,A/A,A)", 0 };
    static cilist io___7 = { 0, 0, 0, "(/1X,I5,A,A,A,A/)", 0 };
    static cilist io___9 = { 0, 0, 0, "(A,I2,A)", 0 };
    static cilist io___10 = { 0, 0, 0, "(A)", 0 };
    static cilist io___12 = { 0, 0, 0, "(1X,I6,1X,A,1X,A)", 0 };
    static cilist io___13 = { 0, 0, 0, "(1H )", 0 };


/*     If COPT='W', CMES is a WARning message from CORG. */
/*     If COPT='D', CMES is a DEBug message from CORG. */
/*         If SET WARnings is in effect (the default), this routine */
/*             prints the warning message CMES coming from CORG. */
/*         If SET NOWarnings is in effect, the warning message is */
/*             stored in a circular buffer of length MAXMES. */
/*         If called with CORG=CMES='SHO', it prints the messages in */
/*             the circular buffer, FIFO, and empties the buffer. */

/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */



    if (s_cmp(corg, "SHO", 3L, 3L) == 0 && s_cmp(cmes, "SHO", 3L, 3L) == 0) {
	goto L200;
    }
/*             Either print warning or put in buffer */
    if (*(unsigned char *)copt == 'W') {
	ityp = 1;
	if (mn7log_1.lwarn) {
	    io___2.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___2);
	    do_fio(&c__1, " MINUIT WARNING IN ", 19L);
	    do_fio(&c__1, corg, corg_len);
	    do_fio(&c__1, " ============== ", 16L);
	    do_fio(&c__1, cmes, cmes_len);
	    e_wsfe();
	    return 0;
	}
    } else {
	ityp = 2;
	if (mn7log_1.lrepor) {
	    io___3.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___3);
	    do_fio(&c__1, " MINUIT DEBUG FOR  ", 19L);
	    do_fio(&c__1, corg, corg_len);
	    do_fio(&c__1, " ============== ", 16L);
	    do_fio(&c__1, cmes, cmes_len);
	    e_wsfe();
	    return 0;
	}
    }
/*                 if appropriate flag is off, fill circular buffer */
    if (mn7cnv_1.nwrmes[ityp - 1] == 0) {
	mn7wri_1.icirc[ityp - 1] = 0;
    }
    ++mn7cnv_1.nwrmes[ityp - 1];
    ++mn7wri_1.icirc[ityp - 1];
    if (mn7wri_1.icirc[ityp - 1] > 10) {
	mn7wri_1.icirc[ityp - 1] = 1;
    }
    ic = mn7wri_1.icirc[ityp - 1];
    s_copy(mn7wrc_1.origin + (ic + ityp * 10 - 11) * 10, corg, 10L, corg_len);
    s_copy(mn7wrc_1.warmes + (ic + ityp * 10 - 11) * 60, cmes, 60L, cmes_len);
    mn7wri_1.nfcwar[ic + ityp * 10 - 11] = mn7cnv_1.nfcn;
    return 0;

/*             'SHO WARnings', ask if any suppressed mess in buffer */
L200:
    if (*(unsigned char *)copt == 'W') {
	ityp = 1;
	s_copy(ctyp, "WARNING", 7L, 7L);
    } else {
	ityp = 2;
	s_copy(ctyp, "*DEBUG*", 7L, 7L);
    }
    if (mn7cnv_1.nwrmes[ityp - 1] > 0) {
	s_copy(englsh, " WAS SUPPRESSED.  ", 20L, 18L);
	if (mn7cnv_1.nwrmes[ityp - 1] > 1) {
	    s_copy(englsh, "S WERE SUPPRESSED.", 20L, 18L);
	}
	io___7.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___7);
	do_fio(&c__1, (char *)&mn7cnv_1.nwrmes[ityp - 1], (ftnlen)sizeof(
		integer));
	do_fio(&c__1, " MINUIT ", 8L);
	do_fio(&c__1, ctyp, 7L);
	do_fio(&c__1, " MESSAGE", 8L);
	do_fio(&c__1, englsh, 20L);
	e_wsfe();
	nm = mn7cnv_1.nwrmes[ityp - 1];
	ic = 0;
	if (nm > 10) {
	    io___9.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___9);
	    do_fio(&c__1, " ONLY THE MOST RECENT ", 22L);
	    do_fio(&c__1, (char *)&c__10, (ftnlen)sizeof(integer));
	    do_fio(&c__1, " WILL BE LISTED BELOW.", 22L);
	    e_wsfe();
	    nm = 10;
	    ic = mn7wri_1.icirc[ityp - 1];
	}
	io___10.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___10);
	do_fio(&c__1, "  CALLS  ORIGIN         MESSAGE", 31L);
	e_wsfe();
	i__1 = nm;
	for (i = 1; i <= i__1; ++i) {
	    ++ic;
	    if (ic > 10) {
		ic = 1;
	    }
	    io___12.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___12);
	    do_fio(&c__1, (char *)&mn7wri_1.nfcwar[ic + ityp * 10 - 11], (
		    ftnlen)sizeof(integer));
	    do_fio(&c__1, mn7wrc_1.origin + (ic + ityp * 10 - 11) * 10, 10L);
	    do_fio(&c__1, mn7wrc_1.warmes + (ic + ityp * 10 - 11) * 60, 60L);
	    e_wsfe();
/* L300: */
	}
	mn7cnv_1.nwrmes[ityp - 1] = 0;
	io___13.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___13);
	e_wsfe();
    }
    return 0;
} /* mnwarn_ */

