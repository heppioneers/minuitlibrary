/* mnline.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__4 = 4;
static integer c__1 = 1;
static integer c__3 = 3;


/* $Id: mnline.F,v 1.1.1.1 1996/03/07 14:31:30 mclareni Exp $ */

/* $Log: mnline.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mnline_(fcn, start, fstart, step, slope, toler, futil)
/* Subroutine */ int (*fcn) ();
doublereal *start, *fstart, *step, *slope, *toler;
/* Subroutine */ int (*futil) ();
{
    /* Initialized data */

    static char charal[26+1] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();
    /* Subroutine */ int s_copy();

    /* Local variables */
    static char chpq[1*12];
    static doublereal slam, sdev;
    static integer i;
    static doublereal coeff[3], denom, flast;
    static char cmess[60];
    static doublereal fvals[3], xvals[3];
    static integer nparx;
    static doublereal f1, fvmin, xvmin, ratio, f2, f3, fvmax;
    static integer nvmax, nxypt;
    static doublereal toler8, toler9;
    static integer kk;
    static logical ldebug;
    static doublereal overal, undral;
    extern /* Subroutine */ int mninex_();
    static doublereal slamin, slamax;
    extern /* Subroutine */ int mnpfit_();
    static doublereal slopem;
    extern /* Subroutine */ int mnwarn_(), mnplot_();
    static integer ipt;
    static doublereal xpq[12], ypq[12];

    /* Fortran I/O blocks */
    static cilist io___7 = { 0, 0, 0, "(A/2E14.5/2X,10F10.5)", 0 };
    static cilist io___35 = { 0, 0, 0, "(A/(2X,6G12.4))", 0 };
    static cilist io___36 = { 0, 0, 0, "(' AFTER',I3,' POINTS,',A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Perform a line search from position START */
/* C        along direction STEP, where the length of vector STEP */
/* C                   gives the expected position of minimum. */
/* C        FSTART is value of function at START */
/* C        SLOPE (if non-zero) is df/dx along STEP at START */
/* C        TOLER is initial tolerance of minimum in direction STEP */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


/* SLAMBG and ALPHA control the maximum individual steps allowed. */
/* The first step is always =1. The max length of second step is SLAMBG. 
*/
/* The max size of subsequent steps is the maximum previous successful */
/*   step multiplied by ALPHA + the size of most recent successful step, 
*/
/*   but cannot be smaller than SLAMBG. */
    /* Parameter adjustments */
    --step;
    --start;

    /* Function Body */
    ldebug = mn7flg_1.idbg[1] >= 1;
/*                  starting values for overall limits on total step SLAM 
*/
    overal = (float)1e3;
    undral = (float)-100.;
/*                              debug check if start is ok */
    if (ldebug) {
	mninex_(&start[1]);
	(*fcn)(&nparx, mn7der_1.gin, &f1, mn7ext_1.u, &c__4, futil);
	++mn7cnv_1.nfcn;
	if (f1 != *fstart) {
	    io___7.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___7);
	    do_fio(&c__1, " MNLINE start point not consistent, F values, par\
ameters=", 57L);
	    i__1 = mn7npr_1.npar;
	    for (kk = 1; kk <= i__1; ++kk) {
		do_fio(&c__1, (char *)&mn7int_1.x[kk - 1], (ftnlen)sizeof(
			doublereal));
	    }
	    e_wsfe();
	}
    }
/*                                      . set up linear search along STEP 
*/
    fvmin = *fstart;
    xvmin = 0.;
    nxypt = 1;
    *(unsigned char *)&chpq[0] = *(unsigned char *)&charal[0];
    xpq[0] = (float)0.;
    ypq[0] = *fstart;
/*               SLAMIN = smallest possible value of ABS(SLAM) */
    slamin = 0.;
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	if (step[i] == 0.) {
	    goto L20;
	}
	ratio = (d__1 = start[i] / step[i], abs(d__1));
	if (slamin == 0.) {
	    slamin = ratio;
	}
	if (ratio < slamin) {
	    slamin = ratio;
	}
L20:
	mn7int_1.x[i - 1] = start[i] + step[i];
    }
    if (slamin == 0.) {
	slamin = mn7cns_1.epsmac;
    }
    slamin *= mn7cns_1.epsma2;
    nparx = mn7npr_1.npar;

    mninex_(mn7int_1.x);
    (*fcn)(&nparx, mn7der_1.gin, &f1, mn7ext_1.u, &c__4, futil);
    ++mn7cnv_1.nfcn;
    ++nxypt;
    *(unsigned char *)&chpq[nxypt - 1] = *(unsigned char *)&charal[nxypt - 1];
    xpq[nxypt - 1] = (float)1.;
    ypq[nxypt - 1] = f1;
    if (f1 < *fstart) {
	fvmin = f1;
	xvmin = (float)1.;
    }
/*                         . quadr interp using slope GDEL and two points 
*/
    slam = (float)1.;
    toler8 = *toler;
    slamax = 5.;
    flast = f1;
/*                         can iterate on two-points (cut) if no imprvmnt 
*/
L25:
/* Computing 2nd power */
    d__1 = slam;
    denom = (flast - *fstart - *slope * slam) * (float)2. / (d__1 * d__1);
/*     IF (DENOM .EQ. ZERO)  DENOM = -0.1*SLOPE */
    slam = (float)1.;
    if (denom != 0.) {
	slam = -(*slope) / denom;
    }
    if (slam < 0.) {
	slam = slamax;
    }
    if (slam > slamax) {
	slam = slamax;
    }
    if (slam < toler8) {
	slam = toler8;
    }
    if (slam < slamin) {
	goto L80;
    }
    if ((d__1 = slam - (float)1., abs(d__1)) < toler8 && f1 < *fstart) {
	goto L70;
    }
    if ((d__1 = slam - (float)1., abs(d__1)) < toler8) {
	slam = toler8 + (float)1.;
    }
    if (nxypt >= 12) {
	goto L65;
    }
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
/* L30: */
	mn7int_1.x[i - 1] = start[i] + slam * step[i];
    }
    mninex_(mn7int_1.x);
    (*fcn)(&mn7npr_1.npar, mn7der_1.gin, &f2, mn7ext_1.u, &c__4, futil);
    ++mn7cnv_1.nfcn;
    ++nxypt;
    *(unsigned char *)&chpq[nxypt - 1] = *(unsigned char *)&charal[nxypt - 1];
    xpq[nxypt - 1] = slam;
    ypq[nxypt - 1] = f2;
    if (f2 < fvmin) {
	fvmin = f2;
	xvmin = slam;
    }
    if (*fstart == fvmin) {
	flast = f2;
	toler8 = *toler * slam;
	overal = slam - toler8;
	slamax = overal;
	goto L25;
    }
/*                                        . quadr interp using 3 points */
    xvals[0] = xpq[0];
    fvals[0] = ypq[0];
    xvals[1] = xpq[nxypt - 2];
    fvals[1] = ypq[nxypt - 2];
    xvals[2] = xpq[nxypt - 1];
    fvals[2] = ypq[nxypt - 1];
/*                             begin iteration, calculate desired step */
L50:
/* Computing MAX */
    d__1 = slamax, d__2 = abs(xvmin) * 2.;
    slamax = max(d__1,d__2);
    mnpfit_(xvals, fvals, &c__3, coeff, &sdev);
    if (coeff[2] <= 0.) {
	slopem = coeff[2] * (float)2. * xvmin + coeff[1];
	if (slopem <= 0.) {
	    slam = xvmin + slamax;
	} else {
	    slam = xvmin - slamax;
	}
    } else {
	slam = -coeff[1] / (coeff[2] * (float)2.);
	if (slam > xvmin + slamax) {
	    slam = xvmin + slamax;
	}
	if (slam < xvmin - slamax) {
	    slam = xvmin - slamax;
	}
    }
    if (slam > 0.) {
	if (slam > overal) {
	    slam = overal;
	}
    } else {
	if (slam < undral) {
	    slam = undral;
	}
    }
/*               come here if step was cut below */
L52:
/* Computing MAX */
    d__2 = toler8, d__3 = (d__1 = toler8 * slam, abs(d__1));
    toler9 = max(d__2,d__3);
    for (ipt = 1; ipt <= 3; ++ipt) {
	if ((d__1 = slam - xvals[ipt - 1], abs(d__1)) < toler9) {
	    goto L70;
	}
/* L55: */
    }
/*                take the step */
    if (nxypt >= 12) {
	goto L65;
    }
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
/* L60: */
	mn7int_1.x[i - 1] = start[i] + slam * step[i];
    }
    mninex_(mn7int_1.x);
    (*fcn)(&nparx, mn7der_1.gin, &f3, mn7ext_1.u, &c__4, futil);
    ++mn7cnv_1.nfcn;
    ++nxypt;
    *(unsigned char *)&chpq[nxypt - 1] = *(unsigned char *)&charal[nxypt - 1];
    xpq[nxypt - 1] = slam;
    ypq[nxypt - 1] = f3;
/*             find worst previous point out of three */
    fvmax = fvals[0];
    nvmax = 1;
    if (fvals[1] > fvmax) {
	fvmax = fvals[1];
	nvmax = 2;
    }
    if (fvals[2] > fvmax) {
	fvmax = fvals[2];
	nvmax = 3;
    }
/*              if latest point worse than all three previous, cut step */
    if (f3 >= fvmax) {
	if (nxypt >= 12) {
	    goto L65;
	}
	if (slam > xvmin) {
/* Computing MIN */
	    d__1 = overal, d__2 = slam - toler8;
	    overal = min(d__1,d__2);
	}
	if (slam < xvmin) {
/* Computing MAX */
	    d__1 = undral, d__2 = slam + toler8;
	    undral = max(d__1,d__2);
	}
	slam = (slam + xvmin) * (float).5;
	goto L52;
    }
/*              prepare another iteration, replace worst previous point */
    xvals[nvmax - 1] = slam;
    fvals[nvmax - 1] = f3;
    if (f3 < fvmin) {
	fvmin = f3;
	xvmin = slam;
    } else {
	if (slam > xvmin) {
/* Computing MIN */
	    d__1 = overal, d__2 = slam - toler8;
	    overal = min(d__1,d__2);
	}
	if (slam < xvmin) {
/* Computing MAX */
	    d__1 = undral, d__2 = slam + toler8;
	    undral = max(d__1,d__2);
	}
    }
    if (nxypt < 12) {
	goto L50;
    }
/*                                            . . end of iteration . . . 
*/
/*            stop because too many iterations */
L65:
    s_copy(cmess, " LINE SEARCH HAS EXHAUSTED THE LIMIT OF FUNCTION CALLS ", 
	    60L, 55L);
    if (ldebug) {
	io___35.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___35);
	do_fio(&c__1, " MNLINE DEBUG: steps=", 21L);
	i__1 = mn7npr_1.npar;
	for (kk = 1; kk <= i__1; ++kk) {
	    do_fio(&c__1, (char *)&step[kk], (ftnlen)sizeof(doublereal));
	}
	e_wsfe();
    }
    goto L100;
/*            stop because within tolerance */
L70:
    s_copy(cmess, " LINE SEARCH HAS ATTAINED TOLERANCE ", 60L, 36L);
    goto L100;
L80:
    s_copy(cmess, " STEP SIZE AT ARITHMETICALLY ALLOWED MINIMUM", 60L, 44L);
L100:
    mn7min_1.amin = fvmin;
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	mn7int_1.dirin[i - 1] = step[i] * xvmin;
/* L120: */
	mn7int_1.x[i - 1] = start[i] + mn7int_1.dirin[i - 1];
    }
    mninex_(mn7int_1.x);
    if (xvmin < (float)0.) {
	mnwarn_("D", "MNLINE", " LINE MINIMUM IN BACKWARDS DIRECTION", 1L, 6L,
		 36L);
    }
    if (fvmin == *fstart) {
	mnwarn_("D", "MNLINE", " LINE SEARCH FINDS NO IMPROVEMENT ", 1L, 6L, 
		34L);
    }
    if (ldebug) {
	io___36.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___36);
	do_fio(&c__1, (char *)&nxypt, (ftnlen)sizeof(integer));
	do_fio(&c__1, cmess, 60L);
	e_wsfe();
	mnplot_(xpq, ypq, chpq, &nxypt, &mn7iou_1.isyswr, &mn7iou_1.npagwd, &
		mn7iou_1.npagln, 1L);
    }
    return 0;
} /* mnline_ */

