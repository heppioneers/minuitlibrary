/* mnlims.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;


/* $Id: mnlims.F,v 1.2 1996/03/15 18:02:48 james Exp $ */

/* $Log: mnlims.F,v $ */
/* Revision 1.2  1996/03/15 18:02:48  james */
/*     Modified Files: */
/* mnderi.F eliminate possible division by zero */
/* mnexcm.F suppress print on STOP when print flag=-1 */
/*          set FVAL3 to flag if FCN already called with IFLAG=3 */
/* mninit.F set version 96.03 */
/* mnlims.F remove arguments, not needed */
/* mnmigr.F VLEN -> LENV in debug print statement */
/* mnparm.F move call to MNRSET to after NPAR redefined, to zero all */
/* mnpsdf.F eliminate possible division by zero */
/* mnscan.F suppress printout when print flag =-1 */
/* mnset.F  remove arguments in call to MNLIMS */
/* mnsimp.F fix CSTATU so status is PROGRESS only if new minimum */
/* mnvert.F eliminate possible division by zero */

/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mnlims_()
{
    /* Format strings */
    static char fmt_134[] = "(\002 LIMITS REMOVED FROM PARAMETER\002,i4)";
    static char fmt_237[] = "(\002 PARAMETER\002,i3,\002 LIMITS SET TO\002,2\
g15.5)";

    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe(), s_cmp();

    /* Local variables */
    static doublereal dxdi;
    static integer kint;
    static doublereal snew;
    static integer i2, newcod;
    extern /* Subroutine */ int mndxdi_(), mnexin_(), mnrset_();
    static integer ifx, inu;

    /* Fortran I/O blocks */
    static cilist io___5 = { 0, 0, 0, "(11X,A,I3)", 0 };
    static cilist io___6 = { 0, 0, 0, fmt_134, 0 };
    static cilist io___9 = { 0, 0, 0, fmt_237, 0 };
    static cilist io___10 = { 0, 0, 0, "(A,I3,A)", 0 };
    static cilist io___11 = { 0, 0, 0, "(A,I3)", 0 };
    static cilist io___13 = { 0, 0, 0, "(A)", 0 };
    static cilist io___14 = { 0, 0, 0, fmt_134, 0 };
    static cilist io___15 = { 0, 0, 0, "(A,I3)", 0 };
    static cilist io___16 = { 0, 0, 0, fmt_237, 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C       Called from MNSET */
/* C       Interprets the SET LIM command, to reset the parameter limits 
*/
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */



    s_copy(mn7tit_1.cfrom, "SET LIM ", 8L, 8L);
    mn7cnv_1.nfcnfr = mn7cnv_1.nfcn;
    s_copy(mn7tit_1.cstatu, "NO CHANGE ", 10L, 10L);
    i2 = (integer) mn7arg_1.word7[0];
    if (i2 > mn7npr_1.maxext || i2 < 0) {
	goto L900;
    }
    if (i2 > 0) {
	goto L30;
    }
/*                                     set limits on all parameters */
    newcod = 4;
    if (mn7arg_1.word7[1] == mn7arg_1.word7[2]) {
	newcod = 1;
    }
    i__1 = mn7npr_1.nu;
    for (inu = 1; inu <= i__1; ++inu) {
	if (mn7inx_1.nvarl[inu - 1] <= 0) {
	    goto L20;
	}
	if (mn7inx_1.nvarl[inu - 1] == 1 && newcod == 1) {
	    goto L20;
	}
	kint = mn7inx_1.niofex[inu - 1];
/*             see if parameter has been fixed */
	if (kint <= 0) {
	    if (mn7flg_1.isw[4] >= 0) {
		io___5.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___5);
		do_fio(&c__1, " LIMITS NOT CHANGED FOR FIXED PARAMETER:", 40L)
			;
		do_fio(&c__1, (char *)&inu, (ftnlen)sizeof(integer));
		e_wsfe();
	    }
	    goto L20;
	}
	if (newcod == 1) {
/*            remove limits from parameter */
	    if (mn7flg_1.isw[4] > 0) {
		io___6.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___6);
		do_fio(&c__1, (char *)&inu, (ftnlen)sizeof(integer));
		e_wsfe();
	    }
	    s_copy(mn7tit_1.cstatu, "NEW LIMITS", 10L, 10L);
	    mndxdi_(&mn7int_1.x[kint - 1], &kint, &dxdi);
	    snew = mn7der_1.gstep[kint - 1] * dxdi;
	    mn7der_1.gstep[kint - 1] = abs(snew);
	    mn7inx_1.nvarl[inu - 1] = 1;
	} else {
/*             put limits on parameter */
	    mn7ext_1.alim[inu - 1] = min(mn7arg_1.word7[1],mn7arg_1.word7[2]);
	    mn7ext_1.blim[inu - 1] = max(mn7arg_1.word7[1],mn7arg_1.word7[2]);
	    if (mn7flg_1.isw[4] > 0) {
		io___9.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___9);
		do_fio(&c__1, (char *)&inu, (ftnlen)sizeof(integer));
		do_fio(&c__1, (char *)&mn7ext_1.alim[inu - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&mn7ext_1.blim[inu - 1], (ftnlen)sizeof(
			doublereal));
		e_wsfe();
	    }
	    mn7inx_1.nvarl[inu - 1] = 4;
	    s_copy(mn7tit_1.cstatu, "NEW LIMITS", 10L, 10L);
	    mn7der_1.gstep[kint - 1] = (float)-.1;
	}
L20:
	;
    }
    goto L900;
/*                                       set limits on one parameter */
L30:
    if (mn7inx_1.nvarl[i2 - 1] <= 0) {
	io___10.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___10);
	do_fio(&c__1, " PARAMETER ", 11L);
	do_fio(&c__1, (char *)&i2, (ftnlen)sizeof(integer));
	do_fio(&c__1, " IS NOT VARIABLE.", 17L);
	e_wsfe();
	goto L900;
    }
    kint = mn7inx_1.niofex[i2 - 1];
/*                                       see if parameter was fixed */
    if (kint == 0) {
	io___11.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___11);
	do_fio(&c__1, " REQUEST TO CHANGE LIMITS ON FIXED PARAMETER:", 45L);
	do_fio(&c__1, (char *)&i2, (ftnlen)sizeof(integer));
	e_wsfe();
	i__1 = mn7fx1_1.npfix;
	for (ifx = 1; ifx <= i__1; ++ifx) {
	    if (i2 == mn7fx1_1.ipfix[ifx - 1]) {
		goto L92;
	    }
/* L82: */
	}
	io___13.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___13);
	do_fio(&c__1, " MINUIT BUG IN MNLIMS. SEE F. JAMES", 35L);
	e_wsfe();
L92:
	;
    }
    if (mn7arg_1.word7[1] != mn7arg_1.word7[2]) {
	goto L235;
    }
/*                                       remove limits */
    if (mn7inx_1.nvarl[i2 - 1] != 1) {
	if (mn7flg_1.isw[4] > 0) {
	    io___14.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___14);
	    do_fio(&c__1, (char *)&i2, (ftnlen)sizeof(integer));
	    e_wsfe();
	}
	s_copy(mn7tit_1.cstatu, "NEW LIMITS", 10L, 10L);
	if (kint <= 0) {
	    mn7fx3_1.gsteps[ifx - 1] = (d__1 = mn7fx3_1.gsteps[ifx - 1], abs(
		    d__1));
	} else {
	    mndxdi_(&mn7int_1.x[kint - 1], &kint, &dxdi);
	    if (abs(dxdi) < (float).01) {
		dxdi = (float).01;
	    }
	    mn7der_1.gstep[kint - 1] = (d__1 = mn7der_1.gstep[kint - 1] * 
		    dxdi, abs(d__1));
	    mn7der_1.grd[kint - 1] *= dxdi;
	}
	mn7inx_1.nvarl[i2 - 1] = 1;
    } else {
	io___15.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___15);
	do_fio(&c__1, " NO LIMITS SPECIFIED.  PARAMETER ", 33L);
	do_fio(&c__1, (char *)&i2, (ftnlen)sizeof(integer));
	do_fio(&c__1, " IS ALREADY UNLIMITED.  NO CHANGE.", 34L);
	e_wsfe();
    }
    goto L900;
/*                                        put on limits */
L235:
    mn7ext_1.alim[i2 - 1] = min(mn7arg_1.word7[1],mn7arg_1.word7[2]);
    mn7ext_1.blim[i2 - 1] = max(mn7arg_1.word7[1],mn7arg_1.word7[2]);
    mn7inx_1.nvarl[i2 - 1] = 4;
    if (mn7flg_1.isw[4] > 0) {
	io___16.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___16);
	do_fio(&c__1, (char *)&i2, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&mn7ext_1.alim[i2 - 1], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&mn7ext_1.blim[i2 - 1], (ftnlen)sizeof(
		doublereal));
	e_wsfe();
    }
    s_copy(mn7tit_1.cstatu, "NEW LIMITS", 10L, 10L);
    if (kint <= 0) {
	mn7fx3_1.gsteps[ifx - 1] = (float)-.1;
    } else {
	mn7der_1.gstep[kint - 1] = (float)-.1;
    }

L900:
    if (s_cmp(mn7tit_1.cstatu, "NO CHANGE ", 10L, 10L) != 0) {
	mnexin_(mn7int_1.x);
	mnrset_(&c__1);
    }
    return 0;
} /* mnlims_ */

