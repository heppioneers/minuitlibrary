/* mninpu.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;


/* $Id: mninpu.F,v 1.1.1.1 1996/03/07 14:31:30 mclareni Exp $ */

/* $Log: mninpu.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mninpu_(iunit, ierr)
integer *iunit, *ierr;
{
    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 0, 0, "(A)", 0 };
    static cilist io___2 = { 0, 0, 0, "(A)", 0 };
    static cilist io___3 = { 0, 0, 0, "(A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C      called by the user to SET INPUT to IUNIT, */
/* C      an alternative to MNSTIN where the user can specify just */
/* C      a logical unit number and he is not interrogated about */
/* C      open files and rewinding, all that is the responsibility */
/* C      of the user and cannot be fixed interactively. */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */



    *ierr = 0;
/*                              IUNIT = 0, revert to previous input file 
*/
    if (*iunit == 0) {
	if (mn7io2_1.nstkrd == 0) {
	    io___1.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___1);
	    do_fio(&c__1, " CALL TO MNINPU(0) IGNORED", 26L);
	    e_wsfe();
	    io___2.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___2);
	    do_fio(&c__1, " ALREADY READING FROM PRIMARY INPUT", 35L);
	    e_wsfe();
	} else {
	    mn7iou_1.isysrd = mn7io2_1.istkrd[mn7io2_1.nstkrd - 1];
	    --mn7io2_1.nstkrd;
	}

/*                               new input file */
    } else {
	if (mn7io2_1.nstkrd >= 10) {
	    io___3.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___3);
	    do_fio(&c__1, " INPUT FILE STACK SIZE EXCEEDED.", 32L);
	    e_wsfe();
	    goto L800;
	}
	++mn7io2_1.nstkrd;
	mn7io2_1.istkrd[mn7io2_1.nstkrd - 1] = mn7iou_1.isysrd;
	mn7iou_1.isysrd = *iunit;
    }

    return 0;
L800:
    *ierr = 1;
    return 0;
} /* mninpu_ */

