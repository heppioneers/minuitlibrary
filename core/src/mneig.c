/* mneig.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"


/* $Id: mneig.F,v 1.1.1.1 1996/03/07 14:31:29 mclareni Exp $ */

/* $Log: mneig.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mneig_(a, ndima, n, mits, work, precis, ifault)
doublereal *a;
integer *ndima, *n, *mits;
doublereal *work, *precis;
integer *ifault;
{
    /* System generated locals */
    integer a_dim1, a_offset, i__1, i__2, i__3;
    doublereal d__1, d__2;

    /* Builtin functions */
    double sqrt();

    /* Local variables */
    static doublereal b, c, f, h;
    static integer i, j, k, l, m;
    static doublereal r, s;
    static integer i0, i1, j1, m1, n1;
    static doublereal hh, gl, pr, pt;


/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */

/*          PRECIS is the machine precision EPSMAC */
    /* Parameter adjustments */
    a_dim1 = *ndima;
    a_offset = a_dim1 + 1;
    a -= a_offset;
    --work;

    /* Function Body */
    *ifault = 1;

    i = *n;
    i__1 = *n;
    for (i1 = 2; i1 <= i__1; ++i1) {
	l = i - 2;
	f = a[i + (i - 1) * a_dim1];
	gl = 0.;

	if (l < 1) {
	    goto L25;
	}

	i__2 = l;
	for (k = 1; k <= i__2; ++k) {
/* L20: */
/* Computing 2nd power */
	    d__1 = a[i + k * a_dim1];
	    gl += d__1 * d__1;
	}
L25:
/* Computing 2nd power */
	d__1 = f;
	h = gl + d__1 * d__1;

	if (gl > 1e-35) {
	    goto L30;
	}

	work[i] = 0.;
	work[*n + i] = f;
	goto L65;
L30:
	++l;

	gl = sqrt(h);

	if (f >= 0.) {
	    gl = -gl;
	}

	work[*n + i] = gl;
	h -= f * gl;
	a[i + (i - 1) * a_dim1] = f - gl;
	f = 0.;
	i__2 = l;
	for (j = 1; j <= i__2; ++j) {
	    a[j + i * a_dim1] = a[i + j * a_dim1] / h;
	    gl = 0.;
	    i__3 = j;
	    for (k = 1; k <= i__3; ++k) {
/* L40: */
		gl += a[j + k * a_dim1] * a[i + k * a_dim1];
	    }

	    if (j >= l) {
		goto L47;
	    }

	    j1 = j + 1;
	    i__3 = l;
	    for (k = j1; k <= i__3; ++k) {
/* L45: */
		gl += a[k + j * a_dim1] * a[i + k * a_dim1];
	    }
L47:
	    work[*n + j] = gl / h;
	    f += gl * a[j + i * a_dim1];
/* L50: */
	}
	hh = f / (h + h);
	i__2 = l;
	for (j = 1; j <= i__2; ++j) {
	    f = a[i + j * a_dim1];
	    gl = work[*n + j] - hh * f;
	    work[*n + j] = gl;
	    i__3 = j;
	    for (k = 1; k <= i__3; ++k) {
		a[j + k * a_dim1] = a[j + k * a_dim1] - f * work[*n + k] - gl 
			* a[i + k * a_dim1];
/* L60: */
	    }
	}
	work[i] = h;
L65:
	--i;
/* L70: */
    }
    work[1] = 0.;
    work[*n + 1] = 0.;
    i__1 = *n;
    for (i = 1; i <= i__1; ++i) {
	l = i - 1;

	if (work[i] == 0. || l == 0) {
	    goto L100;
	}

	i__3 = l;
	for (j = 1; j <= i__3; ++j) {
	    gl = 0.;
	    i__2 = l;
	    for (k = 1; k <= i__2; ++k) {
/* L80: */
		gl += a[i + k * a_dim1] * a[k + j * a_dim1];
	    }
	    i__2 = l;
	    for (k = 1; k <= i__2; ++k) {
		a[k + j * a_dim1] -= gl * a[k + i * a_dim1];
/* L90: */
	    }
	}
L100:
	work[i] = a[i + i * a_dim1];
	a[i + i * a_dim1] = 1.;

	if (l == 0) {
	    goto L110;
	}

	i__2 = l;
	for (j = 1; j <= i__2; ++j) {
	    a[i + j * a_dim1] = 0.;
	    a[j + i * a_dim1] = 0.;
/* L105: */
	}
L110:
	;
    }


    n1 = *n - 1;
    i__1 = *n;
    for (i = 2; i <= i__1; ++i) {
	i0 = *n + i - 1;
/* L130: */
	work[i0] = work[i0 + 1];
    }
    work[*n + *n] = 0.;
    b = 0.;
    f = 0.;
    i__1 = *n;
    for (l = 1; l <= i__1; ++l) {
	j = 0;
	h = *precis * ((d__1 = work[l], abs(d__1)) + (d__2 = work[*n + l], 
		abs(d__2)));

	if (b < h) {
	    b = h;
	}

	i__2 = *n;
	for (m1 = l; m1 <= i__2; ++m1) {
	    m = m1;

	    if ((d__1 = work[*n + m], abs(d__1)) <= b) {
		goto L150;
	    }

/* L140: */
	}

L150:
	if (m == l) {
	    goto L205;
	}

L160:
	if (j == *mits) {
	    return 0;
	}

	++j;
	pt = (work[l + 1] - work[l]) / (work[*n + l] * 2.);
	r = sqrt(pt * pt + 1.);
	pr = pt + r;

	if (pt < 0.) {
	    pr = pt - r;
	}

	h = work[l] - work[*n + l] / pr;
	i__2 = *n;
	for (i = l; i <= i__2; ++i) {
/* L170: */
	    work[i] -= h;
	}
	f += h;
	pt = work[m];
	c = 1.;
	s = 0.;
	m1 = m - 1;
	i = m;
	i__2 = m1;
	for (i1 = l; i1 <= i__2; ++i1) {
	    j = i;
	    --i;
	    gl = c * work[*n + i];
	    h = c * pt;

	    if (abs(pt) >= (d__1 = work[*n + i], abs(d__1))) {
		goto L180;
	    }

	    c = pt / work[*n + i];
	    r = sqrt(c * c + 1.);
	    work[*n + j] = s * work[*n + i] * r;
	    s = 1. / r;
	    c /= r;
	    goto L190;
L180:
	    c = work[*n + i] / pt;
	    r = sqrt(c * c + 1.);
	    work[*n + j] = s * pt * r;
	    s = c / r;
	    c = 1. / r;
L190:
	    pt = c * work[i] - s * gl;
	    work[j] = h + s * (c * gl + s * work[i]);
	    i__3 = *n;
	    for (k = 1; k <= i__3; ++k) {
		h = a[k + j * a_dim1];
		a[k + j * a_dim1] = s * a[k + i * a_dim1] + c * h;
		a[k + i * a_dim1] = c * a[k + i * a_dim1] - s * h;
/* L200: */
	    }
	}
	work[*n + l] = s * pt;
	work[l] = c * pt;

	if ((d__1 = work[*n + l], abs(d__1)) > b) {
	    goto L160;
	}

L205:
	work[l] += f;
/* L210: */
    }
    i__1 = n1;
    for (i = 1; i <= i__1; ++i) {
	k = i;
	pt = work[i];
	i1 = i + 1;
	i__3 = *n;
	for (j = i1; j <= i__3; ++j) {

	    if (work[j] >= pt) {
		goto L220;
	    }

	    k = j;
	    pt = work[j];
L220:
	    ;
	}

	if (k == i) {
	    goto L240;
	}

	work[k] = work[i];
	work[i] = pt;
	i__3 = *n;
	for (j = 1; j <= i__3; ++j) {
	    pt = a[j + i * a_dim1];
	    a[j + i * a_dim1] = a[j + k * a_dim1];
	    a[j + k * a_dim1] = pt;
/* L230: */
	}
L240:
	;
    }
    *ifault = 0;

    return 0;
} /* mneig_ */

