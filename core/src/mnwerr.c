/* mnwerr.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_


/* $Id: mnwerr.F,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: mnwerr.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */


/* Subroutine */ int mnwerr_()
{
    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    double sqrt(), sin();

    /* Local variables */
    static integer ndex, ierr, i, j, k, l, ndiag;
    static doublereal denom;
    static integer k1;
    static doublereal ba, al, dx, du1, du2;
    extern /* Subroutine */ int mnvert_();
    static integer iin;


/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C          Calculates the WERR, external parameter errors, */
/* C      and the global correlation coefficients, to be called */
/* C      whenever a new covariance matrix is available. */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


/*                         calculate external error if v exists */
    if (mn7flg_1.isw[1] >= 1) {
	i__1 = mn7npr_1.npar;
	for (l = 1; l <= i__1; ++l) {
	    ndex = l * (l + 1) / 2;
	    dx = sqrt((d__1 = mn7var_1.vhmat[ndex - 1] * mn7min_1.up, abs(
		    d__1)));
	    i = mn7inx_1.nexofi[l - 1];
	    if (mn7inx_1.nvarl[i - 1] > 1) {
		al = mn7ext_1.alim[i - 1];
		ba = mn7ext_1.blim[i - 1] - al;
		du1 = al + (sin(mn7int_1.x[l - 1] + dx) + (float)1.) * (float)
			.5 * ba - mn7ext_1.u[i - 1];
		du2 = al + (sin(mn7int_1.x[l - 1] - dx) + (float)1.) * (float)
			.5 * ba - mn7ext_1.u[i - 1];
		if (dx > (float)1.) {
		    du1 = ba;
		}
		dx = (abs(du1) + abs(du2)) * (float).5;
	    }
	    mn7err_1.werr[l - 1] = dx;
/* L100: */
	}
    }
/*                          global correlation coefficients */
    if (mn7flg_1.isw[1] >= 1) {
	i__1 = mn7npr_1.npar;
	for (i = 1; i <= i__1; ++i) {
	    mn7err_1.globcc[i - 1] = (float)0.;
	    k1 = i * (i - 1) / 2;
	    i__2 = i;
	    for (j = 1; j <= i__2; ++j) {
		k = k1 + j;
		mn7sim_1.p[i + j * 50 - 51] = mn7var_1.vhmat[k - 1];
/* L130: */
		mn7sim_1.p[j + i * 50 - 51] = mn7sim_1.p[i + j * 50 - 51];
	    }
	}
	mnvert_(mn7sim_1.p, &mn7npr_1.maxint, &mn7npr_1.maxint, &
		mn7npr_1.npar, &ierr);
	if (ierr == 0) {
	    i__2 = mn7npr_1.npar;
	    for (iin = 1; iin <= i__2; ++iin) {
		ndiag = iin * (iin + 1) / 2;
		denom = mn7sim_1.p[iin + iin * 50 - 51] * mn7var_1.vhmat[
			ndiag - 1];
		if (denom <= 1. && denom >= 0.) {
		    mn7err_1.globcc[iin - 1] = (float)0.;
		} else {
		    mn7err_1.globcc[iin - 1] = sqrt((float)1. - (float)1. / 
			    denom);
		}
/* L150: */
	    }
	}
    }
    return 0;
} /* mnwerr_ */

