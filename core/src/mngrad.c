/* mngrad.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__2 = 2;
static integer c__1 = 1;


/* $Id: mngrad.F,v 1.1.1.1 1996/03/07 14:31:29 mclareni Exp $ */

/* $Log: mngrad.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mngrad_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    /* Format strings */
    static char fmt_51[] = "(/\002 CHECK OF GRADIENT CALCULATION IN FCN\002/\
12x,\002PARAMETER\002,6x,\002G(IN FCN)\002,3x,\002G(MINUIT)\002,2x,\002DG(MI\
NUIT)\002,3x,\002AGREEMENT\002)";
    static char fmt_99[] = "(7x,i5,2x,a10,3e12.4,4x,a4)";
    static char fmt_1003[] = "(/\002 MINUIT DOES NOT ACCEPT DERIVATIVE CALCU\
LATIONS BY FCN\002/\002 TO FORCE ACCEPTANCE, ENTER \"SET GRAD    1\"\002/)";

    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    integer s_wsfe(), e_wsfe();
    /* Subroutine */ int s_copy();
    integer s_cmp(), do_fio();

    /* Local variables */
    static integer i;
    static logical lnone;
    static integer nparx;
    static doublereal fzero;
    extern /* Subroutine */ int mnhes1_();
    static doublereal gf[50];
    static integer lc;
    extern /* Subroutine */ int mnderi_(), mninex_();
    static integer istsav;
    static char cwd[4];
    static doublereal err;

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 0, 0, fmt_51, 0 };
    static cilist io___11 = { 0, 0, 0, fmt_99, 0 };
    static cilist io___12 = { 0, 0, 0, "(A)", 0 };
    static cilist io___13 = { 0, 0, 0, fmt_1003, 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C       Called from MNSET */
/* C       Interprets the SET GRAD command, which informs MINUIT whether 
*/
/* C       the first derivatives of FCN will be calculated by the user */
/* C       inside FCN.  It can check the user's derivative calculation */
/* C       by comparing it with a finite difference approximation. */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */




    mn7flg_1.isw[2] = 1;
    nparx = mn7npr_1.npar;
    if (mn7arg_1.word7[0] > 0.) {
	goto L2000;
    }
/*                  get user-calculated first derivatives from FCN */
    i__1 = mn7npr_1.nu;
    for (i = 1; i <= i__1; ++i) {
/* L30: */
	mn7der_1.gin[i - 1] = mn7cns_1.undefi;
    }
    mninex_(mn7int_1.x);
    (*fcn)(&nparx, mn7der_1.gin, &fzero, mn7ext_1.u, &c__2, futil);
    ++mn7cnv_1.nfcn;
    mnderi_(fcn, futil);
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
/* L40: */
	gf[i - 1] = mn7der_1.grd[i - 1];
    }
/*                    get MINUIT-calculated first derivatives */
    mn7flg_1.isw[2] = 0;
    istsav = mn7cnv_1.istrat;
    mn7cnv_1.istrat = 2;
    mnhes1_(fcn, futil);
    mn7cnv_1.istrat = istsav;
    io___6.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___6);
    e_wsfe();
    mn7flg_1.isw[2] = 1;
    lnone = FALSE_;
    i__1 = mn7npr_1.npar;
    for (lc = 1; lc <= i__1; ++lc) {
	i = mn7inx_1.nexofi[lc - 1];
	s_copy(cwd, "GOOD", 4L, 4L);
	err = mn7der_1.dgrd[lc - 1];
	if ((d__1 = gf[lc - 1] - mn7der_1.grd[lc - 1], abs(d__1)) > err) {
	    s_copy(cwd, " BAD", 4L, 4L);
	}
	if (mn7der_1.gin[i - 1] == mn7cns_1.undefi) {
	    s_copy(cwd, "NONE", 4L, 4L);
	    lnone = TRUE_;
	    gf[lc - 1] = (float)0.;
	}
	if (s_cmp(cwd, "GOOD", 4L, 4L) != 0) {
	    mn7flg_1.isw[2] = 0;
	}
	io___11.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___11);
	do_fio(&c__1, (char *)&i, (ftnlen)sizeof(integer));
	do_fio(&c__1, mn7nam_1.cpnam + (i - 1) * 10, 10L);
	do_fio(&c__1, (char *)&gf[lc - 1], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&mn7der_1.grd[lc - 1], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&err, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, cwd, 4L);
	e_wsfe();
/* L100: */
    }
    if (lnone) {
	io___12.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___12);
	do_fio(&c__1, "  AGREEMENT=NONE  MEANS FCN DID NOT CALCULATE THE DER\
IVATIVE", 60L);
	e_wsfe();
    }
    if (mn7flg_1.isw[2] == 0) {
	io___13.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___13);
	e_wsfe();
    }

L2000:
    return 0;
} /* mngrad_ */

