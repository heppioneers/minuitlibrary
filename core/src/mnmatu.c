/* mnmatu.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;


/* $Id: mnmatu.F,v 1.1.1.1 1996/03/07 14:31:30 mclareni Exp $ */

/* $Log: mnmatu.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mnmatu_(kode)
integer *kode;
{
    /* Format strings */
    static char fmt_150[] = "(/\002 PARAMETER  CORRELATION COEFFICIENTS\002\
/\002       NO.  GLOBAL\002,20i6)";
    static char fmt_171[] = "(6x,i3,2x,f7.5,1x,20f6.3)";
    static char fmt_181[] = "(19x,20f6.3)";

    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1;

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();
    double sqrt();

    /* Local variables */
    static integer ndex, i, j, m, n, ncoef;
    static doublereal vline[50];
    static integer nparm, id, it, ix;
    extern /* Subroutine */ int mnemat_(), mnwerr_();
    static integer nsofar, ndi, ndj, iso, isw2, isw5;

    /* Fortran I/O blocks */
    static cilist io___2 = { 0, 0, 0, "(1X,A)", 0 };
    static cilist io___3 = { 0, 0, 0, "(' MNMATU: NPAR=0')", 0 };
    static cilist io___5 = { 0, 0, 0, "(1X,A)", 0 };
    static cilist io___8 = { 0, 0, 0, fmt_150, 0 };
    static cilist io___19 = { 0, 0, 0, fmt_171, 0 };
    static cilist io___23 = { 0, 0, 0, fmt_181, 0 };
    static cilist io___24 = { 0, 0, 0, "(1X,A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        prints the covariance matrix v when KODE=1. */
/* C        always prints the global correlations, and */
/* C        calculates and prints the individual correlation coefficients 
*/
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    isw2 = mn7flg_1.isw[1];
    if (isw2 < 1) {
	io___2.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___2);
	do_fio(&c__1, mn7tit_1.covmes + isw2 * 22, 22L);
	e_wsfe();
	goto L500;
    }
    if (mn7npr_1.npar == 0) {
	io___3.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___3);
	e_wsfe();
	goto L500;
    }
/*                                       . . . . .external error matrix */
    if (*kode == 1) {
	isw5 = mn7flg_1.isw[4];
	mn7flg_1.isw[4] = 2;
	mnemat_(mn7sim_1.p, &mn7npr_1.maxint);
	if (isw2 < 3) {
	    io___5.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___5);
	    do_fio(&c__1, mn7tit_1.covmes + isw2 * 22, 22L);
	    e_wsfe();
	}
	mn7flg_1.isw[4] = isw5;
    }
/*                                       . . . . . correlation coeffs. . 
*/
    if (mn7npr_1.npar <= 1) {
	goto L500;
    }
    mnwerr_();
/*     NCOEF is number of coeff. that fit on one line, not to exceed 20 */
    ncoef = (mn7iou_1.npagwd - 19) / 6;
    ncoef = min(ncoef,20);
    nparm = min(mn7npr_1.npar,ncoef);
    io___8.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___8);
    i__1 = nparm;
    for (id = 1; id <= i__1; ++id) {
	do_fio(&c__1, (char *)&mn7inx_1.nexofi[id - 1], (ftnlen)sizeof(
		integer));
    }
    e_wsfe();
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	ix = mn7inx_1.nexofi[i - 1];
	ndi = i * (i + 1) / 2;
	i__2 = mn7npr_1.npar;
	for (j = 1; j <= i__2; ++j) {
	    m = max(i,j);
	    n = min(i,j);
	    ndex = m * (m - 1) / 2 + n;
	    ndj = j * (j + 1) / 2;
/* L170: */
	    vline[j - 1] = mn7var_1.vhmat[ndex - 1] / sqrt((d__1 = 
		    mn7var_1.vhmat[ndi - 1] * mn7var_1.vhmat[ndj - 1], abs(
		    d__1)));
	}
	nparm = min(mn7npr_1.npar,ncoef);
	io___19.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___19);
	do_fio(&c__1, (char *)&ix, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&mn7err_1.globcc[i - 1], (ftnlen)sizeof(
		doublereal));
	i__2 = nparm;
	for (it = 1; it <= i__2; ++it) {
	    do_fio(&c__1, (char *)&vline[it - 1], (ftnlen)sizeof(doublereal));
	}
	e_wsfe();
	if (i <= nparm) {
	    goto L200;
	}
	for (iso = 1; iso <= 10; ++iso) {
	    nsofar = nparm;
/* Computing MIN */
	    i__2 = mn7npr_1.npar, i__3 = nsofar + ncoef;
	    nparm = min(i__2,i__3);
	    io___23.ciunit = mn7iou_1.isyswr;
	    s_wsfe(&io___23);
	    i__2 = nparm;
	    for (it = nsofar + 1; it <= i__2; ++it) {
		do_fio(&c__1, (char *)&vline[it - 1], (ftnlen)sizeof(
			doublereal));
	    }
	    e_wsfe();
	    if (i <= nparm) {
		goto L192;
	    }
/* L190: */
	}
L192:
L200:
	;
    }
    if (isw2 < 3) {
	io___24.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___24);
	do_fio(&c__1, mn7tit_1.covmes + isw2 * 22, 22L);
	e_wsfe();
    }
L500:
    return 0;
} /* mnmatu_ */

