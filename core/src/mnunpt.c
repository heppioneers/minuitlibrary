/* mnunpt.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"


/* $Id: mnunpt.F,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: mnunpt.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */


logical mnunpt_(cfname, cfname_len)
char *cfname;
ftnlen cfname_len;
{
    /* System generated locals */
    integer i__1;
    logical ret_val;

    /* Builtin functions */
    /* Subroutine */ int s_copy();
    integer i_len();

    /* Local variables */
    static integer i, l, ic;
    static char cpt[80];

/*           is .TRUE. if CFNAME contains unprintable characters. */
    s_copy(cpt, " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456\
7890./;:[]$%*_!@#&+()", 80L, 80L);
    ret_val = FALSE_;
    l = i_len(cfname, cfname_len);
    i__1 = l;
    for (i = 1; i <= i__1; ++i) {
	for (ic = 1; ic <= 80; ++ic) {
	    if (*(unsigned char *)&cfname[i - 1] == *(unsigned char *)&cpt[ic 
		    - 1]) {
		goto L100;
	    }
/* L50: */
	}
	ret_val = TRUE_;
	goto L150;
L100:
	;
    }
L150:
    return ret_val;
} /* mnunpt_ */

