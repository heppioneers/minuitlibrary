/* mnimpr.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__4 = 4;
static integer c__0 = 0;
static integer c__5 = 5;


/* $Id: mnimpr.F,v 1.1.1.1 1996/03/07 14:31:30 mclareni Exp $ */

/* $Log: mnimpr.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mnimpr_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    /* Initialized data */

    static doublereal rnum = 0.;

    /* Format strings */
    static char fmt_1040[] = "(/\002 START ATTEMPT NO.\002,i2,\002 TO FIND N\
EW MINIMUM\002)";
    static char fmt_1000[] = "(\002 AN IMPROVEMENT ON THE PREVIOUS MINIMUM H\
AS BEEN FOUND\002)";
    static char fmt_1030[] = "(/\002 IMPROVE HAS FOUND A TRULY NEW MINIMU\
M\002/\002 \002,37(\002*\002)/)";
    static char fmt_1020[] = "(/\002 COVARIANCE MATRIX WAS NOT POSITIVE-DEFI\
NITE\002)";
    static char fmt_1010[] = "(\002 IMPROVE HAS RETURNED TO REGION OF ORIGIN\
AL MINIMUM\002)";

    /* System generated locals */
    integer i__1, i__2;
    doublereal d__1;

    /* Builtin functions */
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe();

    /* Local variables */
    static doublereal amax, dsav[50];
    static integer npfn, ndex, loop, i, j, ifail;
    static doublereal y[51];
    static integer iseed;
    static doublereal ycalf;
    static integer jhold, nloop, nparx;
    extern /* Subroutine */ int mnrn15_();
    static doublereal ystar, ystst;
    static integer nparp1, jh;
    static doublereal pb, ep;
    static integer jl;
    static doublereal wg;
    extern /* Subroutine */ int mncalf_();
    static doublereal xi;
    extern /* Subroutine */ int mnamin_();
    static doublereal sigsav;
    extern /* Subroutine */ int mnvert_(), mnrazz_(), mninex_(), mnsimp_(), 
	    mnrset_(), mnprin_();
    static doublereal reg, sig2;

    /* Fortran I/O blocks */
    static cilist io___16 = { 0, 0, 0, fmt_1040, 0 };
    static cilist io___29 = { 0, 0, 0, fmt_1000, 0 };
    static cilist io___30 = { 0, 0, 0, fmt_1030, 0 };
    static cilist io___31 = { 0, 0, 0, fmt_1020, 0 };
    static cilist io___32 = { 0, 0, 0, fmt_1010, 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Attempts to improve on a good local minimum by finding a */
/* C        better one.   The quadratic part of FCN is removed by MNCALF 
*/
/* C        and this transformed function is minimized using the simplex 
*/
/* C        method from several random starting points. */
/* C        ref. -- Goldstein and Price, Math.Comp. 25, 569 (1971) */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    if (mn7npr_1.npar <= 0) {
	return 0;
    }
    if (mn7min_1.amin == mn7cns_1.undefi) {
	mnamin_(fcn, futil);
    }
    s_copy(mn7tit_1.cstatu, "UNCHANGED ", 10L, 10L);
    mn7cnv_1.itaur = 1;
    mn7min_1.epsi = mn7min_1.up * (float).1;
    npfn = mn7cnv_1.nfcn;
    nloop = (integer) mn7arg_1.word7[1];
    if (nloop <= 0) {
	nloop = mn7npr_1.npar + 4;
    }
    nparx = mn7npr_1.npar;
    nparp1 = mn7npr_1.npar + 1;
    wg = (float)1. / (real) mn7npr_1.npar;
    sigsav = mn7min_1.edm;
    mn7min_1.apsi = mn7min_1.amin;
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	mn7int_1.xt[i - 1] = mn7int_1.x[i - 1];
	dsav[i - 1] = mn7err_1.werr[i - 1];
	i__2 = i;
	for (j = 1; j <= i__2; ++j) {
	    ndex = i * (i - 1) / 2 + j;
	    mn7sim_1.p[i + j * 50 - 51] = mn7var_1.vhmat[ndex - 1];
/* L2: */
	    mn7sim_1.p[j + i * 50 - 51] = mn7sim_1.p[i + j * 50 - 51];
	}
    }
    mnvert_(mn7sim_1.p, &mn7npr_1.maxint, &mn7npr_1.maxint, &mn7npr_1.npar, &
	    ifail);
    if (ifail >= 1) {
	goto L280;
    }
/*               Save inverted matrix in VT */
    i__2 = mn7npr_1.npar;
    for (i = 1; i <= i__2; ++i) {
	ndex = i * (i - 1) / 2;
	i__1 = i;
	for (j = 1; j <= i__1; ++j) {
	    ++ndex;
/* L12: */
	    mn7vat_1.vthmat[ndex - 1] = mn7sim_1.p[i + j * 50 - 51];
	}
    }
    loop = 0;

L20:
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	mn7int_1.dirin[i - 1] = dsav[i - 1] * (float)2.;
	mnrn15_(&rnum, &iseed);
/* L25: */
	mn7int_1.x[i - 1] = mn7int_1.xt[i - 1] + mn7int_1.dirin[i - 1] * (
		float)2. * (rnum - (float).5);
    }
    ++loop;
    reg = (float)2.;
    if (mn7flg_1.isw[4] >= 0) {
	io___16.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___16);
	do_fio(&c__1, (char *)&loop, (ftnlen)sizeof(integer));
	e_wsfe();
    }
L30:
    mncalf_(fcn, mn7int_1.x, &ycalf, futil);
    mn7min_1.amin = ycalf;
/*                                        . . . . set up  random simplex 
*/
    jl = nparp1;
    jh = nparp1;
    y[nparp1 - 1] = mn7min_1.amin;
    amax = mn7min_1.amin;
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	xi = mn7int_1.x[i - 1];
	mnrn15_(&rnum, &iseed);
	mn7int_1.x[i - 1] = xi - mn7int_1.dirin[i - 1] * (rnum - (float).5);
	mncalf_(fcn, mn7int_1.x, &ycalf, futil);
	y[i - 1] = ycalf;
	if (y[i - 1] < mn7min_1.amin) {
	    mn7min_1.amin = y[i - 1];
	    jl = i;
	} else if (y[i - 1] > amax) {
	    amax = y[i - 1];
	    jh = i;
	}
	i__2 = mn7npr_1.npar;
	for (j = 1; j <= i__2; ++j) {
/* L40: */
	    mn7sim_1.p[j + i * 50 - 51] = mn7int_1.x[j - 1];
	}
	mn7sim_1.p[i + nparp1 * 50 - 51] = xi;
	mn7int_1.x[i - 1] = xi;
/* L45: */
    }

    mn7min_1.edm = mn7min_1.amin;
    sig2 = mn7min_1.edm;
/*                                        . . . . . . .  start main loop 
*/
L50:
    if (mn7min_1.amin < 0.) {
	goto L95;
    }
    if (mn7flg_1.isw[1] <= 2) {
	goto L280;
    }
    ep = mn7min_1.amin * (float).1;
    if (sig2 < ep && mn7min_1.edm < ep) {
	goto L100;
    }
    sig2 = mn7min_1.edm;
    if (mn7cnv_1.nfcn - npfn > mn7cnv_1.nfcnmx) {
	goto L300;
    }
/*         calculate new point * by reflection */
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	pb = (float)0.;
	i__2 = nparp1;
	for (j = 1; j <= i__2; ++j) {
/* L59: */
	    pb += wg * mn7sim_1.p[i + j * 50 - 51];
	}
	mn7sim_1.pbar[i - 1] = pb - wg * mn7sim_1.p[i + jh * 50 - 51];
/* L60: */
	mn7sim_1.pstar[i - 1] = mn7sim_1.pbar[i - 1] * 2. - mn7sim_1.p[i + jh 
		* 50 - 51] * 1.;
    }
    mncalf_(fcn, mn7sim_1.pstar, &ycalf, futil);
    ystar = ycalf;
    if (ystar >= mn7min_1.amin) {
	goto L70;
    }
/*         point * better than jl, calculate new point ** */
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
/* L61: */
	mn7sim_1.pstst[i - 1] = mn7sim_1.pstar[i - 1] * 2. + mn7sim_1.pbar[i 
		- 1] * -1.;
    }
    mncalf_(fcn, mn7sim_1.pstst, &ycalf, futil);
    ystst = ycalf;
/* L66: */
    if (ystst < y[jl - 1]) {
	goto L67;
    }
    mnrazz_(&ystar, mn7sim_1.pstar, y, &jh, &jl);
    goto L50;
L67:
    mnrazz_(&ystst, mn7sim_1.pstst, y, &jh, &jl);
    goto L50;
/*         point * is not as good as jl */
L70:
    if (ystar >= y[jh - 1]) {
	goto L73;
    }
    jhold = jh;
    mnrazz_(&ystar, mn7sim_1.pstar, y, &jh, &jl);
    if (jhold != jh) {
	goto L50;
    }
/*         calculate new point ** */
L73:
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
/* L74: */
	mn7sim_1.pstst[i - 1] = mn7sim_1.p[i + jh * 50 - 51] * .5 + 
		mn7sim_1.pbar[i - 1] * .5;
    }
    mncalf_(fcn, mn7sim_1.pstst, &ycalf, futil);
    ystst = ycalf;
    if (ystst > y[jh - 1]) {
	goto L30;
    }
/*     point ** is better than jh */
    if (ystst < mn7min_1.amin) {
	goto L67;
    }
    mnrazz_(&ystst, mn7sim_1.pstst, y, &jh, &jl);
    goto L50;
/*                                        . . . . . .  end main loop */
L95:
    if (mn7flg_1.isw[4] >= 0) {
	io___29.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___29);
	e_wsfe();
    }
    reg = (float).1;
/*                                        . . . . . ask if point is new */
L100:
    mninex_(mn7int_1.x);
    (*fcn)(&nparx, mn7der_1.gin, &mn7min_1.amin, mn7ext_1.u, &c__4, futil);
    ++mn7cnv_1.nfcn;
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	mn7int_1.dirin[i - 1] = reg * dsav[i - 1];
	if ((d__1 = mn7int_1.x[i - 1] - mn7int_1.xt[i - 1], abs(d__1)) > 
		mn7int_1.dirin[i - 1]) {
	    goto L150;
	}
/* L120: */
    }
    goto L230;
L150:
    mn7cnv_1.nfcnmx = mn7cnv_1.nfcnmx + npfn - mn7cnv_1.nfcn;
    npfn = mn7cnv_1.nfcn;
    mnsimp_(fcn, futil);
    if (mn7min_1.amin >= mn7min_1.apsi) {
	goto L325;
    }
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	mn7int_1.dirin[i - 1] = dsav[i - 1] * (float).1;
	if ((d__1 = mn7int_1.x[i - 1] - mn7int_1.xt[i - 1], abs(d__1)) > 
		mn7int_1.dirin[i - 1]) {
	    goto L250;
	}
/* L220: */
    }
L230:
    if (mn7min_1.amin < mn7min_1.apsi) {
	goto L350;
    }
    goto L325;
/*                                        . . . . . . truly new minimum */
L250:
    mn7log_1.lnewmn = TRUE_;
    if (mn7flg_1.isw[1] >= 1) {
	mn7flg_1.isw[1] = 1;
	mn7min_1.dcovar = max(mn7min_1.dcovar,.5);
    } else {
	mn7min_1.dcovar = (float)1.;
    }
    mn7cnv_1.itaur = 0;
    mn7cnv_1.nfcnmx = mn7cnv_1.nfcnmx + npfn - mn7cnv_1.nfcn;
    s_copy(mn7tit_1.cstatu, "NEW MINIMU", 10L, 10L);
    if (mn7flg_1.isw[4] >= 0) {
	io___30.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___30);
	e_wsfe();
    }
    return 0;
/*                                        . . . return to previous region 
*/
L280:
    if (mn7flg_1.isw[4] > 0) {
	io___31.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___31);
	e_wsfe();
    }
    goto L325;
L300:
    mn7flg_1.isw[0] = 1;
L325:
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	mn7int_1.dirin[i - 1] = dsav[i - 1] * (float).01;
/* L330: */
	mn7int_1.x[i - 1] = mn7int_1.xt[i - 1];
    }
    mn7min_1.amin = mn7min_1.apsi;
    mn7min_1.edm = sigsav;
L350:
    mninex_(mn7int_1.x);
    if (mn7flg_1.isw[4] > 0) {
	io___32.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___32);
	e_wsfe();
    }
    s_copy(mn7tit_1.cstatu, "UNCHANGED ", 10L, 10L);
    mnrset_(&c__0);
    if (mn7flg_1.isw[1] < 2) {
	goto L380;
    }
    if (loop < nloop && mn7flg_1.isw[0] < 1) {
	goto L20;
    }
L380:
    mnprin_(&c__5, &mn7min_1.amin);
    mn7cnv_1.itaur = 0;
    return 0;
} /* mnimpr_ */

