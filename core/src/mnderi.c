/* mnderi.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__4 = 4;
static integer c__1 = 1;
static integer c__2 = 2;


/* $Id: mnderi.F,v 1.2 1996/03/15 18:02:43 james Exp $ */

/* $Log: mnderi.F,v $ */
/* Revision 1.2  1996/03/15 18:02:43  james */
/*     Modified Files: */
/* mnderi.F eliminate possible division by zero */
/* mnexcm.F suppress print on STOP when print flag=-1 */
/*          set FVAL3 to flag if FCN already called with IFLAG=3 */
/* mninit.F set version 96.03 */
/* mnlims.F remove arguments, not needed */
/* mnmigr.F VLEN -> LENV in debug print statement */
/* mnparm.F move call to MNRSET to after NPAR redefined, to zero all */
/* mnpsdf.F eliminate possible division by zero */
/* mnscan.F suppress printout when print flag =-1 */
/* mnset.F  remove arguments in call to MNLIMS */
/* mnsimp.F fix CSTATU so status is PROGRESS only if new minimum */
/* mnvert.F eliminate possible division by zero */

/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mnderi_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    /* Format strings */
    static char fmt_41[] = "(i4,2g11.3,5g10.2)";

    /* System generated locals */
    address a__1[2];
    integer i__1[2], i__2, i__3;
    doublereal d__1, d__2, d__3;
    char ch__1[48], ch__2[54];

    /* Builtin functions */
    integer s_wsfi(), do_fio(), e_wsfi();
    /* Subroutine */ int s_cat();
    integer s_wsfe(), e_wsfe();
    double sqrt(), d_sign(), cos();

    /* Local variables */
    static integer icyc, ncyc, iint, iext;
    static doublereal step;
    static integer i;
    static doublereal dfmin;
    static integer nparx;
    static doublereal stepb4, dd, df;
    static logical ldebug;
    extern /* Subroutine */ int mnamin_(), mninex_();
    static doublereal fs1;
    extern /* Subroutine */ int mnwarn_();
    static doublereal tlrstp, tlrgrd, epspri, vrysml, optstp, stpmax, stpmin, 
	    fs2, grbfor, d1d2, xtf;
    static char cbf1[22];

    /* Fortran I/O blocks */
    static icilist io___6 = { 0, cbf1, 0, "(G12.3)", 12, 1 };
    static cilist io___7 = { 0, 0, 0, "(/'  FIRST DERIVATIVE DEBUG PRINTOUT.\
  MNDERI'/        ' PAR    DERIV     STEP      MINSTEP   OPTSTEP ',         \
      ' D1-D2    2ND DRV')", 0 };
    static cilist io___25 = { 0, 0, 0, fmt_41, 0 };
    static icilist io___26 = { 0, cbf1, 0, "(2E11.3)", 22, 1 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        Calculates the first derivatives of FCN (GRD), */
/* C        either by finite differences or by transforming the user- */
/* C        supplied derivatives to internal coordinates, */
/* C        according to whether ISW(3) is zero or one. */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    nparx = mn7npr_1.npar;
    ldebug = mn7flg_1.idbg[2] >= 1;
    if (mn7min_1.amin == mn7cns_1.undefi) {
	mnamin_(fcn, futil);
    }
    if (mn7flg_1.isw[2] == 1) {
	goto L100;
    }
    if (ldebug) {
/*                       make sure starting at the right place */
	mninex_(mn7int_1.x);
	nparx = mn7npr_1.npar;
	(*fcn)(&nparx, mn7der_1.gin, &fs1, mn7ext_1.u, &c__4, futil);
	++mn7cnv_1.nfcn;
	if (fs1 != mn7min_1.amin) {
	    df = mn7min_1.amin - fs1;
	    s_wsfi(&io___6);
	    do_fio(&c__1, (char *)&df, (ftnlen)sizeof(doublereal));
	    e_wsfi();
/* Writing concatenation */
	    i__1[0] = 36, a__1[0] = "function value differs from AMIN by ";
	    i__1[1] = 12, a__1[1] = cbf1;
	    s_cat(ch__1, a__1, i__1, &c__2, 48L);
	    mnwarn_("D", "MNDERI", ch__1, 1L, 6L, 48L);
	    mn7min_1.amin = fs1;
	}
	io___7.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___7);
	e_wsfe();
    }
    dfmin = mn7cns_1.epsma2 * (float)8. * (abs(mn7min_1.amin) + mn7min_1.up);
/* Computing 2nd power */
    d__1 = mn7cns_1.epsmac;
    vrysml = d__1 * d__1 * (float)8.;
    if (mn7cnv_1.istrat <= 0) {
	ncyc = 2;
	tlrstp = (float).5;
	tlrgrd = (float).1;
    } else if (mn7cnv_1.istrat == 1) {
	ncyc = 3;
	tlrstp = (float).3;
	tlrgrd = (float).05;
    } else {
	ncyc = 5;
	tlrstp = (float).1;
	tlrgrd = (float).02;
    }
/*                                loop over variable parameters */
    i__2 = mn7npr_1.npar;
    for (i = 1; i <= i__2; ++i) {
	epspri = mn7cns_1.epsma2 + (d__1 = mn7der_1.grd[i - 1] * 
		mn7cns_1.epsma2, abs(d__1));
/*         two-point derivatives always assumed necessary */
/*         maximum number of cycles over step size depends on strategy
 */
	xtf = mn7int_1.x[i - 1];
	stepb4 = (float)0.;
/*                               loop as little as possible here! */
	i__3 = ncyc;
	for (icyc = 1; icyc <= i__3; ++icyc) {
/*                 ........ theoretically best step */
	    optstp = sqrt(dfmin / ((d__1 = mn7der_1.g2[i - 1], abs(d__1)) + 
		    epspri));
/*                     step cannot decrease by more than a factor 
of ten */
/* Computing MAX */
	    d__2 = optstp, d__3 = (d__1 = mn7der_1.gstep[i - 1] * (float).1, 
		    abs(d__1));
	    step = max(d__2,d__3);
/*                 but if parameter has limits, max step size = 0.
5 */
	    if (mn7der_1.gstep[i - 1] < 0. && step > (float).5) {
		step = (float).5;
	    }
/*                 and not more than ten times the previous step 
*/
	    stpmax = (d__1 = mn7der_1.gstep[i - 1], abs(d__1)) * (float)10.;
	    if (step > stpmax) {
		step = stpmax;
	    }
/*                 minimum step size allowed by machine precision 
*/
/* Computing MAX */
	    d__2 = vrysml, d__3 = (d__1 = mn7cns_1.epsma2 * mn7int_1.x[i - 1],
		     abs(d__1)) * (float)8.;
	    stpmin = max(d__2,d__3);
	    if (step < stpmin) {
		step = stpmin;
	    }
/*                 end of iterations if step change less than fact
or 2 */
	    if ((d__1 = (step - stepb4) / step, abs(d__1)) < tlrstp) {
		goto L50;
	    }
/*         take step positive */
	    mn7der_1.gstep[i - 1] = d_sign(&step, &mn7der_1.gstep[i - 1]);
	    stepb4 = step;
	    mn7int_1.x[i - 1] = xtf + step;
	    mninex_(mn7int_1.x);
	    (*fcn)(&nparx, mn7der_1.gin, &fs1, mn7ext_1.u, &c__4, futil);
	    ++mn7cnv_1.nfcn;
/*         take step negative */
	    mn7int_1.x[i - 1] = xtf - step;
	    mninex_(mn7int_1.x);
	    (*fcn)(&nparx, mn7der_1.gin, &fs2, mn7ext_1.u, &c__4, futil);
	    ++mn7cnv_1.nfcn;
	    grbfor = mn7der_1.grd[i - 1];
	    mn7der_1.grd[i - 1] = (fs1 - fs2) / (step * (float)2.);
/* Computing 2nd power */
	    d__1 = step;
	    mn7der_1.g2[i - 1] = (fs1 + fs2 - mn7min_1.amin * (float)2.) / (
		    d__1 * d__1);
	    mn7int_1.x[i - 1] = xtf;
	    if (ldebug) {
		d1d2 = (fs1 + fs2 - mn7min_1.amin * (float)2.) / step;
		io___25.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___25);
		do_fio(&c__1, (char *)&i, (ftnlen)sizeof(integer));
		do_fio(&c__1, (char *)&mn7der_1.grd[i - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&step, (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&stpmin, (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&optstp, (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&d1d2, (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&mn7der_1.g2[i - 1], (ftnlen)sizeof(
			doublereal));
		e_wsfe();
	    }
/*         see if another iteration is necessary */
	    if ((d__1 = grbfor - mn7der_1.grd[i - 1], abs(d__1)) / ((d__2 = 
		    mn7der_1.grd[i - 1], abs(d__2)) + dfmin / step) < tlrgrd) 
		    {
		goto L50;
	    }
/* L45: */
	}
/*                           end of ICYC loop. too many iterations */
	if (ncyc == 1) {
	    goto L50;
	}
	s_wsfi(&io___26);
	do_fio(&c__1, (char *)&mn7der_1.grd[i - 1], (ftnlen)sizeof(doublereal)
		);
	do_fio(&c__1, (char *)&grbfor, (ftnlen)sizeof(doublereal));
	e_wsfi();
/* Writing concatenation */
	i__1[0] = 32, a__1[0] = "First derivative not converged. ";
	i__1[1] = 22, a__1[1] = cbf1;
	s_cat(ch__2, a__1, i__1, &c__2, 54L);
	mnwarn_("D", "MNDERI", ch__2, 1L, 6L, 54L);
L50:

/* L60: */
	;
    }
    mninex_(mn7int_1.x);
    return 0;
/*                                        .  derivatives calc by fcn */
L100:
    i__2 = mn7npr_1.npar;
    for (iint = 1; iint <= i__2; ++iint) {
	iext = mn7inx_1.nexofi[iint - 1];
	if (mn7inx_1.nvarl[iext - 1] > 1) {
	    goto L120;
	}
	mn7der_1.grd[iint - 1] = mn7der_1.gin[iext - 1];
	goto L150;
L120:
	dd = (mn7ext_1.blim[iext - 1] - mn7ext_1.alim[iext - 1]) * (float).5 *
		 cos(mn7int_1.x[iint - 1]);
	mn7der_1.grd[iint - 1] = mn7der_1.gin[iext - 1] * dd;
L150:
	;
    }
/* L200: */
    return 0;
} /* mnderi_ */

