/* mnerrs.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_


/* $Id: mnerrs.F,v 1.1.1.1 1996/03/07 14:31:29 mclareni Exp $ */

/* $Log: mnerrs.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mnerrs_(number, eplus, eminus, eparab, gcc)
integer *number;
doublereal *eplus, *eminus, *eparab, *gcc;
{
    /* System generated locals */
    doublereal d__1, d__2;

    /* Builtin functions */
    double sqrt();

    /* Local variables */
    static doublereal dxdi;
    static integer ndiag;
    extern /* Subroutine */ int mndxdi_();
    static integer iin, iex;


/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C    Called by user, utility routine to get MINOS errors */
/* C    If NUMBER is positive, then it is external parameter number, */
/* C                  if negative, it is -internal number. */
/* C    values returned by MNERRS: */
/* C       EPLUS, EMINUS are MINOS errors of parameter NUMBER, */
/* C       EPARAB is 'parabolic' error (from error matrix). */
/* C                 (Errors not calculated are set = 0.) */
/* C       GCC is global correlation coefficient from error matrix */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */



    iex = *number;
    if (*number < 0) {
	iin = -(*number);
	if (iin > mn7npr_1.npar) {
	    goto L900;
	}
	iex = mn7inx_1.nexofi[iin - 1];
    }
    if (iex > mn7npr_1.nu || iex <= 0) {
	goto L900;
    }
    iin = mn7inx_1.niofex[iex - 1];
    if (iin <= 0) {
	goto L900;
    }
/*             IEX is external number, IIN is internal number */
    *eplus = mn7err_1.erp[iin - 1];
    if (*eplus == mn7cns_1.undefi) {
	*eplus = (float)0.;
    }
    *eminus = mn7err_1.ern[iin - 1];
    if (*eminus == mn7cns_1.undefi) {
	*eminus = (float)0.;
    }
    mndxdi_(&mn7int_1.x[iin - 1], &iin, &dxdi);
    ndiag = iin * (iin + 1) / 2;
    *eparab = (d__2 = dxdi * sqrt((d__1 = mn7min_1.up * mn7var_1.vhmat[ndiag 
	    - 1], abs(d__1))), abs(d__2));
/*              global correlation coefficient */
    *gcc = (float)0.;
    if (mn7flg_1.isw[1] < 2) {
	goto L990;
    }
    *gcc = mn7err_1.globcc[iin - 1];
    goto L990;
/*                  ERROR.  parameter number not valid */
L900:
    *eplus = (float)0.;
    *eminus = (float)0.;
    *eparab = (float)0.;
    *gcc = (float)0.;
L990:
    return 0;
} /* mnerrs_ */

