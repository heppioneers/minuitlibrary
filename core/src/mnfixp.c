/* mnfixp.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__50 = 50;


/* $Id: mnfixp.F,v 1.1.1.1 1996/03/07 14:31:29 mclareni Exp $ */

/* $Log: mnfixp.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mnfixp_(iint, ierr)
integer *iint, *ierr;
{
    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();

    /* Local variables */
    static integer kold, nold, ndex, knew, iext, i, j, m, n, lc, ik;
    static doublereal yy[50], yyover;

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 0, 0, "(A,I4)", 0 };
    static cilist io___3 = { 0, 0, 0, "(A,I4,A,I4)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        removes parameter IINT from the internal (variable) parameter 
*/
/* C        list, and arranges the rest of the list to fill the hole. */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


/*                           first see if it can be done */
    *ierr = 0;
    if (*iint > mn7npr_1.npar || *iint <= 0) {
	*ierr = 1;
	io___1.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___1);
	do_fio(&c__1, " MINUIT ERROR.  ARGUMENT TO MNFIXP=", 35L);
	do_fio(&c__1, (char *)&(*iint), (ftnlen)sizeof(integer));
	e_wsfe();
	goto L300;
    }
    iext = mn7inx_1.nexofi[*iint - 1];
    if (mn7fx1_1.npfix >= 50) {
	*ierr = 1;
	io___3.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___3);
	do_fio(&c__1, " MINUIT CANNOT FIX PARAMETER", 28L);
	do_fio(&c__1, (char *)&iext, (ftnlen)sizeof(integer));
	do_fio(&c__1, " MAXIMUM NUMBER THAT CAN BE FIXED IS", 36L);
	do_fio(&c__1, (char *)&c__50, (ftnlen)sizeof(integer));
	e_wsfe();
	goto L300;
    }
/*                           reduce number of variable parameters by one 
*/
    mn7inx_1.niofex[iext - 1] = 0;
    nold = mn7npr_1.npar;
    --mn7npr_1.npar;
/*                       save values in case parameter is later restored 
*/
    ++mn7fx1_1.npfix;
    mn7fx1_1.ipfix[mn7fx1_1.npfix - 1] = iext;
    lc = *iint;
    mn7fx2_1.xs[mn7fx1_1.npfix - 1] = mn7int_1.x[lc - 1];
    mn7fx2_1.xts[mn7fx1_1.npfix - 1] = mn7int_1.xt[lc - 1];
    mn7fx2_1.dirins[mn7fx1_1.npfix - 1] = mn7err_1.werr[lc - 1];
    mn7fx3_1.grds[mn7fx1_1.npfix - 1] = mn7der_1.grd[lc - 1];
    mn7fx3_1.g2s[mn7fx1_1.npfix - 1] = mn7der_1.g2[lc - 1];
    mn7fx3_1.gsteps[mn7fx1_1.npfix - 1] = mn7der_1.gstep[lc - 1];
/*                        shift values for other parameters to fill hole 
*/
    i__1 = mn7npr_1.nu;
    for (ik = iext + 1; ik <= i__1; ++ik) {
	if (mn7inx_1.niofex[ik - 1] > 0) {
	    lc = mn7inx_1.niofex[ik - 1] - 1;
	    mn7inx_1.niofex[ik - 1] = lc;
	    mn7inx_1.nexofi[lc - 1] = ik;
	    mn7int_1.x[lc - 1] = mn7int_1.x[lc];
	    mn7int_1.xt[lc - 1] = mn7int_1.xt[lc];
	    mn7int_1.dirin[lc - 1] = mn7int_1.dirin[lc];
	    mn7err_1.werr[lc - 1] = mn7err_1.werr[lc];
	    mn7der_1.grd[lc - 1] = mn7der_1.grd[lc];
	    mn7der_1.g2[lc - 1] = mn7der_1.g2[lc];
	    mn7der_1.gstep[lc - 1] = mn7der_1.gstep[lc];
	}
/* L100: */
    }
    if (mn7flg_1.isw[1] <= 0) {
	goto L300;
    }
/*                    remove one row and one column from variance matrix 
*/
    if (mn7npr_1.npar <= 0) {
	goto L300;
    }
    i__1 = nold;
    for (i = 1; i <= i__1; ++i) {
	m = max(i,*iint);
	n = min(i,*iint);
	ndex = m * (m - 1) / 2 + n;
/* L260: */
	yy[i - 1] = mn7var_1.vhmat[ndex - 1];
    }
    yyover = (float)1. / yy[*iint - 1];
    knew = 0;
    kold = 0;
    i__1 = nold;
    for (i = 1; i <= i__1; ++i) {
	i__2 = i;
	for (j = 1; j <= i__2; ++j) {
	    ++kold;
	    if (j == *iint || i == *iint) {
		goto L292;
	    }
	    ++knew;
	    mn7var_1.vhmat[knew - 1] = mn7var_1.vhmat[kold - 1] - yy[j - 1] * 
		    yy[i - 1] * yyover;
L292:
	    ;
	}
/* L294: */
    }
L300:
    return 0;
} /* mnfixp_ */

