/* mnpsdf.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__1 = 1;
static integer c__3 = 3;
static integer c__2 = 2;


/* $Id: mnpsdf.F,v 1.2 1996/03/15 18:02:50 james Exp $ */

/* $Log: mnpsdf.F,v $ */
/* Revision 1.2  1996/03/15 18:02:50  james */
/*     Modified Files: */
/* mnderi.F eliminate possible division by zero */
/* mnexcm.F suppress print on STOP when print flag=-1 */
/*          set FVAL3 to flag if FCN already called with IFLAG=3 */
/* mninit.F set version 96.03 */
/* mnlims.F remove arguments, not needed */
/* mnmigr.F VLEN -> LENV in debug print statement */
/* mnparm.F move call to MNRSET to after NPAR redefined, to zero all */
/* mnpsdf.F eliminate possible division by zero */
/* mnscan.F suppress printout when print flag =-1 */
/* mnset.F  remove arguments in call to MNLIMS */
/* mnsimp.F fix CSTATU so status is PROGRESS only if new minimum */
/* mnvert.F eliminate possible division by zero */

/* Revision 1.1.1.1  1996/03/07 14:31:31  mclareni */
/* Minuit */


/* Subroutine */ int mnpsdf_()
{
    /* Format strings */
    static char fmt_550[] = "(\002 EIGENVALUES OF SECOND-DERIVATIVE MATRIX\
:\002)";
    static char fmt_551[] = "(7x,6e12.4)";

    /* System generated locals */
    address a__1[3], a__2[2];
    integer i__1, i__2[3], i__3[2], i__4;
    doublereal d__1;
    char ch__1[44], ch__2[46], ch__3[57];

    /* Builtin functions */
    integer s_wsfi(), do_fio(), e_wsfi();
    /* Subroutine */ int s_cat();
    double sqrt();
    integer s_wsfe(), e_wsfe();
    /* Subroutine */ int s_copy();

    /* Local variables */
    static doublereal padd;
    static integer ndex;
    static doublereal pmin, pmax;
    static integer i, j;
    static doublereal s[50], dgmin;
    extern /* Subroutine */ int mneig_();
    static integer ndexd;
    static doublereal dg;
    static integer ip;
    static char chbuff[12];
    static doublereal epspdf;
    static integer ifault;
    static doublereal epsmin;
    extern /* Subroutine */ int mnwarn_();

    /* Fortran I/O blocks */
    static icilist io___7 = { 0, chbuff, 0, "(I3)", 3, 1 };
    static icilist io___9 = { 0, chbuff, 0, "(E12.2)", 12, 1 };
    static cilist io___17 = { 0, 0, 0, fmt_550, 0 };
    static cilist io___18 = { 0, 0, 0, fmt_551, 0 };
    static icilist io___20 = { 0, chbuff, 0, "(G12.5)", 12, 1 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C        calculates the eigenvalues of v to see if positive-def. */
/* C        if not, adds constant along diagonal to make positive. */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    epsmin = (float)1e-6;
    epspdf = max(epsmin,mn7cns_1.epsma2);
    dgmin = mn7var_1.vhmat[0];
/*                        Check if negative or zero on diagonal */
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	ndex = i * (i + 1) / 2;
	if (mn7var_1.vhmat[ndex - 1] <= 0.) {
	    s_wsfi(&io___7);
	    do_fio(&c__1, (char *)&i, (ftnlen)sizeof(integer));
	    e_wsfi();
/* Writing concatenation */
	    i__2[0] = 25, a__1[0] = "Negative diagonal element";
	    i__2[1] = 3, a__1[1] = chbuff;
	    i__2[2] = 16, a__1[2] = " in Error Matrix";
	    s_cat(ch__1, a__1, i__2, &c__3, 44L);
	    mnwarn_("W", mn7tit_1.cfrom, ch__1, 1L, 8L, 44L);
	}
	if (mn7var_1.vhmat[ndex - 1] < dgmin) {
	    dgmin = mn7var_1.vhmat[ndex - 1];
	}
/* L200: */
    }
    if (dgmin <= 0.) {
	dg = epspdf + 1. - dgmin;
	s_wsfi(&io___9);
	do_fio(&c__1, (char *)&dg, (ftnlen)sizeof(doublereal));
	e_wsfi();
/* Writing concatenation */
	i__3[0] = 12, a__2[0] = chbuff;
	i__3[1] = 34, a__2[1] = " added to diagonal of error matrix";
	s_cat(ch__2, a__2, i__3, &c__2, 46L);
	mnwarn_("W", mn7tit_1.cfrom, ch__2, 1L, 8L, 46L);
    } else {
	dg = 0.;
    }
/*                    Store VHMAT in P, make sure diagonal pos. */
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	ndex = i * (i - 1) / 2;
	ndexd = ndex + i;
	mn7var_1.vhmat[ndexd - 1] += dg;
	if (mn7var_1.vhmat[ndexd - 1] <= 0.) {
	    mn7var_1.vhmat[ndexd - 1] = (float)1.;
	}
	s[i - 1] = (float)1. / sqrt(mn7var_1.vhmat[ndexd - 1]);
	i__4 = i;
	for (j = 1; j <= i__4; ++j) {
	    ++ndex;
/* L213: */
	    mn7sim_1.p[i + j * 50 - 51] = mn7var_1.vhmat[ndex - 1] * s[i - 1] 
		    * s[j - 1];
	}
    }
/*      call eigen (p,p,maxint,npar,pstar,-npar) */
    mneig_(mn7sim_1.p, &mn7npr_1.maxint, &mn7npr_1.npar, &mn7npr_1.maxint, 
	    mn7sim_1.pstar, &epspdf, &ifault);
    pmin = mn7sim_1.pstar[0];
    pmax = mn7sim_1.pstar[0];
    i__4 = mn7npr_1.npar;
    for (ip = 2; ip <= i__4; ++ip) {
	if (mn7sim_1.pstar[ip - 1] < pmin) {
	    pmin = mn7sim_1.pstar[ip - 1];
	}
	if (mn7sim_1.pstar[ip - 1] > pmax) {
	    pmax = mn7sim_1.pstar[ip - 1];
	}
/* L215: */
    }
/* Computing MAX */
    d__1 = abs(pmax);
    pmax = max(d__1,1.);
    if (pmin <= 0. && mn7log_1.lwarn || mn7flg_1.isw[4] >= 2) {
	io___17.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___17);
	e_wsfe();
	io___18.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___18);
	i__4 = mn7npr_1.npar;
	for (ip = 1; ip <= i__4; ++ip) {
	    do_fio(&c__1, (char *)&mn7sim_1.pstar[ip - 1], (ftnlen)sizeof(
		    doublereal));
	}
	e_wsfe();
    }
    if (pmin > epspdf * pmax) {
	goto L217;
    }
    if (mn7flg_1.isw[1] == 3) {
	mn7flg_1.isw[1] = 2;
    }
    padd = pmax * (float).001 - pmin;
    i__4 = mn7npr_1.npar;
    for (ip = 1; ip <= i__4; ++ip) {
	ndex = ip * (ip + 1) / 2;
/* L216: */
	mn7var_1.vhmat[ndex - 1] *= padd + (float)1.;
    }
    s_copy(mn7tit_1.cstatu, "NOT POSDEF", 10L, 10L);
    s_wsfi(&io___20);
    do_fio(&c__1, (char *)&padd, (ftnlen)sizeof(doublereal));
    e_wsfi();
/* Writing concatenation */
    i__2[0] = 32, a__1[0] = "MATRIX FORCED POS-DEF BY ADDING ";
    i__2[1] = 12, a__1[1] = chbuff;
    i__2[2] = 13, a__1[2] = " TO DIAGONAL.";
    s_cat(ch__3, a__1, i__2, &c__3, 57L);
    mnwarn_("W", mn7tit_1.cfrom, ch__3, 1L, 8L, 57L);
L217:

    return 0;
} /* mnpsdf_ */

