/* mnhes1.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__4 = 4;
static integer c__1 = 1;
static integer c__2 = 2;


/* $Id: mnhes1.F,v 1.1.1.1 1996/03/07 14:31:30 mclareni Exp $ */

/* $Log: mnhes1.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:30  mclareni */
/* Minuit */


/* Subroutine */ int mnhes1_(fcn, futil)
/* Subroutine */ int (*fcn) (), (*futil) ();
{
    /* Format strings */
    static char fmt_11[] = "(i4,i2,6g12.5)";

    /* System generated locals */
    address a__1[2];
    integer i__1, i__2, i__3[2];
    doublereal d__1, d__2, d__3;
    char ch__1[48];

    /* Builtin functions */
    double sqrt();
    integer s_wsfe(), do_fio(), e_wsfe();
    double d_sign();
    integer s_wsfi(), e_wsfi();
    /* Subroutine */ int s_cat();

    /* Local variables */
    static doublereal dmin__;
    static integer icyc, ncyc, idrv;
    static doublereal d;
    static integer i;
    static doublereal dfmin, dgmin;
    static integer nparx;
    static doublereal change, chgold;
    static logical ldebug;
    static doublereal grdold, epspri;
    extern /* Subroutine */ int mninex_();
    static doublereal fs1, optstp, fs2, grdnew;
    extern /* Subroutine */ int mnwarn_();
    static doublereal sag, xtf;
    static char cbf1[22];

    /* Fortran I/O blocks */
    static cilist io___20 = { 0, 0, 0, fmt_11, 0 };
    static icilist io___23 = { 0, cbf1, 0, "(2G11.3)", 22, 1 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C      Called from MNHESS and MNGRAD */
/* C      Calculate first derivatives (GRD) and uncertainties (DGRD) */
/* C         and appropriate step sizes GSTEP */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


    ldebug = mn7flg_1.idbg[5] >= 1;
    if (mn7cnv_1.istrat <= 0) {
	ncyc = 1;
    }
    if (mn7cnv_1.istrat == 1) {
	ncyc = 2;
    }
    if (mn7cnv_1.istrat > 1) {
	ncyc = 6;
    }
    idrv = 1;
    nparx = mn7npr_1.npar;
    dfmin = mn7cns_1.epsma2 * (float)4. * (abs(mn7min_1.amin) + mn7min_1.up);
/*                                     main loop over parameters */
    i__1 = mn7npr_1.npar;
    for (i = 1; i <= i__1; ++i) {
	xtf = mn7int_1.x[i - 1];
	dmin__ = mn7cns_1.epsma2 * (float)4. * abs(xtf);
	epspri = mn7cns_1.epsma2 + (d__1 = mn7der_1.grd[i - 1] * 
		mn7cns_1.epsma2, abs(d__1));
	optstp = sqrt(dfmin / ((d__1 = mn7der_1.g2[i - 1], abs(d__1)) + 
		epspri));
	d = (d__1 = mn7der_1.gstep[i - 1], abs(d__1)) * (float).2;
	if (d > optstp) {
	    d = optstp;
	}
	if (d < dmin__) {
	    d = dmin__;
	}
	chgold = (float)1e4;
/*                                       iterate reducing step size */
	i__2 = ncyc;
	for (icyc = 1; icyc <= i__2; ++icyc) {
	    mn7int_1.x[i - 1] = xtf + d;
	    mninex_(mn7int_1.x);
	    (*fcn)(&nparx, mn7der_1.gin, &fs1, mn7ext_1.u, &c__4, futil);
	    ++mn7cnv_1.nfcn;
	    mn7int_1.x[i - 1] = xtf - d;
	    mninex_(mn7int_1.x);
	    (*fcn)(&nparx, mn7der_1.gin, &fs2, mn7ext_1.u, &c__4, futil);
	    ++mn7cnv_1.nfcn;
	    mn7int_1.x[i - 1] = xtf;
/*                                       check if step sizes appro
priate */
	    sag = (fs1 + fs2 - mn7min_1.amin * (float)2.) * (float).5;
	    grdold = mn7der_1.grd[i - 1];
	    grdnew = (fs1 - fs2) / (d * (float)2.);
	    dgmin = mn7cns_1.epsmac * (abs(fs1) + abs(fs2)) / d;
	    if (ldebug) {
		io___20.ciunit = mn7iou_1.isyswr;
		s_wsfe(&io___20);
		do_fio(&c__1, (char *)&i, (ftnlen)sizeof(integer));
		do_fio(&c__1, (char *)&idrv, (ftnlen)sizeof(integer));
		do_fio(&c__1, (char *)&mn7der_1.gstep[i - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&d, (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&mn7der_1.g2[i - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&grdnew, (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&sag, (ftnlen)sizeof(doublereal));
		e_wsfe();
	    }
	    if (grdnew == 0.) {
		goto L60;
	    }
	    change = (d__1 = (grdold - grdnew) / grdnew, abs(d__1));
	    if (change > chgold && icyc > 1) {
		goto L60;
	    }
	    chgold = change;
	    mn7der_1.grd[i - 1] = grdnew;
	    mn7der_1.gstep[i - 1] = d_sign(&d, &mn7der_1.gstep[i - 1]);
/*                  decrease step until first derivative changes b
y <5% */
	    if (change < (float).05) {
		goto L60;
	    }
	    if ((d__1 = grdold - grdnew, abs(d__1)) < dgmin) {
		goto L60;
	    }
	    if (d < dmin__) {
		mnwarn_("D", "MNHES1", "Step size too small for 1st drv.", 1L,
			 6L, 32L);
		goto L60;
	    }
	    d *= (float).2;
/* L50: */
	}
/*                                       loop satisfied = too many ite
r */
	s_wsfi(&io___23);
	do_fio(&c__1, (char *)&grdold, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&grdnew, (ftnlen)sizeof(doublereal));
	e_wsfi();
/* Writing concatenation */
	i__3[0] = 26, a__1[0] = "Too many iterations on D1.";
	i__3[1] = 22, a__1[1] = cbf1;
	s_cat(ch__1, a__1, i__3, &c__2, 48L);
	mnwarn_("D", "MNHES1", ch__1, 1L, 6L, 48L);
L60:
/* Computing MAX */
	d__2 = dgmin, d__3 = (d__1 = grdold - grdnew, abs(d__1));
	mn7der_1.dgrd[i - 1] = max(d__2,d__3);
/* L100: */
    }
/*                                        end of first deriv. loop */
    mninex_(mn7int_1.x);
    return 0;
} /* mnhes1_ */

