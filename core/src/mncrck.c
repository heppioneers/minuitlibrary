/* mncrck.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Table of constant values */

static integer c__1 = 1;


/* $Id: mncrck.F,v 1.1.1.1 1996/03/07 14:31:29 mclareni Exp $ */

/* $Log: mncrck.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:29  mclareni */
/* Minuit */


/* Subroutine */ int mncrck_(crdbuf, maxcwd, comand, lnc, mxp, plist, llist, 
	ierr, isyswr, crdbuf_len, comand_len)
char *crdbuf;
integer *maxcwd;
char *comand;
integer *lnc, *mxp;
doublereal *plist;
integer *llist, *ierr, *isyswr;
ftnlen crdbuf_len;
ftnlen comand_len;
{
    /* Initialized data */

    static char cnull[15+1] = ")NULL STRING   ";
    static char cnumer[13+1] = "123456789-.0+";

    /* Format strings */
    static char fmt_253[] = "(\002 MINUIT WARNING: INPUT DATA WORD TOO LONG\
.\002/\002     ORIGINAL:\002,a/\002 TRUNCATED TO:\002,a)";
    static char fmt_511[] = "(/\002 MINUIT WARNING IN MNCRCK: \002/\002 COMM\
AND HAS INPUT\002,i5,\002 NUMERIC FIELDS, BUT MINUIT CAN ACCEPT ONLY\002,i3)";

    /* System generated locals */
    integer i__1, i__2;
    icilist ici__1;

    /* Builtin functions */
    integer i_len();
    /* Subroutine */ int s_copy();
    integer s_wsfe(), do_fio(), e_wsfe(), s_cmp(), s_rsfi(), e_rsfi();

    /* Local variables */
    static integer ifld, iend, lend, left, nreq, ipos, kcmnd, nextb, ic, 
	    ibegin, ltoadd;
    static char celmnt[19*25];
    static integer ielmnt, lelmnt[25], nelmnt;

    /* Fortran I/O blocks */
    static cilist io___11 = { 0, 0, 0, fmt_253, 0 };
    static cilist io___19 = { 0, 0, 0, fmt_511, 0 };
    static cilist io___20 = { 0, 0, 0, "(A,A,A)", 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C */
/* C       Called from MNREAD. */
/* C       Cracks the free-format input, expecting zero or more */
/* C         alphanumeric fields (which it joins into COMAND(1:LNC)) */
/* C         followed by one or more numeric fields separated by */
/* C         blanks and/or one comma.  The numeric fields are put into */
/* C         the LLIST (but at most MXP) elements of PLIST. */
/* C      IERR = 0 if no errors, */
/* C           = 1 if error(s). */
/* C      Diagnostic messages are written to ISYSWR */
/* C */
    /* Parameter adjustments */
    --plist;

    /* Function Body */
    ielmnt = 0;
    lend = i_len(crdbuf, crdbuf_len);
    nextb = 1;
    *ierr = 0;
/*                                   . . . .  loop over words CELMNT */
L10:
    i__1 = lend;
    for (ipos = nextb; ipos <= i__1; ++ipos) {
	ibegin = ipos;
	if (*(unsigned char *)&crdbuf[ipos - 1] == ' ') {
	    goto L100;
	}
	if (*(unsigned char *)&crdbuf[ipos - 1] == ',') {
	    goto L250;
	}
	goto L150;
L100:
	;
    }
    goto L300;
L150:
/*               found beginning of word, look for end */
    i__1 = lend;
    for (ipos = ibegin + 1; ipos <= i__1; ++ipos) {
	if (*(unsigned char *)&crdbuf[ipos - 1] == ' ') {
	    goto L250;
	}
	if (*(unsigned char *)&crdbuf[ipos - 1] == ',') {
	    goto L250;
	}
/* L180: */
    }
    ipos = lend + 1;
L250:
    iend = ipos - 1;
    ++ielmnt;
    if (iend >= ibegin) {
	s_copy(celmnt + (ielmnt - 1) * 19, crdbuf + (ibegin - 1), 19L, iend - 
		(ibegin - 1));
    } else {
	s_copy(celmnt + (ielmnt - 1) * 19, cnull, 19L, 15L);
    }
    lelmnt[ielmnt - 1] = iend - ibegin + 1;
    if (lelmnt[ielmnt - 1] > 19) {
	io___11.ciunit = *isyswr;
	s_wsfe(&io___11);
	do_fio(&c__1, crdbuf + (ibegin - 1), iend - (ibegin - 1));
	do_fio(&c__1, celmnt + (ielmnt - 1) * 19, 19L);
	e_wsfe();
	lelmnt[ielmnt - 1] = 19;
    }
    if (ipos >= lend) {
	goto L300;
    }
    if (ielmnt >= 25) {
	goto L300;
    }
/*                     look for comma or beginning of next word */
    i__1 = lend;
    for (ipos = iend + 1; ipos <= i__1; ++ipos) {
	if (*(unsigned char *)&crdbuf[ipos - 1] == ' ') {
	    goto L280;
	}
	nextb = ipos;
	if (*(unsigned char *)&crdbuf[ipos - 1] == ',') {
	    nextb = ipos + 1;
	}
	goto L10;
L280:
	;
    }
/*                 All elements found, join the alphabetic ones to */
/*                                form a command */
L300:
    nelmnt = ielmnt;
    s_copy(comand, " ", comand_len, 1L);
    *lnc = 1;
    plist[1] = (float)0.;
    *llist = 0;
    if (ielmnt == 0) {
	goto L900;
    }
    kcmnd = 0;
    i__1 = nelmnt;
    for (ielmnt = 1; ielmnt <= i__1; ++ielmnt) {
	if (s_cmp(celmnt + (ielmnt - 1) * 19, cnull, 19L, 15L) == 0) {
	    goto L450;
	}
	for (ic = 1; ic <= 13; ++ic) {
	    if (*(unsigned char *)&celmnt[(ielmnt - 1) * 19] == *(unsigned 
		    char *)&cnumer[ic - 1]) {
		goto L450;
	    }
/* L350: */
	}
	if (kcmnd >= *maxcwd) {
	    goto L400;
	}
	left = *maxcwd - kcmnd;
	ltoadd = lelmnt[ielmnt - 1];
	if (ltoadd > left) {
	    ltoadd = left;
	}
	i__2 = kcmnd;
	s_copy(comand + i__2, celmnt + (ielmnt - 1) * 19, kcmnd + ltoadd - 
		i__2, ltoadd);
	kcmnd += ltoadd;
	if (kcmnd == *maxcwd) {
	    goto L400;
	}
	++kcmnd;
	*(unsigned char *)&comand[kcmnd - 1] = ' ';
L400:
	;
    }
    *lnc = kcmnd;
    goto L900;
L450:
    *lnc = kcmnd;
/*                      . . . .  we have come to a numeric field */
    *llist = 0;
    i__1 = nelmnt;
    for (ifld = ielmnt; ifld <= i__1; ++ifld) {
	++(*llist);
	if (*llist > *mxp) {
	    nreq = nelmnt - ielmnt + 1;
	    io___19.ciunit = *isyswr;
	    s_wsfe(&io___19);
	    do_fio(&c__1, (char *)&nreq, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&(*mxp), (ftnlen)sizeof(integer));
	    e_wsfe();
	    goto L900;
	}
	if (s_cmp(celmnt + (ifld - 1) * 19, cnull, 19L, 15L) == 0) {
	    plist[*llist] = (float)0.;
	} else {
	    ici__1.icierr = 1;
	    ici__1.iciend = 0;
	    ici__1.icirnum = 1;
	    ici__1.icirlen = 19;
	    ici__1.iciunit = celmnt + (ifld - 1) * 19;
	    ici__1.icifmt = "(BN,F19.0)";
	    i__2 = s_rsfi(&ici__1);
	    if (i__2 != 0) {
		goto L575;
	    }
	    i__2 = do_fio(&c__1, (char *)&plist[*llist], (ftnlen)sizeof(
		    doublereal));
	    if (i__2 != 0) {
		goto L575;
	    }
	    i__2 = e_rsfi();
	    if (i__2 != 0) {
		goto L575;
	    }
	}
	goto L600;
L575:
	io___20.ciunit = *isyswr;
	s_wsfe(&io___20);
	do_fio(&c__1, " FORMAT ERROR IN NUMERIC FIELD: \"", 33L);
	do_fio(&c__1, celmnt + (ifld - 1) * 19, lelmnt[ifld - 1]);
	do_fio(&c__1, "\"", 1L);
	e_wsfe();
	*ierr = 1;
	plist[*llist] = (float)0.;
L600:
	;
    }
/*                                  end loop over numeric fields */
L900:
    if (*lnc <= 0) {
	*lnc = 1;
    }
    return 0;
} /* mncrck_ */

