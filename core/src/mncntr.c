/* mncntr.f -- translated by f2c (version 19950110).
   You must link the resulting object file with the libraries:
	-lf2c -lm   (in that order)
*/

#include "f2c.h"

/* Common Block Declarations */

struct {
    char cpnam[1000];
} mn7nam_;

#define mn7nam_1 mn7nam_

struct {
    doublereal u[100], alim[100], blim[100];
} mn7ext_;

#define mn7ext_1 mn7ext_

struct {
    doublereal erp[50], ern[50], werr[50], globcc[50];
} mn7err_;

#define mn7err_1 mn7err_

struct {
    integer nvarl[100], niofex[100], nexofi[50];
} mn7inx_;

#define mn7inx_1 mn7inx_

struct {
    doublereal x[50], xt[50], dirin[50];
} mn7int_;

#define mn7int_1 mn7int_

struct {
    doublereal xs[50], xts[50], dirins[50];
} mn7fx2_;

#define mn7fx2_1 mn7fx2_

struct {
    doublereal grd[50], g2[50], gstep[50], gin[100], dgrd[50];
} mn7der_;

#define mn7der_1 mn7der_

struct {
    doublereal grds[50], g2s[50], gsteps[50];
} mn7fx3_;

#define mn7fx3_1 mn7fx3_

struct {
    integer ipfix[50], npfix;
} mn7fx1_;

#define mn7fx1_1 mn7fx1_

struct {
    doublereal vhmat[1275];
} mn7var_;

#define mn7var_1 mn7var_

struct {
    doublereal vthmat[1275];
} mn7vat_;

#define mn7vat_1 mn7vat_

struct {
    doublereal p[2550]	/* was [50][51] */, pstar[50], pstst[50], pbar[50], 
	    prho[50];
} mn7sim_;

#define mn7sim_1 mn7sim_

struct {
    integer maxint, npar, maxext, nu;
} mn7npr_;

#define mn7npr_1 mn7npr_

struct {
    integer isysrd, isyswr, isyssa, npagwd, npagln, newpag;
} mn7iou_;

#define mn7iou_1 mn7iou_

struct {
    integer istkrd[10], nstkrd, istkwr[10], nstkwr;
} mn7io2_;

#define mn7io2_1 mn7io2_

struct {
    char cfrom[8], cstatu[10], ctitl[50], cword[20], cundef[10], cvrsn[6], 
	    covmes[88];
} mn7tit_;

#define mn7tit_1 mn7tit_

struct {
    integer isw[7], idbg[11], nblock, icomnd;
} mn7flg_;

#define mn7flg_1 mn7flg_

struct {
    doublereal amin, up, edm, fval3, epsi, apsi, dcovar;
} mn7min_;

#define mn7min_1 mn7min_

struct {
    integer nfcn, nfcnmx, nfcnlc, nfcnfr, itaur, istrat, nwrmes[2];
} mn7cnv_;

#define mn7cnv_1 mn7cnv_

struct {
    doublereal word7[30];
} mn7arg_;

#define mn7arg_1 mn7arg_

struct {
    logical lwarn, lrepor, limset, lnolim, lnewmn, lphead;
} mn7log_;

#define mn7log_1 mn7log_

struct {
    doublereal epsmac, epsma2, vlimlo, vlimhi, undefi, bigedm, updflt;
} mn7cns_;

#define mn7cns_1 mn7cns_

struct {
    doublereal xpt[101], ypt[101];
} mn7rpt_;

#define mn7rpt_1 mn7rpt_

struct {
    char chpt[101];
} mn7cpt_;

#define mn7cpt_1 mn7cpt_

struct {
    doublereal xmidcr, ymidcr, xdircr, ydircr;
    integer ke1cr, ke2cr;
} mn7xcr_;

#define mn7xcr_1 mn7xcr_

/* Table of constant values */

static integer c__4 = 4;
static integer c__1 = 1;


/* $Id: mncntr.F,v 1.1.1.1 1996/03/07 14:31:28 mclareni Exp $ */

/* $Log: mncntr.F,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:28  mclareni */
/* Minuit */


/* Subroutine */ int mncntr_(fcn, ke1, ke2, ierrf, futil)
/* Subroutine */ int (*fcn) ();
integer *ke1, *ke2, *ierrf;
/* Subroutine */ int (*futil) ();
{
    /* Initialized data */

    static char clabel[20+1] = "0123456789ABCDEFGHIJ";

    /* Format strings */
    static char fmt_1351[] = "(\002 INVALID PARAMETER NUMBER(S) REQUESTED.  \
IGNORED.\002/)";

    /* System generated locals */
    integer i__1, i__2;
    real r__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    integer s_wsfe(), do_fio(), e_wsfe();
    /* Subroutine */ int s_copy();

    /* Local variables */
    static doublereal fcna[115], fcnb[115];
    static char chln[115];
    static doublereal devs, xsav, ysav;
    static integer i;
    static char chmid[115];
    static integer ngrid, ixmid;
    static doublereal bwidx;
    static integer nparx;
    static doublereal bwidy, unext, ff;
    static integer nl, ix, nx, ny;
    extern /* Subroutine */ int mnamin_();
    static doublereal contur[20];
    static char chzero[115];
    static integer ki1, ki2;
    extern /* Subroutine */ int mnhess_(), mnwerr_();
    static integer ixzero;
    static doublereal xb4;
    static integer iy;
    static doublereal ylabel;
    static integer nl2, ics;
    static doublereal fmn, fmx, xlo, ylo, xup, yup;

    /* Fortran I/O blocks */
    static cilist io___27 = { 0, 0, 0, "(A,I3,A,A)", 0 };
    static cilist io___29 = { 0, 0, 0, "(12X,A,A)", 0 };
    static cilist io___37 = { 0, 0, 0, "(1X,G12.4,1X,A)", 0 };
    static cilist io___38 = { 0, 0, 0, "(14X,A)", 0 };
    static cilist io___41 = { 0, 0, 0, "(8X,G12.4,A,G12.4)", 0 };
    static cilist io___42 = { 0, 0, 0, "(14X,A,G12.4)", 0 };
    static cilist io___43 = { 0, 0, 0, "(8X,G12.4,A,G12.4,A,G12.4)", 0 };
    static cilist io___44 = { 0, 0, 0, "(6X,A,I3,A,A,A,G12.4)", 0 };
    static cilist io___45 = { 0, 0, 0, "(A,G12.4,A,G12.4,A)", 0 };
    static cilist io___46 = { 0, 0, 0, fmt_1351, 0 };



/* $Id: d506dp.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506dp.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506dp.inc */

/* ************ DOUBLE PRECISION VERSION ************* */
/* C       to print function contours in two variables, on line printer */
/* C */

/* $Id: d506cm.inc,v 1.1.1.1 1996/03/07 14:31:32 mclareni Exp $ */

/* $Log: d506cm.inc,v $ */
/* Revision 1.1.1.1  1996/03/07 14:31:32  mclareni */
/* Minuit */




/* d506cm.inc */


/*                 input arguments: parx, pary, devs, ngrid */
    if (*ke1 <= 0 || *ke2 <= 0) {
	goto L1350;
    }
    if (*ke1 > mn7npr_1.nu || *ke2 > mn7npr_1.nu) {
	goto L1350;
    }
    ki1 = mn7inx_1.niofex[*ke1 - 1];
    ki2 = mn7inx_1.niofex[*ke2 - 1];
    if (ki1 <= 0 || ki2 <= 0) {
	goto L1350;
    }
    if (ki1 == ki2) {
	goto L1350;
    }

    if (mn7flg_1.isw[1] < 1) {
	mnhess_(fcn, futil);
	mnwerr_();
    }
    nparx = mn7npr_1.npar;
    xsav = mn7ext_1.u[*ke1 - 1];
    ysav = mn7ext_1.u[*ke2 - 1];
    devs = mn7arg_1.word7[2];
    if (devs <= 0.) {
	devs = (float)2.;
    }
    xlo = mn7ext_1.u[*ke1 - 1] - devs * mn7err_1.werr[ki1 - 1];
    xup = mn7ext_1.u[*ke1 - 1] + devs * mn7err_1.werr[ki1 - 1];
    ylo = mn7ext_1.u[*ke2 - 1] - devs * mn7err_1.werr[ki2 - 1];
    yup = mn7ext_1.u[*ke2 - 1] + devs * mn7err_1.werr[ki2 - 1];
    ngrid = (integer) mn7arg_1.word7[3];
    if (ngrid <= 0) {
	ngrid = 25;
/* Computing MIN */
	i__1 = mn7iou_1.npagwd - 15;
	nx = min(i__1,ngrid);
/* Computing MIN */
	i__1 = mn7iou_1.npagln - 7;
	ny = min(i__1,ngrid);
    } else {
	nx = ngrid;
	ny = ngrid;
    }
    if (nx < 11) {
	nx = 11;
    }
    if (ny < 11) {
	ny = 11;
    }
    if (nx >= 115) {
	nx = 114;
    }
/*         ask if parameter outside limits */
    if (mn7inx_1.nvarl[*ke1 - 1] > 1) {
	if (xlo < mn7ext_1.alim[*ke1 - 1]) {
	    xlo = mn7ext_1.alim[*ke1 - 1];
	}
	if (xup > mn7ext_1.blim[*ke1 - 1]) {
	    xup = mn7ext_1.blim[*ke1 - 1];
	}
    }
    if (mn7inx_1.nvarl[*ke2 - 1] > 1) {
	if (ylo < mn7ext_1.alim[*ke2 - 1]) {
	    ylo = mn7ext_1.alim[*ke2 - 1];
	}
	if (yup > mn7ext_1.blim[*ke2 - 1]) {
	    yup = mn7ext_1.blim[*ke2 - 1];
	}
    }
    bwidx = (xup - xlo) / (real) nx;
    bwidy = (yup - ylo) / (real) ny;
    ixmid = (integer) ((xsav - xlo) * (real) nx / (xup - xlo)) + 1;
    if (mn7min_1.amin == mn7cns_1.undefi) {
	mnamin_(fcn, futil);
    }
    for (i = 1; i <= 20; ++i) {
/* Computing 2nd power */
	r__1 = (real) (i - 1);
	contur[i - 1] = mn7min_1.amin + mn7min_1.up * (r__1 * r__1);
/* L185: */
    }
    contur[0] += mn7min_1.up * (float).01;
/*                fill FCNB to prepare first row, and find column zero */
    mn7ext_1.u[*ke2 - 1] = yup;
    ixzero = 0;
    xb4 = 1.;
    i__1 = nx + 1;
    for (ix = 1; ix <= i__1; ++ix) {
	mn7ext_1.u[*ke1 - 1] = xlo + (real) (ix - 1) * bwidx;
	(*fcn)(&nparx, mn7der_1.gin, &ff, mn7ext_1.u, &c__4, futil);
	fcnb[ix - 1] = ff;
	if (xb4 < 0. && mn7ext_1.u[*ke1 - 1] > 0.) {
	    ixzero = ix - 1;
	}
	xb4 = mn7ext_1.u[*ke1 - 1];
	*(unsigned char *)&chmid[ix - 1] = '*';
	*(unsigned char *)&chzero[ix - 1] = '-';
/* L200: */
    }
    io___27.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___27);
    do_fio(&c__1, " Y-AXIS: PARAMETER ", 19L);
    do_fio(&c__1, (char *)&(*ke2), (ftnlen)sizeof(integer));
    do_fio(&c__1, ": ", 2L);
    do_fio(&c__1, mn7nam_1.cpnam + (*ke2 - 1) * 10, 10L);
    e_wsfe();
    if (ixzero > 0) {
	*(unsigned char *)&chzero[ixzero - 1] = '+';
	s_copy(chln, " ", 115L, 1L);
	io___29.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___29);
	do_fio(&c__1, chln, ixzero);
	do_fio(&c__1, "X=0", 3L);
	e_wsfe();
    }
/*                 loop over rows */
    i__1 = ny;
    for (iy = 1; iy <= i__1; ++iy) {
	unext = mn7ext_1.u[*ke2 - 1] - bwidy;
/*                 prepare this line's background pattern for contour 
*/
	s_copy(chln, " ", 115L, 1L);
	*(unsigned char *)&chln[ixmid - 1] = '*';
	if (ixzero != 0) {
	    *(unsigned char *)&chln[ixzero - 1] = ':';
	}
	if (mn7ext_1.u[*ke2 - 1] > ysav && unext < ysav) {
	    s_copy(chln, chmid, 115L, 115L);
	}
	if (mn7ext_1.u[*ke2 - 1] > 0. && unext < 0.) {
	    s_copy(chln, chzero, 115L, 115L);
	}
	mn7ext_1.u[*ke2 - 1] = unext;
	ylabel = mn7ext_1.u[*ke2 - 1] + bwidy * (float).5;
/*                 move FCNB to FCNA and fill FCNB with next row */
	i__2 = nx + 1;
	for (ix = 1; ix <= i__2; ++ix) {
	    fcna[ix - 1] = fcnb[ix - 1];
	    mn7ext_1.u[*ke1 - 1] = xlo + (real) (ix - 1) * bwidx;
	    (*fcn)(&nparx, mn7der_1.gin, &ff, mn7ext_1.u, &c__4, futil);
	    fcnb[ix - 1] = ff;
/* L220: */
	}
/*                 look for contours crossing the FCNxy squares */
	i__2 = nx;
	for (ix = 1; ix <= i__2; ++ix) {
/* Computing MAX */
	    d__1 = fcna[ix - 1], d__2 = fcnb[ix - 1], d__1 = max(d__1,d__2), 
		    d__2 = fcna[ix], d__1 = max(d__1,d__2), d__2 = fcnb[ix];
	    fmx = max(d__1,d__2);
/* Computing MIN */
	    d__1 = fcna[ix - 1], d__2 = fcnb[ix - 1], d__1 = min(d__1,d__2), 
		    d__2 = fcna[ix], d__1 = min(d__1,d__2), d__2 = fcnb[ix];
	    fmn = min(d__1,d__2);
	    for (ics = 1; ics <= 20; ++ics) {
		if (contur[ics - 1] > fmn) {
		    goto L240;
		}
/* L230: */
	    }
	    goto L250;
L240:
	    if (contur[ics - 1] < fmx) {
		*(unsigned char *)&chln[ix - 1] = *(unsigned char *)&clabel[
			ics - 1];
	    }
L250:
	    ;
	}
/*                 print a row of the contour plot */
	io___37.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___37);
	do_fio(&c__1, (char *)&ylabel, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, chln, nx);
	e_wsfe();
/* L280: */
    }
/*                 contours printed, label x-axis */
    s_copy(chln, " ", 115L, 1L);
    *(unsigned char *)chln = 'I';
    *(unsigned char *)&chln[ixmid - 1] = 'I';
    *(unsigned char *)&chln[nx - 1] = 'I';
    io___38.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___38);
    do_fio(&c__1, chln, nx);
    e_wsfe();
/*                the hardest of all: print x-axis scale! */
    s_copy(chln, " ", 115L, 1L);
    if (nx <= 26) {
/* Computing MAX */
	i__1 = nx - 12;
	nl = max(i__1,2);
	nl2 = nl / 2;
	io___41.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___41);
	do_fio(&c__1, (char *)&xlo, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, chln, nl);
	do_fio(&c__1, (char *)&xup, (ftnlen)sizeof(doublereal));
	e_wsfe();
	io___42.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___42);
	do_fio(&c__1, chln, nl2);
	do_fio(&c__1, (char *)&xsav, (ftnlen)sizeof(doublereal));
	e_wsfe();
    } else {
/* Computing MAX */
	i__1 = nx - 24;
	nl = max(i__1,2) / 2;
	nl2 = nl;
	if (nl > 10) {
	    nl2 = nl - 6;
	}
	io___43.ciunit = mn7iou_1.isyswr;
	s_wsfe(&io___43);
	do_fio(&c__1, (char *)&xlo, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, chln, nl);
	do_fio(&c__1, (char *)&xsav, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, chln, nl2);
	do_fio(&c__1, (char *)&xup, (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    io___44.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___44);
    do_fio(&c__1, " X-AXIS: PARAMETER", 18L);
    do_fio(&c__1, (char *)&(*ke1), (ftnlen)sizeof(integer));
    do_fio(&c__1, ": ", 2L);
    do_fio(&c__1, mn7nam_1.cpnam + (*ke1 - 1) * 10, 10L);
    do_fio(&c__1, "  ONE COLUMN=", 13L);
    do_fio(&c__1, (char *)&bwidx, (ftnlen)sizeof(doublereal));
    e_wsfe();
    io___45.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___45);
    do_fio(&c__1, " FUNCTION VALUES: F(I)=", 23L);
    do_fio(&c__1, (char *)&mn7min_1.amin, (ftnlen)sizeof(doublereal));
    do_fio(&c__1, " +", 2L);
    do_fio(&c__1, (char *)&mn7min_1.up, (ftnlen)sizeof(doublereal));
    do_fio(&c__1, " *I**2", 6L);
    e_wsfe();
/*                 finished.  reset input values */
    mn7ext_1.u[*ke1 - 1] = xsav;
    mn7ext_1.u[*ke2 - 1] = ysav;
    *ierrf = 0;
    return 0;
L1350:
    io___46.ciunit = mn7iou_1.isyswr;
    s_wsfe(&io___46);
    e_wsfe();
    *ierrf = 1;
    return 0;
} /* mncntr_ */

